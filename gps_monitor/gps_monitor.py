"""
this script monitors the logs from gps device server
the gps server generates log for this script in mongodb
"""
from collections import defaultdict
from multiprocessing import Process
from datetime import timedelta, datetime
import traceback
from urllib.parse import urlparse

import sys
from geopy.distance import vincenty

from pymongo import MongoClient
from tracker import utils
from pyramid_mailer import Mailer
from configparser import ConfigParser
import time
import logging, logging.config
from tracker.utils.email_utils import send_text_mail
# from tracker.utils.sms_utils import send_sms

logger = logging.getLogger(__name__)
device_id_dict = {}
last_load_time = datetime.now()


def initialize(config_filename):
    config = ConfigParser()
    config.read(config_filename)
    settings = config["app:main"]

    logging.config.fileConfig(config_filename, disable_existing_loggers=False)

    db_url = urlparse(settings['mongo_uri'])
    utils.mongo_cli = MongoClient(db_url.hostname, db_url.port)
    if 'mongo_username' in settings:
        utils.mongo_cli.the_database.authenticate(settings['mongo_username'], settings['mongo_password'], source=settings['mongo_authdb'])

    utils.app_db = utils.mongo_cli[db_url.path[1:]]
    utils.gps_db = utils.mongo_cli['trackingdb']

    utils.env = settings['env']

    utils.mailer = Mailer.from_settings(settings)


def get_schools_info():
    logger.info('loading schools')
    global device_id_dict
    device_id_dict = defaultdict(list)
    all_schools = [x for x in utils.app_db['schools'].find()]
    for school in all_schools:
        item = {
            'schoolId': school['_id'],
            'name': school['name'],
            'lat': school['lat'],
            'lng': school['lng'],
            'alertPhones': school['alertPhones'],
            'geoFenceEnabled': school['geoFenceEnabled']
        }
        all_vehicles = [x for x in utils.app_db['vehicles'].find({'schoolId': school['_id']})]
        for vehicle in all_vehicles:
            device_id_dict[vehicle['deviceId']].append({
                'vehicle': vehicle,
                'school': item,
                'status': {'active': False, 'lastUpdated': datetime.now()}
            })
    global last_load_time
    last_load_time = datetime.now()


def latest_tracking_devices_status():
    cursor = utils.mongo_cli['trackingdb']['devices_status'].find().sort([('_id', -1)]).limit(1)
    recs = [x for x in cursor]
    cursor.close()
    if len(recs) > 0:
        return recs[0]


def verify():
    devices_status = latest_tracking_devices_status()
    count = sum([len(data_list) for data_list in device_id_dict.values()])
    item = {
        'totalCount': count,
        'activeCount': count,
        'inactiveDevices': [],
        'oddDevices': [],
        'time': datetime.now(),
        'msg': "OK"
    }
    if not devices_status or datetime.now() - devices_status['time'] > timedelta(0, 180):
        logger.critical({'status': "GPS SERVER DOWN"})
        alert_admin()
        item['activeCount'] = 0
        item['msg'] = "GPS SERVER IS DOWN"
    else:
        for device_id, data_list in device_id_dict.items():
            for data in data_list:
                data['status']['active'] = False
        for device in devices_status['devices']:
            for data in device_id_dict[device['deviceId']]:
                data['status'] = device
                data['status']['active'] = True

        check_geo_fencing()

        for device_id, data_list in device_id_dict.items():
            for data in data_list:
                if data['status']['active'] is False or data['status']['lastUpdated'] is None or datetime.now() - data['status']['lastUpdated'] > timedelta(0, 180):
                    diff = datetime.now() - data['status']['lastUpdated']
                    item['activeCount'] -= 1
                    item['inactiveDevices'].append({
                        'deviceId': device_id,
                        'school': data['school']['name'],
                        'inactiveDuration': "{} mins".format(int(diff.total_seconds() / 60)),
                        'registrationNo': data['vehicle']['registrationNo'],
                        'vehicleName': data['vehicle']['name']
                    })
                elif data['status']['interval'] > 12:
                    item['oddDevices'].append({
                        'deviceId': device_id,
                        'school': data['school']['name'],
                        'interval': data['status']['interval'],
                        'registrationNo': data['vehicle']['registrationNo'],
                        'vehicleName': data['vehicle']['name']
                    })

    utils.app_db['devices_status'].insert(item)
    logger.info(item)

    if datetime.now() - last_load_time > timedelta(0, 3600):
        get_schools_info()


def check_geo_fencing():
    for device_id, data_list in device_id_dict.items():
        for data in data_list:
            if data['status']['lastUpdated'] is None or data['status']['lastUpdated'].year < 2015:
                continue
            if data['school']['geoFenceEnabled'] is True and data['status']['active'] is True \
                    and data['status']['lat'] is not None and data['status']['lng'] is not None\
                    and data['status']['lat'] != 0 and data['status']['lng'] != 0:
                parked_status = parked_in_school(data)
                logger.debug({'current': parked_status, 'parked': data['vehicle']['status']['parked']})
                if parked_status != data['vehicle']['status']['parked']:
                    logger.info({'msg': "parked status changed", 'parked': parked_status,
                                 'vehicle': data['vehicle']['registrationNo'], 'school': data['school']['name']})
                    data['vehicle']['status']['parked'] = parked_status
                    # alert school admin
                    # alert_parking_status(data)
                    # update in db
                    # utils.app_db['vehicles'].update({'_id': data['vehicle']['_id']},
                    #                                 {'$set': {'status.parked': parked_status}})


# def alert_parking_status(data):
#     for phone_no in data['school']['alertPhones']:
#         status = "BUS ENTRY"
#         if not data['vehicle']['status']['parked']:
#             status = "BUS EXIT"
#         msg = status + ": " + data['vehicle']['name'] + " Reg no " + data['vehicle']['registrationNo'] + " at " + data['status']['lastUpdated'].strftime("%d %b %l:%M %p")
#         logger.info({'msg': "sending SMS", 'type': "parking alert", 'phone': phone_no, 'content': msg})
#         send_sms(phone_no, msg)


def parked_in_school(data):
    dist = vincenty((data['status']['lat'], data['status']['lng']), (data['school']['lat'], data['school']['lng'])).kilometers
    logger.debug({'deviceId': data['vehicle']['deviceId'], 'distance': dist})
    if dist > 0.2:
        return False
    return True


def alert_admin():
    email_ads = [
        "sukritsangwan@gmail.com",
        "manuchaudhary4543@gmail.com",
        "shaileshmamgain5@gmail.com",
        "support@edufits.com"
    ]
    sender = "EduFits team"

    p = Process(target=send_text_mail, args=("URGENT! GPS server down", email_ads, "check", sender))
    p.daemon = True
    p.start()
    # send_sms(["8800443925", "9873715477", "8826055223"], "URGENT! GPS server down")


if __name__ == "__main__":
    logger.info('initializing')
    initialize(sys.argv[1])
    get_schools_info()
    while True:
        try:
            verify()
        except:
            traceback.print_exc()
        time.sleep(30)
