define(["marionette", "toaster"], function(Marionette, toaster){
  var app = new Marionette.Application();

  var ModalRegion = Marionette.Region.extend({
    el : "#dialog-region",
    onShow : function() {
      console.log(this);
    }
  });

  app.addRegions({
    headerRegion : "#header-region",
    blackOverlay : "#black-overlay",
    breadCrumbRegion : "#breadcrumb-region",
    mainRegion : "#main-region",
    modal : "#modal-region",
    dashboardRegion : "#dashboard-region",
    fullScreen : "#full-screen-region",
    liveTrackingRegion : "#live-tracking-region"
  });

  var modalWrapper = Marionette.ItemView.extend({
    el: '#modal-container',
    template: false,
    ui : {
      closeButton : ".close-button"
    },
    triggers : {
      'click @ui.closeButton' : 'modal:close'
    }
  });

  app.modalWrapperView = new modalWrapper();
  app.modalWrapperView.render();

  app.modal.on("show", function(view, region, options){
    if(view.options.mediumSizeModal) {
      $("#modal-container").addClass("medium");
    }
    var headingText = view.$el.find(".field-group-header").html();
    $("#modal-container .header .heading-text").html("");
    $("#modal-container .header .heading-text").html(headingText);
    $("#modal-container").show();
    app.showBlackOverlay();
    this.listenTo(view, "modal:close", function() {
      app.hideBlackOverlay();
      region.empty();
    });
    this.listenTo(app.modalWrapperView, "modal:close", function() {
      app.hideBlackOverlay();
      region.empty();
    });
  });

  app.modal.on("before:empty", function(view, region, options){
    this.stopListening();
    $("#modal-container").removeClass("medium");
    $("#modal-container").hide();
  });

  var fullScreen = Marionette.ItemView.extend({
    el: '#black-overlay',
    template: false
  }); 
  app.fullScreenView = new fullScreen();
  app.fullScreenView.render();

  app.fullScreen.on("show", function(view, region, options) {
    region.$el.show();
    app.showBlackOverlay();
    this.listenTo(view, "fullScreen:close", function() {
      region.$el.hide();
      app.hideBlackOverlay();
      region.empty();
    });

    this.listenTo(app.fullScreenView, "fullScreen:close", function() {
      region.$el.hide();
      app.hideBlackOverlay();
      region.empty();
    });
  });

  app.mainRegion.on("before:empty", function() {
    if(!$("body").attr("class")) return;
    var bodyClasses = $("body").attr("class").split(/\s+/g);
    $.each(bodyClasses, function(index, className) {
      if(className.indexOf("custom-") != -1) {
        $("body").removeClass(className);
      }
    });
  });

  app.liveTrackingRegion.on("show", function() {
    $("#live-tracking-region").show();
  });

  app.liveTrackingRegion.on("before:empty", function() {
    $("#live-tracking-region").hide();
  });

  app.navigate = function(route,  options){
    options || (options = {});
    Backbone.history.navigate(route, options);
  };

  app.getCurrentRoute = function(){
    return Backbone.history.fragment
  };

  app.showForbiddenMessage = function() {
    alert("Sorry you are not authorized to do this shit.");
    window.history.back();
  };

  app.openMenu = function() {
    $("#header-region").addClass("open");
    $("#header .menu-bar").attr("invoke", "close");
  };

  app.closeMenu = function() {
    $("#header-region").addClass("hide");
    $("#header-region").removeClass("open");
    $("#header .menu-bar").attr("invoke", "open");
    setTimeout(function() {
      $("#header-region").removeClass("hide");
    }, 300);
  };

  app.getCountries = function() {
    return ["India", "US", "UK"];
  };

  app.handleAPIError = function(response) {
    console.log(response);
  };

  app.on("start", function(){
    overwriteRenderer();
    if(Backbone.history){
      require([], function () {
        app.firstTimeLoad = true;
        Backbone.history.start();
      });
    }
  });

  app.reqres.setHandler("toaster:show", function(){
      var msg  = arguments[0];
      var option = {};
      if(arguments[1]) {
        option = arguments[1];
      }
      toaster.showToaster(msg, option);
  });

  app.pageLoadingView = Marionette.ItemView.extend({
    template : "#page-loading-view"
  });

  app.showPageLoading = function() {
    // if(app.firstTimeLoad) {
    //   app.firstTimeLoad = false;
    //   return;
    // }
    var view = new app.pageLoadingView();
    app.mainRegion.show(view);
  };

  // Following in the initialize:after
  // We'll override the default Marionette.Renderer.render function
  // app.on('initialize:after', function() {
  //   overwriteRenderer();
  // });

  function overwriteRenderer() {
    // Simply use a closure to close over the current render function
    var render = Marionette.Renderer.render;

    // Then override it
    Marionette.Renderer.render = function (template, data){

      // Extend data to inject our translate helper    
      data = _.extend(data, {_t: i18n.t});

      // Extend data to inject our permission helper
      data = _.extend(data, {havePermission: app.havePermission});

      // And finally return the result of calling the original render function
      // With our injected helper
      return render(template, data);
    };
  }

  app.havePermission = function(permissionNames) {
    if(permissionNames.indexOf("superadmin") != -1) {
      var permissionArray = app.request("user:permissionArray");
      if( _.intersection(permissionNames, permissionArray).length > 0 ) return true;
      return false;
    } else {
      // simple school login
      return true;
    }
  }

  app.scrollTop = function() {
    $("body").animate({
      scrollTop : 0
    }, 400);
  }

  app.showBreadCrumb = function(data) {
    var html = "";
    _.each(data, function(item) {
      if(item.isMenuButton) {
        html += "<a href='#/' class='menu-breadcrumb'>Home</a><span class='arrow'>&#8227;</span>";
      } else if(!item.isMenu && item.link && item.link != "") {
        html += "<a href='#/" + item.link + "'>" + item.label + "</a><span class='arrow'>&#8227;</span>";
      } else if(item.isMenu) {
        // var links = app.request("header:entities");
        // var currentMenuItem;
        // _.each(links.models, function(model) {
        //   if(model.get("module") == item.label) {
        //     currentMenuItem = model;
        //   }
        // });
        // if(item.showLink) {
        //   html += "<a href='#/menu/" + currentMenuItem.get("module") + "'>" + currentMenuItem.get("nodeName") + "</a><span class='arrow'>&#8227;</span>";
        // } else {
        //   html += currentMenuItem.get("nodeName");
        // }
      } else {
        html += item.label;
      }
    });
    app.breadCrumbRegion.$el.html(html);
    app.breadCrumbRegion.$el.show();
    $("body").addClass("breadcrumb-active");
    app.breadCrumbRegion.$el.removeClass("sub-navigation-active");
  }

  app.clearBreadCrumb = function() {
    app.breadCrumbRegion.$el.html("");
    app.breadCrumbRegion.$el.hide();
    app.mainRegion.$el.addClass();
    $("body").removeClass("breadcrumb-active");
  }


  var xhrPool = [];

  $(document).ajaxSend(function(e, jqXHR, options){
    xhrPool.push(jqXHR);
  });

  $(document).ajaxComplete(function(e, jqXHR, options) {
    xhrPool = $.grep(xhrPool, function(x){return x!=jqXHR});
  });

  app.abortPendingAjax = function() {
    $.each(xhrPool, function(idx, jqXHR) {
      jqXHR.abort();
    });
  }

  app.executeIfPathActive = function(path, callback) {
    var pathArray = path.split(",");
    var run = false;
    _.each(pathArray, function(path) {
      if(Backbone.history.fragment.indexOf(path) != -1) {
        run = true;
      }
    });
    if(typeof callback == "function" && run) callback();
  }

  app.autocomplete = function(data) {
    var elm = $(data.element);
    elm.on("focus keyup", function() {
      //if(elm.val().length < 3) return;
      data.getResult(elm.val()).then(function(response) {
        elm.parent().find(".autocomplete-ul").remove();
        var html = "";
        _.each(response, function(item) {
          html += "<li>" + _.template(data.template)(item) + "</li>";
        });
        var width = elm.css("width");
        elm.after("<ul class='autocomplete-ul' style='width:" + width + "'>" + html + "</ul>");
        bindEvent();
      });
    });

    // elm.on("blur", function() {
    //   elm.parent().find(".autocomplete-ul").hide();
    // });

    function bindEvent() {
      $(document).off("click.outsideAutoComplete");
      $(elm).off("click.outsideAutoComplete");
      $(".autocomplete-ul li").off("click.outsideAutoComplete");
      $(elm).on("click.outsideAutoComplete", function() {
        event.stopPropagation();
      });
      $(".autocomplete-ul li").on("click.outsideAutoComplete", function() {
        //event.stopPropagation();
      });
      $(document).on("click.outsideAutoComplete", function() {
        var thisElm = $(event.target || event.srcElement);
        elm.parent().find(".autocomplete-ul").remove();
      });
    }
  }

  $(".user-options").on("click", function() {
    event.stopPropagation();
    if(!$(".user-menu-dropdown").hasClass("show")) {
      $(".user-menu-dropdown").show();
    } else {
      $(".user-menu-dropdown").hide();
    }
    setTimeout(function() {
      $(".user-menu-dropdown").toggleClass("show");
    }, 0);
  });

  $(document).on("click", function() {
    $(".user-menu-dropdown").hide();
    $(".user-menu-dropdown").removeClass("show");
  });

  $(document).keyup(function(e) {
       if (e.keyCode == 27) { // escape key maps to keycode `27`
          app.closeAllPopups();
      }
  });

  $(window).bind('hashchange', function() {
     app.closeAllPopups();
     app.liveTrackingRegion.empty();
  });

  app.blackOverlay.$el.on("click", function() {
    app.closeAllPopups();
  });

  app.showBlackOverlay = function() {
    app.blackOverlay.$el.addClass("show");
  };

  app.hideBlackOverlay = function() {
    app.blackOverlay.$el.removeClass("show");
  };


  app.closeAllPopups = function() {
    app.modalWrapperView.trigger("modal:close");
    app.fullScreenView.trigger("fullScreen:close");
  }

  app.getCurrentMonth = function() {
    return 4;
  };

  app.getCurrentYear = function() {
    return 201;
  };

  app.headerRegion.$el.find(".menu-item").on("click", function() {
    setTimeout(function() {
      app.closeMenu();
    }, 500);
  });

  app.form = {
    disableSubmit : function(formView) {
      formView.$el.find("input[type='submit']").attr("disabled","disabled");
      formView.$el.find("input[type='submit']").attr("originalText", formView.$el.find("input[type='submit']").attr("value"));
      formView.$el.find("input[type='submit']").attr("value", i18n.t("pleaseWait"));
    },
    enableSubmit : function(formView) {
      formView.$el.find("input[type='submit']").removeAttr("disabled");
      formView.$el.find("input[type='submit']").attr("value", formView.$el.find("input[type='submit']").attr("originalText"));
    }
  };

  app.helper = {
    pad : function(value, width, leadingItem) {
      leadingItem = leadingItem || '0';
      value = value + '';
      return value.length >= width ? value : new Array(width - value.length + 1).join(leadingItem) + value;
    }
  };

  app.general = {
    bloodGroupOptions : ["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]
  }

  app.defaults = {
    schoolLocation : {
      lat : 28,
      lng : 77
    }
  };

  app.activeHeader = function(name) {
    $("#header .menu-list a").removeClass("active");
    $("#header .menu-list a[data-menu='" + name + "']").addClass("active");
  };

  app.gmapKey = "AIzaSyCxaWgxhss4V7AYD7n54U0xrbffJj3oVqk";
  
  window.app = app;
  return app;
});
