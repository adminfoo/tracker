define(["app"], function(app){
  var retObj = {
    defaultOptions : {
      autoClose : true,
      autoCloseTimeout : 4000
    },
    toasterView : Marionette.ItemView.extend({
        el: '#toaster',
        template: false,
        ui : {
          message : ".message",
          close : ".close"
        },
        events: {
          'click @ui.close': 'closeToaster'
        },
        closeToaster : function() {
          this.ui.message.empty();
          this.$el.hide();
        },
        onRender: function(){
          // manipulate the `el` here. it's already
          // been rendered, and is full of the view's
          // HTML, ready to go.
          this.ui.message.html(retObj.options.message);
        }
    }),
    showToaster : function(message, options){
      if(retObj.closeTimeout) clearInterval(retObj.closeTimeout);
      retObj.options = {};
      retObj.options = _.extend(retObj.defaultOptions,options);
      retObj.options.message = message;
      var toaster = new retObj.toasterView(options).render();
      $("#toaster").show();
      if(retObj.options.autoClose) {
        retObj.closeTimeout = setTimeout(retObj.hideToaster, retObj.options.autoCloseTimeout);
      }
    },
    hideToaster : function() {
      $("#toaster .close").trigger("click");
    }
  };
  return retObj;
});
