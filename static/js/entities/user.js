define(["app"], function(app){
  app.module("Entities", function(Entities, app, Backbone, Marionette, $, _){
    Entities.currentUserModel = Backbone.Model.extend({
      initialize: function() {
        this.set('permissions', window.permissions);
        var permissionArray = [];
        _.each(window.permissions, function(permission) {
          permissionArray = permissionArray.concat(permission);
        });
        this.set("permissionArray", permissionArray);
      }

    });

    var initalizeSession = function(){
      Entities.currentUser = new Entities.currentUserModel();
    };

    var API = {
      getPermissions: function(){
        if(Entities.currentUser === undefined) {
          initalizeSession();
        }
        return Entities.currentUser.get('permissions');
      },
      getPermissionArray : function() {
        if(Entities.currentUser === undefined) {
          initalizeSession();
        }
        return Entities.currentUser.get('permissionArray');
      }
    };

    app.reqres.setHandler("user:permissions", function(){
      return API.getPermissions();
    });

    app.reqres.setHandler("user:permissionArray", function(){
      return API.getPermissionArray();
    });
  });

  return ;
});
