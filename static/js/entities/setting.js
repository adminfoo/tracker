define(["app"], function(app){
  var admissionModuleSettings = {};
  var feeModuleDiscountsList = {};
  var feeModuleStructuresList = {};

  var API = {
    getAdditionalFields : function() {
      var result = $.Deferred();
      loadAdmissionModuleSetting().then(function(response) {
        result.resolve(createDuplicate(admissionModuleSettings.response.additionalFields));
      });
      return result.promise();
    },

    getBatchList : function() {
      var result = $.Deferred();
      loadAdmissionModuleSetting().then(function(response) {
        result.resolve(createDuplicate(admissionModuleSettings.response.settings.coursesOpen));
      });
      return result.promise();
    },

    getCategoryList : function() {
      var result = $.Deferred();
      loadAdmissionModuleSetting().then(function(response) {
        result.resolve(createDuplicate(admissionModuleSettings.response.settings.categories));
      });
      return result.promise();
    },

    getAllCourses : function() {
      var result = $.Deferred();
      loadAdmissionModuleSetting().then(function(response) {
        result.resolve(createDuplicate(admissionModuleSettings.response.allCourses));
      });
      return result.promise();
    },

    getAdmissionSettings : function() {
      var result = $.Deferred();
      loadAdmissionModuleSetting().then(function(response) {
        result.resolve(createDuplicate(admissionModuleSettings.response));
      });
      return result.promise();
    },

    getFeeDiscountsList: function() {
      var result = $.Deferred();
      loadFeeDiscountsList().then(function(response) {
        result.resolve(createDuplicate(feeModuleDiscountsList.response));
      });
      return result.promise();
    },

    getFeeParticularsList: function() {
      var result = $.Deferred();
      loadFeeStructuresList().then(function(response) {
        particulars = []
        _.each(feeModuleStructuresList.response, function(structure) {
          _.each(structure.particulars, function(pa) {
            particulars.push(pa.name);
          });
        });
        result.resolve(createDuplicate(_.uniq(particulars)));
      });
      return result.promise();
    },

    getPermissionsList : function() {
      var result = $.Deferred();
      loadPermissionList().then(function(response) {
        result.resolve(createDuplicate(response));
      });
      return result.promise();
    },

    getDesignationsList : function() {
      var result = $.Deferred();
      loadDesignationList().then(function(response) {
        result.resolve(createDuplicate(response));
      });
      return result.promise();
    },

    getStudentCategoriesList: function() {
      var result = $.Deferred();
      loadStudentCategoryList().then(function(response) {
        result.resolve(createDuplicate(response));
      });
      return result.promise();
    },

    getFinanceCategoriesList: function() {
      var result = $.Deferred();
      loadFinanceCategoryList().then(function(response) {
        result.resolve(createDuplicate(response));
      });
      return result.promise();
    },

    getSchoolSummary: function() {
      var result = $.Deferred();
      loadSchoolSummary().then(function(response) {
        result.resolve(createDuplicate(response));
      });
      return result.promise();
    }
  };

  app.reqres.setHandler("admission:additionalFields", function(){
      return API.getAdditionalFields();
  });

  app.reqres.setHandler("admission:batchList", function(){
      return API.getBatchList();
  });

  app.reqres.setHandler("admission:categoryList", function(){
      return API.getCategoryList();
  });

  app.reqres.setHandler("admission:allCourses", function(){
      return API.getAllCourses();
  });

  app.reqres.setHandler("admission:admissionSettings", function(){
      return API.getAdmissionSettings();
  });

  app.reqres.setHandler("admission:refreshSettings", function() {
      return API.refreshSettings();
  });

  app.reqres.setHandler("fee:discountsList", function() {
      return API.getFeeDiscountsList();
  });

  app.reqres.setHandler("fee:particularsList", function() {
      return API.getFeeParticularsList();
  });

  app.reqres.setHandler("admin:permissionsList", function() {
      return API.getPermissionsList();
  });

  app.reqres.setHandler("hr:allDesignation", function() {
      return API.getDesignationsList();
  });

  app.reqres.setHandler("student:allCategory", function() {
      return API.getStudentCategoriesList();
  });

  app.reqres.setHandler("finance:allCategory", function() {
      return API.getFinanceCategoriesList();
  });

  app.reqres.setHandler("transport:routes", function() {
      return getTransportRoutes();
  });

  app.reqres.setHandler("transport:stops", function() {
      return getTransportStops();
  });

  app.reqres.setHandler("school:summary", function() {
      return API.getSchoolSummary();
  });

  app.reqres.setHandler("common:months", function(count) {
      return [
      {
        val : "1",
        label : "January"
      },
      {
        val : "2",
        label : "Februry"
      },
      {
        val : "3",
        label : "March"
      },
      {
        val : "4",
        label : "April"
      },
      {
        val : "5",
        label : "May"
      },
      {
        val : "6",
        label : "June"
      },
      {
        val : "7",
        label : "July"
      },
      {
        val : "8",
        label : "August"
      },
      {
        val : "9",
        label : "September"
      },
      {
        val : "10",
        label : "October"
      },
      {
        val : "11",
        label : "November"
      },
      {
        val : "12",
        label : "December"
      }];
  });

  function getTransportRoutes() {
    return $.ajax({
      url : "/transportRoutes"
    });
  }

  function getTransportStops() {
    return $.ajax({
      url : "/transportStops"
    });
  }

  // Setting loader functions
  function loadAdmissionModuleSetting() {
    return $.ajax({
      url: "/admissionSettings"
    }).then(function(response) {
      admissionModuleSettings.response = response;
      admissionModuleSettings.fetched = true;
    }, function(response) {
      app.handleAPIError(response);
    });
  }

  function loadFeeDiscountsList() {
    return $.ajax({
      url: "/feeDiscounts"
    }).then(function(response) {
      feeModuleDiscountsList.response = response;
    }, function(response) {
      app.handleAPIError(response);
    });
  }

  function loadFeeStructuresList() {
    return $.ajax({
      url: "/feeStructures"
    }).then(function(response) {
      feeModuleStructuresList.response = response;
    }, function(response) {
      app.handleAPIError(response);
    });
  }

  function createDuplicate(object1) {
    if(!_.isArray(object1)) {
      return $.extend(true, {}, object1);
    }
    return object1;
  }

  function loadPermissionList() {
    return $.ajax({
      url : "/permissions"
    });
  }

  function loadDesignationList() {
    return $.ajax({
      url : "/employeeDesignations"
    });
  }

  function loadStudentCategoryList() {
    return $.ajax({
      url : "/studentCategories"
    });
  }

  function loadFinanceCategoryList() {
    return $.ajax({
      url : "/financeCategories"
    });
  }

  function loadSchoolSummary() {
    return $.ajax({
      url : "/schoolSummary"
    });
  }
});
