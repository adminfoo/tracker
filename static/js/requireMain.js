requirejs.config({
  waitSeconds : 0,
  paths: {
    app : "app",
    vendor : "vendor",
    header : "modules/header/headerApp",
    entityUser : "entities/user",
    entityHeader : "entities/header",
    listController : "modules/header/list/listController",
    listView : "modules/header/list/listView",
    "backbone.picky": "vendor/backbone.picky",
    "backbone.syphon": "vendor/backbone.syphon",
    "jquery-ui": "vendor/jquery-ui",
    localstorage: "vendor/backbone.localstorage",
    spin: "vendor/spin",
    "spin.jquery": "vendor/spin.jquery",
    tpl: "vendor/underscore-tpl",
    text: "vendor/text",
    backbone: "vendor/backbone",
    jquery: "vendor/jquery",
    json2: "vendor/json2",
    json: 'vendor/require-plugin/json',
    marionette: "vendor/backbone.marionette",
    underscore: "vendor/underscore",
    backboneForm: "vendor/backbone.forms",
    backboneFormTemplate : "vendor/bootstrap3",
    backboneFormList : "vendor/list",
    backboneFormMessage : "vendor/helpers/backbone.form.message",
    prototype : "prototype",
    i18n : "vendor/i18next-1.10.1.min",
    timepicki : "vendor/timepicki",
    pikaday : "vendor/pikaday",
    chart : "vendor/chart.min",
    gmap : "gmap",
    'async': 'vendor/async',
    locationPicker : "vendor/locationpicker.jquery.min",
    mapHandler : "modules/common/mapHandler",
    moment : "vendor/moment",

    // Project specific files
    setting : "entities/setting",
    toaster : "entities/toaster",

    // Common model, views
    commonFormModel : "modules/common/formModel",
    commonView : "modules/common/view",
    additionalFieldModel: "modules/common/additionalFieldModel",
    subHeadingListTemplate : "modules/common/templates/subHeadingList.tpl",
    modulePageTemplate : "modules/common/templates/modulePageTemplate.tpl",

    // Library loader
    libaryLoader : "libaryLoader",

    // main module loader which loads other module's loader
    mainModuleLoader : "mainModuleLoader",

    
    passwordChangeController: "modules/basic/password_change/controller",
    passwordChangeView: "modules/basic/password_change/view",
    passwordChangeModel: "modules/basic/password_change/model",
    passwordChangeFormTemplate : "modules/basic/templates/passwordChangeForm.tpl",
    passwordChangePageTemplate : "modules/basic/templates/passwordChangePage.tpl",

    transportModuleLoader : "modules/transport/index",
    
    transportVehicle : "modules/transport/vehicle/controller",
    transportVehicleView : "modules/transport/vehicle/view",
    transportVehicleItemTemplate : "modules/transport/vehicle/templates/item.tpl",
    transportVehicleListTemplate : "modules/transport/vehicle/templates/list.tpl",
    transportVehiclePageTemplate : "modules/transport/vehicle/templates/page.tpl",
    transportVehicleSubItemFormTemplate : "modules/transport/vehicle/templates/subItemForm.tpl",

    transportStaff : "modules/transport/staff/controller",
    transportStaffView : "modules/transport/staff/view",
    transportStaffItemTemplate : "modules/transport/staff/templates/item.tpl",
    transportStaffListTemplate : "modules/transport/staff/templates/list.tpl",
    transportStaffPageTemplate : "modules/transport/staff/templates/page.tpl",
    transportStaffSubItemFormTemplate : "modules/transport/staff/templates/subItemForm.tpl",

    transportRoute : "modules/transport/routes/controller",
    transportRouteAdd : "modules/transport/routes/add/controller",
    transportRouteAddView : "modules/transport/routes/add/view",
    transportRoutesAddPageTemplate : "modules/transport/routes/add/templates/page.tpl",
    transportRoutesAddDriverVechicleForm : "modules/transport/routes/add/templates/driverAndVehicleForm.tpl",
    transportRoutesAddSummaryTemplate : "modules/transport/routes/add/templates/summary.tpl",
    transportRoutesAddVehicleInfoTemplate : "modules/transport/routes/add/templates/vehicleInfo.tpl",
    transportRoutesAddStopLocationMap : "modules/transport/routes/add/templates/map.tpl",
    transportRouteAddStopDetail : "modules/transport/routes/add/templates/detail.tpl",
    transportRouteMapLayoutTemplate : "modules/transport/routes/add/templates/mapLayout.tpl",
    transportRouteStopListInMapTemplate : "modules/transport/routes/add/templates/stopListInMap.tpl",

    transportRoutePassenger : "modules/transport/routes/passenger/controller",
    transportRoutePassengerView : "modules/transport/routes/passenger/view",
    transportRoutePassengerItemTemplate : "modules/transport/routes/passenger/templates/item.tpl",
    transportRoutePassengerListTemplate : "modules/transport/routes/passenger/templates/list.tpl",
    transportRoutePassengerPageTemplate : "modules/transport/routes/passenger/templates/page.tpl",
    transportRoutePassengerSubItemFormTemplate : "modules/transport/routes/passenger/templates/subItemForm.tpl",

    transportRouteList : "modules/transport/routes/list/controller",
    transportRouteListView : "modules/transport/routes/list/view",
    transportRouteItemTemplate : "modules/transport/routes/list/templates/item.tpl",
    transportRouteListTemplate : "modules/transport/routes/list/templates/list.tpl",
    transportRoutePageTemplate : "modules/transport/routes/list/templates/page.tpl",
    transportRouteSubListTemplate : "modules/transport/routes/list/templates/routeList.tpl",
    transportRouteSubItemForm : "modules/transport/routes/list/templates/subItemForm.tpl",

    transportViewsAdminPassenger : "modules/transport/views/admin_passenger/controller",
    transportViewsAdminPassengerView : "modules/transport/views/admin_passenger/view",
    transportViewsAdminPassengerForm : "modules/transport/views/admin_passenger/templates/detail.tpl",

    transportPendingTasksList : "modules/transport/pending_tasks/controller",
    transportPendingTasksListView : "modules/transport/pending_tasks/view",
    transportPendingTasksPageTemplate : "modules/transport/pending_tasks/templates/page.tpl",
    transportPendingTasksItemTemplate : "modules/transport/pending_tasks/templates/item.tpl",
    transportPendingTasksListTemplate : "modules/transport/pending_tasks/templates/list.tpl",

    transportVehicleShow : "modules/transport/vehicle/show/controller",
    transportVehicleShowView : "modules/transport/vehicle/show/view",
    transportVehicleShowPageTemplate : "modules/transport/vehicle/show/templates/page.tpl",
    transportVehicleShowTripListTemplate : "modules/transport/vehicle/show/templates/tripList.tpl",
    transportVehicleShowTripDetailTemplate : "modules/transport/vehicle/show/templates/tripDetail.tpl",

    transportReports : "modules/transport/reports/controller",
    transportReportsView : "modules/transport/reports/view",
    transportReportsPageTemplate : "modules/transport/reports/templates/page.tpl",
    transportReportsTripListTemplate : "modules/transport/reports/templates/tripList.tpl",
    transportReportsTripDetailTemplate : "modules/transport/reports/templates/tripDetail.tpl",
    transportReportsFilterFormTemplate : "modules/transport/reports/templates/filterForm.tpl",
    transportReportsSummaryTemplate : "modules/transport/reports/templates/summary.tpl",
    transportReportsDailyListTemplate : "modules/transport/reports/templates/dailyList.tpl",


    transportLive : "modules/transport/live/controller",
    transportLiveView : "modules/transport/live/view",
    transportLivePageTemplate : "modules/transport/live/templates/page.tpl",
    transportLiveItemTemplate : "modules/transport/live/templates/item.tpl",
    transportLiveListTemplate : "modules/transport/live/templates/list.tpl",
    transportLiveDetailTemplate : "modules/transport/live/templates/detail.tpl",

    adminSchoolList : "modules/admin/schools/controller",
    adminSchoolListView : "modules/admin/schools/view",
    adminSchoolPageTemplate : "modules/admin/schools/templates/page.tpl",
    adminSchoolItemTemplate : "modules/admin/schools/templates/item.tpl",
    adminSchoolListTemplate : "modules/admin/schools/templates/list.tpl",
    adminSchoolSubItemFormTemplate : "modules/admin/schools/templates/subItemForm.tpl",
    adminSchoolLocationMap : "modules/admin/schools/templates/map.tpl",

    adminDashboard : "modules/admin/dashboard/controller",
    adminDashboardView : "modules/admin/dashboard/view",
    adminDashboardAlertItemTemplate : "modules/admin/dashboard/templates/item.tpl",
    adminDashboardAlertListTemplate : "modules/admin/dashboard/templates/list.tpl",
    adminDashboardPageTemplate : "modules/admin/dashboard/templates/page.tpl",
    adminDashboardSummaryTemplate : "modules/admin/dashboard/templates/summary.tpl",

    transportETAReport : "modules/transport/routes/eta_report/controller",
    transportETAReportView : "modules/transport/routes/eta_report/view",
    transportETAReportPageTemplate : "modules/transport/routes/eta_report/templates/page.tpl",
    transportETAReportFormTemplate : "modules/transport/routes/eta_report/templates/form.tpl",
    transportETAReportStopListTemplate : "modules/transport/routes/eta_report/templates/stopList.tpl",
    transportETAReportMapTemplate : "modules/transport/routes/eta_report/templates/map.tpl",

    transportDataVisualize : "modules/transport/data_visualize/controller",
    transportDataVisualizeView : "modules/transport/data_visualize/view",
    transportDataVisualizePageTemplate : "modules/transport/data_visualize/templates/page.tpl",
    transportDataVisualizeDataTemplate : "modules/transport/data_visualize/templates/data.tpl",

    internalDashboard : "modules/transport/internal_dashboard/controller",
    internalDashboardView : "modules/transport/internal_dashboard/view",
    internalDashboardPageTemplate : "modules/transport/internal_dashboard/templates/page.tpl"
  },

  shim: {
    "backbone.picky": ["backbone"],
    "backbone.syphon": ["backbone"],
    "jquery-ui": ["jquery"],
    "i18n" : ["jquery"],
    localstorage: ["backbone"],
    "spin.jquery": ["spin", "jquery"],
    tpl: ["text"],
    underscore: {
      exports: "_"
    },
    backbone: {
      deps: ["jquery", "underscore", "json2"],
      exports: "Backbone"
    },
    marionette: {
      deps: ["backbone"],
      exports: "Marionette"
    },
    backboneForm : {
      deps : ["backbone"]
    },
    backboneFormTemplate : {
      deps : ["backboneForm"]
    },
    backboneFormList : {
      deps : ["backboneForm"]
    },
    backboneFormMessage : {
      deps : ["backboneForm"]
    },
    app : ["libaryLoader"],
    timepicki : ["jquery"],
    locationPicker : ["jquery"]
  }
});



if(!window.initialized) {
    window.initialized = true;
    requirejs.s.contexts._.realNameToUrl = requirejs.s.contexts._.nameToUrl;
    require(["json!cachebusters.json?hash=" + new Date()], function(hashMap) {
        window.fileHashMap = hashMap;
        requirejs.s.contexts._.nameToUrl = function() {
            var url = requirejs.s.contexts._.realNameToUrl.apply(this, arguments);
            if (window.fileHashMap[url.replace("/static/","")]) {
                return url + '?hash=' + window.fileHashMap[url.replace("/static/","")];
            }
            return url;
        };

        require(["vendor","prototype"], function(vendor) {
            var options = { 
              resGetPath: 'static/locales/__ns__-__lng__.json?v=' + new Date(),
              lng: 'en-US',
              shortcutFunction: 'defaultValue',
              debug: true,
              fallbackLng: false,
              load: 'current',
              sendMissing: false
            };
            i18n.init(options, function(err ,t) {
                if(!err) {
                    require(["app", "moment", "mainModuleLoader", "entityUser", "setting"], function(app, moment){
                        app.start();
                        window.moment = moment;
                    });
                }
            });
        });
    });
}