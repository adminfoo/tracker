<div class="driver-info i-b vam">
	<div class="label">Vehicle</div>
	<div class="value"><%= registrationNo %></div>
</div>
<div class="route-info i-b vam">
	<div class="label">Max speed</div>
	<div class="value"><%= maxSpeed %> km/h</div>
</div>
<div class="i-b vam">
	<div class="label">Overspeeding</div>
	<div class="route"><%= overspeedPercent %>% trips</div>
</div>