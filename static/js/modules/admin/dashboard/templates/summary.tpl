<div class="card-container">
	<div style="margin-bottom: 40px;">
		<div class="card vehicle-summary">
			<div class="header">
				Vehicle summary
			</div>
			<div class="content">
				<div class="stat">
					<div class="number">
						<%= vehicleCount %>
					</div>
					<div class="legend">
						total
					</div>
				</div>
				<div class="stat">
					<div class="number">
						<%= vehicleMovingCount %>
					</div>
					<div class="legend">
						moving
					</div>
				</div>
			</div>
		</div>
		<div class="card alert-summary">
			<div class="header">
				Overspeed summary
			</div>
			<div class="content">
				<div class="stat">
					<div class="number">
						<%= overspeedPercentToday %><span class="unit">%</span>
					</div>
					<div class="legend">
						Today
					</div>
				</div>
				<div class="stat">
					<div class="number">
						<%= overspeedPercentYesterday %><span class="unit">%</span>
					</div>
					<div class="legend">
						Yesterday
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		<div class="card route-summary">
			<div class="header">
				Distance summary
			</div>
			<div class="content">
				<div class="stat">
					<div class="number">
						<%= parseInt(vehicleRunToday,10) %><span class="unit">km</span>
					</div>
					<div class="legend">
						Today
					</div>
				</div>
				<div class="stat">
					<div class="number">
						<%= parseInt(vehicleRunYesterday,10) %><span class="unit">km</span>
					</div>
					<div class="legend">
						yesterday
					</div>
				</div>
			</div>
		</div>
		<div class="card passenger-summary">
			<div class="header">
				Vehicle idle summary
			</div>
			<div class="content">
				<div class="stat">
					<div class="number">
						<%= parseInt(vehicleAvgIdleTimeToday / 60) %><span class="unit">min</span>
					</div>
					<div class="legend">
						Today
					</div>
				</div>
				<div class="stat">
					<div class="number">
						<%= parseInt(vehicleAvgIdleTimeYesterday / 60) %><span class="unit">min</span>
					</div>
					<div class="legend">
						yesterday
					</div>
				</div>
			</div>
		</div>
	</div>
</div>