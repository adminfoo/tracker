define(["app", "tpl!adminDashboardAlertItemTemplate", "tpl!adminDashboardAlertListTemplate", "tpl!adminDashboardPageTemplate", "tpl!adminDashboardSummaryTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate, summaryView){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        className : "alert-card",
        triggers : {
          "click .edit" : "edit:school",
          "click .add-marker" : "addMarker:school",
          "click .delete" : "delete:school"
        },
        templateHelpers : {
          color : function(severity) {
            if (severity <= 1) {
              return "#FF7600";
            }
            if (severity <= 3) {
              return "#E84747";
            }
            if (severity <= 5) {
              return "#FF0000";
            }
          }
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.listEmptyView;
        },
        template : listTemplate,
        childViewContainer: ".alert-list"
    }),
    listEmptyView : Marionette.ItemView.extend({
      template: _.template('<div colspan="4" class="empty-collection"><%= _t("noAlerts") %></div>'),
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .create-new-school" : "new:school"
      },
      regions: {
        listContainer: ".alert-container",
        summaryContainer : ".summary-container"
      },
      onShow : function() {
        $("body").addClass("custom-grey-bg");
      }
    }),
    summaryView : Marionette.ItemView.extend({
      template : summaryView
    })
  };
  return retObj;
});
