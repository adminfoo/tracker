define(["app", "adminDashboardView", "tpl!adminDashboardSummaryTemplate"], function(app, View, summaryTemplate){
  var retObj = {
    Model : Backbone.Model.extend({
       urlRoot : "/schoolSummary",
       parse : function(response) {
          // @TODO - remove this when real api is done
//          response.vehicleMovingCount = 2;
//          response.vehicleUnreachableCount = 1;
//          response.vehicleRunToday = 30;
//          response.vehicleRunYesterday = 29;
//          response.vehicleAvgIdleTimeToday = 239;
//          response.vehicleAvgIdleTimeYesterday = 238;
          return response;
       }
    }),
    showDashboard : function() {
      retObj.pageLayout = new View.pageView;
      retObj.model = new retObj.Model();
      retObj.model.fetch({
        success : function(model, response, xhr) {
          retObj.renderDashboard();
          retObj.model.on("change", function() {
            retObj.renderDashboard();
          });
          retObj.refreshInterval = setInterval(function() {
            retObj.model.fetch();
          }, 60000);
        },
        error : function() {

        }
      });
    },
    renderDashboard : function() {
      var SummaryView = Marionette.ItemView.extend({
        template : summaryTemplate,
        model : retObj.model
      });
      var summaryView = new SummaryView();
      app.mainRegion.show(retObj.pageLayout);
      retObj.pageLayout.summaryContainer.show(summaryView);
      var item = retObj.Model;
      var itemsCollection = Backbone.Collection.extend({
        //url : "/alerts",
        model : item
      });
      var items = new itemsCollection(retObj.model.get("overspeedList"));
      var view = new View.items({
        collection : items
      });
      retObj.pageLayout.on("destroy", function() {
        clearInterval(retObj.refreshInterval);
      });
      retObj.pageLayout.listContainer.show(view);
    }
  };

  var internalObj = {
    getDashboardSummary : function() {
      return app.reqres.request("school:summary");
    }
  };

  return retObj;
});
