<form action="">
<div data-fields="userType"></div>
<div data-fields="username"></div>
<div data-fields="firstName"></div>
<div class="filter-form-actions">
	<input type="submit" value="<%= _t('apply')%>" class="btn-primary small" style="margin-right: 15px;"></input>
	<input type="reset" value="<%= _t('formResetButtonText')%>" class="btn-secondary small"></input>
</div>
</form>