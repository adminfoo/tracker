<td>
    <%= id %>
</td>
<td>
    <%= username %>
</td>
<td>
    <%= userType %>
</td>
<td>
    <%= firstName %>
</td>
<td>
    <%= lastName %>
</td>
<td class="action-button-container">
    <a href="/#/admin/users/<%= firstName %>/<%= id %>/permission" class="view"><%= _t("editPermission")%></a>
</td>
