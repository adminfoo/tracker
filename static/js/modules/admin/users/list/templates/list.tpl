<table class="list-table">
        <thead>        
            <tr class="head">
                <th><%= _t("userId") %></th>
                <th><%= _t("username") %></th>
                <th><%= _t("userType") %></th>
                <th><%= _t("firstName") %></th>
                <th><%= _t("lastName") %></th>
                <th class="actions-th" style="text-align: center;"><%= _t("actions") %></th>
            </tr>
        </thead>
        <tbody></tbody>
</table>