<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageUserPermission") %>
	</div>
</div>
<div class="filter-panel">
	<div class="head">
		<%= _t("filter") %>
	</div>
	<div class="filter-form" style="padding: 0 10px; margin-top: 10px;"></div>
</div>
<div class="user-list-container table-container">
	<table class="list-table">
	        <thead>        
	            <tr class="head">
	                <th><%= _t("userId") %></th>
	                <th><%= _t("username") %></th>
	                <th><%= _t("userType") %></th>
	                <th><%= _t("firstName") %></th>
	                <th><%= _t("lastName") %></th>
	                <th class="actions-th" style="text-align: center;"><%= _t("actions") %></th>
	            </tr>
	        </thead>
	        <tbody>
	        	<tr><td colspan="6" style="text-align: center;"><%= _t("loading") %>...</td></tr>
	        </tbody>
	</table>
</div>
<div class="clear"></div>