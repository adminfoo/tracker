define(["app", "tpl!userListFilterTemplate", "tpl!userListTemplate", "tpl!userListItemTemplate", "tpl!userListPageTemplate"],
  function(app, listFilterTemplate, listTemplate, itemTemplate, pageTemplate){
  var retObj = {

    filterPanel : listFilterTemplate,

    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .edit" : "edit:user"
        }
    }),
    listEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="6" class="empty-collection"><%= _t("noUserFound") %></td>'),
      tagName : "tr"
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView : function() {
          return retObj.listEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .new" : "new:user"
      },
      regions: {
        filterForm: ".filter-form",
        listContainer: ".user-list-container"
      }
    }),
    //subItemForm : subItemForm
  };
  return retObj;
});
