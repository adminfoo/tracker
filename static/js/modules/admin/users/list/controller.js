define(["app", "userSearchView"], function(app, View){
  var retObj = {
    renderListing : function(criterionURL) {
      var criterion = internalObj.criterionURLToObject(criterionURL);

      retObj.pageLayout = new View.pageView;
      retObj.showFilterPanel(criterion);

      var item = Backbone.Model.extend({
        urlRoot : "/users",
      });

      var itemsCollection = Backbone.Collection.extend({
        url : "/users?" + internalObj.criterionObjectToURL(criterion) + "&keys=id,username,userType,permissions,firstName,lastName",
        model : item
      });

      var items = new itemsCollection();

      items.fetch({
        success : function(collection, response, options) {
          _.each(collection.models, function(val) {
            val.attributes.userType = i18n.t(val.attributes.userType);
          });
          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:edit:user", function() {
            var model = arguments[1].model;
            // TODO: make an edit view
            //internalObj.showEditSubItem(model, false);
          });
          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
        }
      });
    },
    showFilterPanel : function(criterion) {
      var filterForm = Backbone.Model.extend({
          schema: {
              username:  {title: i18n.t('username'), type: 'Text'},
              userType:  {title: i18n.t('userType'), type: 'Select', options: [{val: -1, label: i18n.t('any')}, {val: "employee", label: i18n.t('employee')}, {val: "student", label: i18n.t('student')}]},
              firstName: {title: i18n.t('firstName'), type: 'Text'},
              lastName: {title: i18n.t('lastName'), type: 'Text'}
          }
      });

      var filterFormModel = new filterForm(criterion.filter);

      var filterFormView = new Backbone.Form({
        model: filterFormModel,
        template : View.filterPanel,
        idPrefix : "user-filter-form-",
        events : {
          'submit': function(event) {
            this.trigger("form:submit", event);
          },
          'reset' : function(event) {
            this.trigger("form:reset", event);
          }
        }
      });

      filterFormView.on('form:submit', function(event) {
        event.preventDefault();
        var error = filterFormView.validate();
        if(error) return;
        filterFormView.commit();
        var criterion = {
          filter : filterFormView.model.attributes
        }
        if (criterion.filter.userType == '-1'){
          delete criterion.filter.userType;
        }
        var newHash = "#/admin/user/search/criterion:" + internalObj.criterionObjectToURL(criterion);
        window.location.hash = newHash;
      });

      filterFormView.on('form:reset', function(event) {
        event.preventDefault();
        var newHash = "#/admin/user/search";
        window.location.hash = newHash;
      });

      app.mainRegion.show(retObj.pageLayout);
      retObj.pageLayout.filterForm.show(filterFormView);
    }
  };

  var internalObj = {
    defaultCriterion : {
      sort : "id",
      skip : 0,
      limit : 20,
      filter : {}
    },

    criterionURLToObject : function(url) {
      var criterion = {
        filter : {}
      };
      var filterKeysArray = [];
      var filterKeysValuesArray = [];
      if(url == null) return criterion;
      url = url.split("&");
      _.each(url, function(value) {
        value = value.split("=");
          if(value[0] == "sort") {
            criterion.sort = value[1];
        }
        if(value[0] == "skip") {
          criterion.skip = value[1];
        }
        if(value[0] == "limit") {
          criterion.limit = value[1];
        }
        if(value[0] == "filterKeys") {
          filterKeysArray = value[1].split(",");
        }
        if(value[0] == "filterValues") {
          filterKeysValuesArray = value[1].split(",");
        }
      });
      if(filterKeysArray.length == filterKeysValuesArray.length) {
        criterion.filter = _.extend(criterion.filter, _.object(filterKeysArray, filterKeysValuesArray));
      }
      return criterion;
    },

    criterionObjectToURL : function(criterion) {
      var url = "";
      if(criterion.sort) {
        url += "sort=" + criterion.sort + "&";
      }
      if(criterion.skip) {
        url += "skip=" + criterion.skip + "&";
      }
      if(criterion.limit) {
        url += "limit=" + criterion.limit + "&";
      }
      if(!_.isEmpty(criterion.filter)) {
        criterion.filter = _.omit(criterion.filter, function(value, key, object) {
          return (!value || value == "");
        });
        var filterKeys = _.keys(criterion.filter).join(",");
        var filterValues = _.values(criterion.filter).join(",");
        url += "filterKeys=" + filterKeys + "&filterValues=" + filterValues;
      }
      return url;
    }
  };

  return retObj;

});
