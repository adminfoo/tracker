define(["app", "tpl!adminUsersEditPermissionsFormTemplate",], function(app, form){
  var retObj = {
    form : Marionette.ItemView.extend({
      template: form,
      triggers : {
      	"click .save" : "permissions:save",
      	"click .select-all" : {
      		event : "selectall",
      		preventDefault: false
      	},
      	"click .module-permission-item" : {
      		event : "singlePermission:change",
      		preventDefault : false
      	}
      }
    })
  };
  return retObj;
});
