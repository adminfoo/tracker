<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageUserPermission") %>
	</div>
</div>
<div style="margin-bottom: 30px;">
<table class="nopadding">
	<tbody>
		<tr>
			<td><b><%= _t("username") %></b></td>
			<td><b>:</b></td>
			<td><%= data.username %></td>
		</tr>
		<tr>
			<td><b><%= _t("userType") %></b></td>
			<td><b>:</b></td>
			<td><%= _t(data.userType) %></td>
		</tr>
		<tr>
			<td><b><%= _t("name") %></b></td>
			<td><b>:</b></td>
			<td><%= data.firstName + " " + data.lastName %></td>
		</tr>
	</tbody>
</table>
</div>
<div class="permission-manage-table table-container">
	<% _.each(permissionsList, function(permission, key) { %>
		<table class="list-table" style="margin-bottom: 40px;">
	        <thead>        
	            <tr class="head d">
	            	<th>
		                <input type="checkbox" class="select-all" data-module="<%= key %>" id="select-all-<%= key %>" style="font-size: 16px;">
		                <label for="select-all-<%= key %>"><%= permission.label %></label>
	                </th>
	            </tr>
	        </thead>
	        <tbody class="module-permission-list" data-module="<%= key %>">
	        	<% _.each(permission.permissions, function(permissionItem) { %>
	        		<tr>
		        		<td>
		        			<% if(checkPermission(permissionItem.name)) { %>
		        				<input name="permissions" type="checkbox" id="select<%= permissionItem.name %>" value="<%= permissionItem.name %>" class="module-permission-item" checked>
		        			<% } else { %>
		        				<input name="permissions" type="checkbox" id="select<%= permissionItem.name %>" value="<%= permissionItem.name %>" class="module-permission-item">
		        			<% } %>
			                <label for="select<%= permissionItem.name %>"><%= permissionItem.label %> - 
			                <i class="description"><%= permissionItem.description %></i></label>
		        		</td>
		        	</tr>
	        	<% }) %>
	        </tbody>
		</table>
	<% }) %>
</div>
<div>
	<input type="submit" class="btn btn-primary save" value="<%= _t('save') %>"/>
</div>