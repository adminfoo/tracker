define(["app","adminUsersEditPermissionsView"], function(app, View){
  var retObj = {
    managePersmissions : function(userId) {
      var userPromise = $.Deferred();
      var permissionListPromise = $.Deferred();
      var Model = Backbone.Model.extend({
        urlRoot : "/users"
      });
      retObj.user = new Model({
        id : userId
      });
      retObj.user.fetch({
        success : function(model, response, options) {
          userPromise.resolve(model);
        }
      });
      app.reqres.request("admin:permissionsList").then(function(permissionsList){
        retObj.permissionsList = permissionsList;
        permissionListPromise.resolve(permissionsList);
      });
      $.when(userPromise, permissionListPromise).done(function() {
        var view = new View.form({
          model : retObj.user,
          templateHelpers : function(){
            return {
              data: retObj.user.attributes,
              permissionsList : retObj.permissionsList,
              checkPermission : function(permission) {
                if(this.permissions.indexOf(permission) == -1) return false;
                return true;
              }
            };
          }
        });
        view.on("permissions:save", function() {
          app.form.disableSubmit(view);
          var permissions = [];
          $("input[name='permissions']:checked").each(function(index, elm){
            permissions.push($(elm).val())
          });
          retObj.user.save({ permissions : permissions },{
            patch : true,
            success : function(model, response, xhr) {
              app.form.enableSubmit(view);
              app.reqres.request("toaster:show", i18n.t("editPermissionSuccessMsg"));
              app.scrollTop();
            },
            error : function(model, response, xhr) {
            app.form.enableSubmit(view);
            console.log("error");
            }
          });
        });

        view.on("selectall", function() {
          var elm = $(event.target || event.srcElement);
          var checkAll = elm.is(':checked');
          var moduleId = $(elm).attr("data-module");
          if(checkAll) {
            $(".module-permission-list[data-module='" + moduleId + "'] input").each(function(index, element) {
              $(element).prop('checked', true);
            });
          } else {
            $(".module-permission-list[data-module='" + moduleId + "'] input").each(function(index, element) {
              $(element).prop('checked', false);
            });
          }
        });

        view.on("singlePermission:change", function() {
          var elm = $(event.target || event.srcElement);
          var moduleId = $(elm).parents(".module-permission-list").attr("data-module");
          internalObj.checkIfAllChecked(moduleId);
        });

        app.mainRegion.show(view);

        $(".module-permission-item").each(function(index, element) {
          var moduleId = $(element).parents(".module-permission-list").attr("data-module");
          internalObj.checkIfAllChecked(moduleId);
        });
        
      });
    }
  };

  var internalObj = {
    checkIfAllChecked : function(moduleId) {
      var allChecked = true;
      $(".module-permission-list[data-module='" + moduleId + "'] input").each(function(index, element) {
        if(!$(element).is(':checked')) allChecked = false;
      });
      if(allChecked) {
        $(".select-all[data-module='" + moduleId + "']").prop('checked', true);
      } else {
        $(".select-all[data-module='" + moduleId + "']").prop('checked', false);
      }
    }
  };

  return retObj;
});
