define(["app", "tpl!adminSchoolItemTemplate", "tpl!adminSchoolListTemplate", "tpl!adminSchoolPageTemplate", "tpl!adminSchoolSubItemFormTemplate", "tpl!adminSchoolLocationMap"],
       function(app, itemTemplate, listTemplate, pageTemplate, subItemForm, addLocationMapTemplate){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .edit" : "edit:school",
          "click .add-marker" : "addMarker:school",
          "click .delete" : "delete:school"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.schoolListEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    schoolListEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="4" class="empty-collection"><%= _t("noSchool") %></td>'),
      tagName : "tr"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .create-new-school" : "new:school"
      },
      regions: {
        listContainer: ".school-container"
      }
    }),
    mapView : Marionette.ItemView.extend({
      template : addLocationMapTemplate,
      triggers : {
        "click .close-btn" : "close:add-marker",
        "click .submit-btn" : "save:location"
      }
    }),
    subItemForm : subItemForm
  };
  return retObj;
});
