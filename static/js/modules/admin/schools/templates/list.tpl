<table class="list-table">
    <thead>        
        <tr class="head">
            <th><%= _t("name") %></th>
            <th><%= _t("username") %></th>
            <th><%= _t("email") %></th>
            <th><%= _t("phone") %></th>
            <th><%= _t("city") %></th>
            <th class="tac"><%= _t("actions") %></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>