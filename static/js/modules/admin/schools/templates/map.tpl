<div class="add-marker-map-container map-container">
	<div class="pindrop-label">
		<%= _t("pinStopLocationOnMap") %>
		<div class="i-b close-btn">
			<i class="fa fa-close"></i>
		</div>
		<div class="clear"></div>
	</div>
	<input class="form-control location-input" type="text" placeholder="Search locality" autocomplete="off">
	<div style="height: 515px; margin-bottom: 30px;">
		<div class="location-picker" style="height:100%;">
		</div>
	</div>
	<input type="hidden" id="lat-value" />
	<input type="hidden" id="lng-value" />
	<input type="submit" class="btn btn-primary submit-btn" value="<%= _t("save") %>"/>&nbsp;&nbsp;(save school location)
</div>