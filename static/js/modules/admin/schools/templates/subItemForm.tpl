<form>
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="name,email,username,phone,city,country,subscriptionFee,geoFenceEnabled,alertPhones"></div>
	</div>
	<div class="field-group">
		<input type="submit" class="btn btn-primary submit align-with-form" value="<%= submitButtonText %>"/>
	</div>
</form>