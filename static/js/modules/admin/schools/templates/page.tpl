<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageSchool") %>
	</div>
</div>
<div class="btn btn-primary create-new-school new-cta"> <i class="fa fa-plus"></i>
		<%= _t("addNew") %>
	</div>
<div class="school-container table-container">
</div>