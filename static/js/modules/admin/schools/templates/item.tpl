<td><%= name %></td>
<td><%= username %></td>
<td><%= email %></td>
<td><%= phone %></td>
<td><%= city %></td>
<td class="action-button-container">
	<a class="edit" data-index="<%= id %>"><%= _t('edit') %></a>
	 | 
	<a class="add-marker" data-index="<%= id %>"><%= _t('pinOnMap') %></a>
	 | 
	<a class="delete" data-index="<%= id %>"><%= _t('delete') %></a>
</td>

