define(["app", "adminSchoolListView", "gmap"], function(app, View){
  var retObj = {
    staffModel : Backbone.Model.extend({
        urlRoot : "/schools"
    }),
    list : function() {
      var item = retObj.staffModel;
      var itemsCollection = Backbone.Collection.extend({
        url : "/schools",
        model : item
      });

      var items = new itemsCollection();

      items.fetch({
        success : function(collection, response, options) {
          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:edit:school", function() {
            var model = arguments[1].model;
            internalObj.showEditSubItem(model, false);
          });

          retObj.view.on("childview:addMarker:school", function() {
            var model = arguments[1].model;
            var view = new View.mapView({
              model : model,
              originalModel : model
            });
            view.on("show", function(view) {
              view.$el.find(".location-picker").locationpicker({
                location: {
                  latitude: view.model.get("lat"),
                  longitude: view.model.get("lng")
                }, 
                radius: 50,
                inputBinding: {
                  latitudeInput: view.$el.find("#lat-value"),
                  longitudeInput: view.$el.find("#lng-value"),
                  locationNameInput: view.$el.find(".location-input")
                },
                enableAutocomplete: true
              });
            });

            view.on("close:add-marker", function() {
              view.trigger("fullScreen:close");
            });

            view.on("save:location", function() {
              view.model.save({
                  lat : $("#lat-value").val(),
                  lng : $("#lng-value").val()
                },{
                patch: true,
                success : function() {
                  app.reqres.request("toaster:show", i18n.t("schoolLocationUpdatedSuccessMsg"));
                },
                error : function() {
                  app.reqres.request("toaster:show", i18n.t("schoolLocationUpdatedErrorMsg"));
                }
              })
              view.trigger("fullScreen:close");
            });

            app.fullScreen.show(view);
          });

          retObj.view.on("childview:delete:school", function() {
            var model = arguments[1].model;
            model.destroy({
              wait : true,
              success : function() {
                app.reqres.request("toaster:show", i18n.t("schoolDeleteSuccessMsg"));
              },
              error : function() {
                if(arguments[1].status == 409) {
                  app.reqres.request("toaster:show", i18n.t("schoolDeleteConflictMsg"));
                } else {
                  app.reqres.request("toaster:show", i18n.t("schoolDeleteErrorMsg"));
                }
              }
            });
          });

          retObj.pageLayout = new View.pageView;

          retObj.pageLayout.on("new:school", function() {
            var Model = retObj.staffModel;
            var model = new Model({
              name : "",
              phone : "",
              email : "",
              lat : app.defaults.schoolLocation.lat,
              lng : app.defaults.schoolLocation.lng,
              username : "",
              city : "",
              country : "",
              geoFenceEnabled: "",
              adminPhones: []
            });
            internalObj.showEditSubItem(model, true);
          });

          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
        }
      });
    }
  };

  var internalObj = {
    showEditSubItem : function(model, isNew) {
      var formModel = Backbone.Model.extend({
        schema : {
          name : { title : i18n.t('name') + "*", validators : ['required']},
          email : { title : i18n.t('email') + "*", validators : ['required']},
          username : { title : i18n.t('username') + "*", validators : ['required']},
          city : { title : i18n.t('city') + "*", validators : ['required']},
          country : { title : i18n.t('country') + "*", validators : ['required']},
          phone : { title : i18n.t('phoneNo') + "*", validators : ['required']},
          subscriptionFee : { title : i18n.t('subscriptionFee')+ "*", validators : ["required"]},
          geoFenceEnabled: { title: i18n.t('geoFenceEnabled'), type: "Checkbox"},
          alertPhones: { title: i18n.t('alertPhones'), type: "List"}
        }
      });

      var form = new formModel({
        name : model.get("name"),
        phone : model.get("phone"),
        email : model.get("email"),
        username : model.get("username"),
        city : model.get("city"),
        country : model.get("country"),
        subscriptionFee : model.get("subscriptionFee"),
        geoFenceEnabled : model.get("geoFenceEnabled"),
        alertPhones : model.get("alertPhones"),
      });

      var formView = new Backbone.Form({
        template : View.subItemForm,
        model : form,
        idPrefix : "school-form-",
        templateData : {
          isNew : isNew,
          fieldFormHeaderText : isNew ? i18n.t("addNew") : i18n.t("Edit"),
          submitButtonText : isNew ? i18n.t("save") : i18n.t("update"),
          successMsg : isNew ? i18n.t("addSuccessMsg") : i18n.t("editSuccessMsg"),
          errorMsg : isNew ? i18n.t("addErrorMsg") : i18n.t("editErrorMsg")
        },
        originalModel : model,
        isNew : isNew
      });

      formView.on("submit", function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate();
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        _.extend(formView.options.originalModel.attributes, formView.model.toJSON());
        formView.options.originalModel.save(formView.options.originalModel.attributes, {
          patch : true,
          success : function(model, response, xhr) {
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", formView.templateData.successMsg);
            formView.trigger("modal:close");
            if(formView.options.isNew) {
              retObj.view.collection.add(model);
            }
            retObj.view.render();
          },
          error : function() {
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", formView.templateData.errorMsg);
            formView.trigger("modal:close");
          }
        });
      });
      app.modal.show(formView);
    }
  };

  return retObj;
});
