define(["app"], function(app){
  var router = Marionette.AppRouter.extend({
    appRoutes: {
      "admin/user/search(/criterion::criterionURL)" : "searchUser",
      "admin/users/*name/*userId/permission" : "editPermission"
    }
  });
  var API = {
    searchUser : function(criterionURL) {
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "admin" //e.g hr, student, admin
      },
      {
        label : i18n.t("manageUserPermission")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_user"])) {
        require(["userSearch"], function(userSearch) {
          app.closeMenu();
          userSearch.renderListing(criterionURL);
        });
      } else {
        app.showForbiddenMessage();
      }
    },

    editPermission : function(name, userId) {
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "admin" //e.g hr, student, admin
      },
      {
        label : i18n.t("manageUserPermission"),
        link : "admin/user/search"
      },
      {
        label : name,
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_user"])) {
        require(["adminUsersEditPermissions"], function(adminUsersEditPermissions) {
          app.closeMenu();
          adminUsersEditPermissions.managePersmissions(userId);
        });
      } else {
        app.showForbiddenMessage();
      }
    }
  };

  new router({
    controller: API
  });
});
