define(["app", "tpl!passwordChangeFormTemplate", "tpl!passwordChangePageTemplate"], function(app, passwordChangeFormTemplate, passwordChangePageTemplate){
  var retObj = {
    passwordForm : function(data) {
      var formView = new Backbone.Form({
        template: passwordChangeFormTemplate,
        model: data.passwordChangeModel,
        idPrefix : "password-form-",
        templateData : data.templateData || {}
      });
      return formView;
    },
    pageView : Marionette.LayoutView.extend({
      template: passwordChangePageTemplate,

      regions: {
        passwordFormContainer: ".password-form-container"
      }
    })
  };
  return retObj;

});
