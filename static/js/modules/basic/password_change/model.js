define(["app"], function(app){
  var retObj = {
    passwordModel : Backbone.Model.extend({
	    urlRoot : "/password",
	    schema: {
	    	oldPassword:		{type: 'Password', validators: ['required']},
	        newPassword:	    {type: 'Password', validators: ['required']},
	        reEnterPassword:	{type: 'Password', validators: ['required', {type: 'match', field: 'newPassword', message: 'Passwords must match'}]}
	    },
	    parse : function(response) {
	    	console.log(response);
	    },
	    templateData: {}
	})
  };

  return retObj;

});