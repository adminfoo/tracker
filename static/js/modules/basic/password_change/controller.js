define(["app", "passwordChangeView", "passwordChangeModel"], function(app, passwordChangeView, passwordChangeModel){
  var retObj = {
    showForm : function() {
      var model = new passwordChangeModel.passwordModel();
      var data = {
        passwordChangeModel: model
      };
      var formView = passwordChangeView.passwordForm(data).render();
      var pageLayout = new passwordChangeView.pageView;
      formView.on('submit', function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate();
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        model.save().then(function(response) {
          app.form.enableSubmit(formView);
          window.alert(response.msg);
          if (!response.exc){
            window.location = "/";
          }
        }, function(response) {
          app.form.enableSubmit(formView);
          window.alert(response.responseJSON.error);
        });
      });
      app.mainRegion.show(pageLayout);
      pageLayout.passwordFormContainer.show(formView);
    }
  };

  return retObj;

});
