<div class="dashboard">

<div class="card-container">
	<div class="card revenue-card lfloat">
		<div class="header">
			<div class="i-b heading-text lfloat">Monthly revenue report</div>
			<div class="btn btn-secondary small rfloat">View all</div>
			<div class="clear"></div>
		</div>
		<div class="content">
			<div class="i-b vat lfloat">
				<canvas id="income-graph" height="200" width="200" style="vertical-align: middle;"></canvas>
				<div id="income-graph-legend" class="legend"></div>
			</div>
			<div class="i-b vat rfloat sldfsdf">
				<div class="sdkfh">
					<div class="ksdf">Expense</div>
					<div><i class="fa fa-inr"></i>&nbsp;1000</div>
				</div>
				<div class="sdkfh">
					<div class="ksdf">Income</div>
					<div><i class="fa fa-inr"></i>&nbsp;1000</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="card absent-teacher-card rfloat">
		<div class="header">
			<div class="i-b heading-text lfloat">List of absent teachers (10)</div>
			<div class="clear"></div>
		</div>
		<div class="content">
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
			<div class="item">
				<div class="profile-pic i-b vam">
					<div class="user-icon">
		            </div>
				</div>
				<div class="info i-b vam">
					<div class="name">
						Manu chaudhary
					</div>
					<div class="lectures">
						7 classes today
					</div>
				</div>
				<div class="btn btn-secondary small adjust-btn vam">
					Adjust classes
				</div>
			</div>
		</div>
	</div>
	<div class="clear" style="height: 50px;"></div>
	<div class="card attendance-summary-card">
		<div class="header">
			<div class="i-b heading-text">Monthly attendance report (Sep)</div>
		</div>
		<div class="content">
			<canvas id="attendane-graph"  style="vertical-align: middle;width:100%;height: 300px;overflow:hidden;"></canvas>
			<div id="attendace-graph-legend" class="legend"></div>
		</div>
	</div>
</div>
</div>