define(["app", "tpl!subHeadingListTemplate"], function(app, subHeadingListTemplate){
  var retObj = {
    subHeadingList : subHeadingListTemplate
  };
  return retObj;
});
