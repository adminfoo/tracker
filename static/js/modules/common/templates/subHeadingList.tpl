<% _.each(items, function(item) { %>
	<% if( (typeof item.permission == "undefined") || (app.havePermission(item.permission)) ) { %>
		<% if(item.link)  { %>
			<a href="<%= item.link %>">
		<% } %>
				<% if(item.active) { %>
					<div class="sub-heading-item active <%= item.className %>" data-id="<%= item.id %>">
				<% } else { %>
					<div class="sub-heading-item <%= item.className %>" data-id="<%= item.id %>">
				<% } %>
					<div class="top-border"></div>
						<div class="sub-heading-text">
							<%= item.heading %>
						</div>
						<div class="sub-heading-introduction">
							<%= item.description %>
						</div>
					<div class="bottom-border"></div>
				</div>
		<% if(item.link)  { %>
			</a>
		<% } %>
	<% } %>
<% }) %>