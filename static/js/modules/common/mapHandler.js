define(['gmap'], function(gmap) {
    var retObj = {
        equatorLength : 40075,   //equator length in kilometer
        distanceBy256px : 40075,
        mapZoomLevel : 1,
        createMap : function(container, options) {
            return new google.maps.Map(document.getElementById(container), options);
        },
        resizeMap : function(map) {
            google.maps.event.trigger(map, "resize");
        },
        getPositionObj : function(lat, lng) {
            return new google.maps.LatLng(lat, lng);
        },
        getEmptyMapBounds : function() {
            return new google.maps.LatLngBounds();
        },
        getMarker : function(options) {
            if(!options || !options.lat || !options.lng) {
                return null;    
            } 

            return new google.maps.Marker({
                position: new google.maps.LatLng(options.lat, options.lng),
                map: options.map || null,
                draggable: options.draggable || false,
                animation: options.animation || "none",
                icon :  options.markerImage   
            });
        },
        getMarkerCluster : function(map, markers) {
            return new MarkerClusterer(map, markers);
        },
        fitToBounds: function(map, mapBounds) {
            map.fitBounds(mapBounds);
        },

        panMap : function(map, offsetX, offsetY) {
            map.panBy(offsetX, offsetY);
        },
        removeFromMap : function(entity) {
            if(entity) {
                entity.setMap(null);    
            }
        },
        getCircle : function(options) {
            return new google.maps.Circle(options);
        },
        isInBounds : function(point, bounds) {
            console.log(bounds);
            return bounds.contains(point);

        },
        getRelativePositionFromCenter: function(point, center, zoom) {
            var point1Lat = 0, point1Lng = 0, centerLat = 0, centerLng = 0;
            var mapContainerHeight = $("#map-canvas").height();

            if(point.getPosition) {
                point1Lat = point.getPosition().lat(),
                point1Lng = point.getPosition().lng();
            } else {
                point1Lat = point.lat(),
                point1Lng = point.lng();
            }

            if(center.getPosition) {
                centerLat = center.getPosition().lat(),
                centerLng = center.getPosition().lng();
            } else {
                centerLat = center.lat(),
                centerLng = center.lng();
            }


            var pixelDistance = this.getPerpendicularPixelDistanceBetweenPoints(point1Lat, point1Lng, centerLat, centerLng, zoom);


            if(pixelDistance.horizontal > pixelDistance.vertical) {
                if(pixelDistance.horizontal > (mapContainerHeight / 2) && (point1Lng > centerLng)) {
                    return "right";
                } else if(pixelDistance.horizontal > (mapContainerHeight/ 2) && (point1Lng < centerLng)) {
                    return "left";
                } else if(pixelDistance.vertical > (mapContainerHeight/ 2) && (point1Lat > centerLat)) {
                    return "top";
                } else if(pixelDistance.vertical > (mapContainerHeight/ 2) && (point1Lat < centerLat)) {
                    return "bottom";
                }    
            } else {
                if(pixelDistance.vertical > (mapContainerHeight/ 2) && (point1Lat > centerLat)) {
                    return "top";
                } else if(pixelDistance.vertical > (mapContainerHeight/ 2) && (point1Lat < centerLat)) {
                    return "bottom";
                } else if(pixelDistance.horizontal > (mapContainerHeight/ 2) && (point1Lng > centerLng)) {
                    return "right";
                } else if(pixelDistance.horizontal > (mapContainerHeight/ 2) && (point1Lng < centerLng)) {
                    return "left";
                } 
            }

            return "in";

        },
        getPixelDistanceBetweenPoints : function(point1, point2, zoomLevel) {
            var kmDistance = (google.maps.geometry.spherical.computeDistanceBetween(point1.getPosition(), point2.getPosition()))/1000;
            
            if(zoomLevel != retObj.mapZoomLevel) {
                retObj.mapZoomLevel = zoomLevel;
                retObj.distanceBy256px = retObj.equatorLength / Math.pow(2, retObj.mapZoomLevel);
            }

            var pixelDistance = (kmDistance / retObj.distanceBy256px) * 256;

            return pixelDistance;
        },
        getPixelDistanceBetweenCoordinates : function(cordinate1, cordinate2, zoomLevel) {
            var kmDistance = (google.maps.geometry.spherical.computeDistanceBetween(cordinate1, cordinate2))/1000;
            
            if(zoomLevel != retObj.mapZoomLevel) {
                retObj.mapZoomLevel = zoomLevel;
                retObj.distanceBy256px = retObj.equatorLength / Math.pow(2, retObj.mapZoomLevel);
            }

            var pixelDistance = (kmDistance / retObj.distanceBy256px) * 256;

            return pixelDistance;
        },
        getPixelDistanceBetweenLatLong : function(lat1, lng1, lat2, lng2, zoomLevel) {
            var point1 = this.getPositionObj(lat1, lng1),
                point2 = this.getPositionObj(lat2, lng2);

            return this.getPixelDistanceBetweenPoints(point1, point2, zoomLevel || 12);
        },
        getPerpendicularPixelDistanceBetweenPoints : function(point1Lat, point1Lng, point2Lat, point2Lng, zoomLevel) {
            var obj = {
                horizontal : 0,
                vertical : 0
            };

            var horizontalPoint1 = this.getPositionObj(point1Lat, point1Lng),
                horizontalPoint2 = this.getPositionObj(point1Lat, point2Lng);

            obj.horizontal = this.getPixelDistanceBetweenCoordinates(horizontalPoint1, horizontalPoint2, zoomLevel);

            var verticalPoint1 = this.getPositionObj(point1Lat, point1Lng),
                verticalPoint2 = this.getPositionObj(point2Lat, point1Lng);

            obj.vertical = this.getPixelDistanceBetweenCoordinates(verticalPoint1, verticalPoint2, zoomLevel);

            return obj;
        }
    };

    return retObj;

});