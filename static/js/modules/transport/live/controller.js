define(["app", "transportLiveView", "mapHandler"], function(app, View, mapHandler){
  var retObj = {
    init : function() {
      retObj.firstMapLoadDone = false;
      retObj.selectedVehicleId = null;
      if(retObj.updateInterval) {
        window.clearInterval(retObj.updateInterval);
      }
    },
    show : function() {
      retObj.init();
      var item = Backbone.Model.extend({
        urlRoot : "/vechicleLive",
        parse : function(response) {
        // //@TOTO - remove these and provide actual parameters in api
        //  if((Math.floor(Math.random() * 2) + 1) % 2 == 0 ){
        //    response.ignition = false;
        //  }
          return response;
        }
      });
      var itemsCollection = Backbone.Collection.extend({
        url : "/vechicleLive",
        model : item
      });

      retObj.items = new itemsCollection();

      retObj.items.on("change", function() {
        // update left list
        retObj.view.render();

        // update map markers
        var bounds = new google.maps.LatLngBounds();
        _.each(retObj.items.models, function(model) {
          if(!model.get("lat") || !model.get("lng")) return;
          // create marker object
          var marker = model.get("marker") || internalObj.setMarker(model.toJSON());
          //marker.icon.rotation = model.get("direction");
          var latlng = new google.maps.LatLng(model.get("lat"), model.get("lng"));
          var infoContent = "<div>" + model.get("registrationNo") + " - " + model.get("model") + "</div><div>" + model.get("driverName") + "</div>";
          var infowindow = new google.maps.InfoWindow({
            content: infoContent
          });
          google.maps.event.clearListeners(marker, 'mouseover');
          google.maps.event.clearListeners(marker, 'click');
          marker.addListener('mouseover', function() {
            infowindow.open(retObj.map, marker);
          });
          marker.addListener('mouseout', function() {
            infowindow.close();

          });
          marker.addListener('click', function() {
            infowindow.close();
            retObj.selectedVehicleId = model.get("id");
            retObj.updateSelectedDetailbox();
          });
          bounds.extend(latlng);

          // if previousattributes are present then animate
          if(!$.isEmptyObject(model.previousAttributes())) {
            marker.setPosition(latlng);
          } else { // otherwise just show marker
            marker.setMap(retObj.map);
          }
          model.set("marker", marker);
        });
        if(!retObj.firstMapLoadDone) {
          google.maps.event.addListenerOnce(retObj.map, 'bounds_changed', function(event) {
            window.map = retObj.map;
            if (this.getZoom() > 18) {
              this.setZoom(18);
            }
          });
          mapHandler.fitToBounds(retObj.map, bounds);
          retObj.firstMapLoadDone = true;
        }

        // Update selected detail box
        retObj.updateSelectedDetailbox(true);
      });

      retObj.items.fetch({
        success : function(collection, response, options) {
          retObj.pageLayout = new View.pageView;
          app.mainRegion.empty();
          app.liveTrackingRegion.show(retObj.pageLayout);
          app.liveTrackingRegion.on("before:empty", function() {
            if(retObj.updateInterval) {
              window.clearInterval(retObj.updateInterval);
            }
          });

          retObj.pageLayout.selectedVehicle.on("show", function() {
            retObj.pageLayout.selectedVehicle.$el.show();
          });

          // Show map
          retObj.map = new google.maps.Map($(retObj.pageLayout.mapContainer.el)[0], {
            zoom: 6,
            center: {lat: 28, lng: 77},
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
          });

          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:select:vehicle", function() {
            var model = arguments[1].model;
            retObj.selectedVehicleId = model.get("id");
            retObj.updateSelectedDetailbox(true);
          });

          retObj.view.on("childview:highlight:vehicle", function() {
            var model = arguments[1].model;
            model.get("marker").icon.url = window.staticPath + "/img/bus_selected.png?v=1";
            model.get("marker").setMap(retObj.map);
          });

          retObj.view.on("childview:unhighlight:vehicle", function() {
            var model = arguments[1].model;
            if(retObj.selectedVehicleId && retObj.selectedVehicleId.trim != "" && retObj.selectedVehicleId == model.get("id")) return;
            if(model.get("moving")) {
                model.get("marker").icon.url = window.staticPath + "/img/bus_moving.png";
            } else {
                model.get("marker").icon.url = window.staticPath + "/img/bus_idle.png";
            }
            model.get("marker").setMap(retObj.map);
          });

          retObj.pageLayout.listContainer.show(retObj.view);

          retObj.items.trigger("change");

          retObj.updateInterval = setInterval(function() {
            retObj.items.fetch({
              success : function() {
                retObj.items.trigger("change");
              }
            });
          }, 10000);
        }
      });
    },
    animateIconOnLine : function (line, successCallback, offset) {
        retObj.changeOffset(line, offset);
        setTimeout(function() {
          offset += 10;
          if(offset >= 100) {
            if(typeof successCallback == "function") {
              successCallback();
              return;
            }
          }
          else {
            retObj.animateIconOnLine(line, successCallback, offset);
          }
        }, 17);
    },
    changeOffset : function(line, offset) {
        var icons = line.get('icons');
        icons[0].offset = offset + '%';
        line.set('icons', icons);
    },
    updateSelectedDetailbox : function(setCenter) {
      if(!retObj.selectedVehicleId || retObj.selectedVehicleId.trim == "") return;
      var model = retObj.items.findWhere({id : retObj.selectedVehicleId});
      var view = new View.detailView({
        model : model
      });
      view.on("detail:close", function() {
        retObj.pageLayout.selectedVehicle.$el.hide();
        retObj.selectedVehicleId = null;
        internalObj.resetItemIcon();
      });
      internalObj.resetItemIcon();
      model.get("marker").icon.url = window.staticPath + "/img/bus_selected.png?v=1";
      model.get("marker").setMap(retObj.map);
      retObj.pageLayout.selectedVehicle.show(view);
      if(setCenter) {
          var latlng = new google.maps.LatLng(model.get("lat"), model.get("lng"));
          retObj.map.setCenter(latlng);
          retObj.map.setZoom(18);
      }
      $(".vehicle-list-ul li").removeClass("active");
    }
  };

  var internalObj = {
    resetItemIcon : function() {
      _.each(retObj.items.models, function(item) {
        var marker = item.get("marker");
        if(!marker) return;
        if(item.get("moving")) {
            marker.icon.url = window.staticPath + "/img/bus_moving.png";
        } else {
            marker.icon.url = window.staticPath + "/img/bus_idle.png";
        }
        marker.setMap(retObj.map);
      });
    },
    busIcon : {
      url : window.staticPath + "/img/bus.png?v=1",
      scale : 0.1
    },
    setMarker : function(item) {
        var options = {
            lat : item.lat,
            lng : item.lng,
            markerImage : {
              scale : 0.1
            }
        };
        if(item.moving) {
          options.markerImage.url = window.staticPath + "/img/bus_moving.png";
        } else {
          options.markerImage.url = window.staticPath + "/img/bus_idle.png";
        }
        var marker = mapHandler.getMarker(options);
        if(!marker) {
            return;
        }
        return marker;
    }
  };

  return retObj;
});
