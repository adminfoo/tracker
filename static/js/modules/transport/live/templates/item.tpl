<div class="vehicle-item">
	<div class="i-b icon"><i class="fa fa-bus"></i></div>
	<div class="info i-b">
		<div class="reg-no"><%= name %> - <%= registrationNo %></div>
		<div class="driver-name"><%= driverName %></div>
		<% if(ac) { %>
			<div class="accesory-icon ac"></div>
		<% } %>
		<% if(ignition) { %>
			<div class="accesory-icon ignition"></div>
		<% } %>
		<% if(!reachable) { %>
			<div class="accesory-icon unreachable"></div>
		<% } %>
	</div>
</div>