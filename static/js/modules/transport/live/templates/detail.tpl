<div>
	<div class="close"><i class="fa fa-close"></i></div>
	<div class="info i-b">
		<div class="reg-no"><%= name %> - <%= registrationNo %></div>
		<div class="driver-name">Driver - <%= driverName %></div>
		<div class="driver-name">Speed - <%= speed %> Km/h</div>
		<% if(ac) { %>
			<div class="accesory-icon ac"></div>
		<% } %>
		<% if(ignition) { %>
			<div class="accesory-icon ignition"></div>
		<% } %>
		<% if(!reachable) { %>
			<div class="accesory-icon unreachable"></div>
		<% } %>
	</div>
</div>