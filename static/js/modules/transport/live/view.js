define(["app", "tpl!transportLiveItemTemplate", "tpl!transportLiveListTemplate", "tpl!transportLivePageTemplate", "tpl!transportLiveDetailTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate, detailTemplate){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "li",
        triggers : {
          "click" : "select:vehicle",
          "mouseover" : "highlight:vehicle",
          "mouseout" : "unhighlight:vehicle"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.listEmptyView;
        },
        template : listTemplate,
        childViewContainer: ".vehicle-list-ul"
    }),
    listEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="4" class="empty-collection"><%= _t("noStops") %></td>'),
      tagName : "tr"
    }),
    detailView : Marionette.ItemView.extend({
        template : detailTemplate,
        triggers : {
          "click .close" : "detail:close"
        }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        listContainer: ".vehicles-list-container",
        mapContainer : ".map-container",
        selectedVehicle : ".selected-vehicle"
      }
    })
  };
  return retObj;
});
