define(["app", "tpl!transportRoutePassengerItemTemplate", "tpl!transportRoutePassengerListTemplate", "tpl!transportRoutePassengerPageTemplate", "tpl!transportRoutePassengerSubItemFormTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate, subItemForm){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .edit" : "edit:passenger",
          "click .delete" : "delete:passenger"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.passengerListEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    passengerListEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="5" class="empty-collection"><%= _t("noPassenger") %></td>'),
      tagName : "tr"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .create-new-passenger" : "new:passenger"
      },
      regions: {
        subNavigation : ".sub-heading-list",
        listContainer: ".passenger-container"
      }
    }),
    subItemForm : subItemForm
  };
  return retObj;
});
