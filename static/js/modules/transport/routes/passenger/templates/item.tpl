<td><%= name %></td>
<td><%= pickupStopName %></td>
<td><%= dropStopName %></td>
<% if(typeof course != "undefined") { %>
    <td><%= course %></td>
<% } else { %>
    <td>Staff</td>
<% } %>

<td class="action-button-container">
	<a class="edit" data-index="<%= id %>"><%= _t('edit') %></a>
	 | 
	<a class="delete" data-index="<%= id %>"><%= _t('delete') %></a>
</td>

