<table class="list-table">
    <thead>        
        <tr class="head">
            <th><%= _t("name") %></th>
            <th><%= _t("pickupStop") %></th>
            <th><%= _t("dropStop") %></th>
            <th><%= _t("course") %></th>
            <th class="tac"><%= _t("actions") %></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>