<form class="add-passenger-form">
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div style="font-size:0;">
		<div class="personal-info i-b vat">
			<div data-fields="<%= personalInfoFields %>"></div>
			<div class="submit-button-container">
				<input type="submit" class="btn btn-primary submit" value="<%= submitButtonText %>"/>
			</div>
		</div>
		<div class="contact-info i-b vat">
			<div data-fields="<%= contactInfoFields %>"></div>
		</div>
	</div>
</form>