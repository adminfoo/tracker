define(["app", "transportRoutePassengerView", "transportRoute"], function(app, View, transportRoute){
  var retObj = {
    passengerModel : Backbone.Model.extend({
        urlRoot : "/passengers"
    }),
    list : function(name,id) {
      retObj.routeInfo = {
        routeId : id,
        name : name
      }
      var item = retObj.passengerModel;
      var itemsCollection = Backbone.Collection.extend({
        url : "/passengers?filterKeys=routeId&filterValues=" + id,
        model : item
      });

      var items = new itemsCollection();
      internalObj.getStopsAndRouteInfo().then(function() {
        items.fetch({
          success : function(collection, response, options) {
            retObj.view = new View.items({
              collection : collection
            });

            retObj.view.on("childview:edit:passenger", function() {
              var model = arguments[1].model;
              internalObj.showEditSubItem(model, false);
            });

            retObj.view.on("childview:delete:passenger", function() {
              var confirm = window.confirm("This will delete the passenger, are you sure?");
              if(!confirm) return;
              var model = arguments[1].model;
              model.destroy({
                wait : true,
                success : function() {
                  app.reqres.request("toaster:show", i18n.t("passengerDeleteSuccessMsg"));
                },
                error : function() {
                  if(arguments[1].status == 409) {
                    app.reqres.request("toaster:show", i18n.t("passengerDeleteConflictMsg"));
                  } else {
                    app.reqres.request("toaster:show", i18n.t("passengerDeleteErrorMsg"));
                  }
                }
              });
            });

            retObj.pageLayout = new View.pageView;

            retObj.pageLayout.on("new:passenger", function() {
              var Model = retObj.passengerModel;
              var model = new Model({
                type : "student",
                name : "",
                phone : "",
                gender : "male",
                bloodGroup : "O+",
                address : "",
                pickupRoute : "",
                pickupStop : "",
                dropRoute : "",
                dropStop : ""
              });
              internalObj.showEditSubItem(model, true);
            });

            app.mainRegion.show(retObj.pageLayout);
            transportRoute.showSubNavigation(retObj.routeInfo.name, retObj.routeInfo.routeId, "passengers", retObj.pageLayout.subNavigation);
            retObj.pageLayout.listContainer.show(retObj.view);
          }
        });
      });
    }
  };

  var internalObj = {
    showEditSubItem : function(model, isNew) {
      var courseOptions = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "KG", "LKG", "UKG", "Nursery", "Pre Nursery"];
      var serviceType = [{
        val : "both",
        label : i18n.t("pickupAndDrop")
      }, {
        val : "pickup",
        label : i18n.t("pickupOnly")
      }, {
        val : "drop",
        label :  i18n.t("dropOnly")
      }];
      var pickupStopOptions = [];
      var dropStopOptions = [];
      if(isNew) {
        pickupStopOptions = retObj.pickupRouteStopOptions[retObj.pickupRouteOptions[0].val];
        dropStopOptions = retObj.dropRouteStopOptions[retObj.dropRouteOptions[0].val];
      } else {
        pickupStopOptions = retObj.pickupRouteStopOptions[model.get("pickupRouteId")];
        dropStopOptions = retObj.dropRouteStopOptions[model.get("dropRouteId")];
      }
      var paxSchema = {
          type : { title : i18n.t("type") + "*", validators : ["required"], type : "Select",
                   options : [{ val : "student", label : i18n.t("student") }, { val : "staff", label : i18n.t("employee") }] },
          name : { title : i18n.t('name') + "*", validators : ['required']},
          idNumber : { title : i18n.t('admissionNo') },
          course : { title : i18n.t('class'), type : "Select", options : courseOptions},
          gender : { title : i18n.t('gender') + "*", validators : ['required'], type : "Select",
                     options : [{ val : "male", label : i18n.t("male") },
                                { val : "female", label : i18n.t("female") },
                                { val : "other", label : i18n.t("other") },] },
          bloodGroup : { title : i18n.t('bloodGroup') + "*", validators : ['required'], type : "Select",
                         options : app.general.bloodGroupOptions },
          guardianName : { title : i18n.t('guardianName') + "*", validators : ['required'] },
          phone : { title : i18n.t('guardianMobile') + "*", validators : ['required'] },
          email : { title : i18n.t('guardianEmail') },
          pickupRouteId : { title : i18n.t('choosePickupRoute'), type : "Select", options : retObj.pickupRouteOptions},
          pickupStopId : { title : i18n.t('choosePickupStop'), type : "Select", options :  pickupStopOptions},
          dropRouteId : { title : i18n.t('chooseDropRoute'), type : "Select", options : retObj.dropRouteOptions },
          dropStopId : { title : i18n.t('chooseDropStop'), type : "Select", options : dropStopOptions }
      };
      if(model.get("type") == "staff"){
        paxSchema = {
          type : { title : i18n.t("type") + "*", validators : ["required"], type : "Select",
                   options : [{ val : "student", label : i18n.t("student") }, { val : "staff", label : i18n.t("employee") }] },
          name : { title : i18n.t('name') + "*", validators : ['required']},
          idNumber : { title : i18n.t('employeeCode') },
          gender : { title : i18n.t('gender') + "*", validators : ['required'], type : "Select",
                     options : [{ val : "male", label : i18n.t("male") },
                                { val : "female", label : i18n.t("female") },
                                { val : "other", label : i18n.t("other") },] },
          bloodGroup : { title : i18n.t('bloodGroup') + "*", validators : ['required'], type : "Select",
                         options : app.general.bloodGroupOptions },
          designation : { title : i18n.t('designation') },
          phone : { title : i18n.t('mobile') + "*", validators : ['required'] },
          email : { title : i18n.t('email') },
          pickupRouteId : { title : i18n.t('choosePickupRoute'), type : "Select", options : retObj.pickupRouteOptions},
          pickupStopId : { title : i18n.t('choosePickupStop'), type : "Select", options :  pickupStopOptions},
          dropRouteId : { title : i18n.t('chooseDropRoute'), type : "Select", options : retObj.dropRouteOptions },
          dropStopId : { title : i18n.t('chooseDropStop'), type : "Select", options : dropStopOptions }
        };
      }
      var formModel = Backbone.Model.extend({
        schema : paxSchema,
        validate : function(attrs) {
          var errs = {};
          var correct = attrs.phone.match(/^[789]\d{9}$/);
          if (!correct) errs.phone = i18n.t("enter10digitNumber");
          return errs;
        }
      });

      var form = new formModel(model.attributes);

      var formView = new Backbone.Form({
        template : View.subItemForm,
        model : form,
        idPrefix : "passenger-form-",
        templateData : {
          isNew : isNew,
          fieldFormHeaderText : isNew ? i18n.t("addNew") : i18n.t("Edit"),
          submitButtonText : isNew ? i18n.t("save") : i18n.t("update"),
          successMsg : isNew ? i18n.t("addSuccessMsg") : i18n.t("editSuccessMsg"), 
          errorMsg : isNew ? i18n.t("addErrorMsg") : i18n.t("editErrorMsg"),
          personalInfoFields : model.get("type") == "staff" ? "type,name,idNumber,gender" : "type,name,idNumber,course,gender",
          contactInfoFields : model.get("type") == "staff" ? "pickupRouteId,pickupStopId,dropRouteId,dropStopId,bloodGroup,phone,email" : "pickupRouteId,pickupStopId,dropRouteId,dropStopId,bloodGroup,guardianName,phone,email"
        },
        originalModel : model,
        isNew : isNew,
        mediumSizeModal: true
      });

      formView.on("type:change", function() {
        var data = $.extend(formView.getValue(), {id : formView.model.get("id")});
        internalObj.showEditSubItem(new retObj.passengerModel(data), isNew);
      });

      formView.on("pickupRouteId:change", function(form, editor) {
        var pickupRouteStopOptions = retObj.pickupRouteStopOptions[editor.getValue()];
        form.fields.pickupStopId.editor.setOptions(pickupRouteStopOptions);
      });

      formView.on("dropRouteId:change", function(form, editor) {
        var dropRouteStopOptions = retObj.dropRouteStopOptions[editor.getValue()];
        form.fields.dropStopId.editor.setOptions(dropRouteStopOptions);
      });

      formView.on("submit", function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate({validate : true});
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        _.extend(formView.options.originalModel.attributes, formView.model.toJSON());
        var extraData = {
          routeId : retObj.routeInfo.routeId
        };
        _.extend(formView.options.originalModel.attributes, extraData)
        formView.options.originalModel.save(formView.options.originalModel.attributes, {
          patch : true,
          success : function(model, response, xhr) {
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", formView.templateData.successMsg);
            formView.trigger("modal:close");
            // model.set("pickupStopName", retObj.pickupRouteStopOptions[_.findIndex(retObj.pickupRouteOptions, {val: model.get("pickupStopId")})].label);
            // model.set("dropStopName", retObj.stopOptions[_.findIndex(retObj.stopOptions, {val: model.get("dropStopId")})].label);
//            if(formView.options.isNew) {
//              if (retObj.routeInfo.routeId == model.get("pickupRouteId") || retObj.routeInfo.routeId == model.get("dropRouteId")){
//                retObj.view.collection.add(model);
//              }
//            }
//            else{
//              if (retObj.routeInfo.routeId != model.get("pickupRouteId") && retObj.routeInfo.routeId != model.get("dropRouteId")){
//                retObj.view.collection.remove(model);
//              }
//            }
            window.location.reload();
            retObj.view.render();
          },
          error : function(){
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", formView.templateData.errorMsg);
          }
        });
      });
      app.modal.show(formView);
    },
    getStopsAndRouteInfo : function() {
      var deffered = $.Deferred();
      var pickupRoutes = $.ajax({
        url : "/transportRoutes?filterKeys=type&filterValues=pickup&keys=name,vehicleRegistartionNo,startTime,stops"
      });
      var dropRoutes = $.ajax({
        url : "/transportRoutes?filterKeys=type&filterValues=drop&keys=name,vehicleRegistartionNo,startTime,stops"
      });
      $.when(pickupRoutes, dropRoutes).done(function(pickup, drop) {
        var pickupRouteOptions = [];
        var pickupRouteStopOptions = {};
        var dropRouteOptions = [];
        var dropRouteStopOptions = {};
        _.each(pickup[0], function(item) {
          pickupRouteOptions.push({
            val : item.id,
            label : item.name + " (" + new Date(item.startTime).format('shortTime') + ")"
          });
          var tempRouteStop = pickupRouteStopOptions[item.id] = [];
          _.each(item.stops, function(item) {
            tempRouteStop.push({
              val : item.id,
              label : item.name
            });
          })
        });
        _.each(drop[0], function(item) {
          dropRouteOptions.push({
            val : item.id,
            label : item.name + " (" + new Date(item.startTime).format('shortTime') + ")"
          });
          var tempRouteStop = dropRouteStopOptions[item.id] = [];
          _.each(item.stops, function(item) {
            tempRouteStop.push({
              val : item.id,
              label : item.name 
            });
          })
        });
        retObj.pickupRouteOptions = pickupRouteOptions;
        retObj.pickupRouteStopOptions = pickupRouteStopOptions;
        retObj.dropRouteOptions = dropRouteOptions;
        retObj.dropRouteStopOptions = dropRouteStopOptions;
        deffered.resolve();
      });
      return deffered.promise();
    }
  };

  return retObj;
});
