define(["app", "transportETAReportView", "mapHandler", "gmap"], function(app, View, mapHandler){
  var retObj = {
    showReport : function() {
      retObj.pageLayout = new View.pageView;
      app.mainRegion.show(retObj.pageLayout);

      internalObj.getRouteAvailable().then(function(availableRoutes) {
        var formModel = Backbone.Model.extend({
          schema : {
            route : { title : i18n.t("routeName"), type : "Select", options : availableRoutes},
            date : { title : i18n.t("date"), type : "Date"},
            tripType : { title : "Trip type" , type : "Select", options : ["pickup","drop"]}
          }
        });

        retObj.formData = new formModel({
          route : "BAJGHERA",
          date : "",
          tripType : "pickup"
        });

        retObj.formView = new Backbone.Form({
          model : retObj.formData,
          template : View.formView,
          templateData : {
            fieldFormHeaderText : i18n.t("routeInformation")
          }
        });

        retObj.formView.on("submit", function(event) {
          event.preventDefault();
          internalObj.clearReport();
          var errors = retObj.formView.commit();
          app.form.disableSubmit(retObj.formView);
          if(errors) {
              app.form.enableSubmit(retObj.formView);
              return;
          }
          var data = this.model.toJSON();
          $.when(internalObj.getRouteData(data), internalObj.getETAReport(data)).done(function(routeData, etaData) {
            app.form.enableSubmit(retObj.formView);
            routeData = routeData[0];
            etaData = etaData[0];
            if(data.tripType == "pickup") {
              var routeData = _.find(routeData.trips, function(obj) {return obj.name == "pickup"});
              var etaData = etaData.pickup;
            } else {
              var routeData = _.find(routeData.trips, function(obj) {return obj.name == "drop"});
              var etaData = etaData.drop;
            }
            retObj.stops = etaData.stops;
            retObj.routePathPoints = routeData.pathPoints;
            retObj.currentDayPoints = etaData.points;
            retObj.renderReports();
          });
        });
        retObj.pageLayout.formContainer.show(retObj.formView);
      });

      retObj.map = new google.maps.Map($(".map-container .map-element")[0], {
        zoom: 6,
        center: {lat: 28, lng: 77},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        disableDoubleClickZoom: false,
        streetViewControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_TOP
        },
      });
    },
    renderReports : function() {
      retObj.stopListView = new View.stopListView({
        model : new Backbone.Model({
          stops : retObj.stops
        })
      });
      retObj.pageLayout.stopListContainer.show(retObj.stopListView);
      var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
      };
      retObj.originalRouteLine = new google.maps.Polyline({
        path: retObj.routePathPoints,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '200px'
        }],
        geodesic: true,
        strokeColor: 'black',
        strokeOpacity: 1.0,
        strokeWeight: 4
      });
      retObj.originalRouteLine.setMap(retObj.map);

      retObj.currentDayRouteLine = new google.maps.Polyline({
        path: retObj.currentDayPoints,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '200px'
        }],
        geodesic: true,
        strokeColor: 'red',
        strokeOpacity: 1.0,
        strokeWeight: 1
      });
      retObj.currentDayRouteLine.setMap(retObj.map);

      var bounds = new google.maps.LatLngBounds();
      _.each(retObj.stops, function(item) {
        var options = {
          lat : item.lat,
          lng : item.lng,
          markerImage : {
            path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
            fillColor: 'red',
            strokeColor: 'red',
            strokeWeight: 10
          }
        }
        var marker = mapHandler.getMarker(options);
        var latlng = new google.maps.LatLng(item.lat, item.lng);
        var content = "<div>" + item.name + "<br>Sch:" + new Date(item.schTime).format("h:MM TT") + "</div>";
        if(!item.actTime) {
            content += "<div>Actual: null</div>";
        } else {
            content += "<div>Actual:" + new Date(item.actTime).format("h:MM TT") + "</div>";
        }
        var infowindow = new google.maps.InfoWindow({
          content: content
        });
        marker.addListener('mouseover', function() {
          infowindow.open(retObj.map, marker);
        });
        marker.addListener('mouseout', function() {
          if(!marker.doNotCloseOnMouseOut) infowindow.close();
        });
        retObj.markers.push(marker);
        marker.setMap(retObj.map);
        bounds.extend(latlng);
      });
      mapHandler.fitToBounds(retObj.map, bounds);
    }
  };

  var internalObj = {
    getRouteData : function(data) {
      return $.ajax({
        method : "GET",
        url : "/transportRoutes/" + data.route
      });
    },
    getETAReport : function(data) {
      return $.ajax({
        method : "GET",
        url : "/routeTrips?date=" + data.date.format("yyyy-mm-dd") + "&routeId=" + data.route
      });
    },
    getRouteAvailable : function() {
      var deffered = $.Deferred();
      $.ajax({
        url : "/transportRoutes",
        method : "GET",
        success : function(data){
          var availableRoutes = [];
          _.each(data, function(item) {
            availableRoutes.push({
              val : item.id,
              label : item.name
            });
          });
          deffered.resolve(availableRoutes);
        }
      });
      return deffered.promise();
    },
    clearReport : function() {
      retObj.pageLayout.stopListContainer.empty();
      if(retObj.originalRouteLine) retObj.originalRouteLine.setMap(null);
      if(retObj.currentDayRouteLine) retObj.currentDayRouteLine.setMap(null);
      if(retObj.markers) {
        _.each(retObj.markers, function(marker) {
          marker.setMap(null);
        });
      }
      retObj.markers = [];
    }
  };

  return retObj;
});
