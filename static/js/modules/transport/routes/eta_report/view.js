define(["app", "tpl!transportETAReportStopListTemplate", "tpl!transportETAReportFormTemplate", "tpl!transportETAReportPageTemplate"],
       function(app, stopListTemplate, formTemplate, pageTemplate){
  var retObj = {
    formView : formTemplate,
    stopListView : Marionette.ItemView.extend({
      template: stopListTemplate,
      templateHelpers : {
        getErrorMsg : function(stop) {
          if(!stop.actTime) return "null";
          var diff = (new Date(stop.schTime).getTime() - new Date(stop.actTime).getTime())/1000;
          var delayed = false;
          if(diff<0) {
            delayed = true;
            diff = 0-diff;
          }
          var time = "";
          if(diff < 60) {
            time = diff + " seconds";
          } else {
            time = parseInt(diff/60,10) + " minutes";
          }
          time = "<b>" + time + "</b>";
          if(delayed) {
            return '<div class="error-in-time delay">Delayed by ' + time + '</div>';
          } else {
            return '<div class="error-in-time rush">Rushed by ' + time + '</div>';
          }
        }
      }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        formContainer: ".form-container",
        stopListContainer : ".stop-list-container",
        mapContainer : ".map-container"
      },
      onShow : function() {
        $("body").addClass("custom-width100");
      }
    })
  };
  return retObj;
});
