define(["app", "tpl!transportRoutesAddPageTemplate", "tpl!transportRoutesAddDriverVechicleForm", "tpl!transportRoutesAddSummaryTemplate", "tpl!transportRouteMapLayoutTemplate", "tpl!transportRouteStopListInMapTemplate"],
       function(app, pageTemplate, driverAndVehicleForm, summaryTemplate, mapLayoutTemplate, stopListInMapTemplate){
  var retObj = {
    summaryView : Marionette.ItemView.extend({
      template: summaryTemplate,
      ui : {
        "driverInput" : ".driver-input",
        "vehicleInput" : ".vehicle-input",
        "newStop" : ".new-stop",
        "stopInput" : ".stop-input",
        "stopDropTimeInput" : ".drop-time",
        "stopPickupTimeInput" : ".pickup-time",
        "deleteButton" : ".delete",
        "addMarkerButton" : ".add-marker",
        "saveRouteButton" : ".save-route"
      },
      events : {
        'change @ui.stopDropTimeInput' : "triggerDropTimeChange",
        'change @ui.stopPickupTimeInput' : "triggerPickupTimeChange",
        'change @ui.stopInput' : 'changeStop',
        "click @ui.deleteButton" : "deleteStop",
        "click @ui.addMarkerButton" : "addMarker",
      },
      triggerDropTimeChange : function(event) {
        this.trigger("change:stopDrop:time", event);
      },
      triggerPickupTimeChange : function(event) {
        this.trigger("change:stopPickup:time", event);
      },
      changeStop : function(event) {
        this.trigger("change:stop", event);
      },
      deleteStop : function(event) {
        this.trigger("delete:stop", event);
      },
      addMarker : function(event) {
        this.trigger("add:marker", event);
      },
      triggers : {
        "click @ui.newStop" : "new:stop",
        "click @ui.saveRouteButton" : "save:route"
      }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,     
      regions: {
        subNavigation : ".sub-heading-list",
        driverAndVehicleForm : ".driverAndVehicleForm",
        summary : ".summary",
        vehicleInfo : ".vehicleInfo"
      }
    }),
    mapLayout : Marionette.LayoutView.extend({
      template : mapLayoutTemplate,
      regions : {
        stopList : ".stop-list-container",
        map : ".map-container"
      }
    }),
    stopListInMapView : Marionette.ItemView.extend({
      template : stopListInMapTemplate,
      ui : {
        "stopItem" : ".stop-list .item",
        "tripType" : ".stop-list .trip-type"
      },
      events : {
        "click @ui.stopItem" : "selectStop",
        "mouseover @ui.stopItem" : "hightlightStop",
        "mouseout @ui.stopItem" : "unhightlightStop",
        "change @ui.tripType" : "changeTripType"
      },
      hightlightStop : function(event) {
        this.trigger("hightlight:stop", event);
      },
      unhightlightStop : function(event) {
        this.trigger("unhightlight:stop", event);
      },
      selectStop : function(event) {
        this.trigger("select:stop", event);
      },
      changeTripType : function(event) {
        this.trigger("change:tripType", $(event.target).val());
      }
    }),
    driverAndVehicleForm : driverAndVehicleForm
  };
  return retObj;
});
