<form>
<div class="form-group-container">
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="name,driver,helper,vehicle"></div>
	</div>
</div>
</form>