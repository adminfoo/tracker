<div class="map-container">
	<div class="pindrop-label">
		<%= _t("pinStopLocationOnMap") %>
		<div class="i-b close-btn">
			<i class="fa fa-close"></i>
		</div>
		<div class="clear"></div>
	</div>
	<div class="stop-list-container">
	</div>
	<div style="height: 598px;margin-left: 200px;">
		<div class="location-picker" style="height:100%;">
		</div>
	</div>
</div>