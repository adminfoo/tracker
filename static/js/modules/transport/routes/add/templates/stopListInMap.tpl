<div class="stop-list">
	<select class="trip-type">
		<option value="pickup" <% if(tripType == "pickup") print('selected'); %> >pickup</option>
		<option value="drop0" <% if(tripType == "drop0") print('selected'); %>>drop0</option>
		<option value="drop" <% if(tripType == "drop") print('selected'); %>>drop</option>
	</select>
	<%  _.each(stops, function(stop, index) { %>
		<div class="item" data-index="<%= index %>">
			<div class="name" data-index="<%= index %>"><%= stop.name %></div>
			<div style="font-size:0.9rem;" data-index="<%= index %>">
				<%= stop.timeObj.hr %>-<%= stop.timeObj.min %> <%= stop.timeObj.meridian %>
			</div>
			<% if(!stop.lat || !stop.lng) { %>
				<div class="latLng" data-index="<%= index %>">Click on any stop in right</div>
			<% } else { %>
				<div class="latLng" data-index="<%= index %>"><%= parseFloat(stop.lat).toFixed(5) %>,<%= parseFloat(stop.lng).toFixed(5) %></div>
			<% } %>
		</div>
	<% }) %>
</div>