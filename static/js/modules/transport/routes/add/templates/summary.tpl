<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("addNewRoute") %>
	</div>
</div>
<div class="stops-container table-container overflow-inherit">
	<table class="list-table" style="width: 100%;">
	    <thead>        
	        <tr class="head">
	            <th><%= _t("stopName") %></th>
	            <th><%= _t("time") %></th>
	            <th class="tac"><%= _t("actions") %></th>
	        </tr>
	    </thead>
	    <tbody>
	    	<% if(stops.length == 0) { %>
	    		<tr><td colspan="5" class="tac"><%= _t("noStopsAdded") %>.&nbsp;<a class="cta new-stop"><%= _t("addNew") %></a></td></tr>
	    	<% } else { %>
	    		<% _.each(stops, function(_stop, _stopIndex) { %>
	    			<tr>
	    				<td>
	    					<input placeholder="<%= _t('stopName') %>" data-stopIndex="<%= _stopIndex %>" class="stop-input" value="<%= _stop.name %>"></input>
	    					
	    				</td>
	    				<td>
	    					<% if(!$.isEmptyObject(_stop.timeObj)) { %>
	    						<%= getTime(_stop.timeObj) %>
	    					<% } %>
	    				</td>
	    				<td class="action-button-container">
	    					<% if(app.havePermission(["superadmin"])) { %>
	    						<a class="add-marker" data-stopIndex="<%= _stopIndex %>"><%= _t('pinOnMap') %></a>
	    					 	| 
	    					<% } %>
							<a class="delete" data-stopIndex="<%= _stopIndex %>"><%= _t('delete') %></a>
						</td>
	    			</tr>
	    		<% }) %>
	    		<tr><td colspan="4" class="tac"><a class="cta new-stop"><%= _t("addMoreStops") %></a></td></tr>
	    	<% } %>
	    </tbody>
	</table>
</div>

<% if(typeof id != "undefined") { %>
	<input type="submit" class="btn btn-primary submit save-route" value="<%= _t("update") %>"/>
<% } else { %>
	<input type="submit" class="btn btn-primary submit save-route" value="<%= _t("save") %>"/>
<% } %>