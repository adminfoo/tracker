define(["app", "transportRouteAddView", "transportRoute", "mapHandler", "gmap"], function(app, View, transportRoute, mapHandler){
  var retObj = {
    routeModel : Backbone.Model.extend({
          urlRoot : "/transportRoutes",
          parse : function(data) { 
            _.each(data.stops, function(stop) {
              stop.timeObj.hr = app.helper.pad(stop.timeObj.hr,2,0);
              stop.timeObj.min = app.helper.pad(stop.timeObj.min,2,0);
            });
            return data;
          }
    }),
    editRoute : function(name, routeId) {
      $.when(internalObj.getVehicles(), internalObj.getDrivers(), internalObj.getHelpers()).done(function(vehicleList, driverList, helperList) {

        var Model = retObj.routeModel;
        var model = new Model({
          id : routeId,
          _vehicleList : vehicleList[0],
          _driverList : driverList[0],
          _helperList : helperList[0]
        });
        retObj.model = model;
         retObj.model.fetch({
           success : function(model) {
             model.set("_vehicle", model.get("_vehicleList")[_.findIndex(model.get("_vehicleList"), {id: model.get("vehicleId")})]);
             model.set("_driver", model.get("_driverList")[_.findIndex(model.get("_driverList"), {id: model.get("driverId")})]);
             model.set("_helper", model.get("_helperList")[_.findIndex(model.get("_helperList"), {id: model.get("helperId")})]);
             retObj.showEditRoute(false);
           }
         });

        //retObj.model.set(internalObj.getRouteData());
        // retObj.model.set("_vehicle", model.get("_vehicleList")[_.findIndex(model.get("_vehicleList"), {id: model.get("vehicleId")})]);
        // retObj.showEditRoute(false);
      });
    },
    addNew : function() {
      $.when(internalObj.getVehicles(), internalObj.getDrivers(), internalObj.getHelpers()).done(function(vehicleList, driverList, helperList) {
        var Model = retObj.routeModel;
        var model = new Model({
          stops : [],
          name : "",
          driverId : "",
          _driverList : driverList[0],
          _driver : {},
          helperId : "",
          _helperList : helperList[0],
          _helper : {},
          vehicleId : "",
          _vehicleList : vehicleList[0],
          _vehicle : {}
        });
        retObj.model = model;
        retObj.showEditRoute(true);
      });
    },
    showEditRoute : function(isNew) {
      retObj.stopPoints = [];
      retObj.tripType  = "pickup";
      retObj.pageLayout = new View.pageView;
      app.mainRegion.show(retObj.pageLayout);

      if(!isNew) {
        var title = retObj.model.get("name") + " " + retObj.model.get("type") + " - " + new Date(retObj.model.get("startTime")).format("shortTime") + " (" + retObj.model.get("vehicleRegistrationNo") + ")";
        transportRoute.showSubNavigation(title, retObj.model.get("id"), "info", retObj.pageLayout.subNavigation);
      }

      retObj.view = new View.summaryView({
        model : retObj.model,
        templateHelpers : {
          getTime : function(time) {
            return time.hr + " : " + time.min + " : " + time.meridian;
          }
        },
        routeSaveSuccessMsg : isNew ? i18n.t("routeAddedSuccessMsg") : i18n.t("routeUpdateSuccessMsg")
      });

      retObj.view.on("new:stop", function(){
        this.model.attributes.stops.push({
          name : "",
          monthlyFees : "",
          timeObj : {}
        });
        retObj.view.render();
      });

      retObj.view.on("change:stop", function(event) {
        var elm = $(event.target || event.srcElement);
        var _stopIndex = parseInt(elm.attr("data-stopIndex"), 10);
        var stop = this.model.attributes.stops[_stopIndex];
        stop.name = elm.val();
      });

      retObj.view.on("delete:stop", function(event) {
        var confirm = window.confirm("This will delete the stop, are you sure?");
        if(!confirm) return;
        var elm = $(event.target || event.srcElement);
        var _stopIndex = parseInt(elm.attr("data-stopIndex"), 10);
        this.model.attributes.stops.splice(_stopIndex, 1);
        retObj.view.render();
      });

      retObj.view.on("add:marker", function(event) {
        retObj.selectedStopIndex = null;
        var stopPoints = internalObj.getAllStopPoints(retObj.model.get("id"));
        stopPoints.then(function() {
          retObj.mapLayout = new View.mapLayout;
          app.fullScreen.show(retObj.mapLayout);
          retObj.mapLayout.$el.find(".close-btn").on("click", function() {
            retObj.mapLayout.trigger("fullScreen:close");
          });
          retObj.map = new google.maps.Map(retObj.mapLayout.$el.find(".location-picker")[0], {
            zoom: 6,
            center: {lat: 28, lng: 77},
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_TOP
            },
          });

          var bounds = new google.maps.LatLngBounds();
          _.each(retObj.stopPoints.pois, function(item) {
            if(true) {
              var options = {
                lat : item.lat,
                lng : item.lng,
                markerImage : {
                  path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
                  fillColor: 'red',
                  strokeColor: 'red',
                  strokeWeight: 10
                }
              }
              var marker = mapHandler.getMarker(options);
              var latlng = new google.maps.LatLng(item.lat, item.lng);
              var content = internalObj.stopInfoWindowContent(item);
              var infowindow = new google.maps.InfoWindow({
                content: content
              });
              google.maps.event.clearListeners(marker, 'mouseover');
              marker.addListener('mouseover', function() {
                infowindow.open(retObj.map, marker);
              });
              marker.addListener('mouseout', function() {
                if(!marker.doNotCloseOnMouseOut) infowindow.close();
              });
              marker.addListener('click', function() {
                if(retObj.selectedStopIndex == null){
                  return;
                }
                var stops = retObj.model.get("stops");
                var stop = stops[retObj.selectedStopIndex];
                stop.lat = item.lat;
                stop.lng = item.lng;
                retObj.model.set("stops", stops);
                retObj.stopListView.render();
              });
              marker.setMap(retObj.map);
              bounds.extend(latlng);
            }
          });
          // Define a symbol using a predefined path (an arrow)
          // supplied by the Google Maps JavaScript API.
          var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
          };
          var flightPath = new google.maps.Polyline({
            path: retObj.stopPoints.points,
            icons: [{
              icon: lineSymbol,
              offset: '0',
              repeat: '200px'
            }],
            geodesic: true,
            strokeColor: '#000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });

          flightPath.setMap(retObj.map);
          mapHandler.fitToBounds(retObj.map, bounds);

          retObj.stopListView = new View.stopListInMapView({
            model : new Backbone.Model({
              stops : retObj.model.get("stops"),
              tripType : retObj.tripType
            })
          });

          retObj.stopListView.on("select:stop", function(event) {
            $(".stop-list .item").removeClass("active");
            var elm = $(event.target || event.srcElement);
            var index = parseInt(elm.attr("data-index"), 10);
            if(retObj.selectedStopIndex && retObj.selectedStopIndex == index) {
              retObj.selectedStopIndex = null;
              $($(".stop-list .item")[index]).removeClass("active");
            } else {
              $($(".stop-list .item")[index]).addClass("active");
              retObj.selectedStopIndex = index;
            }

            _.each(retObj.hightlightedMarkers, function(marker) {
              marker.setMap(null);
            });
            retObj.hightlightedMarkers = [];
            var stop = retObj.model.attributes.stops[index];
            _.each(retObj.stopPoints.pois, function(poi) {
              var isInRange = false;
              _.each(poi.points, function(point) {
                if(internalObj.isPointInStopTimeRange(stop, point)) {
                  isInRange = true;
                }
              });
              if(isInRange) {
                var options = {
                  lat : poi.lat,
                  lng : poi.lng,
                  markerImage : {
                    path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
                    fillColor: 'green',
                    strokeColor: 'green',
                    strokeWeight: 10
                  }
                }
                var marker = mapHandler.getMarker(options);
                var content = internalObj.stopInfoWindowContent(poi);
                var infowindow = new google.maps.InfoWindow({
                  content: content
                });
                google.maps.event.clearListeners(marker, 'mouseover');
                marker.addListener('mouseover', function() {
                  infowindow.open(retObj.map, marker);
                });
                marker.addListener('mouseout', function() {
                  if(!marker.doNotCloseOnMouseOut) infowindow.close();
                });
                marker.addListener('click', function() {
                  if(retObj.selectedStopIndex == null){
                    return;
                  }
                  var stops = retObj.model.get("stops");
                  var stop = stops[retObj.selectedStopIndex];
                  stop.lat = poi.lat;
                  stop.lng = poi.lng;
                  retObj.model.set("stops", stops);
                  retObj.stopListView.render();
                });
                retObj.hightlightedMarkers.push(marker);
                marker.setMap(retObj.map);
              }
            });

          });

          retObj.stopListView.on("hightlight:stop", function(event) {
            var elm = $(event.target || event.srcElement);
            var index = parseInt(elm.attr("data-index"), 10);

            retObj.stopCircle = new google.maps.Circle({
              strokeColor: '#fff000',
              strokeOpacity: 1,
              strokeWeight: 1,
              fillColor: '#fff000',
              fillOpacity:  0.60,
              center: mapHandler.getPositionObj(retObj.model.attributes.stops[index].lat, retObj.model.attributes.stops[index].lng),
              radius: 2*100
            });

            retObj.stopCircle.setMap(retObj.map);
          });

          retObj.stopListView.on("unhightlight:stop", function(event) {
            retObj.stopCircle.setMap(null);
          });

          retObj.stopListView.on("change:tripType", function(type) {
            retObj.tripType = type;
            retObj.mapLayout.trigger("fullScreen:close");
            retObj.view.trigger("add:marker");
          });

          retObj.mapLayout.stopList.show(retObj.stopListView);
        });
      });

      retObj.view.on("save:route", function() {
        app.form.disableSubmit(retObj.view);
        retObj.model.save(this.model.attributes, {
          patch: true,
          success : function(model, response, options) {
            app.form.enableSubmit(retObj.view);
            app.scrollTop();
            app.reqres.request("toaster:show", retObj.view.options.routeSaveSuccessMsg);
          },
          error : function() {
            app.form.enableSubmit(retObj.view);
            app.reqres.request("toaster:show", i18n.t("errorSavingRoute"));
          }
        })
      });

      var vehicleOptions = [{
        val : "",
        label : i18n.t("chooseVehicle")
      }];
      _.each(retObj.model.get("_vehicleList"), function(item) {
        vehicleOptions.push({
          val : item.id,
          label : item.name + ' (' + item.registrationNo + ')'
        });
      });

      var driverOptions = [{
        val : "",
        label : i18n.t("chooseDriver")
      }];
      _.each(retObj.model.get("_driverList"), function(item) {
        driverOptions.push({
          val : item.id,
          label : item.name
        });
      });

      var helperOptions = [{
        val : "",
        label : i18n.t("chooseHelper")
      }];
      _.each(retObj.model.get("_helperList"), function(item) {
        helperOptions.push({
          val : item.id,
          label : item.name
        });
      });

      var Model = Backbone.Model.extend({
        schema : {
          name : { title : i18n.t("routeName") + "*", validators : ["required"] },
          driver : { title : i18n.t("driver") + "*", type : "Select", validators : ["required"], options : driverOptions},
          helper : { title : i18n.t("helper") + "*", type : "Select", validators : ["required"], options : helperOptions},
          vehicle : { title : i18n.t("chooseVehicle") + "*", type : "Select", validators : ["required"], options : vehicleOptions}
        }
      });

      var form = new Model({
        name : retObj.model.get("name"),
        driver : retObj.model.get("driverId"),
        helper : retObj.model.get("helperId"),
        vehicle : retObj.model.get("vehicleId")
      });

      retObj.driverAndVehicleFormView = new Backbone.Form({
        model : form,
        template : View.driverAndVehicleForm,
        templateData : {
          fieldFormHeaderText : i18n.t("routeInformation")
        }
      });

      retObj.driverAndVehicleFormView.on("name:change", function(form, editor) {
        var name = editor.getValue();
        retObj.model.set("name", name);
      });

      retObj.driverAndVehicleFormView.on("driver:change", function(form, editor) {
        var driverId = editor.getValue();
        if(driverId == -1) {
          retObj.model.set("driverId", "");
        } else {
          retObj.model.set("driverId", driverId);
        }
      });

      retObj.driverAndVehicleFormView.on("helper:change", function(form, editor) {
        var helperId = editor.getValue();
        if(helperId == -1) {
          retObj.model.set("helperId", "");
        } else {
          retObj.model.set("helperId", helperId);
        }
      });

      retObj.driverAndVehicleFormView.on("vehicle:change", function(form, editor) {
        var vehicleId = editor.getValue();
        if(vehicleId == -1) {
          retObj.model.set("vehicleId", "");
        } else {
          retObj.model.set("vehicleId", vehicleId);
        }
      });

      retObj.pageLayout.driverAndVehicleForm.show(retObj.driverAndVehicleFormView);

      retObj.pageLayout.summary.show(retObj.view);
    },
    updateSelectedPoint : function() {
      if($.isEmptyObject(retObj.selectedLatLng)) return;
      var view = new View.detailView({
        model : new Backbone.Model({
          stops : retObj.model.get("stops"),
          selectedLat : retObj.selectedLatLng.position().lat,
          selectedLng : retObj.selectedLatLng.position().lng
        })
      });
      view.render();
      retObj.mapView.$el.find(".location-detail").html(view.$el.html());
      retObj.mapView.$el.find(".location-detail .close").on("click", function() {
        retObj.mapView.$el.find(".location-detail").hide();
        retObj.selectedLatLng = null;
      });
      retObj.mapView.$el.find(".location-detail").show();
    }
  };

  var internalObj = {
    getNewTime : function(elm) {
      var hr = elm.attr("data-timepicki-tim");
      var min = elm.attr("data-timepicki-mini");
      var meridian = elm.attr("data-timepicki-meri");
      var newTime = {
        hr : hr,
        min : min,
        meridian : meridian
      }
      return newTime;
    },
    getStops : function() {
      return $.ajax({
        url : "/transportStops"
      });
    },
    getVehicles : function() {
      return $.ajax({
        url : "/transportVehicles"
      });
    },
    getDrivers : function() {
      return $.ajax({
        url : "/drivers?filterKeys=role&filterValues=driver"
      });
    },
    getHelpers : function() {
      return $.ajax({
        url : "/drivers?filterKeys=role&filterValues=helper"
      });
    },
    getAllStopPoints : function(routeId) {
      var result = $.Deferred();
      if(!$.isEmptyObject(retObj.stopPoints)) {
        result.resolve();
      } else {
        $.ajax({
          url : "/routeMarkHelper/" + routeId,
          success : function(data) {
            retObj.stopPoints = data;
            result.resolve();
          }
        });
      }
      return result.promise();
    },
    isPointInStopTimeRange : function(stop, point) {
      var stopTime = new Date(2016, 1, 1, stop.timeObj.hr, stop.timeObj.min, 0, 0).getTime();
      var pointTime = new Date(2016, 1, 1, new Date(point.time).getHours(), new Date(point.time).getMinutes(), 0, 0).getTime();
      var diff = stopTime - pointTime;
      if(diff < 0) diff = 0-diff;
      if(diff <= 3*60*1000) {
        return true;
      }
      return false;
    },
    stopInfoWindowContent : function(poi) {
      var content = "<div style='font-size:0.8rem;'>" + parseInt(poi.precision, 10) + "<br>";
      var dates = {};
      _.each(poi.points, function(point) {
        var date = new Date(point.time).format("dd-mmm-yyyy");
        dates[date] = dates[date] || [];
        dates[date].push(new Date(point.time).format("hh:MM"));
      });
      _.each(dates, function(item, key) {
        content += "<b>" + key + " : </b>";
        _.each(item, function(value) {
          content += value + ", ";
        });
        content += "<br>";
      });
      return content + "</div>";
    },
    getRouteData : function() {
      return {
          oldRouteId: "5667c741ac628c2259a1dc51",
          startTime: "2015-12-22T07:10:30+0530",
          helperName: "ANKLESH",
          driverLicenseNo: "4295608206",
          driverPhone: "8826811818",
          vehicleDeviceId: "355488020138421",
          totalPassengers: 20,
          vehicleId: "565e7b44ac628c049df6807d",
          type: "pickup",
          schoolId: "565e7918ac628c049df68074",
          name: "RAJENDRA PARKG",
          id: "568a4ad7bfed252fa352b7fd",
          driverName: "GEET BAHADUR",
          stops: [
          {
          lng: 77.00679111111111,
          lat: 28.491070555555556,
          id: "srgs",
          loc: {
          type: "Point",
          coordinates: [
          77.00679111111111,
          28.491070555555556
          ]
          },
          name: "DURGA SWEET",
          timeObj: {
          hr: 7,
          min: 10,
          meridian: "AM"
          },
          time: "2015-12-14T07:27:19+0530"
          },
          {
          lng: 77.0104,
          lat: 28.493618333333334,
          id: "dxlp",
          loc: {
          type: "Point",
          coordinates: [
          77.0104,
          28.493618333333334
          ]
          },
          name: "RATAN GARDEN",
          timeObj: {
          hr: 7,
          min: 20,
          meridian: "AM"
          },
          time: "2015-12-14T07:32:34+0530"
          },
          {
          lng: 77.00934611111111,
          lat: 28.496078333333333,
          id: "cxtf",
          loc: {
          type: "Point",
          coordinates: [
          77.00934611111111,
          28.496078333333333
          ]
          },
          name: "VISHNU GARDEN",
          timeObj: {
          hr: 7,
          min: 30,
          meridian: "AM"
          },
          time: "2015-12-15T07:37:57+0530"
          }
          ],
          helperPhone: "8510047109",
          vehicleRegistrationNo: "HR 55 R 6440"
      };
    }
  };

  return retObj;
});
