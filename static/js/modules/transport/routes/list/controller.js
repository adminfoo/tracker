define(["app", "transportRouteListView"], function(app, View){
  var retObj = {
    list : function() {
      var item = Backbone.Model.extend({
        urlRoot : "/transportRoutes"
      });
      var itemsCollection = Backbone.Collection.extend({
        url : "/transportRoutes",
        model : item
      });

      retObj.items = new itemsCollection();

      retObj.items.fetch({
        success : function(collection, response, options) {
          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:delete:route", function() {
            var confirm = window.confirm("This will delete the route, are you sure?");
            if(!confirm) return;
            var model = arguments[1].model;
            model.destroy({
              wait : true,
              success : function() {
                app.reqres.request("toaster:show", i18n.t("routeDeleteSuccessMsg"));
              },
              error : function() {
                if(arguments[1].status == 409) {
                  app.reqres.request("toaster:show", i18n.t("routeDeleteConflictMsg"));
                } else {
                  app.reqres.request("toaster:show", i18n.t("routeDeleteErrorMsg"));
                }
              }
            });
          });

          retObj.view.on("childview:view:routes", function() {
            var model = arguments[1].model;
            var routes = retObj.items.where({vehicleId : model.get("vehicleId")});
            _.each(routes, function(route, index) {
              routes[index] = route.toJSON();
            })
            var view = new View.routeList({
              model : new Backbone.Model({
                vehicleRegistrationNo : model.get("vehicleRegistrationNo"),
                routes : routes
              })
            });
            app.modal.show(view);
          });

          retObj.view.on("childview:sendSms:route", function() {
            var model = arguments[1].model;
            internalObj.showSendSMSPrompt(model);
          });

          retObj.pageLayout = new View.pageView;
          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
          internalObj.initializeBulkSMS();
        }
      });
    }
  };

  var internalObj = {
    initializeBulkSMS : function() {
      $(".send-bulk-sms-all").off("click");
      $(".send-bulk-sms-all").on("click", function() {
        internalObj.showSendSMSPrompt(null);
      });
    },
    showSendSMSPrompt : function(model) {
      var formModel = Backbone.Model.extend({
        schema : {
          msg : { title : i18n.t('message'), type : "TextArea", validators : ['required'], editorAttrs : {rows : "4", cols : "5"}}
        }
      });

      var form = new formModel({
        msg : ""
      });

      var formView = new Backbone.Form({
        template : View.subItemForm,
        model : form,
        idPrefix : "send-bulk-sms-form-",
        templateData : {
          fieldFormHeaderText : !model ? "Send Bulk SMS to all students" : "Send bulk SMS to route " + model.get("name") + " (" + model.get("type") + ")",
          submitButtonText : i18n.t("send")
        }
      });

      formView.on("submit", function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate();
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        var msgBody = formView.model.get("msg");
        $.ajax({
          url : "/bulkSms",
          method : "POST",
          contentType : "application/json",
          data : JSON.stringify({
              msg : msgBody,
              routeIds : !model ? [] : [model.get("id")]
          }),
          success : function(response) {
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", response.msg);
            formView.trigger("modal:close");
          },
          error : function(error) {
            app.form.enableSubmit(formView);
            app.reqres.request("toaster:show", i18n.t("genericErrorMsg"));
            formView.trigger("modal:close");
          }
        });
      });
      app.modal.show(formView);
    }
  };

  return retObj;
});
