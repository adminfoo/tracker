<table class="list-table" style="width: 100%;">
    <thead>        
        <tr class="head">
            <th><%= _t("vehicleRegistrationNo") %></th>
            <th><%= _t("driverName") %></th>
            <th><%= _t("routeName") %></th>
            <th><%= _t("type") %></th>
            <th><%= _t("startTime") %></th>
            <th class="tac"><%= _t("actions") %></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>