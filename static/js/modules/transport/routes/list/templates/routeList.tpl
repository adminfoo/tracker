<div class="field-group-header"><%= vehicleRegistrationNo %> - Routes</div>
<table>
<% _.each(routes, function(route) { %>
	<tr>
		<td>
			<%= route.name %> 
		</td>
		<td>
			<%= route.type %>
		</td>
		<td>
			<%= new Date(route.startTime).format("shortTime") %>
		</td>
		<td class="action-button-container">
			<a href="#/transport/routes/info/edit/<%= route.name %> <%= route.type %> - <%= new Date(route.startTime).format("shortTime") %> (<%= vehicleRegistrationNo %>)/<%= route.id %>"><%= _t('edit') %></a>
		</td>
	</tr>
<% }) %>
</table>