<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageRoutes") %>
	</div>
</div>

<a href="#/transport/routes/new" class="new-cta">
	<div class="btn btn-primary"><i class="fa fa-plus"></i><%= _t("addNew") %></div>
</a>

<div class="btn btn-secondary send-bulk-sms-all"><%= _t("sendBulkSMSToAll") %></div>

<div class="transport-routes-container table-container">
</div>