<td><%= vehicleRegistrationNo %></td>
<td><%= driverName %></td>
<td><%= name %></td>
<td><%= type %></td>
<td><%= new Date(startTime).format("shortTime") %></td>
<td class="action-button-container">
	<a class="send-sms"><%= _t('Send bulk SMS') %></a>
	 |
	<a href="#/transport/routes/info/edit/<%= name %> <%= type %> - <%= new Date(startTime).format("shortTime") %> (<%= vehicleRegistrationNo %>)/<%= id %>"><%= _t('edit') %></a>
	 |
	<a class="delete" data-index="<%= id %>"><%= _t('delete') %></a>
</td>

