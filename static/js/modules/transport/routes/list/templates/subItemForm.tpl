<form class="send-sms-form">
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="msg"></div>
	</div>
	<div class="field-group">
		<input type="submit" class="btn btn-primary submit" value="<%= submitButtonText %>"/>
	</div>
</form>