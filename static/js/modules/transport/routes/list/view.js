define(["app", "tpl!transportRouteItemTemplate", "tpl!transportRouteListTemplate", "tpl!transportRoutePageTemplate", "tpl!transportRouteSubListTemplate", "tpl!transportRouteSubItemForm"],
       function(app, itemTemplate, listTemplate, pageTemplate, routeListTemplate, subItemForm){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .delete" : "delete:route",
          "click .view-routes" : "view:routes",
          "click .send-sms" : "sendSms:route"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.routeListEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    routeListEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="9" class="empty-collection"><%= _t("noRoutesAdded") %></td>'),
      tagName : "tr"
    }),
    routeList : Marionette.ItemView.extend({
      template : routeListTemplate
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        listContainer: ".transport-routes-container"
      }
    }),
    subItemForm : subItemForm
  };
  return retObj;
});
