define(["app", "commonView"], function(app, commonView){
  var retObj = {
    getSubNavigationItems : function(name,id) {
      return [{
          link : "#/transport/routes/info/edit/" + name + "/" + id,
          heading : i18n.t("routeInfo"),
          description : i18n.t("routeInfoTabMsg"),
          id : "info",
        },
        {
          link : "#/transport/routes/passengers/edit/" + name + "/" + id,
          heading : i18n.t("routePassengers"),
          description : i18n.t("routePassengersTabMsg"),
          id : "passengers"
        }];
    },
    showSubNavigation : function(name,id,active,region) {
      var subNavigationItems = retObj.getSubNavigationItems(name,id);
      subNavigationItems[_.findIndex(subNavigationItems, {id: active})].active = true;
      var subNavigation = commonView.subHeadingList({
        items : subNavigationItems
      });
      $(region.el).html(subNavigation);
      $(region.el).show();
      app.breadCrumbRegion.$el.addClass("sub-navigation-active");
    }
  };

  return retObj;
});
