<% if(data.lines && data.lines.length) { %>
    Line set:<br>
    <% _.each(data.lines, function(line, index) { %>
        <div>
            <input type="checkbox" id="chk_line_<%= index %>" data-index="<%= index %>" data-type="line"></input>
            <label style="color:<%= line.color %>;" for="chk_line_<%= index %>"><%= line.name %></label>
        </div>
    <% }) %>
<% } %>

<% if(data.markers && data.markers.length) { %>
    <br>
    Marker set:<br>
    <% _.each(data.markers, function(markerSetData, index) { %>
        <div>
            <input type="checkbox" id="chk_marker_<%= index %>" data-index="<%= index %>" data-type="markerSet"></input>
            <label style="color:<%= markerSetData.color %>;" for="chk_marker_<%= index %>" ><%= markerSetData.name %></label>
        </div>
    <% }) %>
<% } %>

<% if(data.data) { %>
    <br>
    Custom data:
    <div>
        <%= JSON.stringify(data.data, null, 2) %>
    </div>
<% } %>