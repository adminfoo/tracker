define(["app", "transportDataVisualizeView", "mapHandler", "gmap"], function(app, View, mapHandler){
  var retObj = {
    show : function() {
      retObj.pageLayout = new View.pageView;
      app.mainRegion.show(retObj.pageLayout);

      $(".end-point input").on("keypress", function(e) {
        if(e.which == 13) {
            internalObj.getData($(".end-point input").val()).then(function(data) {
               retObj.data = data;
               retObj.render();
            });
        }
      });

      retObj.map = new google.maps.Map($(".map-container .map-element")[0], {
        zoom: 6,
        center: {lat: 28, lng: 77},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        disableDoubleClickZoom: false,
        streetViewControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_TOP
        }
      });
    },
    drawOnMap : function() {
      internalObj.clearMap();
      // draw lines
      var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
      };
      _.each(retObj.selectedLines, function(lineData) {
        var line = new google.maps.Polyline({
          path: lineData.points,
          icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '200px'
          }],
          geodesic: true,
          strokeColor: lineData.color,
          strokeOpacity: 1.0,
          strokeWeight: 4
        });
        line.setMap(retObj.map);
        retObj.printedLines.push(line);
      });

      // draw markers
      var bounds = new google.maps.LatLngBounds();
      _.each(retObj.selectedMarkerSet, function(markerSetData) {
        var markerColor = markerSetData.color;
        _.each(markerSetData.points, function(item) {
          var options = {
            lat : item.lat,
            lng : item.lng,
            markerImage : {
              path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
              fillColor: markerColor,
              strokeColor: markerColor,
              strokeWeight: 10
            }
          }
          var marker = mapHandler.getMarker(options);
          var latlng = new google.maps.LatLng(item.lat, item.lng);
          var infowindow = new google.maps.InfoWindow({
            content: item.msg
          });
          marker.addListener('mouseover', function() {
            infowindow.open(retObj.map, marker);
          });
          marker.addListener('mouseout', function() {
            infowindow.close();
          });
          marker.setMap(retObj.map);
          retObj.markers.push(marker);
          bounds.extend(latlng);
        });
      });
      mapHandler.fitToBounds(retObj.map, bounds);
    },
    render : function() {
      retObj.dataView = new View.dataView({
        model : new Backbone.Model({
          data : retObj.data
        })
      });
      retObj.dataView.on("reRenderMap", function() {
        retObj.selectedMarkerSet = [];
        retObj.selectedLines = [];
        var elms = this.$el.find("input:checked");
        _.each(elms, function(elm) {
          var elm = $(elm);
          var index = elm.attr("data-index");
          var type = elm.attr("data-type");
          if(type == "markerSet") {
            retObj.selectedMarkerSet.push(retObj.data.markers[index]);
          } else if(type == "line") {
            retObj.selectedLines.push(retObj.data.lines[index]);
          }
        });
        retObj.drawOnMap();
      });
      retObj.pageLayout.dataContainer.show(retObj.dataView);
    }
  };

  var internalObj = {
    clearMap : function() {
      retObj.markers = retObj.markers || [];
      _.each(retObj.markers, function(marker) {
        marker.setMap(null);
      });
      retObj.markers = [];
      retObj.printedLines = retObj.printedLines || [];
      _.each(retObj.printedLines, function(line) {
        line.setMap(null);
      });
      retObj.printedLines = [];
    },
    clearReport : function() {
      retObj.pageLayout.dataContainer.empty();
      internalObj.clearMap();
    },
    getData : function(endPoint) {
      //var deferred = $.Deferred();
      //deferred.resolve({
      //  lines : [{
      //    name : "Line Checkbox label 1",
      //    color : "black",
      //    points : [{
      //      lat : 27,
      //      lng : 77
      //    }, {
      //      lat : 27.8383,
      //      lng : 78
      //    }]
      //  }],
      //  markers : [{
      //    name : "Marker Checkbox label 1",
      //    color : "red",
      //    points : [{
      //      lat : 27.1557,
      //      lng : 77.6363,
      //      msg : "Info window message"
      //    }]
      //  }],
      //  data : {
      //    randomeKey : "Random DATA"
      //  }
      //});
      //return deferred.promise();

      return $.ajax({
        url : endPoint,
        method : "GET"
      });
    }
  };

  return retObj;
});
