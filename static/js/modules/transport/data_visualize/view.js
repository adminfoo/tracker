define(["app", "tpl!transportDataVisualizePageTemplate", "tpl!transportDataVisualizeDataTemplate"],
       function(app, pageTemplate, dataTemplate){
  var retObj = {
    dataView : Marionette.ItemView.extend({
      template: dataTemplate,
      triggers : {
        "change input" : "reRenderMap"
      }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        dataContainer : ".data-container"
      },
      onShow : function() {
        $("body").addClass("custom-width100");
      }
    })
  };
  return retObj;
});
