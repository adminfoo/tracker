define(["app", "tpl!transportReportsPageTemplate", "tpl!transportReportsTripListTemplate", "tpl!transportReportsTripDetailTemplate", "tpl!transportReportsFilterFormTemplate", "tpl!transportReportsSummaryTemplate", "tpl!transportReportsDailyListTemplate"],
       function(app, pageTemplate, tripListTemplate, detailTemplate, filterForm, summaryView, dailyListTemplate){
  var retObj = {
    tripListView  : Marionette.ItemView.extend({
        template : tripListTemplate,
        events : {
          "click .view-detail-map" : "showTrip"
        },
        showTrip : function(event) {
          this.trigger("show:trip", event);
        },
    }),
    tripDetailView : Marionette.ItemView.extend({
        template : detailTemplate,
        triggers : {
          "click .close-button" : "close"
        }
    }),
    dailyListView  : Marionette.ItemView.extend({
        template : dailyListTemplate
    }),
    filterFormView : Marionette.ItemView.extend({
        template : filterForm,
        triggers : {
          "click .submit-filter" : "submit:filter",
          "click .download-report" : "download:report"
        }
    }),
    summaryView : Marionette.ItemView.extend({
        template : summaryView,
        templateHelpers : {
          beautifyTime : function(value) {
            if(vehicleId == value) {
              return "selected='selected'";
            }
          }
        }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        filterForm : ".filter-form-container",
        reportSummary : ".report-summary-container",
        listContainer: ".list",
        subNavigation : ".sub-heading-list"
      }
    })
  };
  return retObj;
});
