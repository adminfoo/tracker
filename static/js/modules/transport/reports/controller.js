define(["app", "transportReportsView", "mapHandler", "pikaday", "commonView"], function(app, View, mapHandler, Pikaday, commonView){
  var retObj = {
    show : function(vehicleId, range) {
      if(range == "currentMonth") {
        var from,to;
        from = new moment().subtract('month', 1).toDate().getTime();
        to = new moment().toDate().getTime();
        range = from+"-"+to;
      }
      internalObj.getVehicles().then(function(vehicles) {
        if(vehicleId != "All") {
          retObj.selectedVehcle = _.where(vehicles, {id : vehicleId})[0] || {};
        }
        retObj.pageLayout = new View.pageView();
        app.mainRegion.show(retObj.pageLayout);
        retObj.filterFormView = new View.filterFormView({
          model : new Backbone.Model({vehicleList : vehicles}),
          templateHelpers : {
            selected : function(value) {
              if(vehicleId == value) {
                return "selected='selected'";
              }
            }
          }
        });
        
        retObj.filterFormView.on("submit:filter", function() {
          var vehicleId = $("#vehicle-select").val();
          var vehicleRegNo;
          if(vehicleId == "All") {
            vehicleRegNo = "All"
          } else {
            vehicleRegNo = _.where(vehicles, {id : vehicleId})[0].registrationNo;
          }
          var url = "#/transport/reports/" + vehicleId + "/" + vehicleRegNo + "/" + retObj.fromPicker.getDate().getTime() + "-" + retObj.toPicker.getDate().getTime();
          window.location.href = url;
        });

        retObj.pageLayout.filterForm.show(retObj.filterFormView);
        internalObj.initCalendar(range);

        // show report summary
        internalObj.getSummary(vehicleId, range).then(function(response) {
          retObj.summaryView = new View.summaryView({
            model : new Backbone.Model(response)
          });
          retObj.pageLayout.reportSummary.show(retObj.summaryView);

          // show vehicle trips if single vehicle is selected
          if(vehicleId != "All") {
            retObj.currentFilterValues = {
              vehicleId : retObj.selectedVehcle.id,
              fromDate : retObj.fromPicker.getDate().getTime(),
              toDate : retObj.toPicker.getDate().getTime()
            };
            retObj.trips = {items: response.trips};
            retObj.daily = {items: response.daily};
            retObj._showDailyVehicleSummaryList();
          }
        });
      });
    },
    getSubNavigationItems : function() {
      return [{
          heading : i18n.t("dailyReportHeader"),
          description : i18n.t("dailySummaryInfoTabMsg"),
          id : "daily",
        },
        {
          heading : i18n.t("tripReportHeader"),
          description : i18n.t("tripListTabMsg"),
          id : "trips"
        }];
    },
    showSubNavigation : function(active,region) {
      var subNavigationItems = retObj.getSubNavigationItems();
      subNavigationItems[_.findIndex(subNavigationItems, {id: active})].active = true;
      var subNavigation = commonView.subHeadingList({
        items : subNavigationItems
      });
      $(region.el).html(subNavigation);
      $(region.el).show();
      app.breadCrumbRegion.$el.addClass("sub-navigation-active");
      internalObj.attachReportTypeEvent();
    },
    _showDailyVehicleSummaryList : function() {
      retObj.dailyVehicleSummaryListView = new View.dailyListView({
        model : new Backbone.Model(retObj.daily)
      });
      retObj.pageLayout.listContainer.show(retObj.dailyVehicleSummaryListView);
      retObj.showSubNavigation("daily", retObj.pageLayout.subNavigation);
    },
    _showTripList : function() {
      retObj.tripListView = new View.tripListView({
        model : new Backbone.Model(retObj.trips),
        templateHelpers : {
          durationText : function(durationInSec) {
            return (durationInSec / 60) < 10 ? "0" + Math.floor(durationInSec / 60) : Math.floor(durationInSec / 60);
          }
        }
      });
      retObj.tripListView.on("show:trip", function(event) {
        var elm = $(event.target || event.srcElement);
        var id = elm.attr("data-id");
        retObj.showTripDetail(id);
      });
      retObj.pageLayout.listContainer.show(retObj.tripListView);
      retObj.showSubNavigation("trips", retObj.pageLayout.subNavigation);
    },
    showTripDetail : function(id) {
      internalObj.getTrip(id).then(function(trip) {
        retObj.trip = trip;
        retObj.tripDetailView = new View.tripDetailView({
          model : new Backbone.Model(retObj.trip)
        });

        retObj.tripDetailView.on("close", function() {
          retObj.tripDetailView.trigger("fullScreen:close");
        });

        retObj.tripDetailView.on("show", function() {
          var map = new google.maps.Map(this.$el.find(".map-view")[0], {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
          });

          
          var flightPlanCoordinates = retObj.trip.points;
          var bounds = new google.maps.LatLngBounds();
          _.each(flightPlanCoordinates, function(point) {
            var latlng = new google.maps.LatLng(point.lat, point.lng);
            bounds.extend(latlng);
            var marker = internalObj.setMarker(point);
            var infoContent = "Speed : " + point.speed + "Km/h" + "<br>" + "Time : " + new Date(point.time).format("dd mmm yyyy h:MM TT");
            var infowindow = new google.maps.InfoWindow({
              content: infoContent
            });
            google.maps.event.clearListeners(marker, 'mouseover');
            marker.addListener('mouseover', function() {
              infowindow.open(map, marker);
            });
            google.maps.event.addListener(infowindow,'closeclick',function(){
               marker.doNotCloseOnMouseOut = false;
            });
            marker.addListener('click', function() {
              infowindow.open(map, marker);
              marker.doNotCloseOnMouseOut = true;
            });
            marker.addListener('mouseout', function() {
              if(!marker.doNotCloseOnMouseOut) infowindow.close();
            });
            marker.setMap(map)
          });
          // Define a symbol using a predefined path (an arrow)
          // supplied by the Google Maps JavaScript API.
          var iconsetngs = {
              path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
          };
          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            strokeOpacity: 0.8,
            strokeWeight: 3,
            map: map,
            icons: [{
                icon: iconsetngs,
                repeat:'60px',
                offset: '100%'
            }]
          });
          flightPath.setMap(map);
          mapHandler.fitToBounds(map, bounds);
        });

        app.fullScreen.show(retObj.tripDetailView);
      });
    },
  };

  var internalObj = {
    initCalendar : function(range) {
      var fromDate,toDate;
      fromDate = new Date(parseInt(range.split("-")[0],10));
      toDate = new Date(parseInt(range.split("-")[1], 10));
      retObj.fromPicker = new Pikaday({
        field: $('#from-date')[0],
        format : "DD MMM YYYY",
        maxDate : new Date(),
        defaultDate : fromDate,
        setDefaultDate : true
      });
      retObj.toPicker = new Pikaday({
        field: $('#to-date')[0],
        maxDate : new Date(),
        format : "DD MMM YYYY",
        defaultDate : toDate,
        setDefaultDate : true
      });
    },
    getTrip : function(id) {
      return $.ajax({
        url: "/vehicleTrips/" + id
      });
    },
    getVehicles : function() {
      return $.ajax({
        url: "/transportVehicles"
      });
    },
    getTrips : function(vehicleId, range) {
      return $.ajax({
        url: "/vehicleTrips?vehicleId="+vehicleId+"&fromDate="+range.split("-")[0]+"&toDate="+range.split("-")[1]
      });
    },
    getSummary : function(vehicleId, range) {
      return $.ajax({
        url: "/vehicleSummary?vehicleId="+vehicleId+"&fromDate="+range.split("-")[0]+"&toDate="+range.split("-")[1]
      });
    },
    setMarker : function(item) {
        var icon = {
          path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
          fillColor: 'black',
          fillOpacity: 0.8,
          scale: 0.1,
          strokeColor: 'black',
          strokeWeight: 5
        };
        if(app.havePermission(["superadmin"])) {
          icon.strokeWeight = 10;
          if(item.speed == 0) {
            icon.strokeColor = "red";
          } else {
            icon.strokeColor = "green";
          }
        } else {
          if(item.speed > retObj.selectedVehcle.speedLimit) {
            icon.strokeColor = "red";
            icon.scale = 2;
            icon.strokeWeight = 10;
          }
        }
        var options = {
            lat : item.lat,
            lng : item.lng,
            markerImage : icon
        };
        var marker = mapHandler.getMarker(options);
        if(!marker) {
            return;
        }
        return marker;
    },
    attachReportTypeEvent : function() {
      $(".sub-heading-list .sub-heading-item").off("click");
      $(".sub-heading-list .sub-heading-item").on("click", function() {
        var id = $(this).attr("data-id");
        if(id == "trips") {
          retObj._showTripList();
        } else if(id == "daily") {
          retObj._showDailyVehicleSummaryList();
        }
      });

      $(".vehicle-summary-detail-section .download-report").off("click");
      $(".vehicle-summary-detail-section .download-report").on("click", function() {
        var id = $(this).attr("data-id");
        var url;
        if(id == "trips") {
          url = "/pdf/report/vehicleSummary/" + retObj.currentFilterValues.vehicleId + "/" + retObj.currentFilterValues.fromDate + "-" + retObj.currentFilterValues.toDate + "?reportType=trips";
        } else if(id == "daily") {
          url = "/pdf/report/vehicleSummary/" + retObj.currentFilterValues.vehicleId + "/" + retObj.currentFilterValues.fromDate + "-" + retObj.currentFilterValues.toDate + "?reportType=daily";
        } else if(id == "daily") {
        }
        window.open(url);
      });
    }
  };

  return retObj;
});
