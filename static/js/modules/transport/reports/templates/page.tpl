<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("xyz") %>
	</div>
</div>
<div class="filter-form-container">
</div>
<div class="report-summary-container">
	Loading...
</div>
<div class="sub-heading-list">
</div>
<div class="vehicle-summary-detail-section">
	<div class="list table-container">
		<table class="list-table trip-list-table" style="width: 100%;">
			<tbody>
				<tr>
					<td colspan="5">Select a specific vehicle from the list above to see the daily summary  &amp; all trips in specified duration</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>