<button type="submit" class="btn btn-primary download-report" data-id="trips">
	<i class="fa fa-download fa-lg"></i>&nbsp;Download
</button>
<table class="list-table trip-list-table" style="width: 100%;">
	<thead>
		<tr class="head">
			<th>Time</th>
			<th>Distance</th>
			<th>Duration</th>
			<th>Overspeeding</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<% _.each(items, function(item) { %>
			<tr class="trip-item">
				<td><%= new Date(item.startTime).format("ddd d mmm '<b>'h:MM TT'</b>'") + '&nbsp;to&nbsp;' + new Date(item.endTime).format("'<b>'h:MM TT'</b>'") %></td>
				<td><%= item.distanceText %></td>
				<td><%= item.tripDurationText %></td>
				<td><%= item.overspeedingText %></td>
				<td class="action-button-container">
					<div data-id="<%= item.id %>" class="btn btn-primary small view-detail-map"><%= _t('View on map') %></div>
				</td>
			</tr>
		<% }) %>
		<% if(items.length == 0) { %>
			<tr>
				<td colspan="8" class="empty-collection"><%= _t("noTripsFound") %></td>
			</tr>
		<% } %>
	</tbody>
</table>