<div class="row">
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-road" aria-hidden="true" style="color:#7D7C7C;"></i> -->
			<img src="/static/img/scaled/road.png">
		</div>
		<div class="text">
			<div class="label">Total Km Run</div>
			<div class="value"><%= distanceText %></div>
		</div>
	</div>
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-clock-o" aria-hidden="true" style="color:#A9A93B;"></i> -->
			<img src="/static/img/scaled/clock.png">
		</div>
		<div class="text">
			<div class="label">Running duration</div>
			<div class="value"><%= runningDurationText %></div>
		</div>
	</div>
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-tachometer" aria-hidden="true" style="color:#C35656;"></i> -->
			<img src="/static/img/scaled/overspeed.png">
		</div>
		<div class="text">
			<div class="label">Overspeed Percent</div>
			<div class="value"><%= overspeedPercent %> %</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-truck" aria-hidden="true" style="color:#418A7D;"></i> -->
			<img src="/static/img/scaled/truck.png">
		</div>
		<div class="text">
			<div class="label">Daily Vehicle Run</div>
			<div class="value"><%= dailyVehicleKm %> Km</div>
		</div>
	</div>
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-hourglass-start" aria-hidden="true" style="color:purple;font-size: 40px;"></i> -->
			<img src="/static/img/scaled/idle.png">
		</div>
		<div class="text">
			<div class="label">Idling time</div>
			<div class="value"><%= idleDurationText %></div>
		</div>
	</div>
	<div class="item">
		<div class="icon">
			<!-- <i class="fa fa-toggle-on" aria-hidden="true" style="color:#30C148;"></i> -->
			<img src="/static/img/scaled/ac.png">
		</div>
		<div class="text">
			<div class="label">Total AC time</div>
			<div class="value"><%= acDurationText %></div>
		</div>
	</div>
</div>