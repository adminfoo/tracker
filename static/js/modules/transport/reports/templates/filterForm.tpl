<div class="i-b box">
	<label class="form-label" for="vehicle-select">Select vehicle</label>
	<div class="select-container month-select">
		<select class="form-control" id="vehicle-select" style="width: 200px;">
			<option value="All" <%= selected("All")%>>All</option>
			<% _.each(vehicleList, function(vehicle) { %>
				<option value="<%= vehicle.id %>" <%= selected(vehicle.id)%>><%= vehicle.registrationNo %></option>
			<% }) %>
		</select>
	</div>
</div>
<div class="i-b box">
	<label class="form-label" for="from-date">From</label>
	<div class="form-element">
		<span data-editor="">
			<input id="from-date" class="form-control" name="from" type="text">
		</span>
	</div>
</div>
<div class="i-b box">
	<label class="form-label" for="to-date">To</label>
	<div class="form-element">
		<span data-editor="">
			<input id="to-date" class="form-control" name="to" type="text">
		</span>
	</div>
</div>
<div class="i-b box">
	<input type="submit" class="btn btn-primary submit submit-filter" value="Submit">
</div>