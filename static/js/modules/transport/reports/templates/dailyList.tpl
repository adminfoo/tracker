<button type="submit" class="btn btn-primary download-report" data-id="daily">
	<i class="fa fa-download fa-lg"></i>&nbsp;Download
</button>
<table class="list-table daily-list-table" style="width: 100%;">
	<thead>
		<tr class="head">
			<th>Date</th>
			<th>Distance</th>
			<th>No of trips</th>
			<th>Running duration</th>
			<th>Ac ON duration</th>
			<th>Idle duration</th>
		</tr>
	</thead>
	<tbody>
		<% _.each(items, function(item) { %>
			<tr class="trip-item">
				<td><%= new Date(item.date).format("ddd d mmm") %></td>
				<td><%= item.distanceText %></td>
				<td><%= item.tripsCount %></td>
				<td><%= item.runningDurationText %></td>
				<td><%= item.acDurationText %></td>
				<td><%= item.idleDurationText %></td>
			</tr>
		<% }) %>

		<% if(items.length == 0) { %>
			<tr>
				<td colspan="8" class="empty-collection"><%= _t("No summary found.") %></td>
			</tr>
		<% } %>
	</tbody>
</table>