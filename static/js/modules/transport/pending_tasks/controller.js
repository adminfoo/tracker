define(["app", "transportPendingTasksListView"], function(app, View){
  var retObj = {
    staffModel : Backbone.Model.extend({
        urlRoot : "/superadminTasks"
    }),
    list : function() {
      var item = retObj.staffModel;
      var itemsCollection = Backbone.Collection.extend({
        url : "/superadminTasks",
        model : item
      });

      var items = new itemsCollection();

      items.fetch({
        success : function(collection, response, options) {
          retObj.view = new View.items({
            collection : collection
          });

          retObj.pageLayout = new View.pageView;
          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
        }
      });
    }
  };

  var internalObj = {};

  return retObj;
});
