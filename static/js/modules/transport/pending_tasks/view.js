define(["app", "tpl!transportPendingTasksItemTemplate", "tpl!transportPendingTasksListTemplate", "tpl!transportPendingTasksPageTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr"
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.listEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    listEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="3" class="empty-collection"><%= _t("noPendingTasks") %></td>'),
      tagName : "tr"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        listContainer: ".task-container"
      }
    })
  };
  return retObj;
});
