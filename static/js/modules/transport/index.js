define(["app"], function(app){

  var router = Marionette.AppRouter.extend({
    appRoutes: {
      "transport/manage-vehicle" : "manageVehicle",
      "transport/manage-staff" : "manageStaff",
      "transport/manage-stops" : "manageStops",
      "transport/stops/new" : "newStop",
      "transport/stops/edit/*name/*id" : "editStop",
      "transport/routes/new" : "newRoute",
      "transport/routes/info/edit/*name/*id" : "editRouteInfo",
      "transport/routes/passengers/edit/*name/*id" : "editRoutePassengers",
      "transport/manage-routes" : "manageRoute",
      "vehicle/(*detail)" : "viewVehicleTrips",
      "transport/track" : "showLiveTracking",
      "" : "loadDefaultPage",
      "transport/tasks" : "pendingTasks",
      "manage-schools" : "manageSchools",
      "transport/eta-report" : "etaReport",
      "transport/data-visualize" : "dataVisualize",
      "transport/internal-dashboard" : "internalDashboard",
      "password/change" : "changePassword",
      "transport/reports/*vehicleId/*vehicleRegNo/*range" : "showReport"
    }
  });

  var API = {
    manageVehicle : function() {
      app.activeHeader("vehicle");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        label : i18n.t("manageVehicles")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportVehicle"], function(transportVehicle) {
          app.executeIfPathActive("transport/manage-vehicle", function() {
            app.closeMenu();
            transportVehicle.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    manageStaff : function() {
      app.activeHeader("staff");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        label : i18n.t("manageStaff")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportStaff"], function(transportStaff) {
          app.executeIfPathActive("transport/manage-staff", function() {
            app.closeMenu();
            transportStaff.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    manageStops : function() {
      app.activeHeader("stop");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        label : i18n.t("manageStops")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportStopsList"], function(transportStopsList) {
          app.executeIfPathActive("transport/manage-stops", function() {
            app.closeMenu();
            transportStopsList.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    newStop : function() {
      app.activeHeader("stop");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        link : "transport/manage-stops",
        label : i18n.t("manageStops")
      },{
        label : i18n.t("addNewStop")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportStopsAdd"], function(transportStopsAdd) {
          app.executeIfPathActive("transport/stops/", function() {
            app.closeMenu();
            transportStopsAdd.addStop();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    editStop : function(name, id) {
      app.activeHeader("stop");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        link : "transport/manage-stops",
        label : i18n.t("manageStops")
      },{
        label : name
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportStopsAdd"], function(transportStopsAdd) {
          app.executeIfPathActive("transport/stops/", function() {
            app.closeMenu();
            transportStopsAdd.editStop(id);
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    showLiveTracking : function() {
      app.activeHeader("live");
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportLive"], function(transportLive) {
          app.executeIfPathActive("transport/track,", function() {
            app.closeMenu();
            transportLive.show();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    newRoute : function() {
      app.activeHeader("route");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        link : "transport/manage-routes",
        label : i18n.t("manageRoutes")
      }, {
        label : i18n.t("addNewRoute")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportRouteAdd"], function(transportRouteAdd) {
          app.executeIfPathActive("transport/routes/new", function() {
            app.closeMenu();
            transportRouteAdd.addNew();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    editRouteInfo : function(name,id) {
      app.activeHeader("route");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      }, {
        link : "transport/manage-routes",
        label : i18n.t("manageRoutes")
      }, {
        label : name
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportRouteAdd"], function(transportRouteAdd) {
          app.executeIfPathActive("transport/routes/info/edit", function() {
            app.closeMenu();
            transportRouteAdd.editRoute(name,id);
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    editRoutePassengers : function(name,id) {
      app.activeHeader("route");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      }, {
        link : "transport/manage-routes",
        label : i18n.t("manageRoutes")
      }, {
        label : name
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportRoutePassenger"], function(transportRoutePassenger) {
          app.executeIfPathActive("transport/routes/passengers/edit", function() {
            app.closeMenu();
            transportRoutePassenger.list(name,id);
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    manageRoute : function() {
      app.activeHeader("route");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      },{
        label : i18n.t("manageRoutes")
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportRouteList"], function(transportRouteList) {
          app.executeIfPathActive("transport/manage-routes", function() {
            app.closeMenu();
            transportRouteList.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    viewVehicleTrips : function(detail) {
      app.activeHeader("vehicle");
      var data = {
        regNo : detail.split("/")[0],
        id : detail.split("/")[1]
      };
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      }, {
        link : "transport/manage-vehicle",
        label : i18n.t("manageVehicles")
      }, {
        label : data.regNo
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportVehicleShow"], function(transportVehicleShow) {
          app.executeIfPathActive("vehicle/", function() {
            app.closeMenu();
            transportVehicleShow.showTripList(data);
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    pendingTasks : function() {
      app.activeHeader("superadminTasks");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      }, {
        label : i18n.t("pendingTasks")
      }]);
      app.showPageLoading();
      if(app.havePermission(["superadmin"])) {
        require(["transportPendingTasksList"], function(transportPendingTasksList) {
          app.executeIfPathActive("transport/tasks,", function() {
            app.closeMenu();
            transportPendingTasksList.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    manageSchools : function() {
      app.activeHeader("schools");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        isMenu : true,
        showLink : true,
        label : "transport" //e.g hr, student, admin
      }, {
        label : i18n.t("manageSchools")
      }]);
      app.showPageLoading();
      if(app.havePermission(["superadmin"])) {
        require(["adminSchoolList"], function(adminSchoolList) {
          app.executeIfPathActive("manage-schools,", function() {
            app.closeMenu();
            adminSchoolList.list();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    etaReport : function() {
      app.activeHeader("eta-report");
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["superadmin"])) {
        require(["transportETAReport"], function(transportETAReport) {
          app.executeIfPathActive("transport/eta-report", function() {
            app.closeMenu();
            transportETAReport.showReport();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    dataVisualize : function() {
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["superadmin"])) {
        require(["transportDataVisualize"], function(transportDataVisualize) {
          app.executeIfPathActive("transport/data-visualize", function() {
            app.closeMenu();
            transportDataVisualize.show();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    internalDashboard : function() {
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["superadmin"])) {
        require(["internalDashboard"], function(internalDashboard) {
          app.executeIfPathActive("transport/internal-dashboard", function() {
            app.closeMenu();
            internalDashboard.show();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    showDashboard : function() {
      app.activeHeader("dashboard");
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["adminDashboard"], function(adminDashboard) {
          app.executeIfPathActive("", function() {
            app.closeMenu();
            adminDashboard.showDashboard();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    showReport : function(vehicleId, vehicleRegNo, range) {
      if(vehicleRegNo == null) vehicleRegNo = "All";
      app.activeHeader("reports");
      app.showBreadCrumb([{
        label : i18n.t("mainMenu"),
        isMenuButton : true
      },
      {
        link : "transport/reports/All//currentMonth",
        label : i18n.t("viewReports")
      }, {
        label : vehicleRegNo
      }]);
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["transportReports"], function(transportReports) {
          app.executeIfPathActive("transport/reports/", function() {
            app.closeMenu();
            transportReports.show(vehicleId, range);
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    },
    loadDefaultPage : function() {
      if(app.havePermission(["superadmin"]) && !window.schoolId) {
        API.pendingTasks();
      } else {
        API.showDashboard();
      }
    },
    changePassword : function() {
      // app.showBreadCrumb([{
      //   label : i18n.t("mainMenu"),
      //   isMenuButton : true
      // },
      // {
      //   isMenu : true,
      //   showLink : true,
      //   label : "transport" //e.g hr, student, admin
      // }, {
      //   label : i18n.t("manageSchools")
      // }]);
      app.clearBreadCrumb();
      app.showPageLoading();
      if(app.havePermission(["manage_transport"])) {
        require(["passwordChangeController"], function(passwordChangeController) {
          app.executeIfPathActive("password/change,", function() {
            app.closeMenu();
            passwordChangeController.showForm();
          });
        });
      } else {
        app.showForbiddenMessage();
      }
    }
  };

  new router({
    controller: API
  });

  if(app.havePermission(["superadmin"])) {
    $.ajax({
      url : "/schools",
      success : function(response) {
        var schoolUrlMap = {'0': '/selectSchool/0'};
        var html = "<option value='' disabled selected>Select a school</option>";
        html += "<option value='0'>Use as Admin</option>";
        _.each(response, function(item) {
          schoolUrlMap[item.id] = item.url
          html += "<option value='" + item.id + "'>" + item.name + "</option>";
        });
        $(".school-list select").html(html);
        $(".open-school-dashboard").on("click", function() {
          var id = $(".school-list select").val();
          var win = window.open(schoolUrlMap[id], '_blank');
        });
        if(window.schoolId) {
          $(".school-list select").val(window.schoolId);
        }
        $(".school-list").show();
      }
    });
  }
});
