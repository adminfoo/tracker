define(["app", "transportStaffView"], function(app, View){
  var retObj = {
    staffModel : Backbone.Model.extend({
        urlRoot : "/drivers"
    }),
    list : function() {
      var item = retObj.staffModel;
      var itemsCollection = Backbone.Collection.extend({
        url : "/drivers",
        model : item
      });

      var items = new itemsCollection();

      items.fetch({
        success : function(collection, response, options) {
          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:edit:staff", function() {
            var model = arguments[1].model;
            internalObj.showEditSubItem(model, false);
          });

          retObj.view.on("childview:delete:staff", function() {
            var model = arguments[1].model;
            model.destroy({
              wait : true,
              success : function() {
                app.reqres.request("toaster:show", i18n.t("driverDeleteSuccessMsg"));
              },
              error : function() {
                if(arguments[1].status == 409) {
                  app.reqres.request("toaster:show", i18n.t("driverDeleteConflictMsg"));
                } else {
                  app.reqres.request("toaster:show", i18n.t("driverDeleteErrorMsg"));
                }
              }
            });
          });

          retObj.pageLayout = new View.pageView;

          retObj.pageLayout.on("new:staff", function() {
            var Model = retObj.staffModel;
            var model = new Model({
              name : "",
              phone : "",
              role : "driver",
              licenseNo : ""
            });
            internalObj.showEditSubItem(model, true);
          });

          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
        }
      });
    }
  };

  var internalObj = {
    showEditSubItem : function(model, isNew) {
      var roleOption  = [{
        val : "driver",
        label : i18n.t("Driver")
      }, {
        val : "helper",
        label : i18n.t("Helper")
      }];
      var formModel = Backbone.Model.extend({
        schema : {
          name : { title : i18n.t('name') + "*", validators : ['required']},
          phone : { title : i18n.t('phoneNo') + "*", validators : ['required']},
          role :   { title : i18n.t('role') + "*", type : "Select", options : roleOption},
          licenseNo :   { title : i18n.t('licenseNo')}
        },
        validate: function(attrs, options) {
          var role = attrs.role;
          var licenseNo = attrs.licenseNo;
          if(role == "driver" && licenseNo.trim() == "") {
            return {
              licenseNo : i18n.t("licenseNoRequiredForDriver")
            }
          }
        }
      });

      var form = new formModel({
        name : model.get("name"),
        phone : model.get("phone"),
        role : model.get("role"),
        licenseNo : model.get("licenseNo")
      });

      var formView = new Backbone.Form({
        template : View.subItemForm,
        model : form,
        idPrefix : "staff-form-",
        templateData : {
          isNew : isNew,
          fieldFormHeaderText : isNew ? i18n.t("addNew") : i18n.t("Edit"),
          submitButtonText : isNew ? i18n.t("save") : i18n.t("update"),
          successMsg : isNew ? i18n.t("addSuccessMsg") : i18n.t("editSuccessMsg"), 
        },
        originalModel : model,
        isNew : isNew
      });

      formView.on("submit", function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate({ validate: true });
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        _.extend(formView.options.originalModel.attributes, formView.model.toJSON());
        formView.options.originalModel.save(formView.options.originalModel.attributes, {
          patch : true,
          success : function(model, response, xhr) {
              app.form.enableSubmit(formView);
              app.reqres.request("toaster:show", formView.templateData.successMsg);
              formView.trigger("modal:close");
              if(formView.options.isNew) {
                retObj.view.collection.add(model);
              }
              retObj.view.render();
            }
        });
      });
      app.modal.show(formView);
    }
  };

  return retObj;
});
