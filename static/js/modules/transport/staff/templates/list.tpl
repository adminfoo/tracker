<table class="list-table">
    <thead>        
        <tr class="head">
            <th><%= _t("name") %></th>
            <th><%= _t("phone") %></th>
            <th><%= _t("role") %></th>
            <th><%= _t("licenseNo") %></th>
            <th class="tac"><%= _t("actions") %></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>