<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageDrivers") %>
	</div>
</div>
<div class="btn btn-primary create-new-staff new-cta"> <i class="fa fa-plus"></i>
		<%= _t("addNew") %>
	</div>
<div class="staff-container table-container">
</div>