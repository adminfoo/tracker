<form>
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="name,phone,role,licenseNo"></div>
	</div>
	<div class="field-group">
		<input type="submit" class="btn btn-primary submit align-with-form" value="<%= submitButtonText %>"/>
	</div>
</form>