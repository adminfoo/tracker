define(["app", "tpl!transportStaffItemTemplate", "tpl!transportStaffListTemplate", "tpl!transportStaffPageTemplate", "tpl!transportStaffSubItemFormTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate, subItemForm){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .edit" : "edit:staff",
          "click .delete" : "delete:staff"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.staffListEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    staffListEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="5" class="empty-collection"><%= _t("noStaff") %></td>'),
      tagName : "tr"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .create-new-staff" : "new:staff"
      },
      regions: {
        listContainer: ".staff-container"
      }
    }),
    subItemForm : subItemForm
  };
  return retObj;
});
