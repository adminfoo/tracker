define(["app", "tpl!transportVehicleShowPageTemplate", "tpl!transportVehicleShowTripListTemplate", "tpl!transportVehicleShowTripDetailTemplate"],
       function(app, pageTemplate, tripListTemplate, detailTemplate){
  var retObj = {
    tripListView  : Marionette.ItemView.extend({
        template : tripListTemplate,
        triggers : {
          "change #range-select" : "change:period"
        },
        events : {
          "click .view-detail-map" : "showTrip"
        },
        showTrip : function(event) {
          this.trigger("show:trip", event);
        },
    }),
    tripDetailView : Marionette.ItemView.extend({
        template : detailTemplate,
        triggers : {
          "click .close-button" : "close"
        }
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        tripList: ".trip-list",
        tripDetail: ".trip-detail"
      }
    })
  };
  return retObj;
});
