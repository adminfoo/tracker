define(["app", "transportVehicleShowView", "mapHandler"], function(app, View, mapHandler){
  var retObj = {
    showTripList : function(data) {
      retObj.data = data;
      retObj.data.period = new Date().getMonth() + 1;
      internalObj.getTrips(retObj.data).then(function(res){
        retObj.trips = {trips: res};
        retObj._showList();
      });
    },
    showTripDetail : function(id) {
      internalObj.getTrip(id).then(function(trip) {
        retObj.trip = trip;
        retObj.tripDetailView = new View.tripDetailView({
          model : new Backbone.Model(retObj.trip)
        });

        retObj.tripDetailView.on("close", function() {
          retObj.tripDetailView.trigger("fullScreen:close");
        });

        retObj.tripDetailView.on("show", function() {
          var map = new google.maps.Map(this.$el.find(".map-view")[0], {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
          });

          
          var flightPlanCoordinates = retObj.trip.points;
          var bounds = new google.maps.LatLngBounds();
          _.each(flightPlanCoordinates, function(point) {
            var latlng = new google.maps.LatLng(point.lat, point.lng);
            bounds.extend(latlng);
            var marker = internalObj.setMarker(point);
            var infoContent = "Speed : " + point.speed + "Km/h" + "<br>" + "Time : " + new Date(point.time).format("dd mmm yyyy h:MM TT");
            var infowindow = new google.maps.InfoWindow({
              content: infoContent
            });
            google.maps.event.clearListeners(marker, 'mouseover');
            marker.addListener('mouseover', function() {
              infowindow.open(map, marker);
            });
            google.maps.event.addListener(infowindow,'closeclick',function(){
               marker.doNotCloseOnMouseOut = false;
            });
            marker.addListener('click', function() {
              infowindow.open(map, marker);
              marker.doNotCloseOnMouseOut = true;
            });
            marker.addListener('mouseout', function() {
              if(!marker.doNotCloseOnMouseOut) infowindow.close();
            });
            marker.setMap(map)
          });
          // Define a symbol using a predefined path (an arrow)
          // supplied by the Google Maps JavaScript API.
          var iconsetngs = {
              path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
          };
          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            strokeOpacity: 0.8,
            strokeWeight: 3,
            map: map,
            icons: [{
                icon: iconsetngs,
                repeat:'60px',
                offset: '100%'
            }]
          });
          flightPath.setMap(map);
          mapHandler.fitToBounds(map, bounds);
        });

        app.fullScreen.show(retObj.tripDetailView);
      });
    },
    _showList : function() {
      retObj.tripListView = new View.tripListView({
        model : new Backbone.Model(retObj.trips),
        templateHelpers : {
          selected : function(value) {
            if(retObj.data.period == value) {
              return "selected='selected'";
            }
          }
        }
      });

      retObj.tripListView.on("change:period", function() {
        retObj.data.period = this.$el.find("#range-select").val();
        retObj.pageLayout.tripList.$el.find(".trip-list-table tbody").html('<td colspan="8" class="tac">Loading...</td>');
        internalObj.getTrips(retObj.data).then(function(trips){
          retObj.trips = {trips: trips};
          retObj._showList();
        });
      });

      retObj.tripListView.on("show:trip", function(event) {
        var elm = $(event.target || event.srcElement);
        var id = elm.attr("data-id");
        retObj.showTripDetail(id);
      });

      retObj.pageLayout = new View.pageView();
      app.mainRegion.show(retObj.pageLayout);
      retObj.pageLayout.tripList.show(retObj.tripListView);
    }
  };

  var internalObj = {
    getTrip : function(id) {
      return $.ajax({
        url: "/vehicleTrips/" + id
      });
    },
    getTrips : function(data) {
      return $.ajax({
        url: "/vehicleTrips?vehicleId=" + data.id + "&month=" + retObj.data.period
      });
    },
    setMarker : function(item) {
        var icon = {
          path: 'M-0.2,0a0.2,0.2 0 1,0 0.4,0a0.2,0.2 0 1,0 -0.4,0',
          fillColor: 'black',
          fillOpacity: 0.8,
          scale: 0.1,
          strokeColor: 'black',
          strokeWeight: 5
        };
        if(app.havePermission(["superadmin"])) {
          icon.strokeWeight = 10;
          if(item.speed == 0) {
            icon.strokeColor = "red";
          } else {
            icon.strokeColor = "green";
          }
        }
        var options = {
            lat : item.lat,
            lng : item.lng,
            markerImage : icon
        };
        var marker = mapHandler.getMarker(options);
        if(!marker) {
            return;
        }
        return marker;
    }
  };

  return retObj;
});
