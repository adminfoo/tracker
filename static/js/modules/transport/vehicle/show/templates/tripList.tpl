<div class="select-container month-select">
	<select class="form-control" id="range-select">
		<% _.each(app.request("common:months"), function(month) { %>
			<option value="<%= month.val %>" <%= selected(month.val)%> ><%= month.label %></option>
		<% }) %>
	</select>
</div>
<table class="list-table trip-list-table" style="width: 100%;">
	<thead>
		<tr class="head">
			<th>Start time</th>
			<th>End time</th>
			<th>Distance (Km)</th>
			<th>Duration (mins)</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<% _.each(trips, function(trip) { %>
			<tr class="trip-item">
				<td><%= new Date(trip.startTime).format("dddd d mmm h:MM TT") %></td>
				<td><%= new Date(trip.endTime).format("dddd d mmm h:MM TT") %></td>
				<td><%= trip.distance.toFixed(1) %></td>
				<td><%= ((trip.tripDuration / 60) < 10 ? "0" + Math.floor(trip.tripDuration / 60) : Math.floor(trip.tripDuration / 60)) + ":" + ((trip.tripDuration % 60)  < 10 ? "0" + (trip.tripDuration % 60) : (trip.tripDuration % 60)) %></td>
				<td class="action-button-container">
					<div data-id="<%= trip.id %>" class="btn btn-primary small view-detail-map"><%= _t('View Detail') %></div>
				</td>
			</tr>
		<% }) %>

		<% if(trips.length == 0) { %>
			<tr>
				<td colspan="8" class="empty-collection"><%= _t("noTripsFound") %></td>
			</tr>
		<% } %>
	</tbody>
</table>