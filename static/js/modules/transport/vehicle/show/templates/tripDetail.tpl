<div class="trip-detail-page">
<div class="header">
	<div class="driver-info i-b">
		<div class="name i-b">
			<label>Time</label>
			<div><%= new Date(startTime).format("dddd d mmm '<b>'h:MM TT'</b>'") + '&nbsp;to&nbsp;' + new Date(endTime).format("'<b>'h:MM TT'</b>'") %></div>
		</div>
		<div class="vehicle-regno i-b">
			<label>Registration no</label>
			<div><%= vehicleRegistrationNo %></div>
		</div>
	</div>
	<div class="i-b rfloat close-button">
		<div class="btn btn-secondary">close</div>
	</div>
</div>
<div class="main-content">
	<div class="map-view">
	</div>
</div>
</div>