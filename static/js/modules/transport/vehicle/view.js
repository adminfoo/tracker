define(["app", "tpl!transportVehicleItemTemplate", "tpl!transportVehicleListTemplate", "tpl!transportVehiclePageTemplate", "tpl!transportVehicleSubItemFormTemplate"],
       function(app, itemTemplate, listTemplate, pageTemplate, subItemForm){
  var retObj = {
    item  : Marionette.ItemView.extend({
        template : itemTemplate,
        tagName : "tr",
        triggers : {
          "click .edit" : "edit:vehicle",
          "click .delete" : "delete:vehicle"
        }
    }),
    items : Marionette.CompositeView.extend({
        getChildView : function() {
          return retObj.item;
        },
        getEmptyView: function() {
          // custom logic
          return retObj.vehicleListEmptyView;
        },
        template : listTemplate,
        childViewContainer: "tbody"
    }),
    vehicleListEmptyView : Marionette.ItemView.extend({
      template: _.template('<td colspan="4" class="empty-collection"><%= _t("noVehicles") %></td>'),
      tagName : "tr"
    }),
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      triggers : {
        "click .create-new-vehicle" : "new:vehicle"
      },
      regions: {
        listContainer: ".vehicles-container"
      }
    }),
    subItemForm : subItemForm
  };
  return retObj;
});
