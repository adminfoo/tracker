
<td><%= name %></td>
<td><%= registrationNo %></td>
<% if(app.havePermission(["superadmin"])) { %>
	<td><%= deviceId %></td>
<% } %>
<td><%= speedLimit %></td>
<td><%= noOfSeats %></td>
<td class="action-button-container">
	<a class="view-detail" href="#/transport/reports/<%= id %>/<%= registrationNo %>/currentMonth" data-index="<%= id %>"><%= _t('viewPreviousTrips') %></a>
		|
	<a class="edit" data-index="<%= id %>"><%= _t('edit') %></a>
	<% if(app.havePermission(["superadmin"])) { %>
			|
		<a class="delete" data-index="<%= id %>"><%= _t('delete') %></a>
	<% } %>
</td>