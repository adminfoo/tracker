<table class="list-table" style="width: 100%;">
    <thead>        
        <tr class="head">
            <th><%= _t("name") %></th>
            <th><%= _t("registrationNo") %></th>
            <% if(app.havePermission(["superadmin"])) { %>
                <th><%= _t("deviceId") %></th>
            <% } %>
            <th><%= _t("speedLimit") %></th>
            <th><%= _t("noOfSeats") %></th>
            <th class="tac"><%= _t("actions") %></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>