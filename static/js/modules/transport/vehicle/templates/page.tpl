<div class="page-heading">
	<div class="page-heading-text">
		<%= _t("manageVehicles") %>
	</div>
</div>
<div class="btn btn-primary create-new-vehicle new-cta"> <i class="fa fa-plus"></i>
		<%= _t("addNew") %>
	</div>
<div class="vehicles-container table-container">
</div>