<form>
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="name,registrationNo"></div>
		<% if(app.havePermission(["superadmin"])) { %>
            <div data-fields="deviceId"></div>
        <% } %>
		<div data-fields="speedLimit,noOfSeats,model"></div>
	</div>
	<div class="field-group">
		<input type="submit" class="btn btn-primary submit align-with-form" value="<%= submitButtonText %>"/>
	</div>
</form>