define(["app", "transportVehicleView"], function(app, View){
  var retObj = {
    list : function() {
      var item = Backbone.Model.extend({
        urlRoot : "/transportVehicles",
      });
      var itemsCollection = Backbone.Collection.extend({
        url : "/transportVehicles",
        model : item
      });

      var items = new itemsCollection();

      items.fetch({
        success : function(collection, response, options) {
          retObj.view = new View.items({
            collection : collection
          });

          retObj.view.on("childview:edit:vehicle", function() {
            var model = arguments[1].model;
            internalObj.showEditSubItem(model, false);
          });

          retObj.view.on("childview:delete:vehicle", function() {
            var confirm = window.confirm("This will delete the vehicle, are you sure?");
            if(!confirm) return;
            var model = arguments[1].model;
            model.destroy({
              wait : true,
              success : function() {
                app.reqres.request("toaster:show", i18n.t("vehicleDeleteSuccessMsg"));
              },
              error : function() {
                if(arguments[1].status == 409) {
                  app.reqres.request("toaster:show", i18n.t("vehicleDeleteConflictMsg"));
                } else {
                  app.reqres.request("toaster:show", i18n.t("vehiceDeleteErrorMsg"));
                }
              }
            });
          });

          retObj.pageLayout = new View.pageView;

          retObj.pageLayout.on("new:vehicle", function() {
            var Model = Backbone.Model.extend({
              urlRoot : "/transportVehicles",
            });
            var model = new Model({
              name : "",
              registrationNo : "",
              deviceId : "",
              noOfSeats : "",
              model : ""
            });
            internalObj.showEditSubItem(model, true);
          });

          app.mainRegion.show(retObj.pageLayout);
          retObj.pageLayout.listContainer.show(retObj.view);
        }
      });
    }
  };

  var internalObj = {
    showEditSubItem : function(model, isNew) {
      var formModel = Backbone.Model.extend({
        schema : {
          name : { title : i18n.t('name') },
          registrationNo : { title : i18n.t('registrationNo') + "*", validators : ['required']},
          deviceId : { title : i18n.t('deviceId')},
          speedLimit : { title : i18n.t('speedLimit')},
          noOfSeats : { title : i18n.t('noOfSeats') + "*", validators : ['required']},
          model : { title : i18n.t('vehicleModel') + "*", validators : ['required']}
        }
      });

      var form = new formModel({
        name : model.get("name"),
        registrationNo : model.get("registrationNo"),
        deviceId : model.get("deviceId"),
        speedLimit : model.get("speedLimit"),
        noOfSeats : model.get("noOfSeats"),
        model : model.get("model")
      });

      var formView = new Backbone.Form({
        template : View.subItemForm,
        model : form,
        idPrefix : "vehicle-form-",
        templateData : {
          isNew : isNew,
          fieldFormHeaderText : isNew ? i18n.t("addNewVehicle") : i18n.t("editVehicleDetail"),
          submitButtonText : isNew ? i18n.t("save") : i18n.t("update"),
          successMsg : isNew ? i18n.t("addVehicleSuccessMsg") : i18n.t("editVehicleSuccessMsg"),
          errorMsg : isNew ? i18n.t("addVehicleErrorMsg") : i18n.t("editVehicleErrorMsg"), 
        },
        originalModel : model,
        isNew : isNew
      });

      formView.on("submit", function(event) {
        event.preventDefault();
        app.form.disableSubmit(formView);
        var error = formView.validate();
        if(error) {
          app.form.enableSubmit(formView);
          return;
        }
        formView.commit();
        _.extend(formView.options.originalModel.attributes, formView.model.toJSON());
        formView.options.originalModel.save(formView.options.originalModel.attributes, {
          patch : true,
          success : function(model, response, xhr) {
              app.form.enableSubmit(formView);
              app.reqres.request("toaster:show", formView.templateData.successMsg);
              formView.trigger("modal:close");
              if(formView.options.isNew) {
                retObj.view.collection.add(model);
              }
              retObj.view.render();
          },
          error : function(model, response, xhr) {
              app.form.enableSubmit(formView);
              app.reqres.request("toaster:show", formView.templateData.errorMsg);
              formView.trigger("modal:close");
          }
        });
      });
      app.modal.show(formView);
    }
  };

  return retObj;
});
