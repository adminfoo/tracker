<form class="passanger-transport-tab">
<div class="form-group-container">
	<div class="field-group-header"><%= fieldFormHeaderText %></div>
	<div class="field-group">
		<div data-fields="stopId,routeId,serviceType"></div>
	</div>
</div>

<% if(typeof _route.name != 'undefined') { %>
	<div class="form-group-container">
		<div class="field-group-header"><%= _t("routeInformation") %></div>
		<div class="profile-view">
			<div class="field-group">
				<label>
					<%= _t("driverName") %>
				</label>
				<div class="value">
					<%= _route.driverName %>
				</div>
			</div>
			<div class="field-group">
				<label>
					<%= _t("vehicleRegistrationNo") %>
				</label>
				<div class="value">
					<%= _route.vehicleRegistrationNo %>
				</div>
			</div>
			<div class="field-group">
				<label>
					<%= _t("driverPhoneNo") %>
				</label>
				<div class="value">
					<%= _route.driverPhone %>
				</div>
			</div>
			<% if(formData.serviceType == "both" || formData.serviceType == "pickup") { %>
				<div class="field-group">
					<label>
						<%= _t("pickupTime") %>
					</label>
					<div class="value">
						<%= getTime(_stop.pickupTime) %>
					</div>
				</div>
			<% } %>
			<% if(formData.serviceType == "both" || formData.serviceType == "drop") { %>
				<div class="field-group">
					<label>
						<%= _t("dropTime") %>
					</label>
					<div class="value">
						<%= getTime(_stop.dropTime) %>
					</div>
				</div>
			<% } %>
		</div>
	</div>
<% } %>

<input type="submit" class="btn btn-primary submit save" value="<%= _t("save") %>"/>

</form>