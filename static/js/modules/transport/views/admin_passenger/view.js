define(["app", "tpl!transportViewsAdminPassengerForm"], 
  function(app, transportFormTemplate){
  var retObj = {
    transportView : transportFormTemplate
  };
  return retObj;
});
