define(["app", "transportViewsAdminPassengerView"], function(app, View){
  var retObj = {
    showTransport : function(data) {
      retObj.data = data;
      var routes = app.request("transport:routes");
      var stops = app.request("transport:stops");
      retObj.serviceOption = [{
        val : "both",
        label : i18n.t("pickupAndDrop")
      }, {
        val : "pickup",
        label : i18n.t("pickupOnly")
      }, {
        val : "drop",
        label : i18n.t("dropOnly")
      }];
      $.when(routes, stops).done(function(routes, stops) {
        retObj.routes = routes[0];
        retObj.stops = stops[0];
        internalObj.setRouteOptions();
        var Model  = Backbone.Model.extend({
          urlRoot : retObj.data.endPoint
        });
        retObj.model = new Model({
          id : retObj.data.id
        });
        retObj.model.fetch({
          success : function() {
            retObj.showTransportForm();
          }
        });
      });
    },
    showTransportForm : function() {
      if($.isEmptyObject(retObj.model.get("transport"))) {
        retObj.model.set("transport", {
          stopId : "-1",
          routeId : "-1",
          serviceType : "both"
        });
        retObj.currentRouteOptions = [{
          val : "-1",
          label : i18n.t("firstChooseStop")
        }];
      } else {
        retObj.currentRouteOptions = retObj.routeOptions[retObj.model.get("transport").stopId];
      }

      if(retObj.model.get("transport").routeId != "-1") {
        retObj.model.set("_route", _.find(retObj.routes, function(route) {return route.id == retObj.model.get("transport").routeId}));
      } else {
        retObj.model.set("_route", {
            stops : []
        });
      }

      var Model = Backbone.Model.extend({
        schema : {
          stopId : { title : i18n.t("chooseStop") + "*", type : "Select", validators : ["required"], options : retObj.stopOptions},
          routeId : { title : i18n.t("chooseRoute") + "*", type : "Select", validators : ["required"], options : retObj.currentRouteOptions},
          serviceType : { title : i18n.t("chooseServiceType") + "*", type : "Select", validators : ["required"], options : retObj.serviceOption},
        }
      });

      var form = new Model({
        stopId : retObj.model.get("transport").stopId,
        routeId : retObj.model.get("transport").routeId,
        serviceType : retObj.model.get("transport").serviceType,
      });

      retObj.view = new Backbone.Form({
        model : form,
        template : View.transportView,
        templateData : {
          fieldFormHeaderText : i18n.t("stopAndRouteInformation"),
          formData : form.toJSON(),
          _route : retObj.model.get("_route"),
          _stop : _.find(retObj.model.get("_route").stops, function(stop) {return stop.id == form.get("stopId")}) || {},
          getTime : function(time) {
            return time.hr+":"+time.min+" " + time.meridian;
          }
        }
      });

      retObj.view.on('stopId:change', function(form, stopIdEditor) {
          var stopId = stopIdEditor.getValue(),
              newOptions = retObj.routeOptions[stopId];
          form.fields.routeId.editor.setOptions(newOptions);
          form.setValue({routeId : "-1"});
          var errors = form.commit();
          if(errors) {
              return;
          }
          retObj.saveTransportModel(form);
          retObj.showTransportForm();
      });

      retObj.view.on('routeId:change', function(form, editor) {
        var errors = form.commit();
        if(errors) {
            return;
        }
        retObj.saveTransportModel(form);
        retObj.showTransportForm();
      });

      retObj.view.on('serviceType:change', function(form, editor) {
        var errors = form.commit();
        if(errors) {
            return;
        }
        retObj.saveTransportModel(form);
        retObj.showTransportForm();
      });

      retObj.view.on("submit", function(event) {
        event.preventDefault();
        var errors = retObj.view.commit();
        if(errors) {
            app.form.enableSubmit(retObj.view);
            return;
        }
        retObj.saveTransportModel(retObj.view);
        retObj.showTransportForm();
        app.form.disableSubmit(retObj.view);
        retObj.model.save(retObj.model.attributes, {
          patch : true,
          success : function() {
            app.form.enableSubmit(retObj.view);
            app.reqres.request("toaster:show", i18n.t("changesSaved"));
            app.scrollTop();
          },
          error : function() {
            app.form.enableSubmit(retObj.view);
            app.reqres.request("toaster:show", i18n.t("someErrorOccured"));
            app.scrollTop();
          }
        });
      });
      retObj.data.successCallBack(retObj.view);
    },
    saveTransportModel : function(form) {
      retObj.model.attributes.transport.routeId = form.model.get("routeId");
      retObj.model.attributes.transport.stopId = form.model.get("stopId");
      retObj.model.attributes.transport.serviceType = form.model.get("serviceType");
    }
  };

  var internalObj = {
    setRouteOptions : function() {
      retObj.stopRouteMapping = {};
      retObj.stopOptions = [{
        val : "-1",
        label : i18n.t("chooseStopName")
      }];
      _.each(retObj.stops, function(stop) {
        retObj.stopOptions.push({
          val : stop.id,
          label : stop.name
        });
        retObj.stopRouteMapping[stop.id] = retObj.stopRouteMapping[stop.id] || {};
        _.each(retObj.routes, function(route) {
          _.each(route.stops, function(routeStop) {
            if(stop.id == routeStop.id) {
              retObj.stopRouteMapping[stop.id][route.id] = true;
            }
          });
        });
      });
      retObj.routeOptions = {};
      _.each(retObj.stopRouteMapping, function(item, key) {
        if(!$.isEmptyObject(item)) {
          retObj.routeOptions[key] = [{
            val : "-1",
            label : i18n.t("chooseRoute")
          }];
          _.each(item, function(v,k) {
            var option = {
              val : k,
              label : _.find(retObj.routes, function(route) { return route.id == k;}).name
            };
            retObj.routeOptions[key].push(option);
          });
        } else {
          retObj.routeOptions[key] = [{
            val : "-1",
            label : i18n.t("noRouteForThisStop")
          }];
        }
        retObj.routeOptions["-1"] = [{
            val : "-1",
            label : i18n.t("firstChooseStop")
        }];
      });
    }
  };

  return retObj;
});
