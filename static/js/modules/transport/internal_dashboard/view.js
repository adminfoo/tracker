define(["app", "tpl!internalDashboardPageTemplate"],
       function(app, pageTemplate){
  var retObj = {
    pageView : Marionette.LayoutView.extend({
      template: pageTemplate,
      regions: {
        dataContainer : ".data-container"
      }
    })
  };
  return retObj;
});
