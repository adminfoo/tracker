define(["app", "internalDashboardView", "chart"], function(app, View, Chart){
  var retObj = {
    show : function() {
      retObj.pageLayout = new View.pageView;
      app.mainRegion.show(retObj.pageLayout);
      internalObj.getData().then(function(response) {
        var summaryCanvas = document.getElementById("summaryChart").getContext("2d");
        var data = {
            labels: [], // dates
            datasets: [
                {
                    label: "total",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "alarmCount",
                    fillColor: "black",
                    strokeColor: "black",
                    pointColor: "black",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "accurate",
                    fillColor: "green",
                    strokeColor: "green",
                    pointColor: "green",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "missed",
                    fillColor: "red",
                    strokeColor: "red",
                    pointColor: "red",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "unknown",
                    fillColor: "yellow",
                    strokeColor: "yellow",
                    pointColor: "yellow",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "waiting",
                    fillColor: "blue",
                    strokeColor: "blue",
                    pointColor: "blue",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                }
            ]
        };
        var keys = Object.keys(response);
        keys.sort();
        _.each(keys, function(key) {
          dateWiseItem = response[key];
          var date = new Date(key).format("dd mmm");
          data.labels.push(date);
          _.where(data.datasets, {label : "total"})[0].data.push(dateWiseItem.total);
          _.where(data.datasets, {label : "alarmCount"})[0].data.push(dateWiseItem.alertCount);
          _.where(data.datasets, {label : "missed"})[0].data.push(dateWiseItem.missed);
          _.where(data.datasets, {label : "waiting"})[0].data.push(dateWiseItem.waiting);
          _.where(data.datasets, {label : "accurate"})[0].data.push(dateWiseItem.accurate);
          _.where(data.datasets, {label : "unknown"})[0].data.push(dateWiseItem.unknown);
        });
        var options = {
          datasetFill : false,
        };
        var myLineChart = new Chart(summaryCanvas).Line(data, options);
      });
    }
  };

  var internalObj = {
    getData : function() {
      return $.ajax({
        url : "/alertAccuracy?from=2015-12-19&to=2015-12-31",
        method : "GET"
      });
    }
  };

  return retObj;
});
