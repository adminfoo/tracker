from pymongo import MongoClient

cli = MongoClient('edufits.net')
# cli = MongoClient()
# cli.the_database.authenticate('localUser', 'protip007', source='admin')

all_paxes = [p for p in cli.trackerdb.passengers.find()]

for pax in all_paxes:
    print('pax:', pax['_id'])
    cli.trackerdb.passengers.update({'_id': pax['_id']}, {'$set': {
        'pickupRouteId': pax['routeId'], 'dropRouteId': pax['routeId'],
        'pickupRouteName': pax['routeName'], 'dropRouteName': pax['routeName'],
        'pickupStopId': pax['stopId'], 'dropStopId': pax['stopId'],
        'pickupStopName': pax['stopName'], 'dropStopName': pax['stopName']
    }})