from pymongo import MongoClient

cli = MongoClient('edufits.net')
# cli = MongoClient()
# cli.the_database.authenticate('localUser', 'protip007', source='admin')

all_routes = [p for p in cli.trackerdb.routes.find()]

for r in all_routes:
    print(r['_id'], r['name'])
    if 'trips' not in r:
        print('no trips')
        r['trips'] = []
    for trip in r['trips']:
        for stop in trip['stops']:
            stop['time'] = stop['pickupTime']
            del stop['pickupTime']
            del stop['dropTime']
    cli.trackerdb.routes.update({'_id': r['_id']}, {'$unset': {'pickupPathPoints': None, 'dropPathPoints': None}, '$set': {'trips': r['trips']}})