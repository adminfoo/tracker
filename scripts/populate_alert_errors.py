from datetime import timedelta
from pymongo import MongoClient

cli = MongoClient('edufits.net')
# cli = MongoClient()
# cli.the_database.authenticate('localUser', 'protip007', source='admin')


def populate_it():
    all_alerts = [a for a in cli['trackerdb']['pax_alerts'].find()]
    for a in all_alerts:
        if a['actErrorMins'] is not None:
            continue
        print('alert:', a['_id'])
        route_trip = cli['trackerdb']['route_trips'].find_one({'_id': a['routeTripId']})
        act_time = None
        act_error_mins = None
        if route_trip is not None:
            stop = next((s for s in route_trip['stops'] if s['id'] == a['stopId']), None)
            if stop is not None and stop['actTime'] is not None:
                act_time = stop['actTime']
                act_error_mins = int((stop['actTime'] - (a['alertTime'] + timedelta(0, a['alertAheadMins'] * 60))).total_seconds() / 60)
        cli['trackerdb']['pax_alerts'].update({'_id': a['_id']}, {'$set': {'actTime': act_time, 'actErrorMins': act_error_mins}})


if __name__ == "__main__":
    populate_it()