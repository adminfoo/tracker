var tdb = db.getSisterDB('trackingdb');

var colls = tdb.getCollectionNames();
colls.forEach(function(coll){
    if(coll != "system.indexes"){
        var cursor = tdb[coll].find({'lat': {$exists: true}, 'lng': {$exists: true}});
        cursor.forEach(function(e){
            var location = {type: "Point", coordinates: [e.lng, e.lat]};
            
            tdb[coll].update({_id: e._id}, {$set: {loc: location}});
        });
    }
});
