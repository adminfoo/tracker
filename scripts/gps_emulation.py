from threading import Thread
import time
from datetime import datetime, timedelta
from pymongo import MongoClient
from tracker.utils.common_utils import hr_min_str_to_time
from tracker.utils.validations import day_from_2015

# prod_client = MongoClient("edufits.com")
# prod_client.the_database.authenticate("readUser", "nomoresecrets", "admin")

local_client = MongoClient()


def emulate_gps(src_device_id, target_device_id, past_start_time, past_end_time, current_start_time=None):
    if current_start_time is None:
        current_start_time = datetime.now()
    time_offset = current_start_time - past_start_time
    items = fetch_some_items(src_device_id, past_start_time)

    dump_item(items[0], time_offset, target_device_id)

    if len(items) == 1:
        items = fetch_some_items(src_device_id, items[-1]['insertedAt'])
    del items[0]

    wait_time = items[0]['insertedAt'] + time_offset - datetime.now()
    secs = wait_time.total_seconds()
    print('sleeping for', str(secs), 'secs')
    if secs > 0:
        time.sleep(secs)

    while datetime.now() - time_offset < past_end_time:
        dump_item(items[0], time_offset, target_device_id)

        if len(items) == 1:
            items = fetch_some_items(src_device_id, items[-1]['insertedAt'])
        del items[0]

        wait_time = items[0]['insertedAt'] + time_offset - datetime.now()
        secs = wait_time.total_seconds()
        print('sleeping for', str(secs), 'secs')
        if secs > 0:
            time.sleep(secs)


def fetch_some_items(src_device_id, past_start_time):
    filters = {'time': {'$gt': past_start_time}}
    # recs = [x for x in prod_client['trackingdb'][src_device_id].find(filters).sort([('insertedAt', 1)]).limit(1000)]
    recs = [x for x in local_client['trackingdb'][src_device_id].find(filters).sort([('insertedAt', 1)]).limit(1000)]
    print('fetched', str(len(recs)), 'items')
    if len(recs) == 0:
        raise Exception("No more docs")
    return recs


dump_dict = {}


def dump_item(item, time_offset, target_device_id):
    if '_id' in item:
        del item['_id']
    item['time'] += time_offset
    item['insertedAt'] = datetime.now()
    print('inserting')
    print(item)
    local_client['trackingdb'][target_device_id].insert(item)

    if target_device_id not in dump_dict:
        dump_dict[target_device_id] = item
    else:
        dump_dict[target_device_id]['prev_time'] = dump_dict[target_device_id]['time']
        dump_dict[target_device_id].update(item)


def dump_device_status():
    dev_status = {
        'count': len(dump_dict),
        'time': datetime.now(),
        'devices': []
    }
    for device_id, data in dump_dict.items():
        dev = {
            'deviceId': device_id,
            'interval': ((data['time'] - data['prev_time']).total_seconds()) if 'prev_time' in data else 0,
            'lastUpdated': data['time'],
            'ignition': data['ignition'] if 'ignition' in data else False,
            'lat': data['lat'],
            'lng': data['lng']
        }
        dev_status['devices'].append(dev)
    local_client['trackingdb']['devices_status'].insert(dev_status)


def edit_route_timings(route, time_offset):
    route['startTime'] += time_offset
    for s in route['stops']:
        if 'time' not in s:
            s['time'] = datetime.combine(datetime.now().date(), hr_min_str_to_time(s['timeObj']))
        s['time'] += time_offset
    for s in route['stops']:
        new_time = datetime.combine(datetime.now().date(), hr_min_str_to_time(s['time'])) + time_offset
        s['timeObj'] = datetime_to_obj(new_time)
    print('new stop timing', route['stops'][0]['time'])
    res = local_client['trackerdb']['routes'].update({'_id': route['_id']}, {'$set': {'stops': route['stops'], 'startTime': route['startTime']}})
    print('route update result')
    print(res)


def datetime_to_obj(dt):
    hr = dt.hour
    mn = dt.minute
    meridian = "AM"
    if hr >= 12:
        hr -= 12
        meridian = "PM"
    if hr == 0:
        hr = 12
    return {'hr': hr, 'min': mn, 'meridian': meridian}


def initialize_for_replay(route_id, past_date):
    print('route id', route_id)

    # clear today's trip for this route
    _day = day_from_2015(datetime.now())
    local_client['trackerdb']['route_trips'].remove({'routeId': route_id, 'day': _day})
    print("removed today's trips for route")

    route = local_client['trackerdb']['routes'].find_one({'_id': route_id})
    print('route name', route['name'])
    # prod_route = prod_client['trackerdb']['routes'].find_one({'_id': route['_id']})
    prod_route = local_client['trackerdb']['routes'].find_one({'_id': route['_id']})
    route['stops'] = prod_route['stops']

    past_start_time = datetime.combine(past_date, route['startTime'].time())
    tim = route['stops'][-1]['time'] if 'time' in route['stops'][-1] else route['stops'][-1]['dropTime']
    past_end_time = datetime.combine(past_date, tim.time()) + timedelta(0, 1800)
    print('past_start', past_start_time)
    print('past_end', past_end_time)

    time_offset = datetime.now() - past_start_time
    print('offset', time_offset)

    veh_routes = [r for r in local_client['trackerdb']['routes'].find({'vehicleId': route['vehicleId'], 'startTime': {'$exists': True}})]
    for r in veh_routes:
        edit_route_timings(r, time_offset)
    print('route timing edited')

    # clear passenger alerts for today for this trip
    alert_start_time = datetime.combine(datetime.now().date(), datetime(2016, 1, 1, 0).time())
    alert_end_time = datetime.combine(datetime.now().date(), datetime(2016, 1, 1, 23, 59).time())
    local_client['trackerdb']['pax_alerts'].remove({'routeId': route_id, 'alertTime': {'$gte': alert_start_time, '$lte': alert_end_time}})

    return past_start_time, past_end_time


if __name__ == "__main__":
    print('starting')
    past_date = datetime(2015, 12, 29).date()
    trip_type = "pickup"
    print('relaying data from', past_date)
    all_routes = [r for r in local_client['trackerdb']['routes'].find({'type': trip_type}, {'vehicleDeviceId': True})]
    script_duration = timedelta(0)
    for route in all_routes:
        past_start_time, past_end_time = initialize_for_replay(route['_id'], past_date)
        if past_start_time is None:
            continue

        src_device_id = route['vehicleDeviceId']
        print('src_device_id', src_device_id)
        target_device_id = src_device_id

        t = Thread(target=emulate_gps, args=(src_device_id, target_device_id, past_start_time, past_end_time))
        t.start()
        # emulate_gps(src_device_id, target_device_id, past_start_time, past_end_time)
        if script_duration < past_end_time - past_start_time:
            script_duration = past_end_time - past_start_time

    while script_duration > timedelta(0):
        dump_device_status()
        time.sleep(10)
        script_duration -= timedelta(0, 10)
