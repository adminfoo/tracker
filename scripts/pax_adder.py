"""
TODO: change this script according to new structure of passengers
"""

import csv
from tracker import utils
import logging

logger = logging.getLogger(__name__)


def get_route_dict(school_id):
    vehs = [v for v in utils.mongo_cli.trackerdb.vehicles.find({'schoolId': school_id})]
    veh_dict = {v['_id']: v['name'] for v in vehs}
    routes = [r for r in utils.mongo_cli.trackerdb.routes.find({'schoolId': school_id})]
    return {veh_dict[r['vehicleId']]: r for r in routes}


def get_route_stop_id(text, route_dict):
    bus_name = 'Bus no ' + text.rstrip(')').split('-')[-1]
    route = route_dict[bus_name]
    route_id = str(route['_id'])
    stop_id = str(route['stops'][0]['id'])
    for stop in route['stops']:
        if stop['name'] == text.split('(')[0].strip():
            stop_id = stop['id']
    return route_id, stop_id


def load_students(school_id):
    route_dict = get_route_dict(school_id)
    print(list(route_dict.values()))
    ll = []
    filename = '/home/sukrit/tracker/scripts/paxes.csv'
    with open(filename) as csvfile:
        sr = csv.reader(csvfile)
        for row in sr:
            ll.append(row)
    del ll[0]
    students = []
    for l in ll:
        infos = l
        rid, sid = get_route_stop_id(infos[7], route_dict)
        stud = {
            "type":"student",
            "name":infos[0],
            "phone":infos[6],
            "gender":infos[3].lower(),
            "bloodGroup":"O+",
            "address":"",
            "routeId":rid,
            "serviceType":"both",
            "idNumber":infos[1],
            "course":get_course(infos[4]),
            "guardianName":infos[2],
            "email":"",
            "stopId":sid
        }
        # print(stud)
        students.append(stud)
    return students


def get_course(text):
    text = text.lower()
    if 'nursery' in text:
        return '0'
    if 'kg' in text:
        return '1'
    t_d = {'class i': '2', 'class ii': '3', 'class iii': '4', 'class iv': '5', 'class v': '6', 'class vi': '7',
           'class vii': '8', 'class viii': '9', 'class ix': '10', 'class x': '11', 'class xi': '12', 'class xii': '13',
           'class 10': '11', 'class 12 - science': '13', 'class 12 - commerce': '13'}
    if text in t_d:
        return t_d[text]
    return '4'


def class_correction():
    ll = []
    filename = '/home/sangu/freckles/tracker/scripts/paxes.csv'
    with open(filename) as csvfile:
        sr = csv.reader(csvfile)
        for row in sr:
            ll.append(row)
    del ll[0]

    class_map = {
        'Pre Nursery': "Pre Nursery",
        'Nursery': "Nursery",
        'KG': "KG",
        'Class I': "I",
        'Class II': "II",
        'Class III': "III",
        'Class IV': "IV",
        'Class V': "V",
        'Class VI': "VI",
        'Class VII': "VII",
        'Class VIII': "VIII",
        'Class IX': "IX",
        'Class XI  Science': "XI",
        'Class XI Commerce': "XI",
        'CLASS 10': "X",
        'CLASS 12 – Science': "XII",
        'CLASS 12 – Commerce': "XII"
    }

    for l in ll:
        id_no = l[1]
        class_name = class_map[l[4]]
        update_res = utils.app_db['passengers'].update({'idNumber': id_no}, {'$set': {'course': class_name}})
        logger.info({'msg': "pax class correction", 'idNumber': id_no, 'result': update_res})


def happy_model_loader():
    course_map = {'lkg': "LKG", 'ukg': "UKG", '1': "I", '2': "II", '3': "III", '4': "IV", '5': "V", '6': "VI",
                  '7': "VII", '8': "VIII", '9': "IX", '10': "X", '11': "XI", '12': "XII",
                  'I': "I", 'II': "II", 'III': "III", 'IV': "IV", 'V': "V", 'VI': "VI", 'VII': "VII", 'VIII': "VIII",
                  'IX': "IX", 'X': "X", 'XI': "XI", 'XII': "XII"}
    file_dict = {
        'HR 55 Q 8104': '/home/sangu/freckles/tracker/static/js/modules/transport/routes/add/hms.hr55q8104',
        'HR 55 S 3236': '/home/sangu/freckles/tracker/static/js/modules/transport/routes/add/hsm.hr55s3236',
        'HR 55 S 3237': '/home/sangu/freckles/tracker/static/js/modules/transport/routes/add/hsm.hr55s3237'
    }
    students = []
    for veh, filename in file_dict.items():
        route_details = utils.app_db['routes'].find_one({'vehicleRegistrationNo': veh})
        ll = []
        with open(filename) as csvfile:
            sr = csv.reader(csvfile)
            for row in sr:
                ll.append(row)
        for infos in ll:
            stop = next((s for s in route_details['stops'] if s['name'].lower() == infos[4]), None)
            if stop is None:
                print(infos)
            stud = {
                "type": "student",
                "name": infos[0].title(),
                "phone": infos[3],
                "gender": "male",
                "bloodGroup": "O+",
                "address": "",
                "routeId": route_details['_id'],
                "routeName": route_details['name'],
                "serviceType": "both",
                "idNumber": "",
                "course": course_map[infos[2]],
                "guardianName": infos[1].title(),
                "email": "",
                "stopId": stop['id'],
                "stopName": stop['name']
            }
            students.append(stud)
    return students
