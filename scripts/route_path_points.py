from tracker.utils.validations import validate_object_id
from tracker import utils
from pymongo import MongoClient


def calculate_path_points(school_id):
    school_id = validate_object_id(school_id)
    all_routes = utils.mongo_cli.trackerdb.routes.find({'schoolId': school_id})
    for r in all_routes:
        pass


if __name__ == "__main__":
    # connect mongo
    utils.mongo_cli = MongoClient()