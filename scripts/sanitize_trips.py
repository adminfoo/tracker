"""
There is a redundant field "type" in each trip data
This script removes this redundant field
"""

from pymongo import MongoClient

client = MongoClient()
# client.the_database.authenticate('localUser', 'protip007', source='admin')
client.trackerdb.trips.update({}, {'$unset': {'type': None}}, upsert=False, multi=True)
client.close()
print('done')
