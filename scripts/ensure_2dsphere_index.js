var tdb = db.getSisterDB('trackingdb');

var colls = tdb.getCollectionNames();
colls.forEach(function(coll){
    if(coll != "system.indexes"){
        print(coll);
        tdb[coll].ensureIndex({loc: "2dsphere"});
        tdb[coll].ensureIndex({time: 1});
        tdb[coll].ensureIndex({time: 1, loc: "2dsphere"});
        tdb[coll].ensureIndex({insertedAt: 1});
    }
});
