from datetime import datetime
from pymongo import MongoClient

cli = MongoClient()

all_colls = cli.trackingdb.collection_names()

data_list = []

for coll in all_colls:
    if not coll.startswith('3'):
        continue
    cur = cli.trackingdb[coll].find({'time': {'$gte': datetime(2015, 1, 1)}}, {'time': True, 'insertedAt': True}).sort([('time', 1)])
    data_dict = {'deviceId': coll}
    previous = None
    intervals = []
    for record in cur:
        if previous is None:
            previous = record
            continue
        interval = int((record['time'] - previous['time']).total_seconds())
        intervals.append(interval)
        previous = record
    data_dict['01_30sec'] = len([i for i in intervals if 32 < i < 63])
    data_dict['02_1min'] = len([i for i in intervals if 62 < i < 123])
    data_dict['03_2min'] = len([i for i in intervals if 123 < i < 303])
    data_dict['04_5min'] = len([i for i in intervals if 302 < i < 603])
    data_dict['05_10min'] = len([i for i in intervals if 602 < i < 1803])
    data_dict['06_30min'] = len([i for i in intervals if 1802 < i < 3603])
    data_dict['07_1hr'] = len([i for i in intervals if 3602 < i < 7203])
    data_dict['08_2hr'] = len([i for i in intervals if 7202 < i < 14403])
    data_dict['09_6hr'] = len([i for i in intervals if 14402 < i < 86403])
    data_dict['10_1day'] = len([i for i in intervals if i > 86402])
    data_list.append(data_dict)
    print(data_dict)

