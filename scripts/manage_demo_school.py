import random
import string

from pymongo import MongoClient

from tracker import utils


def generate_random_male_name():
    first_names = ['Gauransh', 'Shivam', 'Rachit', 'Vansh', 'Nitiksh', 'Nitin', 'Tanish', 'Mohit', 'Saran', 'Harpreet', 'Hitin', 'Ishaan', 'Nitish', 'Abhishek', 'Deep', 'Aaban', 'Tarun', 'Sameer', 'Sanjana', 'Lakshya', 'Shubhayan', 'Deepanshu', 'Ayashi', 'Kunal', 'Vishal', 'Kamal', 'Yogesh', 'Anant', 'Ankit', 'Shubham', 'Vikas', 'Aaditya', 'Isha', 'Deepankar', 'Chetan', 'Samarth', 'Yatiksh', 'Shagun', 'Prerna', 'Muskaan', 'Yashwant', 'Raghav', 'Happy', 'Arshman', 'Pradeep', 'Mobin', 'Hardik', 'Udit', 'Sabhyata', 'Ayush', 'Mayank', 'Sajid', 'Sujata', 'Vardan', 'Pooja', 'Sourav', 'Saksham', 'Lakshay', 'Harsh', 'Twinkle', 'Ansh', 'Rinku', 'Ajay', 'Arjun', 'Kumar', 'Rishit', 'Krish', 'Tushar', 'Aryan', 'Akash', 'Manish', 'Vishesh', 'Manoj', 'Priyank', 'Krishna', 'Bhuwan', 'Harshit', 'Devansh', 'Ashish', 'Avinash', 'Divyanshu', 'Aayush', 'Shivank', 'Vipul', 'Pranav', 'Daksh', 'Varun', 'Anubhav', 'Rohit', 'Ritik', 'Mitesh', 'Nikhil', 'Satvinder', 'Hemant', 'Harjinder', 'Bhavesh', 'Jaikant', 'Moh', 'Armaan', 'Yajat', 'Vineet', 'Pratham', 'Riyan', 'Jagmeet', 'Vinay', 'Daya', 'Kshitiz', 'Darshil', 'Dheeraj', 'Vishbhawan', 'Sahil', 'Karan', 'Rohan', 'Gaurav', 'Saurabh', 'Rishab', 'Rajat', 'Chaitanya', 'Saurav', 'Madhv', 'Hitesh', 'Digombari', 'Akshay', 'Raj', 'Ashu', 'Gagandeep', 'Deepeshwar', 'Sagar', 'Nishant', 'Lalit', 'Mohd', 'Simran', 'Jogender', 'Shivesh', 'Shreyansh', 'Deewank', 'Sarthak', 'Dishant', 'Piyush', 'Mohd.', 'Sujal', 'Pankaj', 'Akshat', 'Preetak', 'Sagufta', 'Umang', 'Sachin', 'Bhivesh', 'Rithik', 'Shashank', 'Adharv', 'Ujwal', 'Sanskriti', 'Rajveer', 'Dhananjay', 'Arun', 'Lavesh', 'Sumit', 'Shourya', 'Achint', 'Satish', 'Lokesh', 'Hritik', 'Dinesh', 'Priyansh', 'Ritika', 'Amit', 'Dikshant', 'Divyam', 'Gajender', 'Saviya', 'Uday', 'Subha', 'Naman', 'Ashmita', 'Aditya', 'Jatin', 'Rahul', 'Yash', 'Yojit', 'Bharat', 'Ranveer', 'Parth', 'Vipin', 'Abhay', 'Umki', 'Nadeem', 'Gopesh', 'Suraj', 'Aman', 'Dev', 'Arnav']
    last_names = ['Saha', 'Zohaib', 'Baldiya', 'Kartikeya', 'Khadiya', 'Mudad', 'Dalal', 'Bhardwaj', 'Rana', 'Kapoor', 'Singh', 'Mehra', 'Nasir', 'Dhankhar', 'Sharma', 'Murmu', 'Prakash', 'Ansal', 'Saini', 'Dhankher', 'Chahal', 'Manu', 'Dangi', 'Mehto', 'Prasad', 'Saroya', 'Puri', 'Solanki', 'Gill', 'Raj', 'Kaushik', 'Jaglan', 'Vikram', 'Marndi', 'Rohilla', 'Verma', 'Dabash', 'Mamgain', 'Marothia', 'Gupta', 'Kr', 'Yadav', 'Baberwal', 'Bhateja', 'Punj', 'Dhiman', 'Goyat', 'Mehta', 'Tanwar', 'Patel', 'Raghav', 'Upadhyay', 'Shekhawat', 'Bhateria', 'Lamba', 'Banerjee', 'Dhingra', 'Jangra', 'Arora', 'Battu', 'Saifi', 'Mishra', 'Chaudhary', 'Ahamed', 'Preet', 'Balhara', 'Rajput', 'Kajal', 'Kataria', 'Ali', 'Laur', 'Das', 'Kumar', 'Vashil', 'Rao', 'Janghu', 'Patil', 'Kharab', 'Kaif', 'Dwivedi', 'Shukla', 'Dixit', 'Kabdal', 'Saxena', 'Wadhwa', 'Lohan', 'Koirala', 'Hans', 'Chauhan', 'Dahiya', 'Vivek', 'Goyal', 'Pratap', 'Sehrawat', 'Lohia', 'Bhadauria', 'Gothwal', 'Tyagi', 'Vaid', 'Grewal', 'Gahlot', 'Afjal', 'Tiwari', 'Jha', 'Parcha', 'Garg', 'Noor', 'Sultania', 'Malik', 'T', 'Gahalayan', 'Mohanty', 'Punia', 'Uddin', 'Podel']
    return random.choice(first_names) + ' ' + random.choice(last_names)


def generate_random_female_name():
    first_names = ["Kareena", "Neelam", "Khushboo", "Sweta", "Inaya", "Oshi", "Nevi", "Himanshi", "Aayushi", "Bhawna", "Meenakshi", "Bhoomika", "Vanshika", "Kritika", "Vineeta", "Ankita", "Diksha", "Rajni", "Deeksha", "Pooja", "Kanika", "Kirti", "Rekha", "Mansi", "Anju", "Alisha", "Sumita", "Nisha", "Mamta", "Srishti", "Manjeet", "Mehak", "Prangya", "Hema", "Gaurish", "Kamlesh", "Nishu", "Ridam", "Anwesha", "Poonam", "Tanya", "Nishika", "Anita", "Aarti", "Ishika", "Riya", "Shalini", "Chavi", "Sakshi", "Sneh", "Kavita", "Hiya", "Payal", "Kajal", "Mahi", "Shubhangi", "Twinkle", "Shrey", "Manjit", "Ritu", "Isha", "Nishi", "Tanzeem", "Devshree", "Hanshika", "Usha", "Alankriti", "Aditi", "Manpreet", "Priyansi", "Monika", "Neetu", "Gurnoor", "Jiyanshi", "Aina", "Pinky", "Arushi", "Khushi", "Neha", "Vaishnavi", "Gungun", "Revati", "Nupur", "Vidhi", "Simran", "Veena", "Arzoo", "Priyanka", "Sara", "Yanshika", "Chetna", "Kunika", "Abhinandani", "Adyasha", "Yashika", "Gayatri", "Pintee", "Sarita", "Preeti", "Priya", "Swati", "Priyanshi", "Jahnvi", "Rupender", "Saniya", "Nitika", "Sweety", "Anshika", "Muskan", "Seema", "Thrista", "Dimple", "Gargi", "Kakul", "Anisha", "Palak", "Nidhi", "Anika", "Harshita", "Jyoti", "Yashashvi", "Sheetal", "Archita", "Sonal"]
    last_names = ['Saha', 'Zohaib', 'Baldiya', 'Kartikeya', 'Khadiya', 'Mudad', 'Dalal', 'Bhardwaj', 'Rana', 'Kapoor', 'Singh', 'Mehra', 'Nasir', 'Dhankhar', 'Sharma', 'Murmu', 'Prakash', 'Ansal', 'Saini', 'Dhankher', 'Chahal', 'Manu', 'Dangi', 'Mehto', 'Prasad', 'Saroya', 'Puri', 'Solanki', 'Gill', 'Raj', 'Kaushik', 'Jaglan', 'Vikram', 'Marndi', 'Rohilla', 'Verma', 'Dabash', 'Mamgain', 'Marothia', 'Gupta', 'Kr', 'Yadav', 'Baberwal', 'Bhateja', 'Punj', 'Dhiman', 'Goyat', 'Mehta', 'Tanwar', 'Patel', 'Raghav', 'Upadhyay', 'Shekhawat', 'Bhateria', 'Lamba', 'Banerjee', 'Dhingra', 'Jangra', 'Arora', 'Battu', 'Saifi', 'Mishra', 'Chaudhary', 'Ahamed', 'Preet', 'Balhara', 'Rajput', 'Kajal', 'Kataria', 'Ali', 'Laur', 'Das', 'Kumar', 'Vashil', 'Rao', 'Janghu', 'Patil', 'Kharab', 'Kaif', 'Dwivedi', 'Shukla', 'Dixit', 'Kabdal', 'Saxena', 'Wadhwa', 'Lohan', 'Koirala', 'Hans', 'Chauhan', 'Dahiya', 'Vivek', 'Goyal', 'Pratap', 'Sehrawat', 'Lohia', 'Bhadauria', 'Gothwal', 'Tyagi', 'Vaid', 'Grewal', 'Gahlot', 'Afjal', 'Tiwari', 'Jha', 'Parcha', 'Garg', 'Noor', 'Sultania', 'Malik', 'T', 'Gahalayan', 'Mohanty', 'Punia', 'Uddin', 'Podel']
    return random.choice(first_names) + ' ' + random.choice(last_names)


def copy_vehicles(source_school_id, target_school_id):
    vehicle_ids_map = {}
    all_vehicles = [v for v in utils.mongo_cli['trackerdb']['vehicles'].find({'schoolId': source_school_id})]
    for vehicle in all_vehicles:
        source_id = vehicle['_id']
        print('copying vehicleId', source_id)
        del vehicle['_id']
        vehicle['schoolId'] = target_school_id
        vehicle['registrationNo'] = get_random_plate_no()
        vehicle['model'] = get_random_model_name()
        vehicle['noOfSeats'] = get_random_seat_count()
        result = utils.mongo_cli['trackerdb']['vehicles'].insert_one(vehicle)
        print('copied vehicleId', result.inserted_id)
        vehicle_ids_map[source_id] = {'target_id': result.inserted_id, 'new_registration_no': vehicle['registrationNo']}
    print('copied', len(all_vehicles), 'vehicles')
    return vehicle_ids_map


def copy_drivers(source_school_id, target_school_id):
    driver_ids_map = {}
    all_drivers = [v for v in utils.mongo_cli['trackerdb']['drivers'].find({'schoolId': source_school_id})]
    for driver in all_drivers:
        source_id = driver['_id']
        print('copying driverId', source_id)
        del driver['_id']
        driver['schoolId'] = target_school_id
        driver['licenseNo'] = "ABC" if driver['licenseNo'] != "" else ""
        driver['phone'] = get_random_phone_number()
        result = utils.mongo_cli['trackerdb']['drivers'].insert_one(driver)
        print('copied driverId', result.inserted_id)
        driver_ids_map[source_id] = {'target_id': result.inserted_id, 'name': driver['name'], 'phone': driver['phone'],
                                     'licenseNo': driver['licenseNo']}
    print('copied', len(all_drivers), 'drivers')
    return driver_ids_map


def copy_routes(source_school_id, target_school_id, vehicle_ids_map, driver_ids_map):
    route_ids_map = {}
    all_routes = [r for r in utils.mongo_cli['trackerdb']['routes'].find({'schoolId': source_school_id})]
    i = 0
    for route in all_routes:
        source_id = route['_id']
        print('copying routeId', source_id)
        del route['_id']
        route['schoolId'] = target_school_id
        # route['name'] = "Route no " + str(i)
        # change driver and helper details
        route['driverName'] = driver_ids_map[route['driverId']]['name']
        route['driverPhone'] = driver_ids_map[route['driverId']]['phone']
        route['driverLicenseNo'] = driver_ids_map[route['driverId']]['licenseNo']
        route['driverId'] = driver_ids_map[route['driverId']]['target_id']
        route['helperName'] = driver_ids_map[route['helperId']]['name']
        route['helperPhone'] = driver_ids_map[route['helperId']]['phone']
        route['helperId'] = driver_ids_map[route['helperId']]['target_id']
        # change vehicleId according to the vehicles copied
        route['vehicleRegistrationNo'] = vehicle_ids_map[route['vehicleId']]['new_registration_no']
        route['vehicleId'] = vehicle_ids_map[route['vehicleId']]['target_id']
        result = utils.mongo_cli['trackerdb']['routes'].insert_one(route)
        print('copied routeId', result.inserted_id)
        route_ids_map[source_id] = {'target_id': result.inserted_id, 'new_name': route['name']}
        i += 1
    print('copied', len(all_routes), 'routes')
    return route_ids_map


def copy_passengers(source_school_id, target_school_id, route_ids_map):
    all_paxes = [r for r in utils.mongo_cli['trackerdb']['passengers'].find({'schoolId': source_school_id})]
    from tracker.passengers import create_single_passenger
    for pax in all_paxes:
        print('copying paxId', pax['_id'])
        del pax['_id']
        pax['schoolId'] = target_school_id
        pax['name'] = generate_random_female_name() if pax['gender'] == "female" else generate_random_male_name()
        pax['idNumber'] = ""
        pax['phone'] = "423456" + "".join([random.choice(string.digits) for j in range(4)])
        pax['email'] = "dfahdfn@nsjvbsdfjvb.dsvefunv"
        pax['pickupRouteName'] = route_ids_map[pax['pickupRouteId']]['new_name']
        pax['pickupRouteId'] = route_ids_map[pax['pickupRouteId']]['target_id']
        pax['dropRouteName'] = route_ids_map[pax['dropRouteId']]['new_name']
        pax['dropRouteId'] = route_ids_map[pax['dropRouteId']]['target_id']
        if 'guardianName' in pax:
            pax['guardianName'] = generate_random_male_name()
        result = create_single_passenger(pax, target_school_id)
        print('copied paxId', result['id'])
    print('copied', len(all_paxes), 'passengers')


def get_random_plate_no():
    sample_rto_nos = random.choice(["26", "55", "74"])
    random_number = "".join([random.choice(string.digits) for i in range(4)])
    return "HR " + sample_rto_nos + " " + random.choice(string.ascii_uppercase) + " " + random_number


def get_random_model_name():
    sample_models = ["Eicher Skyline", "Force Minibus", "Mahindra Bus"]
    return random.choice(sample_models)


def get_random_seat_count():
    sample_seat_counts = [24, 36, 48, 52]
    return random.choice(sample_seat_counts)


def get_random_phone_number():
    return "44" + "".join([random.choice(string.digits) for i in range(8)])


if __name__=="__main__":
    print('connecting to mongodb')
    utils.mongo_cli = MongoClient('localhost')
    utils.mongo_cli.the_database.authenticate('localUser', 'protip007', source='admin')
    utils.app_db = utils.mongo_cli['trackerdb']
    print('mongo client connected')

    royal_oak = utils.mongo_cli['trackerdb']['schools'].find_one({'username': "royaloak"})
    source_school_id = royal_oak['_id']
    demo_school = utils.mongo_cli['trackerdb']['schools'].find_one({'username': "demo"})
    target_school_id = demo_school['_id']
    print('copying from royaloak school with id', source_school_id)
    print('copying into demo school with id', target_school_id)

    vehicle_ids_map = copy_vehicles(source_school_id, target_school_id)
    driver_ids_map = copy_drivers(source_school_id, target_school_id)
    route_ids_map = copy_routes(source_school_id, target_school_id, vehicle_ids_map, driver_ids_map)
    copy_passengers(source_school_id, target_school_id, route_ids_map)
    print('finished copying')
