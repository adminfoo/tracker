var tdb = db.getSisterDB('trackingdb');

var colls = tdb.getCollectionNames();
colls.forEach(function(coll){
    if(coll != "system.indexes"){
        tdb[coll].ensureIndex({time: 1});
    }
});
