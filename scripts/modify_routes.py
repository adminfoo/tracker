from datetime import datetime

from voluptuous import Schema, Required, Any, REMOVE_EXTRA

from tracker.utils.common_utils import hr_min_str_to_time
from pymongo import MongoClient
from copy import deepcopy

from tracker.utils.validations import validate_int, validate_str, validate_datetime, validate_float, validate_object_id

validate_timing_obj = Schema({
    Required('hr', default=8): validate_int,
    Required('min', default=15): validate_int,
    Required('meridian', default="AM"): Any("AM", "PM")
})
validate_stop = Schema({
    'id': validate_str,
    'name': validate_str,
    'timeObj': validate_timing_obj,  # the entered time object in UI by school admin
    'time': validate_datetime,  # avg arrival time calculated automatically
    'loc': {'type': "Point", 'coordinates': [validate_float]},
    'lat': validate_float,
    'lng': validate_float
}, extra=REMOVE_EXTRA)
validate_path_point = Schema({
    'lat': validate_float,
    'lng': validate_float,
    'type': str,
    'time': validate_datetime
}, extra=REMOVE_EXTRA)
validation_schema = Schema({
    'schoolId': validate_object_id,
    'name': validate_str,
    'stops': [validate_stop],

    'vehicleId': Any(validate_object_id, str),
    'vehicleRegistrationNo': str,
    'vehicleDeviceId': str,

    'driverName': str,
    'driverPhone': str,
    'driverLicenseNo': str,

    'helperName': str,
    'helperPhone': str,

    'type': Any("pickup", "drop"),
    Required('startTime', default=datetime(2016, 1, 1, 6, 30)): validate_datetime,
    Required('pathPoints', default=[]): [validate_path_point],
    'oldRouteId': validate_object_id
}, required=True, extra=REMOVE_EXTRA)


def create_new_routes(route):
    # make new routes acc to trips of route object
    # save each newly created route in db
    # remove this route from db
    print('route id', route['_id'], 'name', route['name'])
    inserted_ids = []
    if 'trips' not in route or len(route['trips']) == 0:
        t1 = {
            'pathPoints': route['pathPoints'] if 'pathPoints' in route else [],
            'startTime': datetime.combine(datetime.now().date(), hr_min_str_to_time(route['stops'][0]['pickupTime'])),
            'name': "pickup",
            'stops': []
        }
        t2 = {
            'pathPoints': route['pathPoints'] if 'pathPoints' in route else [],
            'startTime': datetime.combine(datetime.now().date(), hr_min_str_to_time(route['stops'][-1]['dropTime'])),
            'name': "drop",
            'stops': []
        }
        route['trips'] = [t1, None, t2]
    for trip in route['trips']:
        if trip is None:
            continue
        r = deepcopy(route)
        del r['_id']
        r['oldRouteId'] = route['_id']
        r['pathPoints'] = trip['pathPoints']
        r['startTime'] = trip['startTime']
        r['type'] = "pickup" if trip['name'] == "pickup" else "drop"
        r['sequence'] = 0 if trip['name'] == "drop0" else 1
        del r['trips']
        for stop in r['stops']:
            matched = next((s for s in trip['stops'] if s['id'] == stop['id']), None)
            print(stop)
            stop['timeObj'] = stop['pickupTime'] if r['type'] == "pickup" else stop['dropTime']
            if matched is not None:
                stop['time'] = matched['time']
            else:
                stop['time'] = datetime.combine(datetime.now().date(), hr_min_str_to_time(stop['pickupTime'] if r['type'] == "pickup" else stop['dropTime']))
            del stop['pickupTime']
            del stop['dropTime']
        print('create', trip['name'], 'route', route['name'])
        r = validation_schema(r)
        res = cli.trackerdb.routes.insert_one(r)
        inserted_ids.append(res.inserted_id)
    return inserted_ids


if __name__ == "__main__":
    # connect mongo
    cli = MongoClient()
    all_routes = [r for r in cli.trackerdb.routes.find()]
    for route in all_routes:
        new_route_ids = create_new_routes(route)
        if len(new_route_ids) == 0:
            print('no trips for route id', route['_id'], 'name', route['name'])
            continue
        paxes = [p for p in cli.trackerdb.passengers.find({'routeId': route['_id']})]
        pickup_route_id = new_route_ids[0]
        for pax in paxes:
            print('passenger id', str(pax['_id']), 'name', pax['name'])
            drop_route_id = new_route_ids[1] if 'course' in pax and pax['course'] in ["Pre Nursery", "Nursery", "KG"] else new_route_ids[-1]
            set_dict = {
                'pickupRouteId': pickup_route_id, 'dropRouteId': drop_route_id,
                'pickupRouteName': pax['routeName'], 'dropRouteName': pax['routeName'],
                'pickupStopId': pax['stopId'], 'dropStopId': pax['stopId'],
                'pickupStopName': pax['stopName'], 'dropStopName': pax['stopName']
            }
            unset_dict = {'routeId': None, 'routeName': None, 'stopId': None, 'stopName': None, 'serviceType': None}
            cli.trackerdb.passengers.update({'_id': pax['_id']}, {'$set': set_dict, '$unset': unset_dict})
    print('finished')
