// Date: 10 April 2016

db = db.getSisterDB('trackerdb');

print('renaming imei to deviceId in vehicles collection');
db.vehicles.update({}, {'$rename': {'imei': 'deviceId'}}, false, true);

print('renaming imei to deviceId in trips collection');
db.trips.update({}, {'$rename': {'imei': 'deviceId'}}, false, true);

print('renaming vehicleImei to vehicleDeviceId in routes collection');
db.routes.update({}, {'$rename': {'vehicleImei': 'vehicleDeviceId'}}, false, true);

print('renaming imei to deviceId in route_trips collection');
db.route_trips.update({}, {'$rename': {'imei': 'deviceId'}}, false, true);

print('done');