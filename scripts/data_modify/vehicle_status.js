// Date: 17 April 2016

db = db.getSisterDB('trackerdb');

print('creating a field "status" in vehicles collection');
var cursor = db.vehicles.find();

cursor.forEach(function(vehicle){
    print(vehicle._id)
    print(vehicle.parkedInSchool)
    var status = {
        lastUpdated : ISODate(new Date().toISOString()),
        reachable : false,
        moving : false,
        parked : vehicle.parkedInSchool,
        ignition : false,
        ac : false,
        idle : false
    };
    db.vehicles.update({_id : vehicle._id}, {$unset: {parkedInSchool: null}, $set: {status : status}})
});

print('done');