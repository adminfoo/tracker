// Date: 22 April 2016

db = db.getSisterDB('trackerdb');

print('creating a field "tripLastChecked" and "parkingLoc" in vehicles collection');
var cursor = db.vehicles.find();

cursor.forEach(function(vehicle){
    print(vehicle._id);
    var tripLastChecked = vehicle.installTime;
    var tripCursor = db.trips.find().sort({_id: -1}).limit(1);
    tripCursor.forEach(function(trip){
        tripLastChecked = trip.endTime;
    });
    var school = db.schools.findOne({_id: vehicle.schoolId});
    db.vehicles.update({_id : vehicle._id}, {$set: {tripLastChecked: tripLastChecked, parkingLoc: school.loc}});
});

print('done');
