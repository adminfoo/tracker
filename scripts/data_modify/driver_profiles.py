from pymongo import MongoClient

from tracker import utils
from tracker.rest_resource import object_store
from tracker.utils.validations import validate_object_id

print('connecting to mongodb')
utils.mongo_cli = MongoClient('localhost')
# utils.mongo_cli.the_database.authenticate('localUser', 'protip007', source='admin')
utils.app_db = utils.mongo_cli['trackerdb']
print('mongo client connected')


def get_driver_id(details):
    existing = utils.app_db["drivers"].find_one({'phone': details['phone']})
    if existing:
        return existing['_id']
    else:
        new_driver_id = create_driver(details)
        print('driver created', details['phone'], details['name'])
        return new_driver_id


def create_driver(details):
    return utils.app_db['drivers'].insert_one(details).inserted_id


route_projection = ['schoolId', 'name', 'driverName', 'driverPhone', 'driverLicenseNo', 'helperName', 'helperPhone']
all_routes = object_store['routes'].list_all(filters=None, projection=route_projection)
for route in all_routes:
    driver_details = {'schoolId': route['schoolId'], 'name': route['driverName'], 'phone': route['driverPhone'],
                      'licenseNo': route['driverLicenseNo'], 'role': "driver", 'email': ""}
    driver_id = get_driver_id(driver_details)
    helper_details = {'schoolId': route['schoolId'], 'name': route['helperName'], 'phone': route['helperPhone'],
                      'licenseNo': "", 'role': "helper", 'email': ""}
    helper_id = get_driver_id(helper_details)
    print(route['id'])
    utils.mongo_cli['trackerdb']['routes'].update({'_id': validate_object_id(route['id'])},
                                                  {'$set': {'driverId': driver_id, 'helperId': helper_id}})
