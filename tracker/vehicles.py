from datetime import datetime

from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config
from voluptuous import Schema, REMOVE_EXTRA, Required
import logging
from tracker.utils import app_db
from tracker.devices import get_current_status
from tracker.rest_resource import Resource
from tracker.tasks import create_task, resolve_task, vehicle_device_id_task_name
from tracker.utils.common_utils import get_projected_item
from tracker.utils.validations import validate_str, validate_object_id, validate_int_foolproof, validate_datetime, \
    validate_float

logger = logging.getLogger(__name__)

collection = "vehicles"
default_status = {
    'lastUpdated': datetime.now(),
    'reachable': False,
    'moving': False,
    'parked': False,
    'ignition': False,
    'ac': False,
    'idle': False
}
validation_schema = Schema({
    Required('schoolId'): validate_object_id,
    Required('deviceId', default=""): str,
    Required('registrationNo'): validate_str,
    Required('model'): str,
    'name': str,
    'noOfSeats': validate_int_foolproof,
    'installTime': validate_datetime,
    Required('status', default=default_status): dict,

    'parkingLoc': Schema({'type': "Point", 'coordinates': [validate_float], 'name': str}),
    'tripLastChecked': datetime,

    'speedLimit': validate_int_foolproof
}, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


@view_config(route_name="vehicles", request_method="GET", permission="school", renderer='json')
def list_all(request):
    filters = request.filters
    filters['schoolId'] = request.user['schoolId']
    return objects.list_all(filters=filters, projection=request.projection)


@view_config(route_name="vehicles", request_method="POST", permission="school", renderer='json')
def create(request):
    data = request.json_body
    data['schoolId'] = request.user['schoolId']
    data['installTime'] = datetime.now()
    ret_obj = objects.create(data)
    if len(data['deviceId']) == 0:
        msg = "Assign a deviceId to vehicle {} School {}".format(data['registrationNo'], request.user['schoolName'])
        create_task(vehicle_device_id_task_name, msg, vehicle_id=ret_obj['id'])
    return ret_obj


@view_config(route_name="vehicle_details", request_method="GET", permission="school", renderer='json')
def detail(request):
    item_id = request.matchdict['pk']
    details = objects.detail(item_id)
    if details is None or details['schoolId'] != request.user['schoolId']:
        raise HTTPNotFound("No vehicle found")
    return details


@view_config(route_name="vehicle_details", request_method="PATCH", permission="school", renderer='json')
def edit(request):
    item_id = validate_object_id(request.matchdict['pk'])
    old_data = objects.detail(item_id)
    data = request.json_body
    if request.user['userType'] == "school":
        allowed_fields = ["model", "name", "noOfSeats", "registrationNo", "speedLimit"]
        data = get_projected_item(data, allowed_fields)
    data['schoolId'] = request.user['schoolId']

    if 'deviceId' in data and len(data['deviceId']) > 0 and data['deviceId'] != old_data['deviceId']:
        data['installTime'] = datetime.now()
    if 'status' in data:  # status is updated automatically, should not be updated from here
        del data['status']
    if 'tripLastChecked' in data:  # tripLastChecked is updated automatically, should not be updated from here
        del data['tripLastChecked']

    ret_obj = objects.edit(item_id, data)

    if 'deviceId' in data and data['deviceId'] != old_data['deviceId']:
        if len(data['deviceId']) == 0:
            msg = "Assign a deviceId to vehicle {} School {}".format(data['registrationNo'], request.user['schoolName'])
            create_task(vehicle_device_id_task_name, msg, vehicle_id=item_id)
        else:
            data['installTime'] = datetime.now()
            resolve_task(vehicle_device_id_task_name, vehicle_id=item_id)

    item_details = objects.detail(item_id)
    # edit if being used in any route
    route_edit_data = {'vehicleRegistrationNo': item_details['registrationNo'], 'vehicleModel': item_details['model'],
                       'vehicleDeviceId': item_details['deviceId'], 'noOfSeats': item_details['noOfSeats']}
    app_db['routes'].update({'vehicleId': item_id}, {'$set': route_edit_data}, multi=True)

    return ret_obj


@view_config(route_name="vehicle_details", request_method="DELETE", permission="superadmin", renderer='json')
def delete(request):
    item_id = request.matchdict['pk']
    ret_obj = objects.delete(item_id, extra_filters={'schoolId': request.user['schoolId']})
    resolve_task(vehicle_device_id_task_name, vehicle_id=item_id)
    return ret_obj


@view_config(route_name="temp", request_method="GET", permission="school", renderer='json')
def temp(request):
    filters = request.filters
    filters['schoolId'] = request.user['schoolId']
    ret_obj = objects.list_all(filters=filters, projection=request.projection)
    for v in ret_obj:
        loc_info = get_current_status(v['deviceId'])
        if loc_info:
            v.update(loc_info)
        v['driverName'] = ""
    return ret_obj


@view_config(route_name="vehicle_live", request_method="GET", renderer='json')
def get_tracking_info(request):
    filters = request.filters
    filters['schoolId'] = request.user['schoolId']
    ret_obj = objects.list_all(filters=filters, projection=request.projection)
    for v in ret_obj:
        loc_info = get_current_status(v['deviceId'])
        if loc_info:
            v.update(loc_info)
        v['driverName'] = ""
    return ret_obj
