from pyramid.view import view_config
from tracker import utils


@view_config(route_name="all_routes_view", renderer="json")
def all_stops_view(request):
    color_map = {'pickup': "green", 'drop0': "red", 'drop': "blue"}
    ret = {'lines': [], 'markers': [], 'data': None}
    all_routes = [r for r in utils.app_db['routes'].find()]
    for r in all_routes:
        if 'trips' not in r:
            continue
        for trip in r['trips']:
            l = {'name': trip['name'] + " " + r['name'], 'color': color_map[trip['name']], 'points': trip['pathPoints']}
            ret['lines'].append(l)
            for s in trip['stops']:
                s['msg'] = "route: " + r['name'] + "<br>stop: " + s['name'] + "<br>type: " + trip['name']
            m = {'name': trip['name'] + " " + r['name'], 'color': "green", 'points': trip['stops']}
            ret['markers'].append(m)
    return ret

