import random
import string
from collections import defaultdict
from datetime import datetime
from datetime import timedelta

from geopy.distance import vincenty
from pyramid.httpexceptions import HTTPNotFound, HTTPConflict
from pyramid.view import view_config
from voluptuous import Schema, Any, REMOVE_EXTRA, Required
import logging
from tracker import utils
from tracker.rest_resource import Resource, object_store
from tracker.tasks import create_task, resolve_task, mark_route_task_name
from tracker.trips import find_points_in_interval
from tracker.utils import app_db
from tracker.utils.sms_utils import send_sms
from tracker.utils.validations import validate_str, validate_object_id, validate_int, validate_float, validate_datetime

logger = logging.getLogger(__name__)

collection = "routes"
validate_timing_obj = Schema({
    Required('hr', default=8): validate_int,
    Required('min', default=15): validate_int,
    Required('meridian', default="AM"): Any("AM", "PM")
})
validate_stop = Schema({
    'id': validate_str,
    'name': validate_str,
    'timeObj': validate_timing_obj,  # the entered time object in UI by school admin
    'time': validate_datetime,  # avg arrival time calculated automatically
    'loc': {'type': "Point", 'coordinates': [validate_float]},
    'lat': validate_float,
    'lng': validate_float,
    'timeToFinish': int  # min run time (in secs) to finish this route, from this stop
}, extra=REMOVE_EXTRA)
validate_path_point = Schema({
    'lat': validate_float,
    'lng': validate_float,
    'type': str,
    'time': validate_datetime
}, extra=REMOVE_EXTRA)
validate_checkpoint = Schema({
    'timeToFinish': int,  # min run time (in secs) to finish this route, from this checkpoint
    'loc': {'type': "Point", 'coordinates': [validate_float]},
    'lat': validate_float,
    'lng': validate_float
}, extra=REMOVE_EXTRA)
validation_schema = Schema({
    'schoolId': validate_object_id,
    'name': validate_str,
    'stops': [validate_stop],

    'vehicleId': Any(validate_object_id, str),
    'vehicleRegistrationNo': str,
    'vehicleDeviceId': str,

    'driverId': validate_object_id,
    'driverName': str,
    'driverPhone': str,
    'driverLicenseNo': str,

    'helperId': validate_object_id,
    'helperName': str,
    'helperPhone': str,

    'type': Any("pickup", "drop"),
    Required('startTime', default=datetime(2016, 1, 1, 6, 30)): validate_datetime,
    Required('pathPoints', default=[]): [validate_path_point],

    Required('checkpoints', default=[]): [validate_checkpoint]
}, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


@view_config(route_name="routes", request_method="GET", permission="school", renderer='json')
def list_all(request):
    filters = request.filters
    filters['schoolId'] = request.user['schoolId']
    filters['pathPoints'] = {'$exists': True}
    projection = request.projection or {}
    if not projection:
        projection['pathPoints'] = False
    items = objects.list_all(filters=filters, projection=projection)
    for item in items:
        item['totalPassengers'] = utils.app_db['passengers'].count({'pickupRouteId': item['id']}) + utils.app_db['passengers'].count({'dropRouteId': item['id']})
    return items


@view_config(route_name="routes", request_method="POST", permission="school", renderer='json')
def create(request):
    # create both pickup and drop routes
    data = request.json_body

    pickup = data.copy()
    pickup['type'] = "pickup"
    p_res = create_route(pickup, request.user['schoolId'], request.user['schoolName'])

    drop = data.copy()
    drop['type'] = "drop"
    d_res = create_route(drop, request.user['schoolId'], request.user['schoolName'])

    return {'pickup': p_res, 'drop': d_res}


def create_route(data, school_id, school_name):
    data['schoolId'] = school_id

    vehicle_detail = object_store['vehicles'].detail(data['vehicleId'])
    data['vehicleRegistrationNo'] = vehicle_detail['registrationNo']
    data['vehicleDeviceId'] = vehicle_detail['deviceId']

    should_mark = False

    if 'stops' in data:
        empty_stop_indices = []
        i = 0
        for s in data['stops']:
            if 'name' not in s or len(s['name']) == 0:
                empty_stop_indices.append(i)
                logger.debug({'msg': "empty stop"})
            if 'id' not in s:
                # TODO:  check duplicacy
                s['id'] = ''.join([random.choice(string.ascii_lowercase) for i in range(4)])
            if 'lat' not in s or 'lng' not in s:
                should_mark = True
                s['lat'] = 28.1
                s['lng'] = 77.1
            s['loc'] = {'type': "Point", 'coordinates': [s['lng'], s['lat']]}
            i += 1
        empty_stop_indices.reverse()
        for index in empty_stop_indices:
            del data['stops'][index]

    if not should_mark:
        data['pathPoints'] = [{'lat': s['lat'], 'lng': s['lng'], 'type': "stop", 'time': datetime.now()} for s in data['stops']]

    driver_details = object_store['drivers'].detail(data['driverId'])
    data['driverPhone'] = driver_details['phone']
    data['driverName'] = driver_details['name']
    data['driverLicenseNo'] = driver_details['licenseNo']
    helper_details = object_store['drivers'].detail(data['helperId'])
    data['helperPhone'] = helper_details['phone']
    data['helperName'] = helper_details['name']

    ret_obj = objects.create(data)

    if should_mark:
        task_msg = "Mark the " + data['type'] + " route " + data['name'] + " vehicle " + data['vehicleRegistrationNo'] + " on map. School " + school_name
        create_task(mark_route_task_name, task_msg, route_id=ret_obj['id'])
    return ret_obj


@view_config(route_name="route_details", request_method="GET", permission="school", renderer='json')
def detail(request):
    item_id = request.matchdict['pk']
    item = objects.detail(item_id)
    if item is None or item['schoolId'] != request.user['schoolId']:
        raise HTTPNotFound("No route found")
    item['totalPassengers'] = utils.app_db['passengers'].count({'$or': [{'pickupRouteId': item['id']}, {'dropRouteId': item['id']}]})
    return item


@view_config(route_name="route_details", request_method="PATCH", permission="school", renderer='json')
def edit(request):
    item_id = validate_object_id(request.matchdict['pk'])
    existing = objects.detail(item_id)

    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    data['schoolId'] = request.user['schoolId']

    if 'vehicleId' in data:
        vehicle_detail = object_store['vehicles'].detail(data['vehicleId'])
        data['vehicleRegistrationNo'] = vehicle_detail['registrationNo']
        data['vehicleDeviceId'] = vehicle_detail['deviceId']
        data['noOfSeats'] = vehicle_detail['noOfSeats']

    resolved = -1
    if 'stops' in data:
        empty_stop_indices = []
        i = 0
        resolved = 1
        for s in data['stops']:
            if 'name' not in s or len(s['name']) == 0:
                empty_stop_indices.append(i)
                logger.debug({'msg': "empty stop"})
            if 'id' not in s:
                # TODO: check duplicate id
                s['id'] = ''.join([random.choice(string.ascii_lowercase) for i in range(4)])
            if 'lat' not in s or 'lng' not in s:
                resolved = 0
                s['lat'] = 28.1
                s['lng'] = 77.1
            s['loc'] = {'type': "Point", 'coordinates': [s['lng'], s['lat']]}
            i += 1
        empty_stop_indices.reverse()
        for index in empty_stop_indices:
            del data['stops'][index]

        # check if any stop is removed that should not have been removed, i.e. active stop with passengers
        for ex_stop in existing['stops']:
            new_stop =  next((s for s in data['stops'] if s['id'] == ex_stop['id']), None)
            if new_stop is None:
                stop_pax = utils.app_db['passengers'].find_one({'stopId': ex_stop['id'], '$or': [{'pickupRouteId': item_id}, {'dropRouteId': item_id}]})
                if stop_pax is not None:
                    raise HTTPConflict("Can not remove the active stop " + ex_stop['name'])

    # TODO: generate path points again

    if 'driverId' in data:
        driver_details = object_store['drivers'].detail(data['driverId'])
        data['driverPhone'] = driver_details['phone']
        data['driverName'] = driver_details['name']
        data['driverLicenseNo'] = driver_details['licenseNo']
    if 'helperId' in data:
        helper_details = object_store['drivers'].detail(data['helperId'])
        data['helperPhone'] = helper_details['phone']
        data['helperName'] = helper_details['name']

    ret_obj = objects.edit(item_id, data)
    if resolved == 0:
        task_msg = "Mark the route with id " + str(item_id) + " on map. School " + request.user['schoolName']
        create_task(mark_route_task_name, task_msg, route_id=item_id)
    elif resolved == 1:
        # remove superadmin task
        resolve_task(mark_route_task_name, route_id=item_id)

    # update route name and stop names in students if changed
    if 'name' in data and existing['name'] != data['name']:
        utils.app_db['passengers'].update({'pickupRouteId': item_id}, {'$set': {'pickupRouteName': data['name']}})
        utils.app_db['passengers'].update({'dropRouteId': item_id}, {'$set': {'dropRouteName': data['name']}})
    if 'stops' in data:
        for existing_stop in existing['stops']:
            new_stop = next((s for s in data['stops'] if s['id'] == existing_stop['id']), None)
            if new_stop is not None and new_stop['name'] != existing_stop['name']:
                utils.app_db['passengers'].update({'pickupRouteId': item_id, 'pickupStopId': existing_stop['id']}, {'$set': {'pickupStopName': new_stop['name']}})
                utils.app_db['passengers'].update({'dropRouteId': item_id, 'dropStopId': existing_stop['id']}, {'$set': {'dropStopName': new_stop['name']}})

    return ret_obj


@view_config(route_name="route_details", request_method="DELETE", permission="school", renderer='json')
def delete(request):
    item_id = validate_object_id(request.matchdict['pk'])
    route_pax = utils.app_db['passengers'].find_one({'$or': [{'pickupRouteId': item_id}, {'dropRouteId': item_id}]})
    if route_pax is not None:
        raise HTTPConflict('can not delete active route with passengers')
    ret_obj = objects.delete(item_id, extra_filters={'schoolId': request.user['schoolId']})
    resolve_task(mark_route_task_name, route_id=item_id)
    return ret_obj


@view_config(route_name="bulk_sms", request_method="POST", permission="school", renderer='json')
def bulk_sms_view(request):
    data = request.json_body
    if 'routeId' in data:
        route_ids = validate_object_id(data['routeIds'])
        paxes = list(app_db['passengers'].find({'pickupRouteId': {'$in': route_ids}}, {'phone': True})) + \
                list(app_db['passengers'].find({'dropRouteId': {'$in': route_ids}}, {'phone': True}))
    else:
        paxes = list(app_db['passengers'].find({}, {'phone': True}))
    phones = [p['phone'] for p in paxes]
    sms_content = data['msg']
    # TODO: enable it properly
    bulk_sms_enabled = False
    if bulk_sms_enabled:
        send_sms(phones, sms_content, tag="BULK_SMS")
    return {'msg': "SMS sent successfully!!"}


def reset_route_path(route):
    school_details = utils.app_db['schools'].find_one({'_id': route['schoolId']})

    to_date = datetime.now()
    from_date = to_date - timedelta(10)

    if school_details['gpsUpDate'] is None:
        logger.warn({'msg': "gpsUpDate not configured", '_id': school_details['_id'], 'school': school_details['name']})
        return
    if from_date < school_details['gpsUpDate']:
        from_date = school_details['gpsUpDate']

    # TODO: instead of school location, identify the parking location for this particular vehicle
    school_geometry = school_details['loc']
    from tracker.route_trips import get_pickup_drop_trippss
    trip_dict = get_pickup_drop_trippss(route['vehicleDeviceId'], from_date, to_date, school_geometry)
    if len(trip_dict) == 0:
        return
    same_veh_routes = objects.list_all(filters={'vehicleId': route['vehicleId'], 'pathPoints': {'$exists': True}},
                                       projection={'startTime': True})
    same_veh_routes.sort(key=lambda r: r['startTime'].time())
    i = 0
    for i in range(len(same_veh_routes)):
        if same_veh_routes[i]['id'] == (route['id'] if 'id' in route else route['_id']):
            break
    if len(trip_dict[i]['trips']) == 0 or len(trip_dict[i]['trips']) == 0:
        return

    trips = trip_dict[i]['trips']
    route['startTime'] = datetime.combine(datetime.now().date(), min([t['startTime'].time() for t in trips]))
    route['stops'] = sorted(find_actual_stop_timings(route, trips), key=lambda s: s['time'].time())
    route['pathPoints'] = []
    trip_pts = find_points_in_interval(route['vehicleDeviceId'], trips[-1]['startTime'], trips[-1]['endTime'])
    for pt in trip_pts:
        route['pathPoints'].append({'lat': pt['lat'], 'lng': pt['lng'], 'time': pt['time'], 'type': "point"})
    route['checkpoints'] = get_checkpoints(trip_pts)
    route_id = validate_object_id(route['_id'] if '_id' in route else route['id'])
    utils.app_db['routes'].update({'_id': route_id}, {'$set': {
        'startTime': route['startTime'],
        'stops': route['stops'],
        'pathPoints': route['pathPoints']
    }})


def get_checkpoints(path_points):
    next_dist = 0.5
    distance = 0.0
    checkpoints = []
    for i in range(1, len(path_points)):
        distance += vincenty((path_points[i - 1]['lat'], path_points[i - 1]['lng']), (path_points[i]['lat'], path_points[i]['lng'])).kilometers
        if distance >= next_dist:
            ckpt = {
                'lat': path_points[i]['lat'],
                'lng': path_points[i]['lng'],
                'loc': {'type': "Point", 'coordinates': [path_points[i]['lng'], path_points[i]['lat']]},
                'timeToFinish': 1000000
            }
            checkpoints.append(ckpt)
            next_dist += 0.5
    return checkpoints


def populate_time_to_finish(checkpoints, trips):
    for trip in trips:
        trip_pts = [p for p in utils.gps_db[trip['vehicleDeviceId']].find({'time': {'$gte': trip['startTime'], '$lte': trip['endTime']}}).sort([('time', -1)])]


def find_actual_stop_timings(route, trips):
    # Returning the earliest time that happened for any stop
    stop_dict = {}
    for trip in trips:
        trip_pts = [p for p in utils.gps_db[trip['vehicleDeviceId']].find({'time': {'$gte': trip['startTime'], '$lte': trip['endTime']}}).sort([('time', -1)])]
        for s in route['stops']:
            filters = {
                'time': {'$gte': trip['startTime'], '$lte': trip['endTime']},
                'loc': {'$near': {'$geometry': s['loc'], '$maxDistance': 50}}
            }
            if route['type'] == "pickup":
                cur = utils.gps_db[route['vehicleDeviceId']].find(filters).sort([('time', -1)]).limit(1)
            else:
                cur = utils.gps_db[route['vehicleDeviceId']].find(filters).sort([('time', 1)]).limit(1)
            recs = [x for x in cur]
            if len(recs) == 0:
                continue
            tim = recs[0]['time']
            if s['id'] not in stop_dict:
                stop_dict[s['id']] = s.copy()
                stop_dict[s['id']]['time'] = tim
            elif stop_dict[s['id']]['time'].time() > tim.time():
                stop_dict[s['id']]['time'] = tim
    return list(stop_dict.values())


def add_running_time_to_stops(route, trips):
    school_run_duration = 6 * 60 * 60
    for trip in trips:
        from_time = trip['startTime']
        for s in route['stops']:
            if route['type'] == "pickup":
                exit_filters = {
                    'time': {'$gte': from_time, '$lte': trip['endTime']},
                    'loc': {'$near': {'$geometry': s['loc'], '$maxDistance': 50}}
                }
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find(exit_filters).sort([('time', -1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                exit_pt = pts[0]
                entry_filters = {
                    'time': {'$gte': from_time, '$lte': exit_pt['time']},
                    'loc': {'$near': {'$geometry': s['loc'], '$minDistance': 50}}
                }
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find(entry_filters).sort([('time', -1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find({'time': {'$gt': pts[0]['time'], '$lte': exit_pt['time']}}).sort([('time', 1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                entry_pt = pts[0]
            else:
                entry_filters = {
                    'time': {'$gte': from_time, '$lte': trip['endTime']},
                    'loc': {'$near': {'$geometry': s['loc'], '$maxDistance': 50}}
                }
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find(entry_filters).sort([('time', 1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                entry_pt = pts[0]
                exit_filters = {
                    'time': {'$gte': entry_pt['time'], '$lte': trip['endTime']},
                    'loc': {'$near': {'$geometry': s['loc'], '$minDistance': 50}}
                }
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find(exit_filters).sort([('time', 1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                pts = [p for p in utils.gps_db[route['vehicleDeviceId']].find({'time': {'$gte': entry_pt['time'], '$lt': pts[0]['time']}}).sort([('time', -1)]).limit(1)]
                if len(pts) == 0:
                    logger.info("possible network outage in trip from " + str(trip['startTime']) + " to " + str(trip['endTime']) + " vehicle " + str(route['vehicleRegistrationNo']))
                    continue
                exit_pt = pts[0]
            s['entryTime'] = entry_pt['time']
            s['exitTime'] = exit_pt['time']
            from_time = s['exitTime']
        for i in range(len(route['stops'])):
            run_start = route['stops'][i - 1]['exitTime'] if i > 0 else trip['startTime']
            run_end = route['stops'][i]['entryTime']
            run_duration = int((run_end - run_start).total_seconds())
            if run_duration < route['stops'][i]['runTime']:
                route['stops'][i]['runTime'] = run_duration
        this_school_run = trip['endTime'] - route['stops'][-1]['exitTime']
        if this_school_run < school_run_duration:
            school_run_duration = this_school_run
    curr_run_time = school_run_duration
    i = len(route['stops']) - 1
    while i >= 0:
        route['stops']['remTime'] = curr_run_time
        curr_run_time += route['stops'][i]['runTime'] + 15  # assume 15 sec stoppage at each stop
    total_run_time = curr_run_time
    return total_run_time


def route_stop_count_in_school(school_id):
    """
    Get the number of routes and number of stops in a school
    In db pickup and drop are stored as separate routes
    While counting routes, the pickup and drop combination is taken as single route
    The stops are counted by unique names
    :param school_id:
    :return: route count and stop count as int
    """
    all_routes = objects.list_all(filters={'schoolId': school_id}, projection={'stops': True, 'vehicleId': True})
    veh_dict = defaultdict(int)
    for r in all_routes:
        veh_dict[r['vehicleId']] += 1
    route_count = sum([int((n + 1) / 2) for v, n in veh_dict.items()])
    stop_dict = {}
    for r in all_routes:
        for s in r['stops']:
            stop_dict[s['name']] = True
    return route_count, len(stop_dict)
