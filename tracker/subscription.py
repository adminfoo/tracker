from datetime import datetime
from pyramid.httpexceptions import HTTPConflict
from pyramid.view import view_config
from voluptuous import Schema, REMOVE_EXTRA, Any
import logging
from tracker.rest_resource import Resource
from tracker.utils.validations import validate_str, validate_float, validate_datetime

logger = logging.getLogger(__name__)

collection = "subscription_plans"
validation_schema = Schema({
    'name': validate_str,
    'amount': validate_float,
    'months': int,
    'active': bool,
    'creationTime': validate_datetime,
    'inactiveTime': Any(None, validate_datetime)
}, required=True, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


@view_config(route_name="subscription_plans", request_method="GET", renderer='json')
def list_all(request):
    projection = {'name': True, 'amount': True, 'months': True}
    filters = request.filters
    if request.user['userType'] != "superadmin":
        filters['active'] = True
    else:
        projection['active'] = True
        projection['creationTime'] = True
        projection['inactiveTime'] = True
    return objects.list_all(filters=filters, projection=projection)


@view_config(route_name="subscription_plans", request_method="POST", permission="superadmin", renderer='json')
def create(request):
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    data['active'] = True
    data['creationTime'] = datetime.now()
    data['inactiveTime'] = None
    return objects.create(data)


@view_config(route_name="subscription_plan_details", request_method="DELETE", permission="superadmin", renderer='json')
def delete(request):
    item_id = request.matchdict['pk']
    item_details = objects.detail(item_id)
    if item_details['active'] is False:
        return HTTPConflict("This plan is already inactive")
    return objects.edit(item_id, data={'active': False, 'inactiveTime': datetime.now()})
