from pyramid.view import view_config
from voluptuous import Schema
import logging
from tracker import utils
from tracker.rest_resource import Resource

logger = logging.getLogger(__name__)

collection = "tasks"
validation_schema = Schema({})

objects = Resource(collection, validation_schema)

vehicle_device_id_task_name = "ADD_VEHICLE"
mark_route_task_name = "ADD_STOP_LOCATION"


@view_config(route_name="tasks", request_method="GET", permission="superadmin", renderer='json')
def list_all(request):
    filters = request.filters
    return objects.list_all(filters=filters, projection=request.projection)


def create_task(task_type, msg, **kwargs):
    data = {
        'task_type': task_type
    }
    data.update(kwargs)
    exists = utils.app_db[collection].find_one(data)
    data['msg'] = msg
    if not exists:
        utils.app_db[collection].insert_one(data)


def resolve_task(task_type, **kwargs):
    data = {
        'task_type': task_type
    }
    data.update(kwargs)
    utils.app_db[collection].remove(data)
