"""
This module contains the model of a vehicle trip
A trip starts when the vehicle leaves the school campus(parking spot)
and ends when the vehicle re-enters the school campus(parking spot)

If the GPS connection is lost for a long period of time, the trip will not be made.
It is assumed that several trips might have happened during the disconnected period.

For future:
A single trip might contains several sub-trips
For example, the vehicle might exit school and go to service center.
Then the vehicle might go to paint shop and after that return to the school
So this single trip will consist of 3 sub trips
"""

from datetime import timedelta, datetime
from geopy.distance import vincenty
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config
from voluptuous import Schema, REMOVE_EXTRA
import logging
from tracker import utils
from tracker.rest_resource import Resource, object_store
from tracker.utils.common_utils import parse_from_to_date, get_mongo_id, secs_to_text
from tracker.utils.location_utils import get_approx_distance
from tracker.utils.validations import validate_str, validate_object_id, validate_datetime, validate_float, validate_int

logger = logging.getLogger(__name__)

collection = "trips"
validation_schema = Schema({
    'deviceId': validate_str,
    'schoolId': validate_object_id,
    'vehicleId': validate_object_id,
    "vehicleRegistrationNo": str,
    'vehicleSpeedLimit': validate_int,

    'startTime': validate_datetime,
    'endTime': validate_datetime,

    'startLoc': Schema({'type': "Point", 'coordinates': [float], 'name': str}),
    'endLoc': Schema({'type': "Point", 'coordinates': [float], 'name': str}),

    'distance': validate_float,             # kms
    'tripDuration': validate_float,         # seconds
    'ignitionDuration': validate_float,     # seconds
    'idleDuration': validate_float,         # seconds
    'acDuration': validate_float,           # seconds
    'maxSpeed': int,

    'finished': bool  # if it is False, trip might not be finished
}, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


###############################################################################
# Views
###############################################################################
@view_config(route_name="vehicle_trips", request_method="GET", permission="school", renderer='json')
def list_view(request):
    vehicle_id = validate_object_id(request.params['vehicleId'])
    from_date, to_date = parse_from_to_date(request.params)

    return get_vehicle_trips(vehicle_id, from_date, to_date)


@view_config(route_name="vehicle_trip", request_method="GET", permission="school", renderer='json')
def detail_view(request):
    item_id = request.matchdict['pk']
    details = objects.detail(item_id)
    if details is None or details['schoolId'] != request.user['schoolId']:
        raise HTTPNotFound("No trip found")

    details['points'] = find_points_in_interval(details['deviceId'], details['startTime'], details['endTime'])

    return details


###############################################################################
# Helper functions
###############################################################################
def find_points_in_interval(device_id, start_time, end_time):
    """
    Finds all the location points for a particular device in a specific time range

    :param device_id: The device id (imei) should be equal to name of collection in db
    :param start_time:
    :param end_time: Current time is used, if end_time is None
    :return: list of dict with fields 'lat', 'lng', 'time', 'speed' and optionally 'direction'
    """
    if end_time is None:
        end_time = datetime.now()
    cur = utils.gps_db[device_id].find({'time': {'$gte': start_time, '$lte': end_time}})
    points = []
    for p in cur:
        point = {'lat': p['lat'], 'lng': p['lng'], 'speed': p['speed'], 'time': p['time']}
        if 'direction' in p:
            point['direction'] = p['direction']
        points.append(point)
    cur.close()
    return points


def get_vehicle_trips(vehicle_id, from_date, to_date):
    """
    Fetches all the trips taken by the vehicle between from_date and to_date

    Trips are saved in the db. This function fetches trips directly from db.
    Also creates any new trip taken by the vehicle after the last trip saved in db.

    :param vehicle_id:
    :param from_date: datetime
    :param to_date: datetime
    :return: list of trips
    """
    vehicle_details = object_store['vehicles'].detail(vehicle_id)
    # sanity check
    if 'deviceId' not in vehicle_details or len(vehicle_details['deviceId']) == 0:
        return []

    if datetime.now() - vehicle_details['tripLastChecked'] > timedelta(0, 60):
        # Check for any new trips taken by vehicle and create in db if necessary
        construct_new_trips(vehicle_details)
        # Check if any trips should be merged and merge if necessary
        finalize_trips(vehicle_details)

    filters = {'vehicleId': vehicle_details['id'], 'startTime': {'$gt': from_date}, 'endTime': {'$lt': to_date}}
    projection = {'vehicleRegistrationNo': False, 'finished': False, 'vehicleId': False, 'type': False,
                  'schoolId': False, 'deviceId': False}
    cur = utils.app_db[collection].find(filters, projection).sort([('startTime', 1)])
    vehicle_trips = []
    for r in cur:
        r['id'] = r['_id']
        del r['_id']
        r['startTimeText'] = r['startTime'].strftime("%d %b %I:%M %p")
        r['endTimeText'] = r['endTime'].strftime("%d %b %I:%M %p")
        r['distanceText'] = "{0:.1f} Km".format(r['distance'])
        r['tripDurationText'] = secs_to_text(r['tripDuration'])
        r['overspeedingText'] = get_overspeeding_text(r['maxSpeed'], vehicle_details['speedLimit'])
        vehicle_trips.append(r)
    return vehicle_trips


def get_overspeeding_text(speed, limit):
    if speed <= limit:
        return "OK"
    elif speed < limit * 1.33:
        return "Overspeed"
    else:
        return "Dangerous"


def construct_new_trips(vehicle_details, record_limit=20000):
    """
    Creates trips based on gps data relayed by device
    A trip starts when a vehicle leaves school and ends when it re-enters the school.

    Logic:
    Find the time when the vehicle exits school (200m away from school)
    Find the time when vehicle enters school after the exit time
    Create a trip based on these timings

    Then again find exit time after the previous trip's end time

    TODO: Handle connection loss

    :param vehicle_details:
    :return:
    """
    device_id = vehicle_details['deviceId']
    latest_trip_end_time = vehicle_details['tripLastChecked']
    logger.info(latest_trip_end_time)

    cur = utils.gps_db[device_id].find({'time': {'$gt': latest_trip_end_time}}).sort([('time', 1)]).limit(record_limit)
    res = [x for x in cur]

    raw_trips = get_trips(res)
    for trip_records in raw_trips:
        trip_duration = int((trip_records[-1]['time'] - trip_records[0]['time']).total_seconds())
        distance = calculate_total_distance(trip_records)
        if trip_duration <= 0 or distance < 0.2:
            continue
        trip = {
            'deviceId': device_id,
            'schoolId': vehicle_details['schoolId'],
            'vehicleId': get_mongo_id(vehicle_details),
            'vehicleRegistrationNo': vehicle_details['registrationNo'],
            'vehicleSpeedLimit': vehicle_details['speedLimit'],

            'startTime': trip_records[0]['time'],
            'endTime': trip_records[-1]['time'],

            'startLoc': trip_records[0]['loc'] if 'loc' in trip_records[0] else {'type': "Point", 'coordinates': [trip_records[0]['lng'], trip_records[0]['lat']]},
            'endLoc': trip_records[-1]['loc'] if 'loc' in trip_records[-1] else {'type': "Point", 'coordinates': [trip_records[-1]['lng'], trip_records[-1]['lat']]},

            'distance': distance,
            'tripDuration': trip_duration,
            'ignitionDuration': calculate_ignition_duration(trip_records),
            'acDuration': calculate_ac_duration(trip_records),
            'idleDuration': calculate_idle_duration(trip_records),
            'maxSpeed': int(max(trip_records, key=lambda t: t['speed'])['speed']),

            'finished': False
        }
        if trip['startTime'] > trip['endTime']:
            logger.warning({'msg': "invalid trip detected", 'trip': trip})
            continue
        objects.create(trip)

    logger.debug({'task': "new trips created"})

    if len(res) > 0:
        trip_last_checked = res[-1]['time']
        logger.debug({'msg': "new trip last checked", 'tripLastChecked': trip_last_checked})
        object_store['vehicles'].edit(get_mongo_id(vehicle_details), {'tripLastChecked': trip_last_checked})


def approx_loc_distance(loc1, loc2):
    pt1 = {'lat': loc1['coordinates'][1], 'lng': loc1['coordinates'][0]}
    pt2 = {'lat': loc2['coordinates'][1], 'lng': loc2['coordinates'][0]}
    return get_approx_distance(pt1, pt2)


def finalize_trips(vehicle_details):
    """
    Find if any trips should be merged and merges if necessary

    Criteria of merging:
    If startTime of any trip is less than 3 minutes after endTime of previous trip

    :param vehicle_details:
    :return: None
    """
    trips = [t for t in utils.app_db[collection].find({'finished': False, 'deviceId': vehicle_details['deviceId']})]

    if len(trips) < 3:
        return
    i = len(trips) - 1
    while i > 0:
        if trips[i]['startTime'] - trips[i - 1]['endTime'] < timedelta(0, 181) and \
                approx_loc_distance(trips[i]['startLoc'], vehicle_details['parkingLoc']) > 0.2:
            trips[i - 1] = merge_trips(trips[i - 1], trips[i])
            objects.delete(trips[i]['_id'])
            del trips[i]
        i -= 1

    for i in range(len(trips) - 1):
        trips[i]['finished'] = True

    for i in range(len(trips)):
        trip_id = trips[i]['_id']
        del trips[i]['_id']
        if trips[i]['startTime'] > trips[i]['endTime']:
            logger.warning({'msg': "invalid trip detected", 'trip': trips[i]})
            continue
        objects.edit(trip_id, trips[i])


def get_trips(points):
    if len(points) == 0:
        return []
    items = []
    tmp = []
    for i in range(len(points)):
        if ('speed' in points[i] and points[i]['speed'] > 0) or \
                (points[i]['status'] is not None and 'ignition' in points[i]['status']
                 and points[i]['status']['ignition'] is True):
            if len(tmp) == 0:
                j = i
                while j > 0:
                    if points[j - 1]['speed'] > 0 or \
                            (points[j]['lat'] == points[j - 1]['lat'] and points[j]['lng'] == points[j - 1]['lng']):
                        break
                    tmp.insert(0, points[j - 1])
                    j -= 1
            tmp.append(points[i])
        else:
            if len(tmp) > 0:
                j = i + 1
                while j < len(points):
                    if points[j]['speed'] > 0 or \
                            (points[j]['lat'] == points[j - 1]['lat'] and points[j]['lng'] == points[j - 1]['lng']):
                        break
                    tmp.append(points[j])
                    j += 1
                items.append(tmp)
                tmp = []
    if len(tmp) > 0:
        items.append(tmp)
    return items


def merge_trips(trip_1, trip_2):
    """
    Returns a merged trip. Assumes the vehicle details are same for both trips

    :param trip_1:
    :param trip_2:
    :return: Merged trip with vehicle details of trip_1
    """
    trip = trip_1.copy()
    trip.update({
        'endTime': trip_2['endTime'],
        'tripDuration': int((trip_2['endTime'] - trip_1['startTime']).total_seconds()),
        'distance': trip_1['distance'] + trip_2['distance'],
        'ignitionDuration': trip_1['ignitionDuration'] + trip_2['ignitionDuration'],
        'acDuration': trip_1['acDuration'] + trip_2['acDuration'],
        'idleDuration': trip_1['idleDuration'] + trip_2['idleDuration'],
        'endLoc': trip_2['endLoc'],
        'maxSpeed': int(max(trip_1['maxSpeed'], trip_2['maxSpeed']))
    })
    trip_records = list(utils.gps_db[trip['deviceId']].find({'time': {'$gte': trip['startTime'], '$lte': trip['endTime']}}).sort([('time', 1)]))
    trip['idleDuration'] = calculate_idle_duration(trip_records)
    return trip


def calculate_total_distance(points):
    """
    Calculates the total distance of a trip

    :param points: list of dict with keys 'lat' and 'lng'
    :return: Distance in KMs
    """
    dist = 0

    for i in range(len(points) - 1):
        # dist += get_approx_distance(points[i], points[i + 1])
        pt1 = (points[i]['lat'], points[i]['lng'])
        pt2 = (points[i + 1]['lat'], points[i + 1]['lng'])
        dist += vincenty(pt1, pt2).kilometers

    return dist


def calculate_ac_duration(records):
    """
    Calculates the total duration of AC

    :param records: list of records from device with keys 'time' and 'status'.
    'status' must be a dict with a key 'ac'
    :return: Distance in KMs
    """
    ac_duration = timedelta(0)

    ac_on = records[0]['status']['ac'] if records[0]['status'] is not None and 'ac' in records[0]['status'] else False
    for i in range(1, len(records)):
        if ac_on:
            time_diff = records[i]['time'] - records[i - 1]['time']
            if time_diff < timedelta(0, 181):
                ac_duration += time_diff
        ac_on = records[i]['status']['ac'] if records[0]['status'] is not None and 'ac' in records[0]['status'] else False

    return int(ac_duration.total_seconds())


def calculate_ignition_duration(records):
    """
    Calculates the total duration of ignition

    :param records: list of records from device with keys 'time' and 'status'.
    'status' must be a dict with a key 'ignition'
    :return: Distance in KMs
    """
    ignition_duration = timedelta(0)

    ignition_on = records[0]['status']['ignition'] if records[0]['status'] is not None and 'ignition' in records[0]['status'] else False
    for i in range(1, len(records)):
        if ignition_on:
            time_diff = records[i]['time'] - records[i - 1]['time']
            if time_diff < timedelta(0, 181):
                ignition_duration += time_diff
        ignition_on = records[i]['status']['ignition'] if records[0]['status'] is not None and 'ignition' in records[0]['status'] else False

    return int(ignition_duration.total_seconds())


def calculate_idle_duration(trip_records):
    """
    Calculates the total idle duration of trip

    :param trip_records: list of records from device with keys 'time' and 'status'.
    'status' must be a dict with a key 'ac'
    :return: Distance in KMs
    """
    idle_duration = 0.0

    for i in range(1, len(trip_records)):
        if trip_records[i]['lat'] == trip_records[i - 1]['lat'] and \
                        trip_records[i]['lng'] == trip_records[i - 1]['lng']:
            time_diff = trip_records[i]['time'] - trip_records[i - 1]['time']
            if time_diff > timedelta(0, 3601):
                continue
            idle_duration += time_diff.total_seconds()

    return int(idle_duration)
