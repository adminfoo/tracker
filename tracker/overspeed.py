"""
This module contains all the logic behind over-speed alerts
Whenever any driver drives above 40 Kmph it is regarded as over-speeding incident
Severity of each incident is also calculated based on maximum speed and duration
"""
from collections import defaultdict
from datetime import datetime, timedelta, time
from pyramid.view import view_config
from tracker import utils
from tracker.route_trips import get_one_day_trips
from tracker.utils.validations import validate_datetime

SPEED_LIMIT = 40


###############################################################################
# Views
###############################################################################
@view_config(route_name="overspeed_incidents", renderer="json")
def overspeed_incidents_view(request):
    query_dict = dict(request.GET)
    date = validate_datetime(query_dict['date'])
    school_id = request.user['schoolId']
    return get_overspeed_incidents(school_id, date)


###############################################################################
# Helper functions
###############################################################################
def consolidate_incidents(incidents_list):
    trip_incidents_dict = defaultdict(list)
    for incident in incidents_list:
        trip_incidents_dict[incident['tripId']].append(incident)
    return [max(incidents, key=lambda i: i['severity']) for incidents in trip_incidents_dict.values()]


def get_overspeed_incidents(school_id, date):
    all_routes = utils.app_db['routes'].find({'schoolId': school_id})
    incidents = []
    for route in all_routes:
        pickup_trip, drop_trip = get_one_day_trips(route, date)
        items = []
        if pickup_trip:
            items = get_incidents(pickup_trip)
            for inc in items:
                inc.update({'tripType': "pickup", 'tripId': pickup_trip['_id']})
            incidents += items
        if drop_trip:
            items = get_incidents(drop_trip)
            for inc in items:
                inc.update({'tripType': "drop", 'tripId': drop_trip['_id']})
            incidents += items
        for inc in items:
            inc.update({
                'driverName': route['driverName'],
                'helperName': route['helperName'],
                'helperPhone': route['helperPhone'],
                'vehicleId': route['vehicleId'],
                'vehicleRegistrationNo': route['vehicleRegistrationNo'],
                'routeId': route['_id'],
                'routeName': route['name'],
                'date': date
            })
    incidents = sorted(incidents, key=lambda i: i['startTime'])
    incidents.reverse()
    return incidents


def get_incidents(trip_details):
    start_time = trip_details['startTime']
    end_time = trip_details['endTime'] \
        if trip_details['endTime'] is not None \
        else datetime.combine(trip_details['startTime'].date(), time(23, 59, 59))
    cur = utils.gps_db[trip_details['deviceId']].find({'time': {'$gte': start_time, '$lte': end_time},
                                                       'speed': {'$gt': SPEED_LIMIT}}).sort([('time', 1)])
    incidents = []
    points = []
    prev_time = datetime(2000, 1, 1)
    for r in cur:
        diff = r['time'] - prev_time
        if diff > timedelta(0, 60):
            if len(points) > 0:
                item = {
                    'startTime': points[0]['time'],
                    'duration': (points[-1]['time'] - points[0]['time']).total_seconds() + 15,
                    'maxSpeed': max([p['speed'] for p in points]),
                    'severity': calculate_severity(points)
                }
                incidents.append(item)
            points = [r]
        else:
            points.append(r)
        prev_time = r['time']
    if len(points) > 0:
        item = {
            'startTime': points[0]['time'],
            'duration': (points[-1]['time'] - points[0]['time']).total_seconds() + 15,
            'maxSpeed': max([p['speed'] for p in points]),
            'severity': calculate_severity(points)
        }
        incidents.append(item)
    return incidents


def calculate_severity(incident_points):
    """
    Calculates severity of an over-speeding incident on a scale of 5

    Logic:
    At each point in incident, find difference of actual speed and speed limit
    Add differences at all points to get a score
    Severity    Score
    1           < 10
    2           < 25
    3           < 50
    4           < 100
    5           > 100

    :param incident_points:
    :return: int 1 to 5
    """
    score = 0
    for p in incident_points:
        score += p['speed'] - SPEED_LIMIT
    if score < 10:
        return 1
    elif score < 25:
        return 2
    elif score < 50:
        return 3
    elif score < 100:
        return 4
    else:
        return 5
