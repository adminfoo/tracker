import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from tracker.utils import app_db
from tracker.views import static_url
from instamojo import Instamojo

logger = logging.getLogger(__name__)

# TODO:
instamojoAPI = Instamojo(api_key='2900921f616700b2d605654908315898', auth_token='ec3b77258dcad7f37c564e05a18ad5e3')


@view_config(route_name="payments", request_method="GET", renderer='../templates/payment.pt')
def render_payment_page(request):
    if request.user['userType'] == "anon":
        url = "/login"
        raise HTTPFound(location=url)

    return {'user': request.user, 'staticHashMap': static_url}


@view_config(route_name="redirect_to_payment_gateway", request_method="GET")
def redirect_to_payment(request):
    from tracker.accounts import get_monthly_subscriptionFee
    query_dict = dict(request.GET)
    subscription_period = query_dict["subscription"]
    if request.user["userType"] == "user":
        # get account information
        account = {
            "amount": get_monthly_subscriptionFee(request.user["accountId"]),
            "name": request.user["name"],
            "email": request.user["email"],
            "phone": str(request.user["phone"])
        }

        total_amount = account["amount"] * int(subscription_period)
        # Create a new Link
        desc_text = 'You are paying total INR ' + str(total_amount) + " for " + str(subscription_period) + " months."
        response = instamojoAPI.link_create(title='Welcome ' + account["name"],
                                            description=desc_text,
                                            base_price=total_amount,
                                            redirect_url="http://tracker.edufits.com/transaction-result")
        logger.info(response)

        if response["success"]:
            # create orderID and insert in orders collection
            order_data = {
                'total_amount': total_amount,
                'orderId': response["link"]["slug"],
                'end_date': datetime.now() + relativedelta(months=int(subscription_period)),
                'accountId': request.user["accountId"]
            }
            app_db['orders'].insert_one(order_data)

            redirect_url = response["link"]["url"] + "?data_name=" + account["name"] + "&data_email=" + \
                           account["email"] + "&data_phone=" + account["phone"] + "&intent=buy"
            raise HTTPFound(location=redirect_url)
        else:
            raise HTTPFound("/payments?error=true")
    else:
        # user logged out
        raise HTTPFound("/payments?error=true")


@view_config(route_name="transaction_result", request_method="GET", renderer='../templates/transaction_result.pt')
def transaction_result(request):
    from tredirect_to_paymentracker.accounts import set_subscription_end_date
    query_dict = dict(request.GET)
    payment_id = query_dict["payment_id"]
    status = query_dict["status"]
    if status == "success":
        # get payment details and match details with corresponding order ID in our db
        response = instamojoAPI.payment_detail(payment_id)
        if response["success"]:
            order = app_db['orders'].find_one({'orderId': response["payment"]["link_slug"]})
            if not order:
                # no corresponding order found in our db
                return {'success': "false"}
            if order["total_amount"] == float(response["payment"]["amount"]):
                set_subscription_end_date(order['accountId'], order["end_date"])
                return {'success': "true"}
            else:
                return {'success': "false"}
        else:
            # no payment with this paymentID found on instamojo
            return {'success': "false"}
