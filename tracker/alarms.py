import random
import string
import time
from collections import defaultdict
from copy import deepcopy
from datetime import datetime, timedelta
from threading import Thread

from geopy.distance import vincenty
from pyramid.view import view_config
from voluptuous import Schema, Any
from tracker import utils
from tracker.rest_resource import Resource, object_store
from tracker.route_trips import is_holiday
import logging
from tracker.routes import reset_route_path
from tracker.utils.common_utils import hr_min_str_to_time
from tracker.utils.sms_utils import send_sms
from tracker.utils.validations import validate_object_id, validate_datetime, validate_int, validate_phone, \
    day_from_2015, validate_str

logger = logging.getLogger(__name__)

collection = "pax_alerts"
validation_schema = Schema({
    'alertId': validate_str,
    'schoolId': validate_object_id,
    'userId': validate_object_id,
    'phone': validate_phone,
    'passengerId': validate_object_id,
    'passengerName': str,
    'routeId': validate_object_id,
    'routeTripId': validate_object_id,
    'stopId': str,
    'stopName': str,
    'schArrival': validate_datetime,
    'actArrival': Any(None, validate_datetime),
    'alertAheadMins': validate_int,
    'alertTime': validate_datetime,
    'actErrorMins': Any(None, validate_int),  # actual error
    'alertDone': bool,
    'extraPassengerIds': [validate_object_id]
})
objects = Resource(collection, validation_schema)


def main_func():
    while True:
        loop_start = datetime.now()

        # we don't want to end this loop in any case, so try except
        try:
            check_alerts()
        except:
            logger.exception("exception in alert loop")

        # sleep for 1 min
        diff = datetime.now() - loop_start
        logger.info({'route_calc_time': diff})
        wait_secs = 60.0 - diff.total_seconds()
        if wait_secs > 0:
            time.sleep(wait_secs)


@view_config(route_name="test_alerts", permission="superadmin", renderer="json")
def tsttt(request):
    token = request.GET['token'] if 'token' in request.GET else None
    if token is not None:
        return utils.app_db.test_results.find_one({'_id': validate_object_id(token)})
    else:
        from_time = validate_datetime(request.GET['from'])
        to_time = validate_datetime(request.GET['to'])
        res = utils.app_db.test_results.insert_one({'testStarted': datetime.now(), 'fromTime': from_time, 'toTime': to_time})
        token = res.inserted_id
        t = Thread(target=start_testing, args=(token, from_time, to_time))
        t.start()
        return {'token': token}


def start_testing(token, from_time, to_time):
    ticc = datetime.now()
    base_time = from_time
    route_trips = []
    all_alerts = []
    time_dict = {'route_trip': 0.0, 'update_trip': 0.0, 'stops_index': 0.0, 'stops_mark': 0.0, 'delay_overspeed': 0.0,
                 'alerts': 0.0, 'total': 0.0}
    all_routes = object_store['routes'].list_all(filters={'pathPoints': {'$exists': True}})
    while base_time <= to_time:
        loop_start = datetime.now()
        try:
            test_single_loop(base_time, route_trips, all_alerts, all_routes, time_dict)
        except:
            logger.exception("exception in alert loop")
        diff = datetime.now() - loop_start
        logger.info({'route_calc_time': diff, 'base_time': base_time})
        base_time += timedelta(0, 60)
    logger.info({'msg': "test over", 't': (datetime.now() - ticc).total_seconds(), 'time_dict': time_dict})
    ret_obj = {}
    for a in all_alerts:
        key = str(a['schArrival'].date())
        if key not in ret_obj:
            ret_obj[key] = {'total': 0, 'alertCount': 0, 'accurate': 0, 'missed': 0, 'waiting': 0, 'unknown': 0, 'missedDetails': [], 'unknownDetails': []}
        obj = ret_obj[key]
        obj['total'] += 1
        if a['alertDone'] is False:
            continue
        obj['alertCount'] += 1
        if a['actErrorMins'] is None:
            obj['unknown'] += 1
            obj['unknownDetails'].append(a)
        elif a['actErrorMins'] < -1:
            obj['missed'] += 1
            obj['missedDetails'].append(a)
        elif a['actErrorMins'] > 2:
            obj['waiting'] += 1
        else:
            obj['accurate'] += 1
    ret_obj['testCompleted'] = datetime.now()
    utils.app_db.test_results.update({'_id': validate_object_id(token)}, {'$set': ret_obj})


def test_single_loop(base_time, route_trips, all_alerts, all_routes, time_dict):
    # check if any alert is pending
    # if any alert is pending send sms
    _total_start = datetime.now()
    for r in all_routes:
        try:
            _route_trip_start = datetime.now()
            # logger.debug({'route': r['name'], 'type': r['type']})
            if len(r['pathPoints']) == 0:
                logger.debug("no pathPoints for route")
                time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()
                continue

            # cur_trip = get_current_route_trip(r, base_time=base_time)
            # get the current trip, create one if necessary
            if is_holiday(base_time) or len(r['vehicleDeviceId']) == 0:
                time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()
                continue
            vehicle_details = utils.app_db['vehicles'].find_one({'_id': r['vehicleId']})
            # logger.debug({'vehicle': vehicle_details['registrationNo'], 'parked': vehicle_details['status]['parked']})

            if len(r['pathPoints']) == 0:
                logger.warn({'msg': "No pathPoints for route", 'routeName': r['name']})
                time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()
                continue

            if base_time.time() < (r['startTime'] - timedelta(0, 600)).time():
                time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()
                continue

            day = day_from_2015(base_time)
            route_id = validate_object_id(r['_id'] if '_id' in r else r['id'])
            # logger.debug({'routeId': route_id, 'day': day, 'pickupOrDrop': r['type']})
            route_trip = next((x for x in route_trips if x['routeId'] == route_id and x['day'] == day), None)

            if route_trip is None:
                if parked_in_school(vehicle_details, base_time):
                    time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()
                    continue
                route_trip = get_new_trip(r, base_time=base_time, create_in_db=False)
                route_trip['id'] = len(route_trips)
                route_trips.append(route_trip)
            time_dict['route_trip'] += (datetime.now() - _route_trip_start).total_seconds()

            _update_trip_start = datetime.now()
            # update_route_trip(route_trip, vehicle_details, base_time=base_time, edit_in_db=False, time_dict=time_dict)
            update_route_trip_2(route_trip, vehicle_details, base_time=base_time, edit_in_db=False, time_dict=time_dict)
            cur_trip = route_trip
            time_dict['update_trip'] += (datetime.now() - _update_trip_start).total_seconds()


            if cur_trip is not None:
                if cur_trip['endTime'] is None:
                    _alerts_start = datetime.now()
                    cur_trip_id = cur_trip['id']
                    trip_alerts = [a for a in all_alerts if a['routeTripId'] == cur_trip_id]
                    if len(trip_alerts) == 0:
                        # create alerts for pickup trip for today, if necessary
                        trip_alerts = create_alerts(cur_trip, create_in_db=False)
                        all_alerts += trip_alerts
                    # check if any alert id due now, send sms if not alerted
                    for alert in trip_alerts:
                        if alert['alertDone'] is True:
                            if 'actArrival' not in alert or alert['actArrival'] is None:
                                # find actual error (in mins) for this alert
                                stop = next((s for s in cur_trip['stops'] if s['id'] == alert['stopId']), None)
                                if stop is not None and stop['actTime'] is not None:
                                    alert['actArrival'] = stop['actTime']
                                    alert['actErrorMins'] = int((stop['actTime'] - (alert['alertTime'] + timedelta(0, alert['alertAheadMins'] * 60))).total_seconds() / 60)
                                    # objects.edit(alert['id'], {'actArrival': alert['actArrival'], 'actErrorMins': alert['actErrorMins']})
                            continue
                        # update the alert time
                        update_alert_time(alert, cur_trip, should_send_sms=False, base_time=base_time, edit_in_db=False)
                    time_dict['alerts'] += (datetime.now() - _alerts_start).total_seconds()
        except:
            logger.exception("exception in alert loop for route " + str(r['id']))
    time_dict['total'] += (datetime.now() - _total_start).total_seconds()


def check_alerts():
    # check if any alert is pending
    # if any alert is pending send sms
    all_routes = object_store['routes'].list_all(filters={'pathPoints': {'$exists': True}})
    for r in all_routes:
        try:
            logger.debug({'route': r['name'], 'type': r['type']})
            if len(r['pathPoints']) == 0:
                logger.debug("no pathPoints for route")
                reset_route_path(r)
                if len(r['pathPoints']) == 0:
                    logger.warn({'msg': "route trips could not be created", 'routeId': r['id'], 'route': r['name']})
                    continue
            cur_trip = get_current_route_trip(r)
            if cur_trip is not None:
                if cur_trip['endTime'] is None:
                    cur_trip_id = validate_object_id(cur_trip['_id'] if '_id' in cur_trip else cur_trip['id'])
                    trip_alerts = objects.list_all(filters={'routeTripId': cur_trip_id})
                    if len(trip_alerts) == 0:
                        # create alerts for pickup trip for today, if necessary
                        create_alerts(cur_trip)
                        trip_alerts = objects.list_all(filters={'routeTripId': cur_trip_id})
                    # check if any alert id due now, send sms if not alerted
                    for alert in trip_alerts:
                        if alert['alertDone'] is True:
                            if 'actArrival' not in alert or alert['actArrival'] is None:
                                # find actual error (in mins) for this alert
                                stop = next((s for s in cur_trip['stops'] if s['id'] == alert['stopId']), None)
                                if stop is not None and stop['actTime'] is not None:
                                    alert['actArrival'] = stop['actTime']
                                    alert['actErrorMins'] = int((stop['actTime'] - (alert['alertTime'] + timedelta(0, alert['alertAheadMins'] * 60))).total_seconds() / 60)
                                    objects.edit(alert['id'], {'actArrival': alert['actArrival'], 'actErrorMins': alert['actErrorMins']})
                            continue
                        # update the alert time
                        update_alert_time(alert, cur_trip)
        except:
            logger.exception("exception in alert loop for route " + str(r['id']))


def get_current_route_trip(route, base_time=None):
    if base_time is None:
        base_time = datetime.now()
    # get the current trip, create one if necessary
    if is_holiday(base_time) or len(route['vehicleDeviceId']) == 0:
        return
    vehicle_details = utils.app_db['vehicles'].find_one({'_id': route['vehicleId']})
    if vehicle_details is None:
        return
    logger.debug({'vehicle': vehicle_details['registrationNo'], 'parked': vehicle_details['status']['parked']})

    if len(route['pathPoints']) == 0:
        logger.warn({'msg': "No pathPoints for route", 'routeName': route['name']})
        return

    if base_time.time() < (route['startTime'] - timedelta(0, 600)).time():
        return

    day = day_from_2015(base_time)
    route_id = validate_object_id(route['_id'] if '_id' in route else route['id'])
    logger.debug({'routeId': route_id, 'day': day, 'pickupOrDrop': route['type']})
    route_trip = utils.app_db['route_trips'].find_one({'routeId': route_id, 'day': day})

    if route_trip is None:
        if vehicle_details['status']['parked']:
            return
        route_trip = get_new_trip(route, base_time=base_time)

    update_route_trip(route_trip, vehicle_details, base_time=base_time)
    return route_trip


# def get_current_route_trip(route):
#     # get the current trip, create one if necessary
#     if is_holiday(datetime.now()) or len(route['vehicleDeviceId']) == 0:
#         return None
#     vehicle_details = utils.mongodb['vehicles'].find_one({'_id': route['vehicleId']})
#     logger.debug({'vehicle': vehicle_details['registrationNo'], 'parked': vehicle_details['status]['parked']})
#
#     if 'trips' not in route or len(route['trips']) == 0:
#         logger.warn({'msg': "No trips for route", 'routeName': route['name']})
#         return
#
#     cur_trip = find_current_trip_type(route['trips'])
#     if cur_trip is None:
#         return None
#     if len(cur_trip['stops']) == 0:
#         logger.warn({'msg': "Stops not configured", 'route': route['name'],
#                      'routeId': route['_id'] if '_id' in route else route['id']})
#         # no need to create a trip
#         # TODO: check at run-time if stops have been configured and create route['trips'] freshly
#         return None
#
#     day = day_from_2015(datetime.now())
#     route_id = validate_object_id(route['_id'] if '_id' in route else route['id'])
#     logger.debug({'routeId': route_id, 'day': day, 'pickupOrDrop': cur_trip['name']})
#     cur = utils.mongodb['route_trips'].find({'routeId': route_id, 'pickupOrDrop': cur_trip['name'],
#                                              'day': day}).sort([('startTime', 1)])
#     existing = [x for x in cur]
#
#     logger.debug({'existing': len(existing)})
#
#     if len(existing) == 0:
#         if vehicle_details['status]['parked']:
#             return
#         route_trip = get_new_trip(route, cur_trip)
#     else:
#         route_trip = existing[0]
#
#     # if local_obj['verbose'] is True:
#     #     logger.debug({'route_trip': route_trip})
#
#     if route_trip is None:
#         return
#     update_route_trip(route_trip, vehicle_details)
#     return route_trip


def find_current_trip_type(route_trips_raw):
    # The "trips" field in any route is its input
    # Returns one of the trips that is appropriate acc to timing
    if len(route_trips_raw) == 0:
        return None
    trips = sorted(route_trips_raw, key=lambda rt: rt['startTime'].time())
    cur_trip = trips[-1] if datetime.now().time() > (trips[-1]['startTime'] - timedelta(0, 600)).time() \
        else trips[-2]
    logger.debug({'cur_trip': cur_trip['name']})
    if datetime.now().time() < (cur_trip['startTime'] - timedelta(0, 600)).time():
        if len(trips) == 2:
            logger.debug('pickup not started')
            return None
        cur_trip = trips[0]
        if datetime.now().time() < (cur_trip['startTime'] - timedelta(0, 600)).time():
            logger.debug({'pickup not started'})
            return None
    logger.debug({'trip': cur_trip['name']})
    return cur_trip


def update_route_trip(route_trip, vehicle_details, base_time=None, edit_in_db=True, time_dict=None):
    if time_dict is None:
        time_dict = {'stops_index': 0.0, 'stops_mark': 0.0, 'delay_overspeed': 0.0}
    if base_time is None:
        base_time = datetime.now()
    if len(route_trip['stops']) == 0:
        return
    if route_trip['endTime'] is not None:
        return
    _stops_index_start = datetime.now()
    start_index, end_index = get_stops_index_to_check(route_trip, base_time=base_time)
    time_dict['stops_index'] += (datetime.now() - _stops_index_start).total_seconds()
    logger.debug({'s': start_index, 'e': end_index, 'r': vehicle_details['deviceId']})
    _stops_mark_start = datetime.now()
    for i in range(start_index, end_index):
        if i >= len(route_trip['stops']):
            break
        stop = route_trip['stops'][i]
        if stop['actTime'] is not None:
            continue
        from_time = route_trip['stops'][start_index - 1]['actTime'] if start_index > 0 else route_trip['startTime']
        filters = {
            'time': {'$gte': from_time, '$lte': base_time},
            'loc': {'$near': {'$geometry': stop['loc'], '$maxDistance': 50}}
        }
        pts = [p for p in utils.gps_db[route_trip['deviceId']].find(filters).sort([('time', 1)]).limit(1)]
        if len(pts) == 0:
            continue
        stop['actTime'] = pts[0]['time']
    time_dict['stops_mark'] += (datetime.now() - _stops_mark_start).total_seconds()

    _delay_overspeed_start = datetime.now()
    # find current delay, i.e. delay calculated at latest possible point
    # TODO: only stops are used to find delay. mark multiple points on the route for finding delay(sort of checkpoints)
    current_delay = int((route_trip['startTime'] - route_trip['schStartTime']).total_seconds() / 60)
    for i in range(len(route_trip['stops'])):
        if route_trip['stops'][i]['actTime'] is not None:
            current_delay = int((route_trip['stops'][i]['actTime'] - route_trip['stops'][i]['schTime']).total_seconds() / 60)

    if (base_time is None and vehicle_details['status']['parked'] is True) or (base_time is not None and parked_in_school(vehicle_details, base_time) is True):
        route_trip['endTime'] = base_time
        # also add overspeed summary
        filters = {'time': {'$gte': route_trip['startTime'], '$lte': route_trip['endTime']}, 'speed': {'$gte': 40}}
        route_trip['overspeed'] = utils.gps_db[route_trip['deviceId']].count(filters)
    route_trip['latestDelay'] = current_delay
    route_trip['lastUpdated'] = base_time
    logger.debug({'routeId': route_trip['routeId'], 'latestDelay': route_trip['latestDelay']})
    time_dict['delay_overspeed'] += (datetime.now() - _delay_overspeed_start).total_seconds()
    if edit_in_db:
        trip_id = route_trip['_id'] if '_id' in route_trip else route_trip['id']
        object_store['route_trips'].edit(trip_id, {
            'stops': route_trip['stops'],
            'endTime': route_trip['endTime'],
            'overspeed': route_trip['overspeed'],
            'latestDelay': route_trip['latestDelay'],
            'lastUpdated': route_trip['lastUpdated']
        })
        return objects.detail(trip_id)
    else:
        return route_trip


def update_route_trip_2(route_trip, vehicle_details, base_time=None, edit_in_db=True, time_dict=None):
    if time_dict is None:
        time_dict = {'stops_index': 0.0, 'stops_mark': 0.0, 'delay_overspeed': 0.0}
    if base_time is None:
        base_time = datetime.now()
    if len(route_trip['stops']) == 0:
        return
    if route_trip['endTime'] is not None:
        return
    _stops_index_start = datetime.now()
    start_index, end_index = get_stops_index_to_check(route_trip, base_time=base_time)
    time_dict['stops_index'] += (datetime.now() - _stops_index_start).total_seconds()
    logger.debug({'s': start_index, 'e': end_index, 'r': vehicle_details['deviceId']})
    _stops_mark_start = datetime.now()
    for i in range(start_index, end_index):
        if i >= len(route_trip['stops']):
            break
        stop = route_trip['stops'][i]
        if stop['actTime'] is not None:
            continue
        from_time = route_trip['stops'][start_index - 1]['actTime'] if start_index > 0 else route_trip['startTime']
        filters = {
            'time': {'$gte': from_time, '$lte': base_time},
            'loc': {'$near': {'$geometry': stop['loc'], '$maxDistance': 50}}
        }
        pts = [p for p in utils.gps_db[route_trip['deviceId']].find(filters).sort([('time', 1)]).limit(1)]
        if len(pts) == 0:
            continue
        stop['actTime'] = pts[0]['time']
    time_dict['stops_mark'] += (datetime.now() - _stops_mark_start).total_seconds()

    _delay_overspeed_start = datetime.now()
    # find current delay, i.e. delay calculated at latest possible point
    # TODO: only stops are used to find delay. mark multiple points on the route for finding delay(sort of checkpoints)
    current_delay = int((route_trip['startTime'] - route_trip['schStartTime']).total_seconds() / 60)
    rem_time = int(route_trip['totalRunTime'] - (datetime.now() - route_trip['startTime']).total_seconds())
    for i in range(len(route_trip['stops'])):
        if route_trip['stops'][i]['actTime'] is not None:
            after_stop_duration = int((datetime.now() - route_trip['stops'][i]['actTime']).total_seconds())
            rem_time = route_trip['stops'][i]['remTime'] - after_stop_duration

    if (base_time is None and vehicle_details['status']['parked'] is True) or (base_time is not None and parked_in_school(vehicle_details, base_time) is True):
        route_trip['endTime'] = base_time
        # also add overspeed summary
        filters = {'time': {'$gte': route_trip['startTime'], '$lte': route_trip['endTime']}, 'speed': {'$gte': 40}}
        route_trip['overspeed'] = utils.gps_db[route_trip['deviceId']].count(filters)
    route_trip['remTime'] = rem_time
    route_trip['lastUpdated'] = base_time
    logger.debug({'routeId': route_trip['routeId'], 'latestDelay': route_trip['latestDelay']})
    time_dict['delay_overspeed'] += (datetime.now() - _delay_overspeed_start).total_seconds()
    if edit_in_db:
        trip_id = route_trip['_id'] if '_id' in route_trip else route_trip['id']
        object_store['route_trips'].edit(trip_id, {
            'stops': route_trip['stops'],
            'endTime': route_trip['endTime'],
            'overspeed': route_trip['overspeed'],
            'remTime': route_trip['remTime'],
            'lastUpdated': route_trip['lastUpdated']
        })
        return objects.detail(trip_id)
    else:
        return route_trip


def get_stops_index_to_check(route_trip, base_time=None):
    if base_time is None:
        base_time = datetime.now()
    # get the indices of stops that should be checked for actual time of bus
    # based on expected time and nothing else
    # TODO: could add adjustment for current delay or some time padding
    start_index = 0
    time_diff = base_time - route_trip['startTime']
    last_time = route_trip['startTime']
    for i in range(len(route_trip['stops'])):
        start_index = i
        stop = route_trip['stops'][i]
        if stop['actTime'] is None:
            break
        time_diff = base_time - route_trip['stops'][i]['actTime']
        last_time = route_trip['stops'][i]['actTime']
    end_index = start_index + 1
    for i in range(end_index, len(route_trip['stops'])):
        if route_trip['stops'][i]['schTime'] - last_time > time_diff:
            end_index = i
            break
    return start_index, end_index


def get_new_trip(route, base_time=None, create_in_db=True):
    if base_time is None:
        base_time = datetime.now()
    trip_date = day_from_2015(base_time)
    stops = [deepcopy(x) for x in route['stops']]

    for s in stops:
        s['schTime'] = datetime.combine(base_time.date(), hr_min_str_to_time(s['time']))
        s['actTime'] = None

    trip = {
        'schoolId': route['schoolId'],
        'deviceId': route['vehicleDeviceId'],
        'vehicleId': route['vehicleId'],
        "vehicleRegistrationNo": route['vehicleRegistrationNo'],

        "driverName": route['driverName'],
        "driverLicenseNo": route['driverLicenseNo'],

        'schStartTime': datetime.combine(base_time.date(), route['startTime'].time()),
        'startTime': base_time - timedelta(0, 60),  # TODO: traceback GPS data to get correct time
        'endTime': None,
        'day': trip_date,
        'tripDuration': 0,
        'overspeed': 0,

        'routeId': validate_object_id(route['_id'] if '_id' in route else route['id']),
        'routeName': route['name'],
        'pickupOrDrop': route['type'],
        'stops': stops,
        'points': [],

        'latestDelay': 0,
        'lastUpdated': base_time
    }
    if create_in_db:
        ret_obj = object_store['route_trips'].create(trip)
        logger.debug(ret_obj)
        trip = object_store['route_trips'].detail(ret_obj['id'])
    return trip


def create_alerts(route_trip, create_in_db=True):
    if route_trip['pickupOrDrop'] == "pickup":
        all_paxes = object_store['passengers'].list_all(filters={'pickupRouteId': route_trip['routeId']})
    else:
        all_paxes = object_store['passengers'].list_all(filters={'dropRouteId': route_trip['routeId']})
    alert_obj_dict = defaultdict(list)
    for pax in all_paxes:
        # if route_trip['pickupOrDrop'] not in pax['dailyTrips']:
        #     continue
        if route_trip['pickupOrDrop'] == "pickup":
            pax_stop = next((s for s in route_trip['stops'] if s['id'] == pax['pickupStopId']), None)
        else:
            pax_stop = next((s for s in route_trip['stops'] if s['id'] == pax['dropStopId']), None)
        if pax_stop is None:
            logger.warn({'msg': "stop not found for " + pax['name'] + " id " + str(pax['id']), 'stopId': pax['stopId']})
            continue
        all_users = object_store['users'].list_all(filters={'passengerIds': pax['id']})
        for u in all_users:
            if u['endDate'] is None:
                continue
            route_trip_id = validate_object_id(route_trip['_id'] if '_id' in route_trip else route_trip['id']) if create_in_db else route_trip['id']
            alert = {
                'alertId': generate_alert_id(),
                'schoolId': pax['schoolId'],
                'userId': u['id'],
                'phone': u['phone'],
                'passengerId': pax['id'],
                'passengerName': pax['name'],
                'routeId': route_trip['routeId'],
                'routeTripId': route_trip_id,
                'stopId': pax_stop['id'],
                'stopName': pax_stop['name'],
                'schArrival': pax_stop['schTime'],
                'actArrival': None,
                'alertAheadMins': u['userSettings']['alertTime'],
                'alertTime': pax_stop['schTime'] - timedelta(0, u['userSettings']['alertTime'] * 60),
                'alertDone': False,
                'actErrorMins': None
            }
            alert_obj_dict[u['phone']].append(alert)
    ret_list = []
    for phone, alerts in alert_obj_dict.items():
        for alert in merge_alerts(alerts):
            if create_in_db:
                objects.create(alert)
            ret_list.append(alert)
    return ret_list


def generate_alert_id():
    alert_id = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(4)])
    while utils.app_db[collection].find_one({'alertId': alert_id}) is not None:
        alert_id = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(4)])
    return alert_id


def merge_alerts(alerts):
    # merge alerts for siblings on same route stop
    alert_dict = {}
    for a in alerts:
        key = (a['routeId'], a['stopId'], a['alertAheadMins'])
        if key not in alert_dict:
            alert_dict[key] = a
        else:
            if 'extraPassengerIds' not in alert_dict[key]:
                alert_dict[key]['extraPassengerIds'] = []
            alert_dict[key]['extraPassengerIds'].append(a['passengerId'])
    return alert_dict.values()


def update_alert_time(alert, route_trip, should_send_sms=True, base_time=None, edit_in_db=True):
    if base_time is None:
        base_time = datetime.now()
    pax_stop = next(s for s in route_trip['stops'] if alert['stopId'] == s['id'])
    # send alert 1 min early
    new_alert_time = pax_stop['schTime'] - timedelta(0, (alert['alertAheadMins'] - route_trip['latestDelay'] + 1) * 60)
    if new_alert_time != alert['alertTime']:
        alert['alertTime'] = new_alert_time
        if edit_in_db:
            objects.edit(alert['id'], {'alertTime': new_alert_time})
    if new_alert_time <= base_time:
        # send sms to ring the alert
        stop_name = alert['stopName'] if 'stopName' in alert else ""
        sms_content = "Bus Arrival Alert. Your bus is arriving at bus stop " + stop_name\
                      + " in " + str(alert['alertAheadMins']) + \
                      "min." + str(alert['alertId']) + '.'
        school_details = utils.app_db['schools'].find_one({'_id': route_trip['schoolId']})
        should_send_sms = school_details['paxAlertsEnabled']
        if should_send_sms:
            send_sms(alert['phone'], sms_content, tag="alert")
        logger.info({"sms": sms_content, "phone": alert['phone']})
        alert['alertDone'] = True
        if edit_in_db:
            objects.edit(alert['id'], {'alertDone': True})


@view_config(route_name="get_alert_time", request_method="GET", permission="user", renderer="json")
def find_alert_time(request):
    user_id = request.user['_id']

    user_details = utils.app_db['users'].find_one({'_id': user_id})

    cur = utils.app_db[collection].find({'userId': user_id}).sort([('_id', -1)]).limit(1)
    recs = [x for x in cur]
    cur.close()

    if len(recs) == 0:
        return {}

    alert = recs[0]

    route_trip_details = object_store['route_trips'].detail(alert['routeTripId'])
    if route_trip_details is None:
        is_alarm = False
    else:
        is_alarm = user_details['userSettings']['alarmOnDrop'] if route_trip_details['pickupOrDrop'] == "drop" \
            else user_details['userSettings']['alarmOnPickup']

    alert_mins = int((alert['alertTime'] - datetime.now()).total_seconds() / 60) - 1

    if alert['alertDone'] is True or route_trip_details is None or alert_mins < -1:
        alert_mins = 1005

    ret_obj = {
        'alertId': alert['_id'],
        'alertTime': alert_mins,
        'isAlarm': is_alarm,
        'data': {'message': "", 'title': ""}
    }
    return ret_obj


@view_config(route_name="mark_alert", request_method="POST", permission="user", renderer="json")
def mark_alert(request):
    data = request.json_body
    objects.edit(data['alertId'], {'alertDone': True})
    return {'ok': "tested"}


@view_config(route_name="alert_accuracy_report", request_method="GET", permission="superadmin", renderer="json")
def alert_accuracy_view(request):
    # summarize the accuracy of alerts for a time range
    # results can be filtered based on school, route and alert time
    # the accuracy params are also configurable
    from_date = validate_datetime(request.GET['from']) if 'from' in request.GET else None
    to_date = validate_datetime(request.GET['to']) if 'to' in request.GET else None
    school_id = validate_object_id(request.GET['schoolId']) if 'schoolId' in request.GET else None
    route_id = validate_object_id(request.GET['routeId']) if 'routeId' in request.GET else None
    low = int(request.GET['low']) if 'low' in request.GET else -1
    high = int(request.GET['high']) if 'high' in request.GET else 2
    alert_ahead_mins = int(request.GET['alertTime']) if 'alertTime' in request.GET else None
    return compile_accuracy_report(from_date, to_date, lower_limit=low, upper_limit=high, school_id=school_id,
                                   route_id=route_id, alert_ahead_mins=alert_ahead_mins)


def compile_accuracy_report(start_time, end_time, lower_limit=-1, upper_limit=2, school_id=None, route_id=None, alert_ahead_mins=None):
    fil = {'schArrival': {'$gte': start_time, '$lte': end_time}}
    if school_id is not None:
        fil['schoolId'] = school_id
    if route_id is not None:
        fil['routeId'] = route_id
    if alert_ahead_mins is not None:
        fil['alertAheadMins'] = alert_ahead_mins
    all_alerts = sorted(objects.list_all(filters=fil), key=lambda a: a['schArrival'])

    # club summary date-wise
    ret_obj = {}
    for a in all_alerts:
        key = str(a['schArrival'].date())
        if key not in ret_obj:
            ret_obj[key] = {'total': 0, 'alertCount': 0, 'accurate': 0, 'missed': 0, 'waiting': 0, 'unknown': 0, 'missedDetails': [], 'unknownDetails': []}
        obj = ret_obj[key]
        obj['total'] += 1
        if a['alertDone'] is False:
            continue
        obj['alertCount'] += 1
        if a['actErrorMins'] is None:
            obj['unknown'] += 1
            obj['unknownDetails'].append(a)
        elif a['actErrorMins'] < lower_limit:
            obj['missed'] += 1
            obj['missedDetails'].append(a)
        elif a['actErrorMins'] > upper_limit:
            obj['waiting'] += 1
        else:
            obj['accurate'] += 1

    return ret_obj


def parked_in_school(vehicle_details, base_time):
    device_id = vehicle_details['deviceId']
    school_details = utils.app_db.schools.find_one({'_id': vehicle_details['schoolId']})
    school_pt = (school_details['lat'], school_details['lng'])
    vehicle_last_rec = [p for p in utils.gps_db[device_id].find({'time': {'$lte': base_time}}).sort([('time', -1)]).limit(1)][0]
    vehicle_pt = (vehicle_last_rec['lat'], vehicle_last_rec['lng'])
    dist = vincenty(school_pt, vehicle_pt).kilometers
    if dist < 0.2:
        return True
    return False


def update_route_trip_2(route_trip, vehicle_details, base_time=None, edit_in_db=True, time_dict=None):
    if time_dict is None:
        time_dict = {'stops_index': 0.0, 'stops_mark': 0.0, 'delay_overspeed': 0.0}
    if base_time is None:
        base_time = datetime.now()
    if len(route_trip['stops']) == 0:
        return
    if route_trip['endTime'] is not None:
        return
    _stops_index_start = datetime.now()
    start_index, end_index = get_stops_index_to_check(route_trip, base_time=base_time)
    time_dict['stops_index'] += (datetime.now() - _stops_index_start).total_seconds()
    logger.debug({'s': start_index, 'e': end_index, 'r': vehicle_details['deviceId']})
    _stops_mark_start = datetime.now()
    for i in range(start_index, end_index):
        if i >= len(route_trip['stops']):
            break
        stop = route_trip['stops'][i]
        if stop['actTime'] is not None:
            continue
        from_time = route_trip['stops'][start_index - 1]['actTime'] if start_index > 0 else route_trip['startTime']
        filters = {
            'time': {'$gte': from_time, '$lte': base_time},
            'loc': {'$near': {'$geometry': stop['loc'], '$maxDistance': 50}}
        }
        pts = [p for p in utils.gps_db[route_trip['deviceId']].find(filters).sort([('time', 1)]).limit(1)]
        if len(pts) == 0:
            continue
        stop['actTime'] = pts[0]['time']
    time_dict['stops_mark'] += (datetime.now() - _stops_mark_start).total_seconds()

    _delay_overspeed_start = datetime.now()
    # find current delay, i.e. delay calculated at latest possible point
    # TODO: only stops are used to find delay. mark multiple points on the route for finding delay(sort of checkpoints)
    current_delay = int((route_trip['startTime'] - route_trip['schStartTime']).total_seconds() / 60)
    for i in range(len(route_trip['stops'])):
        if route_trip['stops'][i]['actTime'] is not None:
            current_delay = int((route_trip['stops'][i]['actTime'] - route_trip['stops'][i]['schTime']).total_seconds() / 60)

    if (base_time is None and vehicle_details['status']['parked'] is True) or (base_time is not None and parked_in_school(vehicle_details, base_time) is True):
        route_trip['endTime'] = base_time
        # also add overspeed summary
        filters = {'time': {'$gte': route_trip['startTime'], '$lte': route_trip['endTime']}, 'speed': {'$gte': 40}}
        route_trip['overspeed'] = utils.gps_db[route_trip['deviceId']].count(filters)
    route_trip['latestDelay'] = current_delay
    route_trip['lastUpdated'] = base_time
    logger.debug({'routeId': route_trip['routeId'], 'latestDelay': route_trip['latestDelay']})
    time_dict['delay_overspeed'] += (datetime.now() - _delay_overspeed_start).total_seconds()
    if edit_in_db:
        trip_id = route_trip['_id'] if '_id' in route_trip else route_trip['id']
        object_store['route_trips'].edit(trip_id, {
            'stops': route_trip['stops'],
            'endTime': route_trip['endTime'],
            'overspeed': route_trip['overspeed'],
            'latestDelay': route_trip['latestDelay'],
            'lastUpdated': route_trip['lastUpdated']
        })
        return objects.detail(trip_id)
    else:
        return route_trip
