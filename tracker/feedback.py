from datetime import datetime
from multiprocessing import Process
from bson import ObjectId
from pyramid.view import view_config
from voluptuous import Schema, Any
import logging
from tracker import utils
from tracker.rest_resource import Resource
from tracker.tasks import create_task, resolve_task
from tracker.utils.validations import validate_object_id, validate_datetime
from tracker.utils.email_utils import send_text_mail


logger = logging.getLogger(__name__)

collection = "feedbacks"
validation_schema = Schema({
    'userId': validate_object_id,
    'msg': str,
    'time': validate_datetime,
    'status': Any("pending", "resolved"),
    'replies': [{'time': validate_datetime, 'msg': str}]
}, required=True)

objects = Resource(collection, validation_schema)


@view_config(route_name="feedbacks", request_method="GET", permission="superadmin", renderer='json')
def list_all(request):
    filters = request.filters
    # filters['schoolId'] = request.user['schoolId']
    return objects.list_all(filters=filters, projection=request.projection)


@view_config(route_name="feedbacks", request_method="POST", permission="user", renderer='json')
def create(request):
    data = request.json_body
    data['userId'] = request.user['_id']
    data['time'] = datetime.now()
    data['status'] = "pending"
    data['replies'] = []
    ret_obj = objects.create(data)
    user_details = utils.app_db['users'].find_one({'_id': request.user['_id']})
    html_body = str(user_details) + "<br><br><br>" + data['msg']
    p = Process(target=send_text_mail, args=("New Feedback received", ["support@edufits.com", "sukritsangwan@gmail.com"], html_body, "EDUFITS"))
    p.daemon = True
    p.start()
    return ret_obj


@view_config(route_name="feedback_details", request_method="GET", permission="school", renderer='json')
def detail(request):
    item_id = request.matchdict['pk']
    return objects.detail(item_id)


@view_config(route_name="feedback_details", request_method="PATCH", permission="superadmin", renderer='json')
def edit(request):
    item_id = request.matchdict['pk']
    data = request.json_body

    # TODO: send the reply to the user

    ret_obj = utils.app_db[collection].update({'_id': ObjectId(item_id)}, {'$push': {'replies': {'msg': data['msg'], 'time': datetime.now()}},
                                                                            '$set': {'status': data['status']}})

    if data['status'] == "pending":
        create_task("USER_FEEDBACK", "New feedback received from " + request.user['name'], feedback_id=item_id)
    else:
        resolve_task("USER_FEEDBACK", feedback_id=item_id)

    return ret_obj
