import unittest
from datetime import datetime, timedelta
from bson import ObjectId
from pymongo import MongoClient

from tracker.utils.exceptions import DbException


time1 = datetime(2015, 6, 22)
time2 = datetime(2015, 6, 21)


class MongoUtilTests(unittest.TestCase):
    def setUp(self):
        host = '192.168.0.101'
        port = 27017
        self.db_name = 'testdb'
        self.dba = DbHelper(host, port, self.db_name)
        self.tester_client = MongoClient(host, port)

    def tearDown(self):
        self.dba.close()
        self.tester_client.drop_database(self.db_name)
        self.tester_client.close()

    def test_insertion(self):
        """Test the insertion happens as expected

        Test that DBException is raised if
            an attempt is made to insert in db without user info or
            an attempt is made to insert in db with a field named 'deleted'

        Test the inserted item has these fields
            'deleted': False,
            'lastUpdated': datetime,
            'updatedBy': as provided

        """
        collection = "insertion_tests"
        item = {'field01': "value01", 'field02': "value02"}
        self.assertRaises(DbException, self.dba.insert_item, item, collection)

        item = {'field11': "value11", 'field12': "value12", 'updatedBy': 'sangu', 'deleted': True}
        self.assertRaises(DbException, self.dba.insert_item, item, collection)

        item = {'field11': "value11", 'field12': "value12", 'updatedBy': 'sangu'}
        mongo_id = self.dba.insert_item(item, collection)
        print(mongo_id)

        read_item = self.tester_client[self.db_name][collection].find_one({'_id': ObjectId(mongo_id)})

        # check that fields are present
        self.assertEqual(read_item['field11'], "value11")
        self.assertEqual(read_item['field12'], "value12")
        self.assertEqual(read_item['updatedBy'], "sangu")

        # check that auto-generated fields are there
        self.assertAlmostEqual(read_item['lastUpdated'], datetime.now(), delta=timedelta(0, 60))
        self.assertEqual(read_item['deleted'], False)

    def test_read(self):
        """Test that auto-generated fields are not read"""
        collection = "read_tests"
        # insert an item in mongo via plain pymongo
        item = {'field21': "value21", 'field22': "value22",
                'updatedBy': "sangu", 'lastUpdated': datetime.now(),
                'deleted': False, 'changeLog': []}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        # read via our testee client
        read_item = self.dba.get_item(collection, {'_id': obj_id})

        # check that fields are read
        self.assertEqual(read_item['field21'], "value21")
        self.assertEqual(read_item['field22'], "value22")
        self.assertIn('_id', read_item)

        # check that auto-generated fields are not read
        self.assertNotIn('updatedBy', read_item)
        self.assertNotIn('lastUpdated', read_item)
        self.assertNotIn('deleted', read_item)
        self.assertNotIn('changeLog', read_item)

        # now insert an item with 'deleted' as True, and then try to read
        item = {'field21': "value21", 'field22': "value22",
                'updatedBy': "sangu", 'lastUpdated': datetime.now(),
                'deleted': True, 'changeLog': [{'field21': "value21", 'field22': "value22",
                                                'updatedBy': "sangu", 'lastUpdated': datetime.now() - timedelta(1),
                                                'deleted': False, }]}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        # read via our testee client
        read_item = self.dba.get_item(collection, {'_id': obj_id})
        self.assertIsNone(read_item)

    def test_read_projections(self):
        """Test that projections work as expected"""
        collection = "read_projection_tests"
        # insert an item in mongo via plain pymongo
        item = {'field21': "value21", 'field22': "value22",
                'updatedBy': "sangu", 'lastUpdated': datetime.now(),
                'deleted': False, 'changeLog': []}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        # read via our testee client
        projection = {'field21': True}
        read_item = self.dba.get_item(collection, {'_id': obj_id}, projection)

        # check that fields are read
        self.assertEqual(read_item['field21'], "value21")
        self.assertNotIn('field22', read_item)
        self.assertIn('_id', read_item)

        # check that auto-generated fields are not read
        self.assertNotIn('updatedBy', read_item)
        self.assertNotIn('lastUpdated', read_item)
        self.assertNotIn('deleted', read_item)
        self.assertNotIn('changeLog', read_item)

    def test_update(self):
        """Test the update actions happen as expected

        Test that DBException is raised if
            an attempt is made to update in db without user info or
            an attempt is made to update in db with a field named 'deleted'

        Test the update item has these fields
            'deleted': False,
            'lastUpdated': datetime,
            'updatedBy': as provided
            'changeLog': list containing snapshots of previous version of updated doc
        """
        collection = "updation_tests"

        # insert a doc via plain pymongo, that will be updated by testee client
        item = {'field21': "wow", 'field22': "value22",
                'updatedBy': "shailesh", 'lastUpdated': time1,
                'deleted': False, 'changeLog': [{'field21': "value21", 'field22': "value22", 'updatedBy': "sangu",
                                                 'lastUpdated': time2, 'deleted': False}]}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        new_item = {'field21': "wow", 'field22': "dumb"}
        self.assertRaises(DbException, self.dba.update_item, obj_id, new_item, collection)

        new_item = {'field21': "wow", 'field22': "dumb", 'updatedBy': 'sangu', 'deleted': True}
        self.assertRaises(DbException, self.dba.update_item, obj_id, new_item, collection)

        new_item = {'field21': "wow", 'field22': "dumb", 'updatedBy': 'sangu'}
        self.dba.update_item(obj_id, new_item, collection)

        read_item = self.tester_client[self.db_name][collection].find_one({'_id': obj_id})

        # check that fields are present
        self.assertEqual(read_item['field21'], "wow")
        self.assertEqual(read_item['field22'], "dumb")
        self.assertEqual(read_item['updatedBy'], "sangu")

        # check that auto-generated fields are there
        self.assertAlmostEqual(read_item['lastUpdated'], datetime.now(), delta=timedelta(0, 60))
        self.assertEqual(read_item['deleted'], False)

        # check changeLog
        change_log = [{'field21': "value21", 'field22': "value22", 'updatedBy': "sangu",
                       'lastUpdated': time2, 'deleted': False},
                      {'field21': "wow", 'field22': "value22", 'updatedBy': "shailesh",
                       'lastUpdated': time1, 'deleted': False}]
        self.assertListEqual(read_item['changeLog'], change_log)

    def test_edit(self):
        """Test the edit actions happen as expected

        Test that DBException is raised if
            an attempt is made to edit in db without user info or
            an attempt is made to edit in db with a field named 'deleted'
            and attempt is made to edit in db with field name '_id'

        Test the update item has these fields
            'deleted': False,
            'lastUpdated': datetime,
            'changeLog': list containing difference of previous version of updated doc
            'updatedBy': as provided
        """
        collection = "edit_tests"

        # insert a doc via plain pymongo, that will be updated by testee client
        item = {'field21': "wow", 'field22': "value22",
                'createdBy': "shailesh", 'lastUpdated': time1,
                'deleted': False, 'changeLog': []}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        new_item = {'field21': "wow", 'field22': "dumb"}
        self.assertRaises(DbException, self.dba.edit_items, collection, new_item, {'id': obj_id})

        new_item = {'field21': "wow", 'field22': "dumb", 'updatedBy': 'sangu', 'deleted': True}
        self.assertRaises(DbException, self.dba.edit_items, collection, new_item, {'id': obj_id})

        new_item = {'field21': "wow", 'field22': "dumb", 'updatedBy': 'sangu'}
        self.dba.edit_items(collection, new_item, {'id': obj_id})

        # read via our testee client
        read_item = self.dba.get_item(collection, {'_id': obj_id})

        read_item = self.tester_client[self.db_name][collection].find_one({'_id': obj_id})

        # check that fields are present
        self.assertEqual(read_item['field21'], "wow")
        self.assertEqual(read_item['field22'], "dumb")
        self.assertAlmostEqual(read_item['lastUpdated'], datetime.now(), delta=timedelta(0, 60))

        # check changeLog
        self.assertAlmostEqual(read_item['changeLog']['updatedAt'], datetime.now(), delta=timedelta(0, 60))
        self.assertEqual(read_item['old'], {'field21': "wow", 'field22': "value22"})
        self.assertEqual(read_item['new'], {'field21': "wow", 'field22': "dumb"})
        self.assertEqual(read_item['updatedBy'], False)
        self.assertEqual(read_item['deleted'], "sangu")


    def test_delete(self):
        """Test the delete action happens as expected

        Test that DBException is raised if
            an attempt is made to delete in db without user info

        Test the update item has these fields
            'deleted': True,
            'lastUpdated': datetime,
            'updatedBy': as provided
            'changeLog': list containing snapshots of previous version of updated doc
        """
        collection = "deletion_tests"

        # insert a doc via plain pymongo, that will be deleted by the testee client
        item = {'field21': "wow", 'field22': "value22",
                'updatedBy': "shailesh", 'lastUpdated': time1,
                'deleted': False, 'changeLog': [{'field21': "value21", 'field22': "value22", 'updatedBy': "sangu",
                                                 'lastUpdated': time2, 'deleted': False}]}
        insertion_result = self.tester_client[self.db_name][collection].insert_one(item)
        obj_id = insertion_result.inserted_id

        # if no username is passed while deleting, DbException must be raised
        self.assertRaises(DbException, self.dba.delete_item, obj_id, collection, None)

        self.dba.delete_item(obj_id, collection, "sangu")

        read_item = self.tester_client[self.db_name][collection].find_one({'_id': obj_id})

        # check that fields are not present
        self.assertNotIn('field21', read_item)
        self.assertNotIn('field22', read_item)

        # check that auto-generated fields are there
        self.assertEqual(read_item['updatedBy'], "sangu")
        self.assertAlmostEqual(read_item['lastUpdated'], datetime.now(), delta=timedelta(0, 60))
        self.assertEqual(read_item['deleted'], True)

        # check changeLog
        change_log = [{'field21': "value21", 'field22': "value22", 'updatedBy': "sangu",
                       'lastUpdated': time2, 'deleted': False, },
                      {'field21': "wow", 'field22': "value22", 'updatedBy': "shailesh",
                       'lastUpdated': time1, 'deleted': False}]
        self.assertListEqual(read_item['changeLog'], change_log)
