import logging
from datetime import datetime, time, timedelta

from pyramid.view import view_config
from voluptuous import Schema

from tracker.rest_resource import Resource
from tracker.utils.validations import validate_object_id, validate_datetime, validate_float, validate_date_only

logger = logging.getLogger(__name__)

collection = "attendance_entries"
validation_schema = Schema({
    'schoolId': validate_object_id,
    'passengerId': validate_object_id,
    'time': validate_datetime,
    'routeId': validate_object_id,
    'lat': validate_float,
    'lng': validate_float
}, required=True)

objects = Resource(collection, validation_schema)


@view_config(route_name="bus_attendance", request_method="POST", permission="driver", renderer="json")
def attendance_mark_view(request):
    data = request.json_body
    for e in data['entries']:
        e['schoolId'] = request.user['schoolId']
        e['routeId'] = data['routeId']
        objects.create(e)
    return {'ok': True}


@view_config(route_name="bus_attendance", request_method="GET", permission="driver", renderer="json")
def get_attendance_view(request):
    filters = {}
    if 'passengerId' in request.params:
        filters['passengerId'] = validate_object_id(request.params['passengerId'])
    else:
        day = validate_date_only(request.params['date'])
        filters['routeId'] = validate_object_id(request.params['routeId'])
        filters['time'] = {'$gte': datetime.combine(day, time(0)), '$lt': datetime.combine(day + timedelta(1), time(0))}
    return objects.list_all(filters=filters)
