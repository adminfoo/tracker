from datetime import datetime, timedelta
from voluptuous import Schema, REMOVE_EXTRA, Any
import logging
from tracker import utils
from tracker.rest_resource import Resource, object_store
from tracker.utils.validations import validate_datetime, validate_object_id

logger = logging.getLogger(__name__)

collection = "accounts"
validation_schema = Schema({
    'passengerIds': [validate_object_id],
    'subscriptionName': str,
    'endDate': Any(None, validate_datetime)
}, required=True, extra=REMOVE_EXTRA)

DEMO_DAYS = 15

objects = Resource(collection, validation_schema)


def create_new_account(passenger_id, passenger_data):
    data = {
        'passengerIds': [passenger_id],
        'subscriptionName': "Demo Subscription",
        'endDate': None
    }
    ret_obj = objects.create(data)
    new_user = {
        'name': passenger_data['guardianName'] if passenger_data['type'] == "student" else passenger_data['name'],
        'phone': passenger_data['phone'],
        'email': passenger_data['email'],
        'accountId': ret_obj['id'],
        'primaryUser': True,
        'subscriptionName': "Demo Subscription",
        'endDate': None,
        'passengerIds': [passenger_id],
        'userSettings': {
            'alarmOnDrop': True,
            'alarmOnPickup': True,
            'schoolArrivalNot': True,
            'alertTime': 10
        }
    }
    object_store['users'].create(new_user)
    return ret_obj['id']


def add_new_passenger(passenger_id, account_id):
    passenger_id = validate_object_id(passenger_id)
    utils.app_db[collection].update({'_id': account_id}, {'$push': {'passengerIds': passenger_id}})
    utils.app_db['users'].update({'accountId': account_id}, {'$push': {'passengerIds': passenger_id}}, multi=True)


def remove_passenger(passenger_id):
    pax_acc = utils.app_db[collection].find_one({'passengerIds': passenger_id})
    if pax_acc:
        pax_acc['passengerIds'].remove(passenger_id)
        utils.app_db[collection].update({'_id': pax_acc['_id']}, {'$set': {'passengerIds': pax_acc['passengerIds']}})
        account_users = [x for x in utils.app_db['users'].find({'accountId': pax_acc['_id']})]
        for u in account_users:
            utils.app_db['users'].update({'_id': u['_id']}, {'$set': {'passengerIds': pax_acc['passengerIds']}})


def mark_demo_beginning(account_id):
    end_date = datetime.now() + timedelta(15)
    utils.app_db[collection].update({'_id': account_id}, {'$set': {'endDate': end_date}})
    utils.app_db['users'].update({'accountId': account_id}, {'$set': {'endDate': end_date}}, multi=True)


def set_subscription_end_date(account_id, end_date):
    utils.app_db[collection].update({'_id': account_id}, {'$set': {'endDate': end_date}})
    utils.app_db['users'].update({'accountId': account_id}, {'$set': {'endDate': end_date}}, multi=True)


def get_monthly_subscriptionFee(account_id):
    account = utils.app_db[collection].find_one({'_id': account_id})
    amount = 0
    fee = 0
    for passengerId in account['passengerIds']:
        passenger = utils.app_db["passengers"].find_one({'_id' : passengerId})
        if passenger:
            schoolId = passenger["schoolId"]
            school = utils.app_db["schools"].find_one({'_id' : schoolId})
            if school:
                fee = school["subscriptionFee"] if "subscriptionFee" in school else 100
                amount += fee
    return amount

