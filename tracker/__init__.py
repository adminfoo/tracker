from threading import Thread
from urllib.parse import urlparse
from datetime import datetime
from bson import ObjectId
from pyramid.renderers import JSON
from pyramid.security import authenticated_userid

from pyramid.session import SignedCookieSessionFactory
from pyramid.config import Configurator
from pyramid_mailer import Mailer
from tracker.init.authorization import SimpleAuthorizationPolicy
from tracker import utils
from pymongo import MongoClient
from tracker.init.authentication import SimpleAuthenticationPolicy
from tracker.utils.common_utils import assign_data_type
import logging

MAX_LIMIT = 100
logger = logging.getLogger(__name__)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)

    my_session_factory = SignedCookieSessionFactory("dumbjoker", max_age=31536000, timeout=864000, reissue_time=86400)
    config.set_session_factory(my_session_factory)

    authn_policy = SimpleAuthenticationPolicy()
    config.set_authentication_policy(authn_policy)

    authz_policy = SimpleAuthorizationPolicy()
    config.set_authorization_policy(authz_policy)

    config.include('pyramid_chameleon')
    # config is an instance of pyramid.config.Configurator
    config.add_static_view(name=settings['static_path'], path='../static')
    # config.add_static_view('static', 'static', cache_max_age=3600)

    utils.static_path = settings['static_path']
    utils.env = settings['env']

    db_url = urlparse(settings['mongo_uri'])
    utils.mongo_cli = MongoClient(db_url.hostname, db_url.port)
    if 'mongo_username' in settings:
        utils.mongo_cli.the_database.authenticate(settings['mongo_username'], settings['mongo_password'], source=settings['mongo_authdb'])  # TODO: check if authentication is successful(returns True)
    utils.app_db = utils.mongo_cli[db_url.path[1:]]
    utils.gps_db = utils.mongo_cli['trackingdb']

    add_request_methods(config)

    # add static_path to each request to be used in templates
    def add_static_path(request):
        return settings['static_path']

    config.add_request_method(add_static_path, 'static_path', reify=True)

    # add some adapters for json renderer
    def object_id_adapter(obj, request):
        return str(obj)

    def datetime_adapter(obj, request):
        # Append '+0530' for IST
        return obj.strftime("%Y-%m-%dT%H:%M:%S+0530")

    json_renderer = JSON()
    json_renderer.add_adapter(ObjectId, object_id_adapter)
    json_renderer.add_adapter(datetime, datetime_adapter)
    config.add_renderer('json', json_renderer)

    config.include('tracker.routing.routing_table')

    # add tween
    config.add_tween('tracker.init.tween_factory.my_tween_factory')

    # add pyramid_mailer
    config.include('pyramid_mailer')
    utils.mailer = Mailer.from_settings(settings)

    # start the infinite loop
    from tracker import alarms
    thread = Thread(target=alarms.main_func, daemon=True)
    thread.start()

    return config.make_wsgi_app()


def add_request_methods(config):

    def add_from_app(request):
        if request.user_agent.startswith("Android"):
            return True
        return False

    # add user to each request; it also sets user object and remote_addr for mongo_utils
    def add_user(request):
        user = authenticated_userid(request)
        if not user:
            user = {'username': "", 'userType': "anon"}
        logger.info({'user': user})
        return user

    def add_filters(request):
        filters = {}

        # Searching for query parameters in URLs
        query_dict = dict(request.GET) if request.GET else {}
        filter_keys = query_dict['filterKeys'].split(',') if 'filterKeys' in query_dict else []
        filter_values = query_dict['filterValues'].split(',') if 'filterValues' in query_dict else []

        for i in range(len(filter_keys)):
            k = filter_keys[i]
            v = filter_values[i]

            # discard un-named filters
            if len(k) == 0:
                continue

            # multiple values for a same field are separated by a '*'
            if '*' in v:
                assigned_filter_values = []  # List to contain assigned data types
                for value in v.split('*'):
                    value = assign_data_type(k, value)
                    assigned_filter_values.append(value)
                filters[k] = {'$in': assigned_filter_values}
            else:
                filters[k] = assign_data_type(k, v)

        logger.debug({'filters': filters})
        return filters

    def add_projection(request):
        projection = None
        query_dict = dict(request.GET) or {}
        if 'keys' in query_dict:
            keys = query_dict['keys'].split(',')
            projection = {}
            for k in keys:
                projection[k] = True
            return projection

        logger.debug({'projection': projection})
        return projection

    config.add_request_method(add_from_app, 'from_app', reify=True)
    config.add_request_method(add_user, 'user', reify=True)
    config.add_request_method(add_filters, 'filters', reify=True)
    config.add_request_method(add_projection, 'projection', reify=True)
