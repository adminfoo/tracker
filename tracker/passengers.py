from bson import ObjectId
from pyramid.httpexceptions import HTTPNotFound, HTTPUnauthorized, HTTPBadRequest
from pyramid.view import view_config
from voluptuous import Schema, Any, REMOVE_EXTRA, Optional
import logging
from scripts.pax_adder import load_students, class_correction, happy_model_loader
from tracker import utils
from tracker.accounts import create_new_account, add_new_passenger, remove_passenger
from tracker.rest_resource import Resource, object_store
from tracker.utils.validations import validate_str, validate_object_id, validate_phone, validate_email

logger = logging.getLogger(__name__)

collection = "passengers"
staff_schema_doc = {
    'schoolId': validate_object_id,
    'name': validate_str,
    Optional('idNumber'): str,
    Optional('designation'): str,
    'gender': Any("male", "female", "other"),
    'bloodGroup': Any("O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"),
    'phone': validate_phone,
    'email': Any(validate_email, str),
    'type': "staff",

    'pickupRouteId': validate_object_id,
    'pickupRouteName': str,
    'pickupStopId': validate_str,
    'pickupStopName': str,

    'dropRouteId': validate_object_id,
    'dropRouteName': str,
    'dropStopId': validate_str,
    'dropStopName': str
}
staff_validation_schema = Schema(staff_schema_doc, required=True, extra=REMOVE_EXTRA)
student_schema_doc = {
    'schoolId': validate_object_id,
    'name': validate_str,
    Optional('idNumber'): str,
    Optional('course'): Any("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "KG", "LKG",
                            "UKG", "Nursery", "Pre Nursery"),
    'gender': Any("male", "female", "other"),
    'bloodGroup': Any("O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"),
    'phone': validate_phone,
    'email': Any(validate_email, str),
    'type': "student",
    'guardianName': validate_str,

    'pickupRouteId': Any(None, validate_object_id),
    'pickupRouteName': Any(None, str),
    'pickupStopId': Any(None, validate_str),
    'pickupStopName': Any(None, str),

    'dropRouteId': Any(None, validate_object_id),
    'dropRouteName': Any(None, str),
    'dropStopId': Any(None, validate_str),
    'dropStopName': Any(None, str)
}
student_validation_schema = Schema(student_schema_doc, required=True, extra=REMOVE_EXTRA)

objects = Resource(collection, student_validation_schema)


@view_config(route_name="passengers", request_method="GET", permission="school", renderer='json')
def list_all(request):
    filters = request.filters
    if 'routeId' in filters:
        filters['$or'] = [
            {'pickupRouteId': ObjectId(filters['routeId'])},
            {'dropRouteId': ObjectId(filters['routeId'])}
        ]
        del filters['routeId']
    filters['schoolId'] = request.user['schoolId']
    return objects.list_all(filters=filters, projection=request.projection)


@view_config(route_name="passengers_add_all", request_method="GET", renderer='json')
def pax_all_add(request):
    if 'updateClass' in request.GET:
        class_correction()
        return {'ok': "done"}

    studs = []
    if request.GET['school'] == "royaloak":
        studs = load_students(request.user['schoolId'])
    if request.GET['school'] == "happym":
        studs = happy_model_loader()
    logger.debug({'count': len(studs)})
    ret_obj = {'students': []}
    for data in studs:
        create_res = create_single_passenger(data, request.user['schoolId'])
        logger.info({'msg': "pax added", 'pax': data})
        ret_obj['students'].append({'id': create_res['id'], 'name': data['name']})
    return ret_obj


@view_config(route_name="passengers", request_method="POST", permission="school", renderer='json')
def create(request):
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)

    return create_single_passenger(data, request.user['schoolId'])


def create_single_passenger(pax_data, school_id):
    schema = student_validation_schema
    if pax_data['type'] == "staff":
        schema = staff_validation_schema

    pax_data['schoolId'] = school_id

    pickup_route_details = object_store['routes'].detail(pax_data['pickupRouteId'])
    pax_data['pickupRouteName'] = pickup_route_details['name']
    pickup_stop_details = next(s for s in pickup_route_details['stops'] if s['id'] == pax_data['pickupStopId'])
    pax_data['pickupStopName'] = pickup_stop_details['name']

    drop_route_details = object_store['routes'].detail(pax_data['dropRouteId'])
    pax_data['dropRouteName'] = drop_route_details['name']
    drop_stop_details = next(s for s in drop_route_details['stops'] if s['id'] == pax_data['dropStopId'])
    pax_data['dropStopName'] = drop_stop_details['name']

    ret_obj = objects.create(pax_data, schema=schema)

    # create a user and link the passenger, if not already present
    user = utils.app_db['users'].find_one({'phone': pax_data['phone']})
    if user:
        add_new_passenger(ret_obj['id'], user['accountId'])
    else:
        create_new_account(ret_obj['id'], pax_data)

    return ret_obj


@view_config(route_name="passenger_details", request_method="GET", permission="school", renderer='json')
def detail(request):
    item_id = request.matchdict['pk']
    details = objects.detail(item_id)
    if details is None or details['schoolId'] != request.user['schoolId']:
        raise HTTPNotFound("No passenger found")
    return details


@view_config(route_name="passenger_change_stop", request_method="POST", permission="user", renderer='json')
def change_stop(request):
    data = request.json_body
    logger.debug({'data': data})
    user_id = request.user['_id']
    user = utils.app_db['users'].find_one({'_id': user_id})
    pax_id = validate_object_id(data['passengerId'])
    if pax_id not in user['passengerIds']:
        raise HTTPUnauthorized("You are not allowed to do this action")
    logger.debug({'paxId': pax_id})
    route_details = utils.app_db['routes'].find_one({'_id': validate_object_id(data['routeId'])})
    stop_id = data['stopId']

    ff = next((s for s in route_details['stops'] if s['id'] == stop_id), None)
    if ff is None:
        raise HTTPBadRequest("stop not in route")
    stop_name = ff['name']
    logger.debug({'stop name': stop_name})
    if route_details['type'] == "pickup":
        objects.edit(pax_id, {'pickupStopId': stop_id, 'pickupStopName': stop_name})
    else:
        objects.edit(pax_id, {'dropStopId': stop_id, 'dropStopName': stop_name})

    stop_details = next(s for s in route_details['stops'] if s['id'] == stop_id)
    del stop_details['loc']
    stop_details['time'] = stop_details['time'].strftime("%H:%M %p")
    stop_details['sta'] = 21  # TODO: implement it correctly
    return stop_details


@view_config(route_name="passenger_get_stops", request_method="GET", permission="user", renderer='json')
def get_all_valid_stops(request):
    route_id = dict(request.GET)['routeId']
    route_details = utils.app_db['routes'].find_one({'_id': validate_object_id(route_id)})
    return {'stops': [{'id': s['id'], 'name': s['name']} for s in route_details['stops']]}


@view_config(route_name="passenger_details", request_method="PATCH", permission="school", renderer='json')
def edit(request):
    item_id = request.matchdict['pk']
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    data['schoolId'] = request.user['schoolId']

    pickup_route_details = object_store['routes'].detail(data['pickupRouteId'])
    data['pickupRouteName'] = pickup_route_details['name']
    pickup_stop_details = next(s for s in pickup_route_details['stops'] if s['id'] == data['pickupStopId'])
    data['pickupStopName'] = pickup_stop_details['name']
    drop_route_details = object_store['routes'].detail(data['dropRouteId'])
    data['dropRouteName'] = drop_route_details['name']
    drop_stop_details = next(s for s in drop_route_details['stops'] if s['id'] == data['dropStopId'])
    data['dropStopName'] = drop_stop_details['name']

    schema = student_validation_schema
    if data['type'] == "staff":
        schema = staff_validation_schema

    # TODO: if guardianName, phone or email is changed, update the user linked to account
    # TODO: handle all cases, might be a bit tricky

    return objects.edit(item_id, data, schema=schema)


@view_config(route_name="passenger_details", request_method="DELETE", permission="school", renderer='json')
def delete(request):
    item_id = request.matchdict['pk']
    ret_obj = objects.delete(item_id, extra_filters={'schoolId': request.user['schoolId']})

    # remove from account
    remove_passenger(ObjectId(item_id))
    return ret_obj
