import logging

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.security import remember
from pyramid.view import view_config
from voluptuous import Schema, Any, REMOVE_EXTRA, Required

from tracker.rest_resource import Resource, object_store
from tracker.utils import app_db
from tracker.utils.sms_utils import send_otp, retrieve_otp, send_otp_dummy
from tracker.utils.validations import validate_str, validate_phone, validate_email, validate_object_id

logger = logging.getLogger(__name__)

collection = "drivers"
validation_schema = Schema({
    'schoolId': validate_object_id,
    'name': validate_str,
    'phone': validate_phone,
    Required('email', default=""): Any("", validate_email),
    Required('licenseNo', default=""): str,
    Required('role', default="driver"): str
}, required=True, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


@view_config(route_name="drivers", request_method="POST", permission="school", renderer="json")
def driver_create_view(request):
    data = request.json_body
    data['schoolId'] = request.user['schoolId']
    return objects.create(data, unique_field='phone')


@view_config(route_name="driver_details", request_method="PATCH", permission="school", renderer="json")
def driver_update_view(request):
    driver_id = request.matchdict['pk']
    data = request.json_body
    return objects.edit(driver_id, data, unique_field='phone')


@view_config(route_name="drivers", request_method="GET", permission="school", renderer="json")
def drivers_list_view(request):
    filters = request.filters
    if filters is None:
        filters = {}
    filters['schoolId'] = request.user['schoolId']
    all_drivers = objects.list_all(request.filters, request.projection)
    return all_drivers


@view_config(route_name="driver_details", request_method="GET", permission="school", renderer="json")
def driver_detail_view(request):
    driver_id = request.matchdict['pk']
    return objects.detail(driver_id)


@view_config(route_name="drivers", request_method="POST", permission="school", renderer="json")
def driver_create_view(request):
    data = request.json_body
    return objects.create(data)


@view_config(route_name="driver_otp", request_method="POST", renderer='json')
def driver_otp_view(request):
    data = request.json_body
    phone = data['phone']
    debug = True

    if phone[0] in ['1', '2', '3']:
        phone = str(int(phone[0]) + 6) + phone[1:]
        debug = True

    driver = app_db[collection].find_one({'phone': phone})
    if not driver:
        return {'isValid': False, 'msg': "This number is not registered. Please contact your school admin."}

    # send OTP sms to phone
    if debug:
        send_otp_dummy(data['phone'])
    else:
        send_otp(data['phone'])

    return {'isValid': True}


@view_config(route_name="driver_login", request_method="POST", renderer='json')
def driver_login_view(request):
    data = request.json_body

    phone = data['phone']
    otp = data['otp']
    debug = False

    if phone[0] in ['1', '2', '3']:
        phone = str(int(phone[0]) + 6) + phone[1:]
        debug = True

    if debug is False:
        correct_otp = retrieve_otp(phone)['otp']
    else:
        correct_otp = "2199"

    if otp != correct_otp:
        return HTTPBadRequest("OTP is not correct")

    driver = objects.find_one({'phone': phone})

    # mark the user logged in
    user_info = {'username': phone, 'name': driver['name'], 'phone': phone, 'email': driver['email'],
                 'userType': "driver", '_id': driver['id'], 'schoolId': driver['schoolId']}
    remember(request, user_info)

    # remove otp entry
    app_db['otps'].remove({'phone': phone})

    return get_driver_startup_info(driver['id'])


@view_config(route_name="driver_start_info", request_method="GET", permission="driver", renderer='json')
def driver_start_view(request):
    driver_id = request.user['_id']
    all_info = get_driver_startup_info(driver_id)
    return {'currentRouteId': all_info['currentRouteId']}


def drivers_current_route(driver_routes):
    from tracker.alarms import get_current_route_trip
    for route in driver_routes:
        cur_trip = get_current_route_trip(route)
        if cur_trip is not None:
            return route['id']


def get_driver_routes(driver_id):
    routes_filters = {
        'type': {'$exists': True},
        '$or': [{'driverId': validate_object_id(driver_id)}, {'helperId': validate_object_id(driver_id)}]
    }
    return object_store['routes'].list_all(filters=routes_filters)


def get_driver_startup_info(driver_id):
    driver_details = objects.detail(driver_id)
    driver_routes = get_driver_routes(driver_id)
    return_object = {
        'driver': driver_details,
        'routes': driver_routes,
        'currentRouteId': drivers_current_route(driver_routes)
    }
    pickup_route_ids = []
    drop_route_ids = []
    for route in return_object['routes']:
        if route['type'] == "pickup":
            pickup_route_ids.append(route['id'])
        else:
            drop_route_ids.append(route['id'])
    pax_filters = {'$or': [{'pickupRouteId': {'$in': pickup_route_ids}}, {'dropRouteId': {'$in': drop_route_ids}}]}
    return_object['passengers'] = object_store['passengers'].list_all(filters=pax_filters)
    return return_object
