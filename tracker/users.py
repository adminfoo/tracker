from copy import deepcopy
from datetime import datetime, timedelta
from pyramid.httpexceptions import HTTPUnauthorized, HTTPConflict, HTTPBadRequest
from pyramid.security import remember
from pyramid.view import view_config
from voluptuous import Schema, Any, ALLOW_EXTRA, Required
import logging
from tracker import utils
from tracker.accounts import mark_demo_beginning
from tracker.devices import get_current_location
from tracker.rest_resource import Resource, object_store
from tracker.utils.sms_utils import send_otp, retrieve_otp
from tracker.utils.validations import validate_str, validate_object_id, validate_phone, validate_email, \
    validate_datetime, validate_int

logger = logging.getLogger(__name__)

collection = "users"
user_settings_schema = Schema({
    'alarmOnDrop': bool,
    'alarmOnPickup': bool,
    'schoolArrivalNot': bool,
    'alertTime': validate_int
}, extra=ALLOW_EXTRA)
validation_schema = Schema({
    'name': validate_str,
    'phone': validate_phone,
    'email': Any("", validate_email),
    'accountId': validate_object_id,
    Required('primaryUser', default=False): bool,
    'subscriptionName': str,
    'endDate': Any(None, validate_datetime),
    'passengerIds': [validate_object_id],
    'userSettings': user_settings_schema
}, required=True)

objects = Resource(collection, validation_schema)

local_obj = {'verbose': False}


@view_config(route_name="init_register", request_method="POST", renderer='json')
def init_login(request):
    data = request.json_body
    phone = data['phone']
    debug = False

    if phone[0] in ['1', '2', '3']:
        phone = str(int(phone[0]) + 6) + phone[1:]
        debug = True

    user = utils.app_db[collection].find_one({'phone': phone})
    if not user:
        return {'isValid': False, 'msg': "This number is not registered. Please contact your school admin."}
    if len(user['passengerIds']) == 0:
        return {'isValid': False, 'msg': "No ward registered against your number. Please contact your school admin."}

    # send OTP sms to phone
    if debug is False:
        send_otp(data['phone'])

    return {'isValid': True}


@view_config(route_name="finish_register", request_method="POST", renderer='json')
def finish_login(request):
    data = request.json_body

    phone = data['phone']
    otp = data['otp']
    debug = False

    if phone[0] in ['1', '2', '3']:
        phone = str(int(phone[0]) + 6) + phone[1:]
        debug = True

    if debug is False:
        correct_otp = retrieve_otp(phone)['otp']
    else:
        correct_otp = "2199"

    if otp != correct_otp:
        return HTTPBadRequest("OTP is not correct")

    # remove otp entry
    utils.app_db['otps'].remove({'phone': phone})

    user = utils.app_db[collection].find_one({'phone': phone})
    if user['endDate'] is None:
        mark_demo_beginning(user['accountId'])
        user = utils.app_db[collection].find_one({'phone': phone})

    # mark the user logged in
    user_info = {'username': phone, 'name': user['name'], 'phone': phone, 'email': user['email'], 'userType': "user",
                 '_id': user['_id'], 'accountId': user['accountId']}
    remember(request, user_info)

    return get_full_info(user['_id'])


@view_config(route_name="finish_register", request_method="POST", header="api-version:1.1", renderer='json')
def finish_login_v1_1(request):
    data = request.json_body

    phone = data['phone']
    otp = data['otp']
    debug = False

    if phone[0] in ['1', '2', '3']:
        phone = str(int(phone[0]) + 6) + phone[1:]
        debug = True

    if debug is False:
        correct_otp = retrieve_otp(phone)['otp']
    else:
        correct_otp = "2199"

    if otp != correct_otp:
        return HTTPBadRequest("OTP is not correct")

    # remove otp entry
    utils.app_db['otps'].remove({'phone': phone})

    user = utils.app_db[collection].find_one({'phone': phone})
    if user['endDate'] is None:
        mark_demo_beginning(user['accountId'])
        user = utils.app_db[collection].find_one({'phone': phone})

    # mark the user logged in
    user_info = {'username': phone, 'name': user['name'], 'phone': phone, 'email': user['email'], 'userType': "user",
                 '_id': user['_id'], 'accountId': user['accountId']}
    remember(request, user_info)

    return get_full_info_v1_1(user['_id'], return_route_info=True)


@view_config(route_name="startup_info", request_method="GET", permission="user", renderer='json')
def get_startup_info(request):
    query_dict = dict(request.GET)
    return_route_info = True
    if 'routeInfo' in query_dict and (query_dict['routeInfo'] == "false" or query_dict['routeInfo'] == False):
        return_route_info = False
    return get_full_info(request.user['_id'], return_route_info)


@view_config(route_name="startup_info", request_method="GET", header="api-version:1.1", permission="user",
             renderer='json')
def get_startup_info_v1(request):
    query_dict = dict(request.GET)
    return_route_info = False
    if 'routeInfo' in query_dict and (query_dict['routeInfo'] == "true" or query_dict['routeInfo'] == True):
        return_route_info = True
    return get_full_info_v1_1(request.user['_id'], return_route_info)


@view_config(route_name="startup_info", request_method="GET", permission="user", renderer='json')
def get_startup_info(request):
    query_dict = dict(request.GET)
    return_route_info = True
    if 'routeInfo' in query_dict and (query_dict['routeInfo'] == "false" or query_dict['routeInfo'] == False):
        return_route_info = False
    return get_full_info(request.user['_id'], return_route_info)


def get_full_info_v1_1(user_id, return_route_info=True):
    user = utils.app_db[collection].find_one({'_id': user_id})
    if not user:
        raise HTTPUnauthorized("Not logged in")
    all_paxes = object_store['passengers'].list_all(filters={'_id': {'$in': user['passengerIds']}})

    pickup_routes_dict = {}
    drop_routes_dict = {}

    for pax in all_paxes:
        pickup_route = object_store['routes'].detail(pax['pickupRouteId'])
        drop_route = object_store['routes'].detail(pax['dropRouteId'])
        if len(pickup_route['pathPoints']) == 0:
            # TODO: create path points here only
            logger.warn({'msg': "No path points for pickup route", 'route': pickup_route['name'], '_id': pickup_route['id']})
        if len(drop_route['pathPoints']) == 0:
            # TODO: create path points here only
            logger.warn({'msg': "No path points for drop route", 'route': drop_route['name'], '_id': drop_route['id']})

        pax_pickup_stop = deepcopy(next(s for s in pickup_route['stops'] if s['id'] == pax['pickupStopId']))
        del pax_pickup_stop['loc']

        t = pax_pickup_stop['timeObj']
        pax_pickup_stop['time'] = "{0:02d}:{1:02d} {2}".format(t['hr'], t['min'], t['meridian'])
        del pax_pickup_stop['timeObj']
        pax_pickup_stop['sta'] = get_sta_from_route_trips(pickup_route, pax_pickup_stop['id'])

        del pickup_route['stops']

        pax_drop_stop = deepcopy(next(s for s in drop_route['stops'] if s['id'] == pax['dropStopId']))
        del pax_drop_stop['loc']

        t = pax_drop_stop['timeObj']
        pax_drop_stop['time'] = "{0:02d}:{1:02d} {2}".format(t['hr'], t['min'], t['meridian'])
        del pax_drop_stop['timeObj']
        pax_drop_stop['sta'] = get_sta_from_route_trips(drop_route, pax_drop_stop['id'])

        del drop_route['stops']

        school_loc = utils.app_db['schools'].find_one({'_id': pickup_route['schoolId']},
                                                      {'lat': True, 'lng': True, '_id': False})
        pickup_route['schoolLocation'] = school_loc
        drop_route['schoolLocation'] = school_loc

        pax['pickupStop'] = pax_pickup_stop
        pax['dropStop'] = pax_drop_stop
        pickup_route['myStop'] = pax_pickup_stop
        drop_route['myStop'] = pax_drop_stop

        del pickup_route['vehicleId']
        del pickup_route['schoolId']
        del pickup_route['vehicleDeviceId']
        if 'pickupPathPoints' in pickup_route:
            del pickup_route['pickupPathPoints']
        if 'dropPathPoints' in pickup_route:
            del pickup_route['dropPathPoints']
        if 'trips' in pickup_route:
            del pickup_route['trips']
        for pt in pickup_route['pathPoints']:
            if 'time' in pt:
                del pt['time']
            if 'type' in pt:
                del pt['type']
        pickup_routes_dict[pickup_route['id']] = pickup_route

        del drop_route['vehicleId']
        del drop_route['schoolId']
        del drop_route['vehicleDeviceId']
        if 'pickupPathPoints' in drop_route:
            del drop_route['pickupPathPoints']
        if 'dropPathPoints' in drop_route:
            del drop_route['dropPathPoints']
        if 'trips' in drop_route:
            del drop_route['trips']
        for pt in drop_route['pathPoints']:
            if 'time' in pt:
                del pt['time']
            if 'type' in pt:
                del pt['type']
        drop_routes_dict[drop_route['id']] = drop_route

    acc_users = objects.list_all(filters={'accountId': user['accountId'], '_id': {'$ne': user['_id']}})

    sub_mon = utils.app_db['accounts'].find_one({'_id': user['accountId']})
    days_to_go = (sub_mon['endDate'].date() - datetime.now().date()).days
    subscription_obj = {
        'subscriptionName': sub_mon['subscriptionName'],
        'endDate': sub_mon['endDate'].strftime("%d %b %Y"),
        'daysToGo': days_to_go,
        'showRenew': True if days_to_go <= 7 else False
    }

    ret_obj = user
    if return_route_info:
        ret_obj['routeInfo'] = {
            'pickupRoutes': list(pickup_routes_dict.values()),
            'dropRoutes': list(drop_routes_dict.values())
        }
    ret_obj['passengers'] = all_paxes
    ret_obj['currentSubscription'] = subscription_obj
    ret_obj['addedUsers'] = [{'id': u['id'], 'name': u['name'], 'phone': u['phone']} for u in acc_users]
    if '_id' in ret_obj:
        ret_obj['id'] = ret_obj['_id']
        del ret_obj['_id']
    del ret_obj['accountId']

    if ret_obj['endDate'] > datetime.now():
        ret_obj['subscriptionOver'] = False
    else:
        ret_obj['subscriptionOver'] = True

    ret_obj['sta'] = min(min([pax['pickupStop']['sta'] for pax in ret_obj['passengers']]),
                         min([pax['dropStop']['sta'] for pax in ret_obj['passengers']]))

    return ret_obj


def get_full_info(user_id, return_route_info=True):
    user = utils.app_db[collection].find_one({'_id': user_id})
    if not user:
        raise HTTPUnauthorized("Not logged in")
    all_paxes = object_store['passengers'].list_all(filters={'_id': {'$in': user['passengerIds']}})

    routes_dict = {}

    for pax in all_paxes:
        pickup_route = object_store['routes'].detail(pax['pickupRouteId'])
        drop_route = object_store['routes'].detail(pax['dropRouteId'])
        pax['routeId'] = pax['pickupRouteId']
        pax['routeName'] = pax['pickupRouteName']
        pax['stopId'] = pax['pickupStopId']
        pax['stopName'] = pax['pickupStopName']
        pax['serviceType'] = "both"  # TODO: make it correct
        route_details = object_store['routes'].detail(pax['routeId'])
        del route_details['pathPoints']
        del route_details['startTime']
        del route_details['type']
        route_details['pickupPathPoints'] = pickup_route['pathPoints']
        route_details['dropPathPoints'] = drop_route['pathPoints']
        # if route_details['id'] in routes_dict:
        #     continue
        pickup_stop = next(s for s in pickup_route['stops'] if s['id'] == pax['pickupStopId'])
        drop_stop = next(s for s in drop_route['stops'] if s['id'] == pax['dropStopId'])

        pax_stop = deepcopy(next(s for s in route_details['stops'] if s['id'] == pax['stopId']))
        del pax_stop['loc']
        del pax_stop['timeObj']
        del pax_stop['time']

        t = pickup_stop['timeObj']
        pax_stop['pickupTime'] = "{0:02d}:{1:02d} {2}".format(t['hr'], t['min'], t['meridian'])
        t = drop_stop['timeObj']
        pax_stop['dropTime'] = "{0:02d}:{1:02d} {2}".format(t['hr'], t['min'], t['meridian'])
        pax_stop['sta'] = min(get_sta_from_route_trips(pickup_route, drop_stop['id']),
                              get_sta_from_route_trips(drop_route, pickup_stop['id']))

        del route_details['stops']

        school_loc = utils.app_db['schools'].find_one({'_id': route_details['schoolId']},
                                                      {'lat': True, 'lng': True, '_id': False})
        route_details['schoolLocation'] = school_loc

        pax['stop'] = pax_stop
        route_details['myStop'] = pax_stop

        del route_details['vehicleId']
        del route_details['schoolId']
        del route_details['vehicleDeviceId']
        if 'trips' in route_details:
            del route_details['trips']
        for pt in route_details['pickupPathPoints']:
            if 'time' in pt:
                del pt['time']
            if 'type' in pt:
                del pt['type']
        for pt in route_details['dropPathPoints']:
            if 'time' in pt:
                del pt['time']
            if 'type' in pt:
                del pt['type']
        routes_dict[route_details['id']] = route_details

    acc_users = objects.list_all(filters={'accountId': user['accountId'], '_id': {'$ne': user['_id']}})

    sub_mon = utils.app_db['accounts'].find_one({'_id': user['accountId']})
    days_to_go = (sub_mon['endDate'].date() - datetime.now().date()).days
    subscription_obj = {
        'subscriptionName': sub_mon['subscriptionName'],
        'endDate': sub_mon['endDate'].strftime("%d %b %Y"),
        'daysToGo': days_to_go,
        'showRenew': True if days_to_go <= 7 else False
    }

    ret_obj = user
    if return_route_info:
        ret_obj['routeInfo'] = list(routes_dict.values())
    ret_obj['passengers'] = all_paxes
    ret_obj['currentSubscription'] = subscription_obj
    ret_obj['addedUsers'] = [{'id': u['id'], 'name': u['name'], 'phone': u['phone']} for u in acc_users]
    if '_id' in ret_obj:
        ret_obj['id'] = ret_obj['_id']
        del ret_obj['_id']
    del ret_obj['accountId']

    if ret_obj['endDate'] > datetime.now():
        ret_obj['subscriptionOver'] = False
    else:
        ret_obj['subscriptionOver'] = True

    ret_obj['sta'] = min([pax['stop']['sta'] for pax in ret_obj['passengers']])

    return ret_obj


def get_sta_from_route_trips(route, stop_id):
    start_time = datetime.combine(datetime.now().date(), route['startTime'].time())
    stop = next((s for s in route['stops'] if s['id'] == stop_id), None)
    if stop is None:
        return 6
    stop_time = datetime.combine(datetime.now().date(), stop['time'].time())
    sta = int((stop_time - start_time).total_seconds() / 60)
    return sta


@view_config(route_name="pax_tracking_info", request_method="GET", permission="user", renderer="json")
def get_tracking_info(request):
    if request.user['phone'] == "9873715477":
        local_obj['verbose'] = True
    else:
        local_obj['verbose'] = False
    query_dict = dict(request.GET)
    pax_ids = query_dict['passengerIds'].split(',')
    routes_dict = {}
    from tracker.alarms import get_current_route_trip
    for pax_id in pax_ids:
        if len(pax_id) == 0:
            continue
        # if ObjectId(pax_id) not in request.user['pax_ids']:
        #     return HTTPNotFound("No data for this passenger Id")

        pax_details = object_store['passengers'].detail(pax_id)
        if pax_details is None:
            continue
        pickup_route_details = object_store['routes'].detail(pax_details['pickupRouteId'])
        current_trip = get_current_route_trip(pickup_route_details)
        if current_trip is not None:
            if pax_details['pickupRouteId'] not in routes_dict:
                routes_dict[pax_details['pickupRouteId']] = {
                    'route_details': pickup_route_details,
                    'current_trip': current_trip,
                    'paxes': [],
                    'myStopId': pax_details['pickupStopId']
                }
            routes_dict[pax_details['pickupRouteId']]['paxes'].append(pax_details)
            continue
        drop_route_details = object_store['routes'].detail(pax_details['dropRouteId'])
        current_trip = get_current_route_trip(pickup_route_details)
        if pax_details['dropRouteId'] not in routes_dict:
            routes_dict[pax_details['dropRouteId']] = {
                'route_details': drop_route_details,
                'current_trip': current_trip,
                'paxes': [],
                'myStopId': pax_details['dropStopId']
            }
        routes_dict[pax_details['dropRouteId']]['paxes'].append(pax_details)

    ret_obj = []
    for k, v in routes_dict.items():
        route_details = v['route_details']
        current_trip = v['current_trip']
        # print(route_details['id'])

        if current_trip is None or current_trip['endTime'] is not None:
            status = "inactive"
            comment = "Parked"
        else:
            status = current_trip['pickupOrDrop']
            comment = "In transit"

        resp = {'status': status, 'comment': comment}
        if status != "inactive":
            loc_info = get_current_location(route_details['vehicleDeviceId'])
            if loc_info:
                # route_trip = pickup_trip if status == "pickup" else drop_trip
                delay = int(current_trip['latestDelay'])
                if delay < 0:
                    delay = 0
                my_stop = next((s for s in current_trip['stops'] if s['id'] == v['myStopId']), None)
                if my_stop is not None:
                    if my_stop['actTime'] is not None:
                        stop_time = "Departed"
                        stop_arrival = -1
                    else:
                        stop_time = (my_stop['schTime'] + timedelta(0, delay)).strftime("%H:%M")
                        stop_arrival = int((my_stop['schTime'] + timedelta(0, delay) -
                                            datetime.now()).total_seconds() / 60)
                    if status == "drop":
                        school_time = "Departed"
                        school_arrival = -1
                    else:
                        sch_time = current_trip['stops'][-1]['schTime']
                        school_time = (sch_time + timedelta(0, 600)).strftime("%H:%M")
                        school_arrival = int((sch_time + timedelta(0, 600) - datetime.now()).total_seconds() / 60)
                    resp.update({'delay': delay, 'myStopTime': stop_time, 'schoolTime': school_time,
                                 'myStopArrival': stop_arrival, 'schoolArrival': school_arrival})
                    resp.update(loc_info)
                else:
                    resp['status'] = "inactive"
                    resp['comment'] = "Temporary GPS down"
        resp.update({'names': [p['name'] for p in v['paxes']], 'routeId': route_details['id']})
        ret_obj.append(resp)
    return {'routeTrackInfo': ret_obj}


@view_config(route_name="users", request_method="POST", permission="user", renderer="json")
def add_linked_user(request):
    data = request.json_body
    this_user = utils.app_db[collection].find_one({'_id': request.user['_id']})
    if this_user['primaryUser'] is False:
        raise HTTPUnauthorized("You are not authorized for this action")
    added_users = objects.list_all(filters={'accountId': this_user['accountId'], '_id': {'$ne': this_user['_id']}})
    if len(added_users) >= 3:
        raise HTTPConflict("Maximum limit reached for linked users. Please contact support@edufits.com")
    if 'email' not in data:
        data['email'] = ""
    data['accountId'] = this_user['accountId']
    data['subscriptionName'] = this_user['subscriptionName']
    data['endDate'] = this_user['endDate']
    data['passengerIds'] = this_user['passengerIds']
    data['userSettings'] = {
        'alarmOnDrop': True,
        'alarmOnPickup': True,
        'schoolArrivalNot': True,
        'alertTime': 10
    }
    data['primaryUser'] = False
    objects.create(data)
    account_users = objects.list_all(filters={'accountId': this_user['accountId'], '_id': {'$ne': this_user['_id']}})
    ret_obj = {'addedUsers': [{'id': u['id'], 'name': u['name'], 'phone': u['phone']} for u in account_users]}
    return ret_obj


@view_config(route_name="user_details", request_method="DELETE", permission="user", renderer="json")
def remove_linked_user(request):
    item_id = request.matchdict['pk']
    this_user = utils.app_db[collection].find_one({'_id': request.user['_id']})
    if item_id == str(this_user['_id']):
        raise HTTPConflict("Can not delete self")
    if this_user['primaryUser'] is False:
        raise HTTPUnauthorized("You are not authorized for this action")
    objects.delete(item_id)
    account_users = objects.list_all(filters={'accountId': this_user['accountId'], '_id': {'$ne': this_user['_id']}})
    ret_obj = {'addedUsers': [{'id': u['id'], 'name': u['name'], 'phone': u['phone']} for u in account_users]}
    return ret_obj


@view_config(route_name="save_settings", request_method="POST", permission="user", renderer="json")
def save_settings(request):
    data = request.json_body
    objects.edit(request.user['_id'], {'userSettings': data})
    updated_details = objects.detail(request.user['_id'])
    return {'userSettings': updated_details['userSettings']}


@view_config(route_name="users", request_method="GET", permission="superadmin", renderer="json")
def users_view(request):
    all_users = objects.list_all(filters=request.filters, projection=request.projection)
    for u in all_users:
        u['feedbacks'] = [f for f in utils.app_db['feedbacks'].find({'userId': validate_object_id(u['id'])})]
        u['passengers'] = [p for p in utils.app_db['passengers'].find({'_id': {'$in': u['passengerIds']}})]
    return all_users
