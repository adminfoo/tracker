import re
from datetime import datetime, timedelta, date, time
from bson import ObjectId
from voluptuous import Invalid
import logging


logger = logging.getLogger(__name__)


def validate_datetime(value):
    """
    Validates a date string
    Returns a datetime object, if string is a valid date

    Raises voluptuous.Invalid for invalid date
    :param value:
    :return:
    """
    if isinstance(value, datetime):
        return value
    try:
        return datetime.fromtimestamp(int(value))
    except:
        pass
    try:
        return datetime.fromtimestamp(int(value) / 1000)
    except:
        pass
    value = value.replace('T', ' ')
    date_formats = ["%Y-%m-%d %H:%M:%S.%fZ", "%Y/%m/%d %H:%M:%S.%fZ", "%Y-%m-%d %H:%M:%SZ", "%Y/%m/%d %H:%M:%SZ"]
    for fmt in date_formats:
        try:
            dt = datetime.strptime(value, fmt) + timedelta(0, 19800)
            return dt
        except ValueError:
            pass

    # TODO: implement time zone, for now taking +0530
    date_formats = ["%Y-%m-%d %H:%M:%S.%f%z", "%Y/%m/%d %H:%M:%S.%f%z", "%Y-%m-%d %H:%M:%S%z",
                    "%Y/%m/%d %H:%M:%S%z", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S.%f"]
    for fmt in date_formats:
        try:
            dt = datetime.strptime(value, fmt)
            return dt
        except ValueError:
            pass

    date_formats = ["%d-%m-%Y", "%d/%m/%Y", "%Y-%m-%d", "%Y/%m/%d"]
    for fmt in date_formats:
        try:
            dt = datetime.strptime(value, fmt)
            return dt
        except ValueError:
            pass

    date_formats = ["%H:%M:%S.%fZ", "%H:%M:%SZ", "%H:%M:%S.%f%z", "%Y-%m-%d %H:%M:%S", "%H:%M:%S.%f"]
    for fmt in date_formats:
        try:
            dt = datetime.strptime(value, fmt)
            return dt
        except ValueError:
            pass
    logger.debug({'msg': "invalid date", 'value': value})
    raise Invalid("date is not valid")


def validate_date_only(value):
    """
    Validates a date string
    Returns a datetime object, if string is a valid date

    Raises voluptuous.Invalid for invalid date
    :param value:
    :return:
    """
    return validate_datetime(value).date()


def validate_time_only(value):
    """
    Validates a date string
    Returns a datetime object, if string is a valid date

    Raises voluptuous.Invalid for invalid date
    :param value:
    :return:
    """
    return validate_datetime(value).time()


def validate_object_id(val):
    if isinstance(val, ObjectId):
        return val
    return ObjectId(val)


def validate_float(value):
    try:
        return float(value)
    except:
        raise Invalid("expected float value")


def validate_int(value):
    try:
        return int(value)
    except:
        raise Invalid("expected int value")


def validate_int_foolproof(value):
    try:
        return int(value)
    except:
        return int(0)


def validate_str(value):
    value = str(value)
    if len(value) == 0:
        raise Invalid("expected a non-empty string")
    return value


def validate_phone(phone_str):
    return phone_str


user_regex = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*$"  # dot-atom
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"$)',  # quoted-string
    re.IGNORECASE)
domain_regex = re.compile(
    # max length of the domain is 249: 254 (max email length) minus one
    # period, two characters for the TLD, @ sign, & one character before @.
    r'(?:[A-Z0-9](?:[A-Z0-9-]{0,247}[A-Z0-9])?\.)+(?:[A-Z]{2,6}|[A-Z0-9-]{2,}(?<!-))$',
    re.IGNORECASE)


def validate_email(value):
    """
    Validates an email, raises voluptuous.Invalid for invalid email
    :param value:
    :return:
    """
    message = "email is not valid"

    if not value or '@' not in value:
        logger.debug({'msg': "invalid email", 'value': value})
        raise Invalid(message)

    user_part, domain_part = value.rsplit('@', 1)

    if not user_regex.match(user_part) or not domain_regex.match(domain_part):
        logger.debug({'msg': "invalid email", 'value': value})
        raise Invalid(message)

    return value

new_year_date = datetime(2015, 1, 1)


def day_from_2015(date_value):
    if isinstance(date_value, int):
        return date_value
    if isinstance(date_value, date):
        date_value = datetime.combine(date_value, time(0))
    return int((date_value - new_year_date).days)
