class DbException(Exception):
    """Raised during db operation"""
    def __init__(self, msg, db_name=None, collection=None):
        self._msg = msg
        self._db_name = db_name
        self._collection = collection

    def __str__(self):
        return "db: " + self._db_name + ", collection: " + self._collection + "\n" + self._msg
