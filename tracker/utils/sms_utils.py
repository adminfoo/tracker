import random
import string

from httplib2 import Http
from urllib.parse import urlencode
import logging
from datetime import datetime, timedelta
from multiprocessing import Process
from tracker import utils

logger = logging.getLogger(__name__)

AUTH_KEY = "94478AvY11Q45561b0524"
SENDER = "EDUFIT"
URL = "http://api.msg91.com/sendhttp.php"
h = Http()

prod_white_list = {"8800443925": True, "9873715477": True, "8826055223": True, "8826631818": True, "8860155966": True}
local_white_list = {"8800443925": True, "9873715477": True, "8826055223": True}


def send_sms(phones, content, tag="GENERIC"):
    if not isinstance(phones, list):
        phones = [phones]
    for phone in phones:
        p = Process(target=send_one_sms, args=(phone, content, tag))
        p.daemon = True
        p.start()


def send_one_sms(phone, content, tag="GENERIC"):
    logger.info({'msg': "sending sms", 'phone': phone, 'content': content})
    if utils.env != "prod":
        if phone not in local_white_list:
            return
    if len(phone) == 10:
        phone = "91" + phone
    payload = {
        'authkey': AUTH_KEY,
        'mobiles': phone,
        'message': content,
        'sender': SENDER,
        'route': 4,
        'country': 91
    }
    url = "https://control.msg91.com/api/sendhttp.php?" + urlencode(payload)
    logger.info({'sms_url': url})
    res, con = h.request(url)
    try:
        response_body = con.decode('utf-8')
        sms_detail = {
            'requestId' : response_body,
            'tag' : tag,
            'phone': phone,
            'content': content,
            'sent': datetime.now(),
            'delivered': None
        }
        utils.app_db['sms_report'].insert_one(sms_detail)
        logger.debug(response_body)
    except:
        logger.warn(con)
        return None


def send_otp(phone):
    sms_enabled = True

    existing = utils.app_db['otps'].find_one({'phone': phone})
    otp = ''.join(random.choice(string.digits) for i in range(4))
    if not sms_enabled:
        otp = "1234"
        utils.app_db['otps'].remove({'phone': phone})
        existing = None
    if existing:
        if existing['expire'] > datetime.now():
            otp = existing['otp']
        else:
            utils.app_db['otps'].remove({'phone': phone})
            existing = None

    # send sms
    sms_body = "Login to Edufits app with this OTP pin : " + otp + ". It will be valid for 5 mins"
    if sms_enabled:
        send_sms(phone, sms_body, tag="otp")

    # save in db
    if existing:
        utils.app_db['otps'].update({'phone': phone}, {'$set': {'expire': datetime.now() + timedelta(0, 300)}})
    else:
        item = {'otp': otp, 'phone': phone, 'expire': datetime.now() + timedelta(0, 300)}
        utils.app_db['otps'].insert_one(item)


def send_otp_dummy(phone):
    utils.app_db['otps'].remove({'phone': phone})
    otp = "1234"
    item = {'otp': otp, 'phone': phone, 'expire': datetime.now() + timedelta(0, 300)}
    utils.app_db['otps'].insert_one(item)


def retrieve_otp(phone):
    return utils.app_db['otps'].find_one({'phone': phone})
