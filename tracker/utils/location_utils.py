def get_approx_distance(pt1, pt2):
    lat_diff = abs(pt1['lat'] - pt2['lat'])
    lng_diff = abs(pt1['lng'] - pt2['lng'])
    diff = abs(lat_diff - lng_diff)
    addition = lat_diff + lng_diff
    return (diff + (3 * addition)) * 25
