from pyramid_mailer.message import Message
import transaction
from voluptuous import Schema, Required
from tracker import utils
from tracker.utils.validations import validate_str
import logging

logger = logging.getLogger(__name__)

mail_schema_doc = {
    Required('subject'): validate_str,
    Required('recipients'): validate_str,
    Required('body'): validate_str,
    'sender': str,

}

validate_mail_data = Schema(mail_schema_doc)


def send_text_mail(subject=None, recipients=None, html=None, sender=None):
    if subject is None or subject == "":
        raise Exception("No subject provided. Subject is required")
    if html is None or html == "":
        raise Exception("No body provided. body is required")
    if recipients is None or recipients == []:
        raise Exception("No recipient provided. recipient is required")
    if sender is None or sender == "":
        sender = "Edufits Team"

    try:
        logger.debug({'msg': "sending mail", 'recipients': recipients, 'subject': subject})
        message = Message(subject=subject, recipients=recipients, html=html, sender=sender)
        utils.mailer.send(message)
        transaction.commit()
    except:
        return "Error in sending mail."
    return True