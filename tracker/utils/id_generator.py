import logging
from tracker import utils


logger = logging.getLogger(__name__)


def generate_next(name):
    """Generates an id number to be used at new object insertion in db
    TODO: make the generation random, currently it is just an incrementing counter

    :param name: The name of the counter, against which the id is to be generated
    :return: returns a new id number
    """
    collection = "auto_counters"
    res = utils.app_db[collection].find_one({'name': name})
    if res:
        counter = res['counter'] + 1
        utils.app_db[collection].update({'name': name}, {'$set': {'counter': counter}})
    else:
        counter = 1
        utils.app_db[collection].insert_one({'name': name, 'counter': counter})

    logger.debug({'counter_name': name, 'counter_value': counter})
    return counter
