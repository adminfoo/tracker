from datetime import time, datetime, timedelta
import json, logging
from httplib2 import Http
import re
import pdfkit
from pyramid.renderers import render
from pyramid.response import Response
from tracker.utils.validations import validate_int, validate_date_only, validate_object_id

logger = logging.getLogger(__name__)
h = Http()


def get_pdf_bytes(template_path, data, page_size="A4", orientation="Portrait"):
    html_str = render("../../" + template_path, data)
    options = {
        'page-size': page_size,
        'orientation': orientation,
        'margin-left': "0.75in",
        'margin-right': "0.75in",
        'margin-top': "0.75in",
        'margin-bottom': "0.75in"
    }
    return pdfkit.from_string(html_str, False, options=options)


def get_pdf_response(template_path, data, fileName, page_size="A4", orientation="Portrait"):
    pdf_bytes = get_pdf_bytes(template_path, data, page_size, orientation)
    # Let it be attachment; because it will prompt file download hence avoice blank page exp while
    # file is being downloaded.
    content_disposition = 'attachment; filename=' + fileName + ".pdf"
    return Response(pdf_bytes, content_type='application/pdf', content_disposition=content_disposition)


def get_single_dict(multi_dict):
    single_dict = {}
    for k, v in multi_dict.items():
        single_dict[k] = v
    return single_dict


def assign_data_type(key, value):
    try:
        if 'id' in key.lower():
            return int(value)
    except:
        pass
    return value


def make_filters_case_insensitive(filter_dict, searchable_keys):
    for key in searchable_keys:
        if key in filter_dict:
            filter_dict[key] = re.compile(filter_dict[key], re.IGNORECASE)


def assign_int_type(filter_dict, int_keys):
    for key in int_keys:
        if key in filter_dict:
            try:
                filter_dict[key] = int(filter_dict[key])
            except:
                try:
                    filter_dict[key] = {'$in': [int(x) for x in filter_dict[key]['$in']]}
                except:
                    pass


def get_int_fields(schema_dict):
    return [str(k) for k in schema_dict.keys() if schema_dict[k] == int or schema_dict[k] == validate_int]


def limit_projection(data_dict, fields_list):
    ret_dict = {}
    for f in fields_list:
        if f in data_dict:
            ret_dict[f] = data_dict[f]
    return ret_dict


def get_month_int(month_str):
    month_number_map = {"Jan": 1, "Feb": 2, "Mar": 3, "April": 4, "May": 5, "Jun": 6, "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12}
    return month_number_map[month_str]


def get_month_str(month_int):
    month_int_map = {1: "Jan", 2: "Feb", 3: "Mar", 4: "April", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec"}
    return month_int_map[month_int]


def http_request_json(url):
    try:
        headers, content = h.request(url)
        return json.loads(str(content, 'utf-8'))
    except Exception as ex:
        logger.exception("exception in http get")
        return


def hr_min_str_to_time(val):
    if isinstance(val, time):
        return val
    if isinstance(val, datetime):
        return val.time()
    hr = val['hr']
    if hr == 12:
        hr = 0
    min = val['min']
    if val['meridian'] == "PM":
        hr += 12
    return time(hr, min)


def get_projected_item(item, projection):
    """
    Returns a dict by filtering input item according to projection
    :param item: a dict
    :param projection: can be list or dict of field names, in case of dict, the values of keys must be True or False
    :return:
    """
    if isinstance(projection, list):
        projection = {f: True for f in projection}
    ret_item = {}
    for key, value in item.items():
        if key in projection and projection[key] is True:
            ret_item[key] = value
    return ret_item


def parse_from_to_date(query_params):
    """
    Parses the params in request and returns datetime objects for from_date and to_date

    Logic:
    Takes out the date part of fromDate and toDate
    fromDate and toDate are inclusive. So the time of fromDate is 00:00 and toDate is 23:59:59
    if "month" param is passed in query, then it takes precedence and fromDate and toDate are returned accordingly
    If "toDate" is not passed, then datetime.now() is returned
    If "fromDate" is not passed, it is assumed to be 14 days prior to "toDate"
    :param query_params: dict of all the query params of the request
    :return: from_date and to_date as datetime objects
    """
    if 'toDate' in query_params:
        to_date = datetime.combine(validate_date_only(query_params['toDate']), time(23, 59, 59))
    else:
        to_date = datetime.now()
    if 'fromDate' in query_params:
        from_date = datetime.combine(validate_date_only(query_params['fromDate']), time(0))
    else:
        from_date = datetime.combine(to_date.date(), time(0)) - timedelta(14, 0)

    if 'month' in query_params:
        month_no = int(query_params['month'])
        year_no = datetime.now().year
        from_date = datetime(year_no, month_no, 1)
        if from_date > datetime.now():
            year_no -= 1
            from_date = datetime(year_no, month_no, 1)
        month_no += 1
        if month_no == 13:
            month_no = 1
            year_no += 1
        to_date = datetime(year_no, month_no, 1) - timedelta(0, 1)
    return from_date, to_date


def get_mongo_id(item):
    if '_id' in item:
        return validate_object_id(item['_id'])
    elif 'id' in item:
        return validate_object_id(item['id'])
    else:
        return None


def secs_to_text(seconds):
    seconds = int(seconds)
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    days = int(hours / 24)
    if days > 1:
        hours %= 24
        return '{0} days {1} hours'.format(days, hours)
    elif days == 1:
        hours %= 24
        return '{0} day {1} hours'.format(days, hours)
    elif hours > 1:
        minutes %= 60
        return '{0} hours {1} mins'.format(hours, minutes)
    elif hours == 1:
        minutes %= 60
        return '{0} hour {1} mins'.format(hours, minutes)
    elif minutes > 1:
        seconds %= 60
        return '{0} mins {1} secs'.format(minutes, seconds)
    elif minutes == 1:
        seconds %= 60
        return '{0} min {1} secs'.format(minutes, seconds)
    else:
        return '{0} secs'.format(seconds)
