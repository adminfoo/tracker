import logging
import googlemaps

logger = logging.getLogger(__name__)
GMAP_API_KEY = "AIzaSyBnp1NGc103r6qT_w97OtSrjoMCJTo2y8w"

gmaps = googlemaps.Client(key=GMAP_API_KEY)


def get_snapped_points(dict_list):
    try:
        res = gmaps.snap_to_roads([{'latitude': float(p['lat']), 'longitude': float(p['lng'])} for p in dict_list], interpolate=True)
        logger.debug(res)
        return [{'lat': p['location']['latitude'], 'lng': p['location']['longitude'], 'type': "stop" if 'originalIndex' in p else "point"} for p in res]
    except:
        return [{'lat': p['lat'], 'lng': p['lng'], 'type': "stop"} for p in dict_list]
