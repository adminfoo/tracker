from tracker.utils import gps_db


def device_summary(device_id, start_time, end_time):
    cursor = gps_db[device_id].find({'insertedAt': {'$gte': start_time, '$lte': end_time}})
    # for record in cursor