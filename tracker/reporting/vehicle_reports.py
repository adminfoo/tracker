from datetime import datetime, timedelta, time
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.view import view_config
from tracker.rest_resource import object_store
from tracker.trips import get_vehicle_trips
from tracker.utils.common_utils import parse_from_to_date, get_mongo_id, get_pdf_response, secs_to_text
from tracker.utils.validations import validate_object_id


@view_config(route_name="vehicle_report_pdf", request_method="GET", permission="school")
def vehicle_report_pdf(request):
    from_date, to_date = parse_from_to_date({'fromDate': request.matchdict['from'], 'toDate': request.matchdict['to']})
    vehicle_id = validate_object_id(request.matchdict['vehicleId'])
    vehicle_details = object_store['vehicles'].detail(vehicle_id)
    if request.user['userType'] != "superadmin" and vehicle_details['schoolId'] != request.user['schoolId']:
        raise HTTPUnauthorized("You are not authorized to do this action")
    data = get_vehicle_summary(vehicle_details, from_date, to_date)
    data['distanceText'] = '{} Km'.format(int(data['distance']))
    data['runningDurationText'] = secs_to_text(data['runningDuration'])
    data['idleDurationText'] = secs_to_text(data['idleDuration'])
    data['acDurationText'] = secs_to_text(data['acDuration'])
    data['vehicle'] = vehicle_details
    data['fromDate'] = from_date.strftime("%d %b, %Y")
    data['toDate'] = to_date.strftime("%d %b, %Y")

    if request.params['reportType'] == "trips":
        return get_pdf_response('templates/pdf/vehicle_trips_report.pt', data, 'Vehcle trip list')
    if request.params['reportType'] == "daily":
        return get_pdf_response('templates/pdf/vehicle_daily_summary_report.pt', data, 'Vehicle daily summary')


@view_config(route_name="vehicle_summary", request_method="GET", permission="school", renderer='json')
def vehicle_summary_view(request):
    from_date, to_date = parse_from_to_date(request.params)

    if request.params['vehicleId'] == "All":
        all_vehicles = object_store['vehicles'].list_all(filters={'schoolId': request.user['schoolId']})
        ret_obj = {'distance': 0, 'runningDuration': 0, 'overspeeds': 0, 'idleDuration': 0,
                   'acDuration': 0, 'tripsCount': 0, 'days': 0}
        for vehicle in all_vehicles:
            vehicle_summary = get_vehicle_summary(vehicle, from_date, to_date)
            ret_obj['distance'] += vehicle_summary['distance']
            ret_obj['runningDuration'] += vehicle_summary['runningDuration']
            ret_obj['overspeeds'] += vehicle_summary['overspeeds']
            ret_obj['tripsCount'] += vehicle_summary['tripsCount']
            ret_obj['idleDuration'] += vehicle_summary['idleDuration']
            ret_obj['acDuration'] += vehicle_summary['acDuration']
            ret_obj['days'] += vehicle_summary['days']
        ret_obj['overspeedPercent'] = int(ret_obj['overspeeds'] * 100 / ret_obj['tripsCount']) \
            if ret_obj['tripsCount'] > 0 else 0
        ret_obj['dailyVehicleKm'] = int(ret_obj['distance'] / len(all_vehicles) / ret_obj['days']) \
            if ret_obj['days'] > 0 and len(all_vehicles) > 0 else 0
    else:
        vehicle_id = validate_object_id(request.params['vehicleId'])
        vehicle_details = object_store['vehicles'].detail(vehicle_id)
        if request.user['userType'] != "superadmin" and vehicle_details['schoolId'] != request.user['schoolId']:
            raise HTTPUnauthorized("You are not authorized to do this action")
        ret_obj = get_vehicle_summary(vehicle_details, from_date, to_date)
    add_formatted_text(ret_obj)
    return ret_obj


def add_formatted_text(item):
    if 'date' in item:
        item['dateText'] = item['date'].strftime("%a %d %b")
    if 'distance' in item:
        item['distanceText'] = '{} Km'.format(int(item['distance']))
    if 'runningDuration' in item:
        item['runningDurationText'] = secs_to_text(item['runningDuration'])
    if 'idleDuration' in item:
        item['idleDurationText'] = secs_to_text(item['idleDuration'])
    if 'acDuration' in item:
        item['acDurationText'] = secs_to_text(item['acDuration'])


def get_vehicle_summary(vehicle, start_time, end_time):
    vehicle_id = get_mongo_id(vehicle)
    vehicle_trips = get_vehicle_trips(vehicle_id, start_time, end_time)
    daily_list = []
    day_time = datetime.combine(start_time.date(), time(0))
    while day_time < end_time:
        distance = 0
        run_seconds = 0
        overspeeds = 0
        idle_seconds = 0
        ac_seconds = 0
        count = 0
        for trip in vehicle_trips:
            if trip['startTime'] < day_time or trip['startTime'] > day_time + timedelta(1):
                continue
            count += 1
            distance += trip['distance']
            run_seconds += trip['tripDuration'] if trip['tripDuration'] > 0 else 0
            overspeeds += 1 if trip['maxSpeed'] > 60 else 0
            idle_seconds += trip['idleDuration'] if 'idleDuration' in trip else 0
            ac_seconds += trip['acDuration'] if 'acDuration' in trip else 0
        overspeed_percent = int(overspeeds * 100 / count) if count > 0 else 0

        daily = {'distance': distance, 'runningDuration': run_seconds, 'acDuration': ac_seconds,
                 'idleDuration': idle_seconds, 'overspeedPercent': overspeed_percent,
                 'overspeeds': overspeeds, 'tripsCount': count, 'date': day_time}
        add_formatted_text(daily)
        if count > 0:
            daily_list.append(daily)
        day_time += timedelta(1)

    distance = 0
    run_seconds = 0
    overspeeds = 0
    idle_seconds = 0
    ac_seconds = 0
    for trip in vehicle_trips:
        distance += trip['distance']
        run_seconds += trip['tripDuration'] if trip['tripDuration'] > 0 else 0
        overspeeds += 1 if trip['maxSpeed'] > 60 else 0
        idle_seconds += trip['idleDuration'] if 'idleDuration' in trip else 0
        ac_seconds += trip['acDuration'] if 'acDuration' in trip else 0
    if vehicle['installTime'] > start_time:
        start_time = vehicle['installTime']
    if end_time > datetime.now():
        end_time = datetime.now()
    days = (end_time - start_time + timedelta(1) - timedelta(0, 1)).days
    avg_run = int(distance / days) if days > 0 else 0
    overspeed_percent = int(overspeeds * 100 / len(vehicle_trips)) if len(vehicle_trips) > 0 else 0

    return {'vehicleId': vehicle_id, 'distance': distance, 'runningDuration': run_seconds, 'acDuration': ac_seconds,
            'dailyVehicleKm': avg_run, 'idleDuration': idle_seconds, 'overspeedPercent': overspeed_percent,
            'overspeeds': overspeeds, 'tripsCount': len(vehicle_trips), 'days': days, 'trips': vehicle_trips,
            'daily': daily_list}
