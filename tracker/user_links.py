from datetime import datetime
import random
import string
from tracker import utils


def generate_install_link(phone):
    redirect_link = "https://play.google.com/store/apps/details?id=com.edufits.tracker"
    return generate_short_link(redirect_link, phone, "install")


def generate_help_link(phone):
    redirect_link = "http://tracker.edufits.com/how-to-use"
    return generate_short_link(redirect_link, phone, "help")


def generate_short_link(redirect_url, phone=None, tag=None):
    link_id = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(4)])
    while utils.app_db['user_links'].find_one({'linkId': link_id}) is not None:
        link_id = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(4)])
    obj = {
        'phone': phone,
        'link': redirect_url,
        'linkId': link_id,
        'count': 0,
        'tag': tag,
        'insertedAt': datetime.now()
    }
    utils.app_db['user_links'].insert(obj)
    return "http://edufits.com/link/" + link_id
