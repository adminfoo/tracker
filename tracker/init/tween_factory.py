import time
import logging
from pyramid.httpexceptions import HTTPBadRequest

logger = logging.getLogger(__name__)


def my_tween_factory(handler, registry):

    def my_tween(request):
        start = time.time()

        try:
            if request.content_length is not None and request.content_length > 0 and \
                    (request.content_type == "text/plain" or request.content_type == "text/json" or
                             request.content_type == "application/json"):
                data = request.json_body
                logger.debug({'post_data': data})
        except Exception as ex:
            logger.exception({'msg': "invalid json", 'body': request.body.decode('utf-8'), 'exception': ex})
            end = time.time()
            logger.info({'processing_time': end - start, 'path': request.path})
            raise HTTPBadRequest("json is invalid")

        response = handler(request)

        end = time.time()
        ip = request.headers['X-REAL-IP'] if 'X-REAL-IP' in request.headers else 'configure nginx'
        logger.info({'t': end - start, 'path': request.path, 'method': request.method,
                     'ip': ip, 'status': response.status_code})

        return response

    return my_tween