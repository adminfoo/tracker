from pyramid.interfaces import IAuthorizationPolicy
from zope.interface import implementer


@implementer(IAuthorizationPolicy)
class SimpleAuthorizationPolicy:

    def permits(self, context, principals, permission):
        if permission in principals or 'superadmin' in principals:
            return True
        return False
