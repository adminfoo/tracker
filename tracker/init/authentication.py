from pyramid.authentication import SessionAuthenticationPolicy
from zope.interface import implementer
from pyramid.interfaces import IAuthenticationPolicy


@implementer(IAuthenticationPolicy)
class SimpleAuthenticationPolicy(SessionAuthenticationPolicy):

    def effective_principals(self, request):
        if request.user and 'userType' in request.user:
            return request.user['userType']

        return []
