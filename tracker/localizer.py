import json
import os

file_name = os.path.dirname(__file__) + "/../static/locales/translation-en-US.json"

with open(file_name) as json_file:
    en_us_dict = json.load(json_file)


def _t(str_key, locale="en"):
    try:
        return en_us_dict[str_key]
    except:
        return str_key