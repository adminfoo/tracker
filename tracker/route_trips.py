from collections import defaultdict
from datetime import timedelta, datetime, time
from geopy.distance import vincenty
from pyramid.view import view_config
from voluptuous import Schema, REMOVE_EXTRA, Any
import logging
from tracker import utils
from tracker.rest_resource import Resource, object_store
from tracker.trips import find_points_in_interval
from tracker.utils.location_utils import get_approx_distance
from tracker.utils.validations import validate_str, validate_object_id, validate_datetime, validate_float, day_from_2015

logger = logging.getLogger(__name__)

collection = "route_trips"
validate_stop = Schema({
    'id': str,
    'name': str,
    'loc': {'type': "Point", 'coordinates': [float]},
    'lat': validate_float,
    'lng': validate_float,
    'schTime': datetime,
    'actTime': Any(None, datetime)
}, extra=REMOVE_EXTRA)
validate_point = Schema({
    'lat': validate_float,
    'lng': validate_float,
    'overspeed': bool,
    'speed': validate_float,
    'direction': validate_float
})
validation_schema = Schema({
    'schoolId': validate_object_id,
    'deviceId': validate_str,
    'vehicleId': validate_object_id,
    "vehicleRegistrationNo": str,

    "driverName": str,
    "driverLicenseNo": str,

    'schStartTime': datetime,
    'startTime': datetime,
    'endTime': Any(None, datetime),
    'day': day_from_2015,
    'tripDuration': validate_float,
    'overspeed': int,

    'routeId': validate_object_id,
    'routeName': str,
    'pickupOrDrop': Any("pickup", "drop", "drop0"),
    'stops': [validate_stop],
    'points': [validate_point],

    'latestDelay': validate_float,  # no of seconds
    'lastUpdated': validate_datetime
}, extra=REMOVE_EXTRA)

objects = Resource(collection, validation_schema)


@view_config(route_name="route_trips", renderer="json")
def get_list(request):
    query_dict = dict(request.GET)
    trip_date = validate_datetime(query_dict['date'])
    try:
        route_id = utils.app_db['routes'].find_one({'name': query_dict['routeName']})['_id']
    except:
        route_id = validate_object_id(query_dict['routeId'])
    route_details = object_store['routes'].detail(route_id)
    # return get_trip_details(route_id, trip_date, query_dict['type'])
    day_trips = get_one_day_trips(route_details, trip_date)
    ret_obj = {"pickup": None, 'drop0': None, 'drop': None}
    for trip in day_trips:
        if trip is not None:
            trip['points'] = find_points_in_interval(trip['deviceId'], trip['startTime'], trip['endTime'])
            ret_obj[trip['pickupOrDrop']] = trip
    return ret_obj


def get_one_day_trips(route_details, trip_date):
    day = day_from_2015(trip_date)
    route_id = route_details['_id'] if '_id' in route_details else route_details['id']
    existing = [x for x in utils.app_db[collection].find({'routeId': route_id, 'day': day}).sort([('startTime', 1)])]

    if len(existing) == 0:
        if len(route_details['vehicleDeviceId']) == 0:
            return [None, None]
        if is_holiday(trip_date):
            return [None, None]

    pickup_trip = None if len(existing) < 1 else existing[0]
    drop_trip = None if len(existing) < 2 else existing[1]

    return [pickup_trip, drop_trip]


def is_holiday(date):
    if utils.env == "dev":
        return False
    if datetime(2016, 5, 15) < date < datetime(2016, 7, 1):
        return True
    weekday = date.weekday()
    if weekday == 5 or weekday == 6:
        return True
    return False


speed_limit = 40


@view_config(route_name="route_mark_helper", request_method="GET", permission="superadmin", renderer="json")
def get_route_points_raw(request):
    route_id = request.matchdict['pk']
    # return {'groups': get_points_to_mark(route_id)}
    # return get_points_to_mark(route_id)[0]
    route_details = object_store['routes'].detail(route_id)
    vehicle_device_id = route_details['vehicleDeviceId']
    school_details = utils.app_db['schools'].find_one({'_id': request.user['schoolId']})
    school_geometry = school_details['loc']
    alll = get_round_trip_timings(vehicle_device_id, school_geometry, route_start_time=route_details['startTime'])
    if route_details['startTime'] is not None:
        return alll[0]
    else:
        alll = sorted(alll, key=lambda a: a['startTime'].time())
        return alll[route_details['sequenceNo']]


def get_round_trip_timings(vehicle_device_id, school_geometry, from_date=None, to_date=None, route_start_time=None):
    abc_time = datetime.now()
    if to_date is None:
        to_date = datetime.now()
    if from_date is None:
        from_date = datetime.combine((to_date - timedelta(10)).date(), time(0))
    if from_date < datetime(2015, 12, 9, 0, 0):
        from_date = datetime(2015, 12, 9, 0, 0)
    from_date = datetime(2015, 12, 13, 0, 0)  # TODO: temporary hack, gice data for 10 days
    to_date = datetime(2015, 12, 25, 0, 0)
    groups = get_pickup_drop_trippss(vehicle_device_id, from_date, to_date, school_geometry, route_start_time)
    logger.debug({'task': 'trip finding', 'time': (datetime.now() - abc_time).total_seconds()})
    abc_time = datetime.now()
    extract_pois(groups, vehicle_device_id)
    logger.debug({'task': "extracting pois", 'time': (datetime.now() - abc_time).total_seconds()})
    return groups


def get_pickup_drop_trippss(vehicle_device_id, from_date, to_date, school_geometry, route_start_time=None):
    # get all gps records when the vehicle is in school's vicinity
    # make the trips based on these records.
    # If there is some gap between subsequent records, that means a trip
    # Now extract regular daily trips

    ret_list = []
    filters = {
        'time': {'$gte': from_date, '$lte': to_date},
        'loc': {'$near': {'$geometry': school_geometry, '$maxDistance': 200}}
    }
    # TODO: limit this query. Do this action in batches, so that 10 day limit can be maximized
    cursor = utils.gps_db[vehicle_device_id].find(filters, {'time': True}).sort([('time', 1)])
    prev = {'time': datetime.now()}
    for pt in cursor:
        # considering minimum time for trip to be 20 mins
        # TODO: Is 20 min OK?
        if pt['time'] - prev['time'] > timedelta(0, 1200):
            ret_list.append({'startTime': prev['time'] - timedelta(0, 90),
                             'endTime': pt['time'] + timedelta(0, 90),
                             'timeOnly': (prev['time'] - timedelta(0, 90)).time(),
                             'vehicleDeviceId': vehicle_device_id})
        prev = pt
    cursor.close()
    logger.debug({'task': "finding trips", 'count': len(ret_list)})
    if route_start_time is None:
        start_hours = extract_regular_timings(ret_list)
    else:
        start_hours = [route_start_time, ]
    return group_by_hour(ret_list, start_hours)


def extract_pois(groups, vehicle_device_id):
    for v in groups:
        all_points = []
        abc_time = datetime.now()
        for trip in v['trips']:
            all_points += find_points_in_interval(vehicle_device_id, trip['startTime'], trip['endTime'])
            del trip['timeOnly']
        logger.debug({'task': 'fetching points',
                      'time': (datetime.now() - abc_time).total_seconds(),
                      'points': len(all_points)})
        abc_time = datetime.now()
        pois = group_by_vicinity_stops(all_points)
        logger.debug({'task': 'point grouping', 'time': (datetime.now() - abc_time).total_seconds()})
        v['pois'] = pois
        # all_points = get_vincenty_points(get_ww_points(vehicle_device_id,
        #                                                v['trips'][0]['startTime'], v['trips'][0]['endTime']))
        # for i in range(1, len(v['trips'])):
        #     new_points = get_vincenty_points(get_ww_points(vehicle_device_id, v['trips'][i]['startTime'],
        #                                                    v['trips'][i]['endTime']))
        #     all_points = merge_points_list(all_points, new_points)
        # v['points'] = [{'lat': p[0], 'lng': p[1]} for p in all_points]
        v['points'] = all_points


def group_by_vicinity_stops(points):
    abc_time = datetime.now()
    halts = [pt for pt in points if pt['speed'] == 0]
    if len(halts) == 0:
        return []
    sorted_list = [halts[0]]
    del halts[0]
    while len(halts) > 0:
        index = get_nearest_point_index(sorted_list[-1], halts)
        sorted_list.append(halts[index])
        del halts[index]
    logger.debug({'task': 'sorting halts', 'time': (datetime.now() - abc_time).total_seconds()})
    groups = []
    tmp = [sorted_list[0]]
    i = 1
    for i in range(1, len(sorted_list)):
        # dist = vincenty((sorted_list[i]['lat'], sorted_list[i]['lng']),
        #                 (sorted_list[i - 1]['lat'], sorted_list[i-1]['lng'])).kilometers
        # if dist < 0.03:
        if should_merge_point(sorted_list[i], sorted_list[i - 1]):
            if len(tmp) == 0:
                tmp.append(sorted_list[i - 1])
            tmp.append(sorted_list[i])
        else:
            if len(tmp) == 0:
                tmp.append(sorted_list[i - 1])
            item = {'points': tmp}
            date_dict = defaultdict(int)
            for t in tmp:
                date_dict[t['time'].day] += 10
            item['lat'] = tmp[int(len(tmp) / 2)]['lat']
            item['lng'] = tmp[int(len(tmp) / 2)]['lng']
            item['intervals'] = list(date_dict.values())
            item['precision'] = get_precision(item['points'], item['lat'], item['lng'])
            if len(date_dict) > 1:
                groups.append(item)
            tmp = []

    if len(tmp) == 0:
        tmp.append(sorted_list[i - 1])
    item = {'points': tmp}
    date_dict = defaultdict(int)
    for t in tmp:
        date_dict[t['time'].date()] += 10
    item['lat'] = tmp[int(len(tmp) / 2)]['lat']
    item['lng'] = tmp[int(len(tmp) / 2)]['lng']
    item['intervals'] = list(date_dict.values())
    item['precision'] = get_precision(item['points'], item['lat'], item['lng'])
    if len(date_dict) > 1:
        groups.append(item)
    return groups


def should_merge_point(pt1, pt2):
    lat_diff = abs(pt1['lat'] - pt2['lat'])
    lng_diff = abs(pt1['lng'] - pt2['lng'])
    diff = abs(lat_diff - lng_diff)
    addition = lat_diff + lng_diff
    if diff + (3 * addition) > 0.0012:
        return False
    else:
        return True


def get_precision(points, lat, lng):
    distance = 0
    for p in points:
        distance += vincenty((lat, lng), (p['lat'], p['lng'])).kilometers * 1000
    return distance / len(points)


def get_nearest_point_index(point, all_points):
    index = 0
    min_dist = 10000
    for i in range(1, len(all_points)):
        # d = vincenty((point['lat'], point['lng']), (all_points[i]['lat'], all_points[i]['lng'])).kilometers
        d = get_approx_distance(point, all_points[i])
        if d < min_dist:
            index = i
            min_dist = d
    return index


def group_by_hour(trips, start_timings):
    groups = []
    for t in start_timings:
        groups.append({'trips': [], 'startTime': t})
    for trip in trips:
        for i in range(len(start_timings)):
            if timedelta(0, -900) < get_time_diff(trip['startTime'], start_timings[i].time()) < timedelta(0, 900):
                groups[i]['trips'].append(trip)
    return groups


def extract_regular_timings(trips):
    if len(trips) <= 1:
        return []
    trips = sorted(trips, key=lambda t: t['startTime'].time())
    groups = []
    cur_group = [trips[0], ]
    for i in range(1, len(trips)):
        td = get_time_diff(trips[i]['startTime'], cur_group[0]['startTime'].time())
        if td <= timedelta(0, 1800):
            cur_group.append(trips[i])
        else:
            if len(cur_group) > 1:
                ftd = get_time_diff(cur_group[1]['startTime'], cur_group[0]['startTime'].time())
                ltd = get_time_diff(trips[i]['startTime'], cur_group[-1]['startTime'].time())
                if ftd > ltd:
                    del cur_group[0]
                    cur_group.append(trips[i])
                    continue
            groups.append(cur_group)
            cur_group = []
            cur_group.append(trips[i])
    if len(cur_group) > 0:
        groups.append(cur_group)
    max_len = max([len(g) for g in groups])
    return [(g[0]['startTime'] + timedelta(0, get_time_diff(g[-1]['startTime'], g[0]['startTime'].time()).total_seconds() / 2)) for g in groups if len(g) >= max_len * 0.6]


def get_time_diff(date_time_val, time_val):
    return date_time_val - datetime.combine(date_time_val.date(), time_val)


def points_in_vicinity(pt1, pt2, max_dist=0.5):
    tup1 = (pt1['coordinates'][1], pt1['coordinates'][0])
    tup2 = (pt2['coordinates'][1], pt2['coordinates'][0])
    if vincenty(tup1, tup2).kilometers < max_dist:
        return True
    return False


def merge_points_list(list1, list2):
    i = 0
    j = 0
    points = [list1[i], list2[j]]
    while i < len(list1) - 1 or j < len(list2) - 1:
        base_point = ((list1[i][0] + list2[j][0]) / 2, (list1[i][1] + list2[j][1]) / 2)
        if i >= len(list1) - 1:
            points.append(list2[j + 1])
            j += 1
            continue
        else:
            d1 = vincenty(list1[i + 1], base_point).kilometers
        if j >= len(list2) - 1:
            points.append(list1[i + 1])
            i += 1
            continue
        else:
            d2 = vincenty(list2[j + 1], base_point).kilometers
        if d1 < d2:
            points.append(list1[i + 1])
            i += 1
        else:
            points.append(list2[j + 1])
            j += 1
    return points

