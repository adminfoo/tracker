import logging
from pyramid.httpexceptions import HTTPBadRequest, HTTPConflict
from voluptuous import Schema
from bson import ObjectId
from tracker import utils
from tracker.utils.validations import validate_object_id

logger = logging.getLogger(__name__)
object_store = {}


class Resource:
    def __init__(self, collection_name, schema):
        """Initialize rest api resource

        'collection' is the name of mongo collection to use
        'default_list_projection' represents the data fields to show, when reading from db

        Note that the argument "schema" is a callable that returns validated data
        """
        self.collection = utils.app_db[collection_name]
        self.schema = schema
        object_store[collection_name] = self

    def list_all(self, filters=None, projection=None, return_count=False):
        """
        List all the items in the collection

        The projection of the data can be provided, else it will use the specified default projection
        :param filters:
        :param projection:
        :param return_count:
        :return:
        """
        if filters is None:
            filters = {}

        records = list(self.collection.find(filters, projection))

        for x in records:
            x['id'] = x['_id']
            del x['_id']

        if return_count:
            count = self.collection.count(filters)
            return records, count
        else:
            return records

    def is_duplicate(self, unique_field, data):
        if '_id' in data:
            _id = data['_id']
        elif 'id' in data:
            _id = validate_object_id(data['id'])
        else:
            _id = -1
        filters_dict = {'_id': {'$ne': _id}}
        if unique_field in data:  # mandatory fields are checked in schema validation, no need to check here
            filters_dict[unique_field] = data[unique_field]
        if self.collection.find_one(filters_dict) is None:
            return False
        else:
            return True

    def create(self, data, schema=None, unique_field=None):
        """
        Create an item in the db

        The data is validated against the schema provided in __init__
        You can also specify schema of the data for validation, to override the default
        :param data:
        :param schema:
        :param unique_field:
        :return:
        """
        if not schema:
            schema = self.schema

        try:
            data = schema(data)
        except Exception as ex:
            logger.debug('data:\n' + str(data) + '\n\n')
            logger.exception("Data validation failed")
            raise HTTPBadRequest("Data validation failed\n" + str(ex))

        if unique_field and self.is_duplicate(unique_field, data):
            raise HTTPConflict("Duplicate " + unique_field + ". It must be unique")

        result = self.collection.insert_one(data)
        return {'id': result.inserted_id}

    def detail(self, _id, projection=None):
        """
        Get details of a particular item from db
        :param _id:
        :param projection:
        :return:
        """
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)
        return self.find_one({'_id': _id}, projection)

    def find_one(self, filters, projection=None):
        """
        Get details of a particular item from db
        :param filters:
        :param projection:
        :return:
        """
        record = self.collection.find_one(filters, projection)
        if record is None:
            return
        record['id'] = record['_id']
        del record['_id']
        return record

    def edit(self, _id, data, schema=None, unique_field=None):
        """
        Update item(s) in the database

        By default extracts the item_id from the match dict in request
        Alternately you can also specify it
        :param _id:
        :param data:
        :param schema:
        :param unique_field:
        :return:
        """
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)

        if not schema:
            schema = self.schema

        schema_doc = schema.schema.copy()
        unnecessary_schema_keys = []
        for k in schema_doc.keys():
            if str(k) not in data:
                unnecessary_schema_keys.append(k)
        for k in unnecessary_schema_keys:
            del schema_doc[k]
        schema = Schema(schema_doc, required=schema.required, extra=schema.extra)

        if 'id' in data:
            del data['id']
        try:
            data = schema(data)
        except Exception as ex:
            logger.debug(data)
            logger.exception("Data validation failed")
            raise HTTPBadRequest("Data validation failed. " + str(ex))

        data['_id'] = _id

        if unique_field and self.is_duplicate(unique_field, data):
            raise HTTPConflict("Duplicate " + unique_field + ". It must be unique")

        result = self.collection.update_one({'_id': _id}, {'$set': data})
        return {'count': result.modified_count}

    def delete(self, _id, extra_filters=None):
        """

        :param _id:
        :param extra_filters:
        :return:
        """
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)
        filters = {'_id': _id}
        if extra_filters is not None:
            filters.update(extra_filters)
        result = self.collection.delete_one(filters)
        return {'count': result.deleted_count}
