import logging
from threading import Thread
import time
from datetime import datetime
from pyramid.view import view_config

from tracker.utils import app_db, gps_db
from tracker.utils.common_utils import hr_min_str_to_time
from tracker.utils.validations import day_from_2015, validate_object_id, validate_date_only
import eventlet

logger = logging.getLogger(__name__)


def relay_gps_data(src_device_id, target_device_id, past_start_time, past_end_time=None, current_start_time=None):
    conn = eventlet.connect(('localhost', 5016), )

    if current_start_time is None:
        current_start_time = datetime.now()
    time_offset = current_start_time - past_start_time
    items = fetch_some_items(src_device_id, past_start_time, past_end_time)
    if len(items) == 0:
        conn.close()
        return

    if '_id' in items[0]:
        del items[0]['_id']
    items[0]['time'] += time_offset
    bytes_to_send = get_bytes_to_send(items[0], time_offset, target_device_id)
    conn.sendall(bytes_to_send)

    if len(items) == 1:
        items = fetch_some_items(src_device_id, items[-1]['time'], past_end_time)
        if len(items) == 0:
            conn.close()
            return
    del items[0]

    wait_time = items[0]['insertedAt'] + time_offset - datetime.now()
    secs = wait_time.total_seconds()
    logger.debug('sleeping for' + str(secs) + 'secs')
    if secs > 0:
        time.sleep(secs)

    while datetime.now() - time_offset < past_end_time:
        if '_id' in items[0]:
            del items[0]['_id']
        items[0]['time'] += time_offset
        bytes_to_send = get_bytes_to_send(items[0], time_offset, target_device_id)
        conn.sendall(bytes_to_send)

        if len(items) == 1:
            items = fetch_some_items(src_device_id, items[-1]['time'], past_end_time)
            if len(items) == 0:
                conn.close()
                return
        del items[0]

        wait_time = items[0]['insertedAt'] + time_offset - datetime.now()
        secs = wait_time.total_seconds()
        logger.debug('sleeping for' + str(secs) + 'secs')
        if secs > 0:
            time.sleep(secs)


def fetch_some_items(src_device_id, past_start_time, end_time):
    filters = {'time': {'$gt': past_start_time, '$lte': end_time}}
    recs = [x for x in gps_db[src_device_id].find(filters).sort([('insertedAt', 1)]).limit(1000)]
    print('fetched', str(len(recs)), 'items')
    return recs


def get_bytes_to_send(item, time_offset, target_device_id):
    if '_id' in item:
        del item['_id']
    if 'insertedAt' in item:
        del item['insertedAt']
    item['time'] += time_offset
    item['time'] = int((item['time']).timestamp())
    item['deviceId'] = target_device_id
    content = bytes(str(item), encoding="ascii")
    length = len(content)
    content = bytes.fromhex("8989") + length.to_bytes(2, 'big') + content
    return content


def datetime_to_obj(dt):
    hr = dt.hour
    mn = dt.minute
    meridian = "AM"
    if hr >= 12:
        hr -= 12
        meridian = "PM"
    if hr == 0:
        hr = 12
    return {'hr': hr, 'min': mn, 'meridian': meridian}


def past_date_route_trips_for_vehicle(vehicle_id, past_date):
    cursor = app_db['route_trips'].find({'vehicleId': vehicle_id, 'day': day_from_2015(past_date)})
    return [t for t in cursor]


def edit_route_timings(route_details, offset_timedelta):
    vehicle_routes = [r for r in app_db['routes'].find({'vehicleId': route_details['vehicleId'], 'startTime': {'$exists': True}})]
    for route in vehicle_routes:
        route['startTime'] += offset_timedelta
        for s in route['stops']:
            if 'time' not in s:
                s['time'] = datetime.combine(datetime.now().date(), hr_min_str_to_time(s['timeObj']))
            s['time'] += offset_timedelta
        for s in route['stops']:
            new_time = datetime.combine(datetime.now().date(), hr_min_str_to_time(s['time'])) + offset_timedelta
            s['timeObj'] = datetime_to_obj(new_time)
        app_db['routes'].update({'_id': route['_id']}, {'$set': {'stops': route['stops'], 'startTime': route['startTime']}})
        logger.debug({'new_route_start_time': route['startTime'], 'route': route['name'], 'type': route['type']})


def clear_todays_route_data(route_details):
    # clear today's trip for this route
    _day = day_from_2015(datetime.now())
    app_db['route_trips'].remove({'routeId': route_details['_id'], 'day': _day})
    logger.debug("removed today's trips for route")

    # clear passenger alerts for today for this trip
    alert_start_time = datetime.combine(datetime.now().date(), datetime(2016, 1, 1, 0).time())
    alert_end_time = datetime.combine(datetime.now().date(), datetime(2016, 1, 1, 23, 59).time())
    app_db['pax_alerts'].remove({
        'routeId': route_details['_id'],
        'alertTime': {'$gte': alert_start_time, '$lte': alert_end_time}
    })


@view_config(route_name="replay_route", renderer="json")
def route_replay_view(request):
    logger.info(request.params)
    route_id = validate_object_id(request.params['routeId'])
    route_details = app_db['routes'].find_one({'_id': route_id})
    if 'isTest' not in route_details or route_details['isTest'] is False:
        return {'ok': False, 'error': "Can not replay non-test route"}
    logger.debug({'route_name': route_details['name']})

    past_date = validate_date_only(request.params['pastDate']) if 'pastDate' in request.params else datetime(2015, 12, 29).date()
    trip_type = request.params['tripType'] if 'tripType' in request.params else "pickup"
    source_route_id = validate_object_id(request.params['sourceRouteId']) if 'sourceRouteId' in request.params else route_details['_id']

    past_route_trip = app_db['route_trips'].find_one({'routeId': source_route_id, 'day': day_from_2015(past_date), 'pickupOrDrop': trip_type})
    if past_route_trip is None:
        return {'ok': False, 'error': "no trips on pastDate"}  # TODO: warning to admin
    offset_timedelta = datetime.now() - past_route_trip['startTime']
    clear_todays_route_data(route_details)
    edit_route_timings(route_details, offset_timedelta)

    src_device_id = past_route_trip['deviceId']
    logger.debug({'src_device_id': src_device_id})
    target_device_id = route_details['vehicleDeviceId']

    t = Thread(target=relay_gps_data, args=(src_device_id, target_device_id, past_route_trip['startTime'], past_route_trip['endTime']))
    t.start()
    return {'ok': True, 'msg': "started"}
