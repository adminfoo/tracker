from datetime import datetime, timedelta
import logging
from voluptuous import Schema

from pyramid.view import view_config

from tracker import utils
from tracker.rest_resource import Resource
from tracker.utils import gps_db
from tracker.utils.validations import validate_object_id

default_lat = 28.5323
default_lng = 77.99383

logger = logging.getLogger(__name__)

collection = "devices"
validation_schema = Schema({
    'deviceId': str,  # imei

    'schoolId': validate_object_id,
    'vehicleId': validate_object_id
})

objects = Resource(collection, validation_schema)


def get_current_location(device_id):
    if not device_id or len(device_id) == 0:
        return None
    # random.shuffle(speeds)
    # x = random.uniform(-0.01, 0.01)
    # y = random.uniform(-0.01, 0.01)
    #
    # return {"lat": x, "lng": y, "speed": speeds[0], "direction": random.randint(1, 359), 'time': datetime.now()}
    tmp = utils.gps_db[device_id].find().sort([('time', -1)]).limit(1)
    # tmp = utils.gps_db[device_id].find({"protocol_no": 18}).sort([('time', -1)]).limit(1)
    record = [x for x in tmp]
    if len(record) == 0:
        return None
    fields = ['lat', 'lng', 'speed', 'direction', 'time']
    ret_obj = {"lat": default_lat, "lng": default_lng, "speed": 0, "direction": 0, 'time': datetime.now()}
    for f in fields:
        if f in record[0]:
            ret_obj[f] = record[0][f]

    return ret_obj


def get_current_status(device_id):
    if not device_id or len(device_id) == 0:
        return None

    tmp = list(utils.gps_db[device_id].find().sort([('time', -1)]).limit(1))
    # tmp = utils.gps_db[device_id].find({"protocol_no": 18}).sort([('time', -1)]).limit(1)
    if len(tmp) == 0:
        return {'reachable': False, 'ignition': False, 'moving': False, 'ac': False, 'lat': default_lat, 'lng': default_lng}
    record = tmp[0]
    fields = ['lat', 'lng', 'speed', 'direction', 'time', 'ignition', 'ac']
    ret_obj = {"lat": default_lat, "lng": default_lng, "speed": 0, "direction": 0, 'time': datetime.now()}
    for f in fields:
        if f in record:
            ret_obj[f] = record[f]
    if 'status' in record and record['status'] is not None:
        for f in fields:
            if f in record['status']:
                ret_obj[f] = record['status'][f]
    if 'io_data' in record and record['io_data'] is not None:
        for f in fields:
            if f in record['io_data']:
                ret_obj[f] = record['io_data'][f]
    if 'ignition' not in ret_obj:
        ret_obj['ignition'] = False
    if 'ac' not in ret_obj:
        ret_obj['ac'] = False
    if 'speed' in record and record['speed'] > 0:
        ret_obj['moving'] = True
    else:
        ret_obj['moving'] = False
    if datetime.now() - record['time'] > timedelta(0, 300):
        ret_obj['reachable'] = False
        ret_obj['ac'] = False
        ret_obj['ignition'] = False
        ret_obj['moving'] = False
        ret_obj['speed'] = 0
    else:
        ret_obj['reachable'] = True

    return ret_obj


@view_config(route_name="devices", request_method="GET", permission="superadmin", renderer='json')
def all_devices_view(request):
    return gps_db.collection_names()


@view_config(route_name="device_details", request_method="GET", permission="superadmin", renderer='json')
def device_records_view(request):
    device_id = request.matchdict['pk']
    records = [x for x in gps_db[device_id].find().sort([('_id', -1)]).limit(3)]
    return records


@view_config(route_name="device_records", request_method="GET", permission="superadmin", renderer='json')
def device_records_view(request):
    device_id = request.matchdict['pk']
    filters = request.filters or {}
    projection = request.projection
    skip = int(request.params['skip']) if 'skip' in request.params else 0
    limit = int(request.params['limit']) if 'limit' in request.params else 100
    records = [x for x in gps_db[device_id].find(filters, projection).sort([('_id', -1)]).skip(skip).limit(limit)]
    return records
