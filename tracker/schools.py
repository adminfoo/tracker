"""
This module contains all about school objects and school onboarding
Also contains the school summary
"""
from collections import defaultdict
from datetime import datetime, time, timedelta
import logging
import random
import string
from multiprocessing import Process
from pyramid.httpexceptions import HTTPConflict, HTTPBadRequest, HTTPNotFound, HTTPFound
from pyramid.security import remember
from pyramid.view import view_config
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from voluptuous import Schema, REMOVE_EXTRA, Required, Any
from tracker import utils
from tracker.rest_resource import Resource
from tracker.routes import route_stop_count_in_school
from tracker.user_links import generate_install_link, generate_help_link
from tracker.utils.email_utils import send_text_mail
from tracker.utils.sms_utils import send_sms
from tracker.utils.validations import validate_str, validate_email, validate_phone, validate_float, validate_object_id, \
    validate_int, validate_datetime
from tracker.views import get_password_reset_link

logger = logging.getLogger(__name__)

collection = 'schools'
schema_doc = {
    'name': validate_str,
    'phone': validate_phone,
    'email': validate_email,

    'username': validate_str,
    'password': validate_str,

    'city': validate_str,
    'country': str,

    'lat': validate_float,
    'lng': validate_float,
    'loc': {'type': "Point", 'coordinates': [validate_float]},

    Required('geoFenceEnabled', default=False): bool,
    Required('alertPhones', default=[]): [validate_phone],
    Required('paxAlertsEnabled', default=True): bool,
    Required('subscriptionFee', default=100) : validate_int,

    Required('gpsUpDate', default=None): Any(None, validate_datetime)
}

objects = Resource(collection, Schema(schema_doc, required=True, extra=REMOVE_EXTRA))


###############################################################################
# Views
###############################################################################
@view_config(route_name="select_school", request_method="GET", permission="superadmin", renderer='json')
def select_school_view(request):
    try:
        school_id = validate_object_id(request.matchdict['pk'])
        school_data = objects.detail(school_id)
        remember_data = {
            'username': request.user['username'],
            'userType': request.user['userType'],
            '_id': request.user['_id'],
            'schoolId': school_id,
            'schoolName': school_data['name']
        }
        remember(request, remember_data)
    except:
        remember_data = {
            'username': request.user['username'],
            'userType': request.user['userType'],
            '_id': request.user['_id']
        }
        remember(request, remember_data)

    raise HTTPFound("/")


@view_config(route_name="schools", request_method="GET", permission="superadmin", renderer='json')
def get_schools_list(request):
    all_items = objects.list_all(filters = {}, projection={'password': False})
    for item in all_items:
        item['url'] = '/selectSchool/{0}'.format(item['id'])
    return all_items


@view_config(route_name="schools", request_method="POST", permission="superadmin", renderer='json')
def create_school(request):
    data = request.json_body

    if 'username' not in data:
        data['username'] = generate_username(data['name'])
    elif not is_unique_username(data['username']):
        raise HTTPConflict("this username is already taken")

    if 'password' not in data:
        data['password'] = ''.join(random.choice(string.ascii_lowercase) for i in range(0, 6))

    raw_pwd = data['password']
    data['password'] = encrypt_password(data['password'])

    if 'lat' not in data:
        data['lat'] = 28.4
    if 'lng' not in data:
        data['lng'] = 77.1
    data['loc'] = {'type': "Point", 'coordinates': [data['lng'], data['lat']]}

    ret_obj = objects.create(data)
    logger.info({'msg': "school create", 'username': data['username'], 'password': raw_pwd, 'ret_obj': ret_obj})

    # email password set link
    send_password_set_link(data['username'], data['name'], request.domain)

    return ret_obj


@view_config(route_name="school_details", request_method="GET", permission="superadmin", renderer='json')
def detail(request):
    item_id = request.matchdict['pk']
    details = objects.detail(item_id)
    if details is None:
        raise HTTPNotFound("No school found")
    return details


@view_config(route_name="school_details", request_method="PATCH", permission="superadmin", renderer='json')
def edit(request):
    item_id = request.matchdict['pk']
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)

    if 'password' in data:
        del data['password']

    if 'lat' in data and 'lng' in data:
        data['loc'] = {'type': "Point", 'coordinates': [data['lng'], data['lat']]}

    return objects.edit(item_id, data)


@view_config(route_name="school_summary", request_method="GET", permission="school", renderer='json')
def school_summary_view(request):
    """
    This view caters to the data required at the dashboard of any school
    Gives overview stats for the school
    :param request:
    :return:
    """
    school_id = request.user['schoolId']

    vehicle_count = utils.app_db['vehicles'].count({'schoolId': school_id})
    moving_vehicle_count = utils.app_db['vehicles'].count({'schoolId': school_id, 'status.moving': True})
    unreachable_vehicle_count = utils.app_db['vehicles'].count({'schoolId': school_id, 'status.reachable': False})

    today_date = datetime.combine(datetime.now().date(), time(0))
    yesterday_date = today_date - timedelta(1)
    today_trips = list(utils.app_db['trips'].find({'schoolId': school_id, 'startTime': {'$gte': today_date}}))
    yesterday_trips = list(utils.app_db['trips'].find({'schoolId': school_id, 'startTime': {'$gte': yesterday_date, '$lt': today_date}}))

    route_count, stop_count = route_stop_count_in_school(school_id)
    student_count = utils.app_db['passengers'].count({'schoolId': school_id, 'type': "student"})
    staff_count = utils.app_db['passengers'].count({'schoolId': school_id, 'type': "staff"})
    logger.debug({'vehicles': vehicle_count, 'routes': route_count, 'stops': stop_count,
                  'students': student_count, 'staff': staff_count})
    # overspeed_incidents = overspeed.get_overspeed_incidents(school_id, datetime.now())
    # alert_count = len(overspeed_incidents)
    # logger.debug({'alerts': alert_count})
    # avg_severity = sum([i['severity'] for i in overspeed_incidents]) / alert_count if alert_count > 0 else 0

    overspeed_today = len([1 for t in today_trips if t['maxSpeed'] > t['vehicleSpeedLimit']])
    overspeed_yesterday = len([1 for t in yesterday_trips if t['maxSpeed'] > t['vehicleSpeedLimit']])

    all_trips = yesterday_trips + today_trips
    vehicle_wise = defaultdict(list)
    for t in all_trips:
        vehicle_wise[t['vehicleRegistrationNo']].append(t)
    for reg_no in vehicle_wise.keys():
        vehicle_wise[reg_no] = {
            'registrationNo': reg_no,
            'maxSpeed': max(vehicle_wise[reg_no], key=lambda t: t['maxSpeed'])['maxSpeed'],
            'overspeedPercent': int(len([1 for t in vehicle_wise[reg_no] if t['maxSpeed'] > t['vehicleSpeedLimit']]) * 100 / len(vehicle_wise[reg_no]))
        }

    overspeed_list = list(sorted(vehicle_wise.values(), key=lambda i: i['overspeedPercent'], reverse=True))

    ret_obj = {
        'vehicleCount': vehicle_count,
        'vehicleMovingCount': moving_vehicle_count,
        'vehicleUnreachableCount': unreachable_vehicle_count,

        'vehicleRunToday': int(sum([t['distance'] for t in today_trips])),
        'vehicleRunYesterday': int(sum([t['distance'] for t in yesterday_trips])),

        'vehicleAvgIdleTimeToday': int(sum([t['idleDuration'] for t in today_trips])),
        'vehicleAvgIdleTimeYesterday': int(sum([t['idleDuration'] for t in yesterday_trips])),

        'routeCount': route_count,
        'stopCount': stop_count,
        'studentCount': student_count,
        'staffCount': staff_count,

        # 'alertCount': alert_count,
        # 'alertSeverity': avg_severity,
        # 'overspeedIncidents': overspeed.consolidate_incidents(overspeed_incidents),

        'overspeedPercentToday': int(overspeed_today * 100 / len(today_trips)) if len(today_trips) > 0 else 0,
        'overspeedPercentYesterday': int(overspeed_yesterday * 100 / len(yesterday_trips)) if len(yesterday_trips) > 0 else 0,
        'overspeedList': overspeed_list
    }
    return ret_obj


@view_config(route_name="sms_onboard", request_method="GET", permission="superadmin", renderer="json")
def send_sms_onboard(request):
    school_id = validate_object_id(dict(request.GET)['schoolId'])
    school_details = utils.app_db['schools'].find_one({'_id': school_id})
    all_paxes = [p for p in utils.app_db['passengers'].find({'schoolId': school_id})]

    send_dict = {}
    admin_phones = ["8800443925", "8800454534"]
    for pax in all_paxes:
        admin_phone = random.choice(admin_phones)
        if pax['phone'] not in send_dict:
            if pax['type'] == "student":
                name = pax['guardianName'] if 'guardianName' in pax else ""
                hello = ("Hello " + name).strip()
                content = hello + ", Ensure your child's safety. Track school bus in android app. To install click " + \
                                  generate_install_link(pax['phone']) + " For help call " + admin_phone + \
                                  " or click " + generate_help_link(pax['phone'])
            else:
                name = pax['name']
                content = "Hello " + pax['name'] + ", track school bus in android app. To install click " + \
                          generate_install_link(pax['phone']) + " For help call " + admin_phone + " or click " + \
                          generate_help_link(pax['phone'])
            send_dict[pax['phone']] = {'content': content, 'name': name}
    logger.debug({'school_id': school_id, 'school': school_details['name'], 'passengers': len(all_paxes),
                  'phones': len(send_dict)})
    for phone, obj in send_dict.items():
        # if phone in admin_phones:
        send_sms(phone, obj['content'], tag="onboard")
        onboard_tracker = {
            'phone': phone,
            'name': obj['name'],
            'schoolId': school_id,
            'schoolName': school_details['name'],
            'smsSent': None,
            'smsDelivered': None,
            'installLink': None,
            'loginAttempt': None,
            'loginSuccess': None,
            'helpLink': None
        }
        utils.app_db['onboarding_reports'].insert(onboard_tracker)
    return send_dict


@view_config(route_name="onboard_resend", request_method="GET", permission="superadmin", renderer="json")
def resend_onboarding_single(request):
    phone = request.GET['phone']
    onboard_obj = utils.app_db['onboarding_reports'].find_one({'phone': phone})
    sms_obj = utils.app_db['sms_report'].find_one({'phone': "91" + phone, 'tag': "onboard"})
    send_sms(phone, sms_obj['content'], tag="reonboard")
    utils.app_db['onboarding_reports'].update({'_id': onboard_obj['_id']}, {'$set': {'resentTime': datetime.now()}})
    logger.info({'msg': "resend onboarding", 'onboard': onboard_obj, 'sms_obj': sms_obj})
    return {'onboard_obj': onboard_obj, 'sms_obj': sms_obj}


@view_config(route_name="onboard_summary", request_method="GET", permission="superadmin", renderer="json")
def get_onboarding_summary(request):
    query_dict = dict(request.GET)
    school_id = validate_object_id(query_dict['schoolId'])
    objs = [o for o in utils.app_db['onboarding_reports'].find({'schoolId': school_id})]

    logger.debug({'total': len(objs)})

    ret_obj = {
        'total': len(objs),
        'smsSent': 0,
        'smsDelivered': 0,
        'installLink': 0,
        'loginAttempt': 0,
        'loginSuccess': 0,
        'helpLink': 0
    }

    for obj in objs:
        update_onboarding_object(obj)
        if obj['smsSent'] is not None:
            ret_obj['smsSent'] += 1
        if obj['smsDelivered'] is not None:
            ret_obj['smsDelivered'] += 1
        if obj['installLink'] is not None:
            ret_obj['installLink'] += 1
        if obj['loginAttempt'] is not None:
            ret_obj['loginAttempt'] += 1
        if obj['loginSuccess'] is not None:
            ret_obj['loginSuccess'] += 1
        if obj['helpLink'] is not None:
            ret_obj['helpLink'] += 1

    if 'type' in query_dict:
        if query_dict['type'] == "smsSent":
            ret_obj['objects'] = [o for o in objs if o['smsSent'] is None]
        if query_dict['type'] == "smsDelivered":
            ret_obj['objects'] = [o for o in objs if o['smsDelivered'] is None]
        if query_dict['type'] == "installLink":
            ret_obj['objects'] = [o for o in objs if o['installLink'] is None]
        if query_dict['type'] == "loginAttempt":
            ret_obj['objects'] = [o for o in objs if o['loginAttempt'] is None]
        if query_dict['type'] == "loginSuccess":
            ret_obj['objects'] = [o for o in objs if o['loginSuccess'] is None]
        if query_dict['type'] == "helpLink":
            ret_obj['objects'] = [o for o in objs if o['helpLink'] is not None]
        if query_dict['type'] == "all":
            ret_obj['objects'] = objs

    return ret_obj


@view_config(route_name="beta_sms", request_method="GET", permission="superadmin", renderer="json")
def send_beta_sms(request):
    valid_users = [u for u in utils.app_db['users'].find({'endDate': {'$ne': None}})]
    phone_dict = {u['phone']: {'name': u['name']} for u in valid_users}
    already_done = [o['phone'] for o in utils.app_db['sms_report'].find({'tag': "beta"})]
    admin_phones = ["8800443925", "9873715477"]
    ret_obj = []
    for phone, obj in phone_dict.items():
        if "91" + phone not in already_done:
            admin_phone = random.choice(admin_phones)
            content = "Hi " + obj['name'] + ", Thanks for using Edufits app. The app is currently in beta mode. " \
                                            "It means that there can be some problems. Please look out for problems " \
                                            "and inform us so that we can fix it. Call " + \
                      admin_phone + " or give feedback within app. Thanks"
            send_sms(phone, content, tag="beta")
            ret_obj.append(phone)
    return ret_obj


###############################################################################
# Helper functions
###############################################################################
def encrypt_password(pwd_str):
    pwd_str = pwd_str.strip()
    if len(pwd_str) < 6:
        raise HTTPBadRequest("password too short")
    return pbkdf2_sha512.encrypt(pwd_str)


def generate_username(seed_str):
    seed_str = seed_str.split()[0]
    if len(seed_str) < 6:
        shortage = 6 - len(seed_str)
        seed_str += ''.join(random.choice(string.ascii_lowercase) for i in range(shortage))
    seed_str = seed_str.lower()
    username = seed_str
    suffix = 0
    while not is_unique_username(username):
        suffix += 1
        username = seed_str + str(suffix)
    return username


def is_unique_username(username, internal_id=-1):
    user = utils.app_db['users'].find_one({'username': username, '_id': {'$ne': internal_id}}, {'_id': True})
    if user is not None:
        return False
    return True


def send_password_set_link(username, name, domain):
    user = utils.app_db['schools'].find_one({'username': username}, {'username': True, 'email': True, 'name': True})

    reset_link = get_password_reset_link(user['_id'], domain)

    # send email
    recipients = [user['email']]
    sub = "Welcome to Edufits"
    html = "Hi " + name + ",<br><br>You now have access to the dashboard to track your vehicles.<br>" + \
           "We have created an account for you with username: " + username + \
           ".Click on the below link to set your password.<br><br>" + reset_link + "<br><br>" + \
           "This link will expire after 2 hours.<br>Feel free to enquire further at edufits@gmail.com<br><br>" + \
           "Regards,<br>EduFits Team"
    sender = "EduFits team"

    p = Process(target=send_text_mail, args=(sub, recipients, html, sender))
    p.daemon = True
    p.start()


def update_onboarding_object(obj):
    should_update = False
    if obj['smsSent'] is None or obj['smsDelivered'] is None:
        sms_rep = utils.app_db['sms_report'].find_one({'phone': "91" + obj['phone'], 'tag': "onboard"})
        if sms_rep is None:
            return
        if obj['smsSent'] != sms_rep['sent']:
            should_update = True
        obj['smsSent'] = sms_rep['sent']
        if obj['smsDelivered'] != sms_rep['delivered']:
            should_update = True
        obj['smsDelivered'] = sms_rep['delivered']

    if obj['installLink'] is None and obj['smsSent'] is not None:
        # update status of install link click
        link_obj = utils.app_db['user_links'].find_one({'phone': obj['phone'], 'tag': "install"})
        if link_obj['count'] > 0:
            obj['installLink'] = link_obj['lastVisited']
            should_update = True

    if obj['helpLink'] is None and obj['smsSent'] is not None:
        # update status of install link click
        link_obj = utils.app_db['user_links'].find_one({'phone': obj['phone'], 'tag': "help"})
        if link_obj['count'] > 0:
            obj['helpLink'] = link_obj['lastVisited']
            should_update = True

    if obj['loginAttempt'] is None:
        otp_obj = utils.app_db['sms_report'].find_one({'phone': "91" + obj['phone'], 'tag': "otp"})
        if otp_obj is not None:
            obj['loginAttempt'] = otp_obj['sent']
            should_update = True

    if obj['loginSuccess'] is None:
        user_obj = utils.app_db['users'].find_one({'phone': obj['phone']})
        if user_obj['endDate'] is not None:
            obj['loginSuccess'] = True
            should_update = True

    if should_update:
        utils.app_db['onboarding_reports'].update({'_id': obj['_id']}, obj)

    return obj
