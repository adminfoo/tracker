def routing_table(config):
    # Login stuff
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('dashboard', '/')
    config.add_route('username_change', '/username')
    config.add_route('password_change', '/password')
    config.add_route('forgot_password', '/forgot-password')
    config.add_route('reset_password', '/reset-password/{pk}')

    # Superadmin access
    config.add_route('schools', '/schools')
    config.add_route('school_details', '/schools/{pk}')
    config.add_route('tasks', '/superadminTasks')
    config.add_route('subscription_plans', '/subscriptionPlans')
    config.add_route('subscription_plan_details', '/subscriptionPlans/{pk}')

    # School Admin stuff
    config.add_route('vehicle_staff', '/vehicleStaff')
    config.add_route('vehicle_staff_details', '/vehicleStaff/{pk}')
    config.add_route('passengers', '/passengers')
    config.add_route('passenger_details', '/passengers/{pk}')
    config.add_route('routes', '/transportRoutes')
    config.add_route('route_details', '/transportRoutes/{pk}')
    config.add_route('vehicles', '/transportVehicles')
    config.add_route('vehicle_details', '/transportVehicles/{pk}')

    config.add_route('vehicle_trip', '/vehicleTrips/{pk}')
    config.add_route('vehicle_trips', '/vehicleTrips')
    config.add_route('vehicle_live', '/vechicleLive')
    config.add_route('temp', '/temp')

    config.add_route('users', '/users')
    config.add_route('user_details', '/users/{pk}')

    config.add_route('route_trips', '/routeTrips')

    #
    config.add_route('alerts', '/alerts')
    config.add_route('overspeed_incidents', '/overspeedIncidents')

    config.add_route('school_summary', '/schoolSummary')
    config.add_route('select_school', '/selectSchool/{pk}')

    config.add_route('devices_summary', '/deviceSummary')

    config.add_route('route_mark_helper', '/routeMarkHelper/{pk}')

    config.add_route('payments', '/payments')

    config.add_route('redirect_to_payment_gateway', '/redirect-to-payment')

    config.add_route('transaction_result', '/transaction-result')

    config.add_route('passengers_add_all', '/paxAddAll')

    config.add_route('how_to_use', '/how-to-use')

    config.add_route('install_application', '/install')

    config.add_route('sms_delivery', '/sms-delivery')

    config.add_route('sms_reports', '/smsReports')
    config.add_route('sms_onboard', '/smsOnboard')
    config.add_route('onboard_summary', '/onboardingSummary')
    config.add_route('beta_sms', '/betaSms')

    config.add_route('alert_accuracy_report', '/alertAccuracy')

    config.add_route('all_routes_view', '/allRoutesView')
    config.add_route('onboard_resend', '/onboardResend')

    config.add_route('test_alerts', '/testAlerts')

    # For android app
    config.add_route('android_app_info', '/appInfo')
    config.add_route('init_register', '/initRegister')
    config.add_route('finish_register', '/finishRegister')
    config.add_route('startup_info', '/startupInfo')
    config.add_route('pax_tracking_info', '/passengerTrackingInfo')
    config.add_route('user_location', '/userLocation')
    config.add_route('get_alert_time', '/etaAlert')
    config.add_route('mark_alert', '/confirmAlert')
    config.add_route('feedbacks', '/feedbacks')
    config.add_route('feedback_details', '/feedbacks/{pk}')
    config.add_route('passenger_get_stops', '/routeStops')
    config.add_route('passenger_change_stop', '/changeStop')
    config.add_route('save_settings', '/saveSettings')

    # For demo
    config.add_route('demo_instruction', '/demo-instruction')
    config.add_route('register_demo_phone', '/registerDemo')

    config.add_route('replay_route', '/replayRoute')

    config.add_route('devices', '/devices')
    config.add_route('device_details', '/devices/{pk}')
    config.add_route('device_records', '/devices/{pk}/records')

    config.add_route('drivers', '/drivers')
    config.add_route('driver_details', '/drivers/{pk}')
    config.add_route('driver_otp', '/driverOtp')
    config.add_route('driver_login', '/driverLogin')
    config.add_route('driver_start_info', '/driverStartInfo')

    config.add_route('bus_attendance', '/busAttendance')

    config.add_route('bulk_sms', '/bulkSms')

    config.add_route('vehicle_summary', '/vehicleSummary')

    config.add_route('vehicle_report_pdf', '/pdf/report/vehicleSummary/{vehicleId}/{from}-{to}')

    config.scan()
