import ast
from datetime import datetime, timedelta
import json
from multiprocessing import Process
import string
import os
from bson import ObjectId
from passlib.handlers.pbkdf2 import pbkdf2_sha512
from pyramid.httpexceptions import HTTPFound, HTTPBadRequest, HTTPUnauthorized, HTTPConflict, HTTPNotFound, \
    HTTPException, HTTPOk
from pyramid.renderers import render
from pyramid.response import Response
from pyramid.security import remember, forget
from pyramid.view import view_config
import random
from tracker import utils
import logging
from tracker.utils.email_utils import send_text_mail
from tracker.utils.validations import validate_datetime


logger = logging.getLogger(__name__)


file_name = os.path.dirname(__file__) + "/../static/js/cachebusters.json"

with open(file_name) as json_file:
    static_hash_map = json.load(json_file)


def static_url(key):
    ret = utils.static_path + '/' + key
    hash_key = ret.replace('/static/', "")
    random_hash = ''.join([random.choice(string.ascii_lowercase) for i in range(10)])
    _hash = static_hash_map[hash_key] if hash_key in static_hash_map else random_hash
    return ret + "?hash=" + _hash


@view_config(route_name='dashboard', renderer='../templates/dashboard.pt')
def dashboard_view(request):
    if request.user['userType'] == "anon":
        url = "/login"
        raise HTTPFound(location=url)

    return {'user': request.user, 'permissions': {'transport': request.user['userType']}, 'staticHashMap': static_url}


@view_config(route_name='login', request_method='GET', renderer='../templates/login.pt')
def login_view(request):
    query_dict = dict(request.GET)

    redirect_url = "/"
    if 'redirectUrl' in query_dict:
        redirect_url = query_dict['redirectUrl']

    if request.user['userType'] != "anon":
        raise HTTPFound(location=redirect_url)

    ret_dict = {'redirectUrl': redirect_url, 'error': None}
    if 'invalid' in query_dict:
        ret_dict['error'] = "Invalid username/password combination"
    return ret_dict


@view_config(route_name='logout', request_method='GET', renderer='json')
def logout_view(request):
    if request.user:
        forget(request)
        logger.debug({'msg': "logged out", 'username': request.user['username']})

    if request.from_app is False:
        return Response(render("../templates/login.pt", {'redirectUrl': "/", 'error': None}))
    return {'success': True}


def check_pwd(pwd_str, correct_hash):
    return pbkdf2_sha512.verify(pwd_str, correct_hash)


@view_config(route_name='password_change', request_method='POST', renderer='json')
def change_password(request):
    if request.user['userType'] == "anon":
        raise HTTPUnauthorized("Please login")

    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    if not data or 'oldPassword' not in data or 'newPassword' not in data:
        raise HTTPBadRequest('oldPassword and newPassword required')
    old_password = data['oldPassword']
    new_password = data['newPassword']
    correct_password = utils.app_db['schools'].find_one({'username': request.user['username']},
                                                        {'password': True})['password']
    if check_pwd(old_password, correct_password):
        utils.app_db['schools'].update_one({'username': request.user['username']},
                                           {'$set': {'password': pbkdf2_sha512.encrypt(new_password)}})
        forget(request)
        logger.info({'msg': "password changed", 'username': request.user['username']})
        return {'msg': "Successfully changed password. Please login again"}
    else:
        logger.debug({'msg': "unsuccessful password change attempt", 'data': data})
        raise HTTPBadRequest("Wrong password")


@view_config(route_name='username_change', request_method='POST', renderer='json')
def change_username(request):
    if request.user['userType'] != "anon":
        raise HTTPUnauthorized("Please login")

    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    if not data or 'newUsername' not in data or 'password' not in data:
        raise HTTPBadRequest('newUsername and password required')
    password = data['password']
    new_uname = data['newUsername']
    from tracker.schools import is_unique_username
    if not is_unique_username(new_uname, request.user['id']):
        raise HTTPConflict("This username is taken. Please choose another username")
    correct_password = utils.app_db['schools'].find_one({'username': request.user['username']},
                                                        {'password': True})['password']
    if check_pwd(password, correct_password):
        utils.app_db['schools'].update_one({'username': request.user['username']}, {'$set': {'username': new_uname}})
        forget(request)
        logger.info({'msg': "username changed", 'old_username': request.user['username'], "new_username": new_uname})
        return {'msg': "Successfully changed username."}
    else:
        logger.debug({'msg': "unsuccessful username change attempt", 'data': data})
        raise HTTPUnauthorized("Wrong password")


@view_config(route_name="forgot_password", request_method="GET", renderer='../templates/forgot_password.pt')
def forgot_password_view(request):
    return {'error': None}


@view_config(route_name="forgot_password", request_method="POST", renderer='json')
def send_reset_password_link(request):
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)

    if 'username' not in data:
        raise HTTPBadRequest("username is required")
    username = data['username']
    user = utils.app_db['schools'].find_one({'username': username}, {'username': True, 'email': True, 'name': True})
    if not user:
        return Response(render("../templates/forgot_password.pt", {'error': "invalid username"}))

    reset_link = get_password_reset_link(user['_id'], request.domain)

    # send email
    recipients = [user['email']]
    sub = "Password reset request"
    html = "Hi " + user['name'] + ",<br><br>We have received a request to reset your password.<br>" + \
           "Click on the below link to reset your password.<br><br>" + reset_link + "<br><br>" + \
           "This link will expire after 2 hours.<br>Feel free to enquire further at edufits@gmail.com<br><br>" + \
           "Regards,<br>EduFits Team"
    sender = "EduFits team"

    p = Process(target=send_text_mail, args=(sub, recipients, html, sender))
    p.daemon = True
    p.start()

    raise HTTPFound(location="/")


def get_password_reset_link(mongo_id, domain):
    reset_link_data = {
        'obj_id': mongo_id,
        'expiryTime': datetime.now() + timedelta(0, 7200),
        'type': "password-reset"
    }

    reset_link_res = utils.app_db['email_links'].insert_one(reset_link_data)
    reset_link = "http://" + domain + "/reset-password/" + str(reset_link_res.inserted_id)

    return reset_link


@view_config(route_name="reset_password", request_method="GET", renderer='../templates/reset_password.pt')
def reset_password_view(request):
    link_id = request.matchdict['pk']
    return {'linkId': link_id, 'error': None}


@view_config(route_name="reset_password", request_method="POST", renderer="json")
def reset_password(request):
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)

    if 'password' not in data:
        raise HTTPBadRequest("password is required")

    new_password = data['password']

    link_id = ObjectId(request.matchdict['pk'])
    link_data = utils.app_db['email_links'].find_one({'_id': link_id})

    if not link_data:
        raise HTTPNotFound("This link has expired")
    if datetime.now() > link_data['expiryTime']:
        utils.app_db['email_links'].remove({'_id': link_id})
        raise HTTPNotFound("This link has expired")

    obj_id = link_data['obj_id']
    data = utils.app_db['schools'].find_one({'_id': obj_id})
    username = data['username']

    if not data:
        raise HTTPNotFound("This link is invalid")

    utils.app_db['schools'].update_one({'username': username},
                                       {'$set': {'password': pbkdf2_sha512.encrypt(new_password)}})
    forget(request)
    logger.info({'msg': "password changed", 'username': username})

    utils.app_db['email_links'].remove({'_id': link_id})

    raise HTTPFound(location="/")


@view_config(context=HTTPException)
def bad_request_view(exc, request):
    if isinstance(exc, HTTPFound):
        return exc
    return Response(render("json", {'error': exc.message}), exc.code, content_type="text/json")


@view_config(route_name="android_app_info", request_method="GET", renderer="json")
def android_app_info(request):
    f_name = os.path.dirname(__file__) + "/../static/app/android_app_info.json"

    with open(f_name) as json_f:
        app_info = json.load(json_f)

    return app_info


@view_config(route_name='login', request_method='POST', renderer='json')
def login(request):
    query_dict = dict(request.GET)
    try:
        data = request.json_body
    except ValueError:
        data = dict(request.POST)
    logger.debug({'login_data': data})

    if not data or 'username' not in data or 'password' not in data:
        raise HTTPUnauthorized("Please provide username and password")
    username = data['username']
    password = data['password']
    logger.debug({'username': username, 'password': password})

    projection = {'_id': True, 'username': True, 'name': True, 'email': True, 'password': True}
    school_user = utils.app_db['schools'].find_one({'username': username}, projection)
    logger.debug({'school': school_user})
    if school_user:
        if check_pwd(password, school_user['password']):
            remember_data = {
                'username': school_user['username'],
                'userType': "school",
                '_id': school_user['_id'],
                'schoolId': school_user['_id'],
                'schoolName': school_user['name']
            }
            remember(request, remember_data)
            logger.debug({'msg': "logged in", 'username': username, '_id': school_user['_id']})
            if 'redirectUrl' in query_dict:
                raise HTTPFound(location=query_dict['redirectUrl'])
            else:
                ret_obj = {'_id': school_user['_id'], 'name': school_user['name'], 'email': school_user['email'],
                           'username': school_user['username']}
                return ret_obj
    else:
        superadmin_user = utils.app_db['superadmins'].find_one({'username': username}, projection)
        if superadmin_user and check_pwd(password, superadmin_user['password']):
            remember_data = {
                'username': superadmin_user['username'],
                'userType': "superadmin",
                '_id': superadmin_user['_id']
            }
            remember(request, remember_data)
            logger.debug({'msg': "logged in", 'username': username, '_id': superadmin_user['_id']})
            if 'redirectUrl' in query_dict:
                raise HTTPFound(location=query_dict['redirectUrl'])
            else:
                ret_obj = {'_id': superadmin_user['_id'], 'name': superadmin_user['name'],
                           'email': superadmin_user['email'], 'username': superadmin_user['username']}
                return ret_obj

    logger.debug({'msg': "unsuccessful authentication attempt", 'data': data})
    if 'redirectUrl' in query_dict:
        raise HTTPFound(location="/login?invalid=1&redirectUrl=" + query_dict['redirectUrl'])
    else:
        raise HTTPUnauthorized('Invalid username/password combination')


@view_config(route_name="alerts", renderer="json")
def accept_alert(request):
    return {"ok": "tested"}


@view_config(route_name="devices_summary", request_method="GET", permission="superadmin", renderer="json")
def get_devices_summary(request):
    return get_latest_gps_status()


@view_config(route_name="how_to_use", request_method="GET", renderer="../templates/how_to_use.pt")
def how_to_use(request):
    query_dict = dict(request.GET)
    if 'phone' in query_dict:
        phone = query_dict["phone"]
        # record info for this phone number, first step, user opened link sent in SMS
    else :
        phone = ""
    return {"phone" : phone}


@view_config(route_name="install_application", request_method="GET")
def install_application(request):
    query_dict = dict(request.GET)
    if 'phone' in query_dict:
        phone = query_dict["phone"]
        user_agent = request.user_agent
        # update info in db, second step , user clicked the link in how-to-use page
    raise HTTPFound("https://play.google.com/store/apps/details?id=com.edufits.tracker")


@view_config(route_name="sms_delivery", request_method="POST", renderer='json')
def sms_delivery(request):
    try:
        data = request.json_body
    except ValueError:
        logger.debug(request.POST)
        data = ast.literal_eval(dict(request.POST)['data'])
    logger.debug({'sms-delivery-data' : data})
    requestId = data['requestId']
    for key, value in data['numbers'].items():
        res = utils.app_db['sms_report'].update_one({'requestId': requestId},
                                                    {'$set': {'delivered': validate_datetime(value['date']),
                                                      'status': value['status'],
                                                      'desc': value['desc']}})
        logger.debug(res)
    raise HTTPOk()


def get_latest_gps_status():
    cursor = utils.app_db['devices_status'].find().sort([('_id', -1)]).limit(1)
    recs = [x for x in cursor]
    cursor.close()
    if len(recs) > 0:
        return recs[0]


@view_config(route_name="sms_reports", request_method="GET", permission="superadmin", renderer='json')
def view_sms_reports(request):
    if request.filters is None:
        filters = {}
    else:
        filters = request.filters
    all_reports = [x for x in utils.app_db['sms_report'].find(filters, request.projection)]
    return all_reports


@view_config(route_name="user_location", request_method="POST", permission="user", renderer='json')
def use_location_post_view(request):
    data = request.json_body
    logger.info(data)
    utils.app_db['user_locations'].insert({'userId': request.user['_id'], 'insertedAt': datetime.now(), 'data': data})
