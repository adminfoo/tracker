"""
One demo school is created as a copy of royal oak school
"manage_demo_schoool.py" script is used to create the demo school

Anyone can register herself to get access to demo on mobile app too
The view for the same is provided in this module
"""
import random
import string

from pyramid.view import view_config

from scripts.manage_demo_school import generate_random_male_name, generate_random_female_name
from tracker.passengers import create_single_passenger
from tracker.user_links import generate_short_link
from tracker.utils import app_db
import logging

from tracker.utils.sms_utils import send_sms

logger = logging.getLogger(__name__)


@view_config(route_name="demo_instruction", renderer='../templates/demo_instruction.pt')
def demo_instruction_view(request):
    return {}


@view_config(route_name="register_demo_phone", request_method="GET", renderer='json')
def register_phone_view(request):
    data = request.params

    name = data['name'] if 'name' in data else " "
    phone = data['phone']

    demo_school = app_db['schools'].find_one({'isDemo': True})
    school_id = demo_school['_id']

    # create a new passenger in demo school with this phone
    # assign a random route and random stop to pax
    pax_data = create_random_pax_data(school_id, name, phone)
    logger.debug(pax_data)
    # save data in db and return pax id
    ret_obj = create_single_passenger(pax_data, school_id)

    app_link = "https://play.google.com/store/apps/details?id=com.edufits.tracker"
    short_link = generate_short_link(app_link, phone, "demo")

    sms_content = "You have registered yourself for demo at Edufits. Use this link to install our app " + short_link + \
                  " Call 8800443925 for help"
    send_sms(phone, sms_content, tag="demo")

    return ret_obj


def create_random_pax_data(school_id, name, phone):
    gender = random.choice(["male", "female"])
    if gender == "male":
        ward_name = generate_random_male_name()
    else:
        ward_name = generate_random_female_name()

    all_routes = [r for r in app_db['routes'].find({'schoolId': school_id, 'type': "pickup"})]
    pickup_route = random.choice(all_routes)
    drop_route = app_db['routes'].find_one({'schoolId': school_id, 'type': "drop", 'vehicleId': pickup_route['vehicleId']})

    stop_index = random.randint(0, len(pickup_route['stops']) - 1)

    data = {
        'schoolId': school_id,
        'name': ward_name,
        'idNumber': ''.join([random.choice(string.digits) for i in range(5)]),
        'course': random.choice(["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII",
                                 "KG", "LKG", "UKG", "Nursery", "Pre Nursery"]),
        'gender': gender,
        'bloodGroup': random.choice(["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]),
        'phone': phone,
        'email': "",
        'type': "student",
        'guardianName': name,

        'pickupRouteId': pickup_route['_id'],
        'pickupRouteName': pickup_route['name'],
        'pickupStopId': pickup_route['stops'][stop_index]['id'],
        'pickupStopName': pickup_route['stops'][stop_index]['name'],

        'dropRouteId': drop_route['_id'],
        'dropRouteName': drop_route['name'],
        'dropStopId': drop_route['stops'][stop_index]['id'],
        'dropStopName': drop_route['stops'][stop_index]['name']
    }
    return data
