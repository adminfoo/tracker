module.exports = function(grunt) {
	// Do grunt-related things in here

	//require('jit-grunt')(grunt);

	grunt.initConfig({
		requirejs: {
		  compile: {
		    options: {
		    	appDir: "../static",
			    baseUrl: "js",
				mainConfigFile: '../static/js/requireMain.js',
			    dir: "../static/build",
			    keepBuildDir : false,
			    optimize : "none",
			    optimizeCss: "standard",
			    fileExclusionRegExp: /node_modules|less/,
			    modules: [
			    	{
			            name: "vendor",
			            create: true,
			            include: ["marionette", "backboneForm", "i18n", "timepicki", "locationPicker", "pikaday", "moment"]
			    	}
			    ],
			    skipDirOptimize: false,
			    keepAmdefine: true,
			    allowSourceOverwrites: true,
			    logLevel: 0,
		    }
		  }
		},
		less: {
			developent: {
				options: {
				  compress: false,
				  yuicompress: false,
				  optimization: 2
				},
				files: {
				  "../static/css/main.css": "../static/css/less/main.less" // destination file and source file
				}
			},
			production: {
				options: {
				  compress: true,
				  yuicompress: true,
				  optimization: 2
				},
				files: {
				  "../static/css/main.css": "../static/css/less/main.less" // destination file and source file
				}
			}
	    },
	    cachebuster: {
	    	staticAssetsForProd : {
	            options: {
	                format: 'json',
	                basedir: '../static'
	            },
	            src: '../static/**/*',
	            dest: '../static/build/js/cachebusters.json'
	        },
	        staticAssetsForDev : {
	            options: {
	                format: 'json',
	                basedir: '../static'
	            },
	            src: '../static/**/*',
	            dest: '../static/js/cachebusters.json'
	        }
	    },
	    copy : {
	    	// copy compiler vendor.js from build to dev
	    	main: {
	    		src: '../static/build/js/vendor.js',
			    dest: '../static/js/vendor.js',
			},
	    },
	    watch : {
	    	scripts: {
	    		files: '../static/css/**/*.less',
		    	tasks: ['less:developent'],
		    	options: {
		    		debounceDelay: 250,
		    	},
		  },
	    }
	});

	grunt.task.registerTask('default', ['less:developent', 'requirejs', 'cachebuster', 'copy', 'watch']);
	grunt.task.registerTask('production', ['less:production', 'requirejs', 'cachebuster']);

	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-cachebuster');
};