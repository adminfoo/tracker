import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid==1.6a2',
    'pyramid_chameleon==0.3',
    'pyramid_debugtoolbar==2.4.1',
    'pyramid_mailer==0.14.1',
    'waitress==0.8.10',
    'pymongo==3.0.3',
    'voluptuous==0.8.7',
    'gunicorn==19.3.0',
    'passlib==1.6.5',
    'pdfkit==0.5.0',
    'geopy==1.11.0',
    'httplib2==0.9.2',
    'googlemaps==2.3',
    'python-dateutil==2.4.2',
    # 'instamojo==1.1',
    'eventlet',
    ]

# Chameleon==2.22
# Mako==1.0.2
# MarkupSafe==0.23
# PasteDeploy==1.5.2
# Pygments==2.0.2
# WebOb==1.5.0b0
# pyramid-mako==1.0.2
# repoze.lru==0.6
# repoze.sendmail==4.2
# requests==2.6.0
# six==1.10.0
# -e git+git@bitbucket.org:adminfoo/tracker.git@aa98e5d17326b937d00beca8d50ac503143d03d2#egg=tracker-master
# transaction==1.4.4
# translationstring==1.3
# venusian==1.0
# zope.deprecation==4.1.2
# zope.interface==4.1.2

setup(name='tracker',
      version='0.1.1.1',
      description='tracker',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="tracker",
      entry_points="""\
      [paste.app_factory]
      main = tracker:main
      """,
      )
