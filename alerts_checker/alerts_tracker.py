"""
This script has an infinite loop. It has multiple functions.

It finds whether any vehicle is parked or not, and sends appropriate alerts to school admin

For parent app, it checks whether any alert is due. If any alert is due, it sends SMS.

For each school, it creates a readable live summary and dumps in db.
"""
from datetime import timedelta, datetime
from urllib.parse import urlparse

import sys
from geopy.distance import vincenty

from pymongo import MongoClient
from tracker import utils
from pyramid_mailer import Mailer
from configparser import ConfigParser
import time
import logging
import logging.config

from tracker.utils.sms_utils import send_sms

logger = logging.getLogger(__name__)

LOOP_INTERVAL_SECS = 30


def initialize(config_filename):
    config = ConfigParser()
    config.read(config_filename)
    settings = config["app:main"]

    logging.config.fileConfig(config_filename, disable_existing_loggers=False)

    db_url = urlparse(settings['mongo_uri'])
    utils.mongo_cli = MongoClient(db_url.hostname, db_url.port)
    if 'mongo_username' in settings:
        utils.mongo_cli.the_database.authenticate(settings['mongo_username'], settings['mongo_password'],
                                                  source=settings['mongo_authdb'])

    utils.app_db = utils.mongo_cli[db_url.path[1:]]
    utils.gps_db = utils.mongo_cli['trackingdb']

    utils.env = settings['env']

    utils.mailer = Mailer.from_settings(settings)

    from tracker import vehicles  # so that object store has vehicles


def send_entry_exit_alert(vehicle_details, phone_nos, current_parking_status, time_value):
    for phone_no in phone_nos:
        status = "ENTRY ALERT"
        if current_parking_status is False:
            status = "EXIT ALERT"
        msg = '{}: {} Reg no {} at {}'.format(status, vehicle_details['name'], vehicle_details['registrationNo'],
                                              time_value.strftime("%d %b %l:%M %p"))
        logger.info({'msg': "sending SMS", 'type': "parking alert", 'phone': phone_no, 'content': msg})
        send_sms(phone_no, msg)


def parked_in_school(vehicle_details, latest_record, school_details):
    p1 = (latest_record['lat'], latest_record['lng'])
    p2 = (school_details['lat'], school_details['lng'])
    dist = vincenty(p1, p2).kilometers
    logger.debug({'deviceId': vehicle_details['deviceId'], 'distance': dist})
    if dist > 0.2:
        return False
    return True


def check_entry_exit(vehicle, school, records):
    latest_record = records[-1]
    lat = latest_record['lat']
    lng = latest_record['lng']
    if lat is None or lat == 0 or lng is None or lng == 0:
        return

    previous_status = vehicle['status']['parked']

    parked_status = parked_in_school(vehicle, latest_record, school)
    logger.debug({'current': parked_status, 'previouslyParked': previous_status})
    if parked_status != previous_status:
        logger.info({'msg': "parked status changed", 'parked': parked_status,
                     'vehicle': vehicle['registrationNo'], 'school': school['name']})
        # alert school admin
        if school['geoFenceEnabled'] is True:
            send_entry_exit_alert(vehicle, school['alertPhones'], parked_status, latest_record['insertedAt'])
        # update in db
        utils.app_db['vehicles'].update({'_id': vehicle['_id']}, {'$set': {'status.parked': parked_status,
                                                                           'status.lastUpdated': datetime.now()}})
    else:
        utils.app_db['vehicles'].update({'_id': vehicle['_id']}, {'$set': {'status.lastUpdated': datetime.now()}})
    return parked_status


def update_moving_ac_ignition(vehicle, records):
    edit_data = {'status.ignition': False, 'status.ac': False, 'status.moving': False}
    if len(records) == 0:
        # if device is disconnected mark it as not moving, ignition off etc
        records = list(utils.gps_db[vehicle['deviceId']].find({'lat': {'$exists': True}}).sort([('time', -1)]).limit(1))
        if len(records) > 0 and datetime.now() - records[0]['time'] > timedelta(0, 7200):
            utils.app_db['vehicles'].update({'_id': vehicle['_id']}, {'$set': edit_data})
        return
    latest_record = records[-1]
    lat = latest_record['lat']
    lng = latest_record['lng']
    if lat is None or lat == 0 or lng is None or lng == 0:
        return

    if latest_record['status'] is not None:
        if 'high_acc' in latest_record['status']:
            edit_data['status.ignition'] = latest_record['status']['high_acc']
        if 'ignition' in latest_record['status']:
            edit_data['status.ignition'] = True if latest_record['status']['ignition'] is 1 else False
        if 'ac' in latest_record['status']:
            edit_data['status.ac'] = True if latest_record['status']['ac'] is 1 else False
        if 'movement' in latest_record['status']:
            edit_data['status.moving'] = True if latest_record['status']['movement'] is 1 else False
    if latest_record['speed'] > 0:
        edit_data['status.moving'] = True
    utils.app_db['vehicles'].update({'_id': vehicle['_id']}, {'$set': edit_data})


def check_vehicle(vehicle, school):
    logger.debug({'msg': "starting to check", 'vehicleId': vehicle['_id']})
    try:
        filters = {'insertedAt': {'$gte': vehicle['status']['lastUpdated'] - timedelta(0, 1)},
                   'lat': {'$exists': True}}
        records = list(utils.gps_db[vehicle['deviceId']].find(filters))
        logger.debug({'records': len(records)})
        if len(records) > 0:
            # check parked status and alert school admin if necessary
            check_entry_exit(vehicle, school, records)

        update_moving_ac_ignition(vehicle, records)

        if datetime.now() - vehicle['tripLastChecked'] > timedelta(0, 60):
            # Check for any new trips taken by vehicle and create in db if necessary
            trips.construct_new_trips(vehicle, 5000)
            # Check if any trips should be merged and merge if necessary
            trips.finalize_trips(vehicle)
    except:
        logger.exception("Error in checking for vehicle id " + str(vehicle['_id']))


if __name__ == "__main__":
    logger.info('initializing')
    initialize(sys.argv[1])
    from tracker import trips
    last_checked_time = datetime.now()
    while True:
        # This loop periodically checks for any new vehicles and creates new thread for new vehicles
        # The loop runs once per 30s
        loop_start_time = datetime.now()
        try:
            logger.info('loading schools')
            all_schools = list(utils.app_db['schools'].find())
            logger.debug({'schools': len(all_schools)})
            for s in all_schools:
                school_vehicles = list(utils.app_db['vehicles'].find({'schoolId': s['_id']}))
                logger.debug({'school': s['name'], 'vehicles': len(school_vehicles)})
                for v in school_vehicles:
                    check_vehicle(v, s)
        except:
            logger.exception("Failed while loading new vehicles")
        last_checked_time = loop_start_time
        wait_seconds = (loop_start_time + timedelta(0, LOOP_INTERVAL_SECS) - datetime.now()).total_seconds()
        if wait_seconds > LOOP_INTERVAL_SECS:
            wait_seconds = LOOP_INTERVAL_SECS
        if wait_seconds < 0:
            wait_seconds = 0
        time.sleep(wait_seconds)
